import { Component, ViewEncapsulation, NgZone } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  AlertController,
  LoadingController,
  ToastController
} from "ionic-angular";
import * as _ from "lodash";
import { CacheService, Cache } from "ionic-cache-observable";
import { Observable, Subscription } from "rxjs";
import "rxjs/add/operator/finally";

import { CreatorsServicesProvider } from "../../providers/services/creators";

import { CreatorsEntryPage } from "../creators-entry/creators-entry";
import { StorageProvider } from "../../providers/storage/storage";
import { LoadingProvider } from "../../providers/loading/loading";

@IonicPage()
@Component({
  selector: "page-creators",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "creators.html"
})
export class CreatorsPage {
  mode: string = "all";
  creatorsArr: any[] = [];

  creatorsArrCopy: any[] = [];

  fakeArr: any[] = [];

  business: any = {};
  showContent: boolean = false;
  showAllContentError: boolean = false;

  allContentError: any = {};

  mediaArr$: Observable<any>;
  private mediaCache: Cache<any>;
  public refreshSubscription: Subscription;

  constructor(
    public zone: NgZone,
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public creators: CreatorsServicesProvider,
    public storageService: StorageProvider,
    public cacheService: CacheService,
    public loader: LoadingProvider
  ) {
    for (var i = 0; i < 12; ++i) {
      this.fakeArr.push(i);
    }
  }

  private initData(refresher?: any) {
    this.showContent = false;
    const dataObservable = Observable.fromPromise(
      this.creators.creator_business()
    );

    const cacheKey =
      this.storageService.cacheKey.CREATORS + "_" + this.business.id;
    this.cacheService.register(cacheKey, dataObservable).subscribe(cache => {
      this.mediaCache = cache;
      this.zone.run(() => {
        this.mediaArr$ = cache.get$;

        this.mediaCache.refresh().subscribe(
          () => {
            this.loader.start();
          },
          () => {
            this.loader.complete();
          },
          () => {
            this.loader.complete();
          }
        );
      });
    });

    this.mediaArr$.subscribe(
      (data: any) => {
        if (data && data.success) {
          if (_.isArray(data.data.data) && data.data.data[0] !== false) {
            const item = data.data;

            this.zone.run(() => {
              this.creatorsArr = item.data;
              this.creatorsArrCopy = item.data;
            });
          }
        }
        this.showContent = true;
        if (refresher) {
          refresher.complete();
        }
      },
      error => {
        this.showAllContentError = true;
        if (refresher) {
          refresher.complete();
        }
        if (error && !error.success) {
          this.allContentError = error;
        }
      }
    );
  }

  private async initBusiness() {
    const business = await this.storageService.getStorage(
      this.storageService.storageKey.CURRENT_BUSINESS
    );
    this.business = !_.isEmpty(business) ? JSON.parse(business) || {} : {};
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad CreatorsPage");
    this.initBusiness().then(() => {
      this.initData();
    });
  }

  doRefresh(refresher: any) {
    if (this.mediaCache) {
      this.showContent = false;
      this.zone.run(() => {
        this.initData(refresher);
      });
    } else {
      console.warn("Cache has not been initialized.");
      if (refresher) {
        refresher.cancel();
      }
    }
  }

  getItems(ev: any) {
    let val = ev.target.value;
    if (val && val.trim() != "") {
      this.creatorsArr = _.filter(this.creatorsArrCopy, { firstName: val });
    } else {
      this.creatorsArr = this.creatorsArrCopy;
    }
  }

  createCreators() {
    if (this.business.role !== 1) {
      this.toastCtrl
        .create({
          message: "Invalid Access!!!",
          duration: 3000
        })
        .present();
      return;
    }

    this.navCtrl.push(CreatorsEntryPage, {
      action: "create"
    });
  }

  updateCreators(item) {
    if (this.business.role !== 1) {
      this.toastCtrl
        .create({
          message: "Invalid Access!!!",
          duration: 3000
        })
        .present();
      return;
    }

    this.navCtrl.push(CreatorsEntryPage, {
      action: "update",
      item: item
    });
  }

  deleteCreator(item: any, slidingItem) {
    let vm = this;

    if (this.business.role !== 1) {
      this.toastCtrl
        .create({
          message: "Invalid Access!!!",
          duration: 3000
        })
        .present();
      return;
    }

    const deleteCreator = () => {
      const loading = this.loadingCtrl.create({
        dismissOnPageChange: true,
        content: "Unlinking..."
      });
      loading.present();
      this.creators
        .creator_unlink_business({
          creatorId: item.id
        })
        .then(
          (data: any) => {
            if (data && data.success) {
              console.log("data: ", data);
              if (data.result.error) {
                this.alertCtrl
                  .create({
                    title: "WARNING",
                    message: data.result.msg,
                    buttons: ["OK"]
                  })
                  .present();
              } else {
                this.alertCtrl
                  .create({
                    title: "SUCCESS",
                    message: data.message,
                    buttons: ["OK"]
                  })
                  .present();
              }
            }
            loading.dismiss();
          },
          (error: any) => {
            loading.dismiss();
            console.log("error: ", error);
            if (error && !error.success) {
              this.alertCtrl
                .create({
                  title: "ERROR",
                  message: error.error,
                  buttons: ["OK"]
                })
                .present();
              return;
            }
          }
        );
    };

    slidingItem.close();
    const confirm = this.alertCtrl.create({
      title: "Unlink this creator?",
      message: "Do you want to unlink this creator?",
      buttons: [
        {
          text: "No",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Yes",
          handler: () => {
            console.log("Agree clicked");
            deleteCreator();
          }
        }
      ]
    });
    confirm.present();
  }
}
