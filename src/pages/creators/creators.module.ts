import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreatorsPage } from './creators';

import { ComponentsModule } from '../../components/components.module';

@NgModule({
	declarations: [
		CreatorsPage,
	],
	imports: [
		ComponentsModule,
		IonicPageModule.forChild(CreatorsPage),
	],
})
export class CreatorsPageModule { }
