import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { ReactiveFormsModule } from "@angular/forms";

import { CollectionsEntryPage } from "./collections-entry";

@NgModule({
  declarations: [CollectionsEntryPage],
  imports: [ReactiveFormsModule, IonicPageModule.forChild(CollectionsEntryPage)]
})
export class CollectionsEntryPageModule {}
