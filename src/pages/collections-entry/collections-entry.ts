import { Component, ViewEncapsulation } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ViewController,
  LoadingController,
  AlertController
} from "ionic-angular";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from "@angular/forms";

import { CollectionsServicesProvider } from "../../providers/services/collections";

@IonicPage()
@Component({
  selector: "page-collections-entry",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "collections-entry.html"
})
export class CollectionsEntryPage {
  collection: any = {};
  token: string;
  action: string;

  collectionForm: FormGroup;

  isSaving = false;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public collections: CollectionsServicesProvider
  ) {
    this.action = navParams.get("action");

    this.collectionForm = this.formBuilder.group({
      title: ["", Validators.required],
      description: [""],
      privacy: ["", Validators.required]
    });
  }

  get f(): any {
    return this.collectionForm.controls;
  }

  private validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  ionViewDidLoad() {
    if (this.action == "update") {
      this.collection = this.navParams.get("collection");
    }
  }

  close() {
    this.viewCtrl.dismiss();
  }

  saveEntry() {
    this.submitted = true;
    this.isSaving = true;

    if (this.collectionForm.invalid) {
      this.isSaving = false;
      this.validateAllFormFields(this.collectionForm);
      return;
    }

    const confirm = this.alertCtrl.create({
      title: "Save Entry?",
      message: "Save collection entry?",
      buttons: [
        {
          text: "Cancel",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Confirm",
          handler: () => {
            if (this.action == "update") {
              this.updateEntry();
            } else if (this.action == "create") {
              this.createEntry();
            }
          }
        }
      ]
    });
    confirm.present();
  }

  private createEntry() {
    const loading = this.loadingCtrl.create({
      dismissOnPageChange: true,
      content: "Saving..."
    });
    loading.present();

    const formData = new FormData();
    formData.append("title", this.collectionForm.value.title);
    formData.append("privilege", this.collectionForm.value.privacy);
    formData.append("privacy", this.collectionForm.value.privacy);
    this.collections
      .collection_create(formData)
      .then(
        (data: any) => {
          if (data && data.success) {
            const alert = this.alertCtrl.create({
              title: "Success",
              message: data.message || "Collection successfully created",
              buttons: ["OK"]
            });
            alert.onDidDismiss(() => {
              this.viewCtrl.dismiss("save");
            });
            alert.present();
          } else {
            const alert = this.alertCtrl.create({
              title: "Warning",
              message: data.message,
              buttons: ["OK"]
            });
            alert.present();
          }
          loading.dismiss();
        },
        (error: any) => {
          console.log("error: ", error);
          loading.dismiss();
          if (error && !error.success) {
            const alert = this.alertCtrl.create({
              title: "Error",
              message: error.message,
              buttons: ["OK"]
            });
            alert.present();
          }
        }
      )
      .catch((error: any) => {
        console.log("error: ", error);
        loading.dismiss();
      });
  }

  private updateEntry() {
    const loading = this.loadingCtrl.create({
      dismissOnPageChange: true,
      content: "Updating..."
    });
    loading.present();
    this.collections
      .collection_edit(this.collection)
      .then(
        (data: any) => {
          if (data && data.success) {
            const alert = this.alertCtrl.create({
              title: "Success",
              message: data.data,
              buttons: ["OK"]
            });
            alert.onDidDismiss(() => {
              this.viewCtrl.dismiss("save");
            });
            alert.present();
          } else {
            const alert = this.alertCtrl.create({
              title: "Warning",
              message: data.message,
              buttons: ["OK"]
            });
            alert.present();
          }
          loading.dismiss();
          this.submitted = false;
          this.isSaving = false;
        },
        (error: any) => {
          console.log("error: ", error);
          loading.dismiss();
          this.submitted = false;
          this.isSaving = false;
          if (error && !error.success) {
            const alert = this.alertCtrl.create({
              title: "Error",
              message: error.message,
              buttons: ["OK"]
            });
            alert.present();
          }
        }
      );
  }
}
