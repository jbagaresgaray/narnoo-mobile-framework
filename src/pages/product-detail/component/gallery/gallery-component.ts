import {
  Component,
  Input,
  Output,
  EventEmitter,
  ViewEncapsulation
} from "@angular/core";

@Component({
  selector: "gallery-component",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "gallery-component.html"
})
export class GalleryTabComponent {
  @Input() product: any = {};
  @Input() perPage: number;

  @Output() clickViewImageDetail = new EventEmitter<any>();

  viewImageDetail(item) {
    this.clickViewImageDetail.emit(item);
  }
}
