import { Component, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'details-component',
  encapsulation: ViewEncapsulation.None,
	templateUrl: 'details-component.html',
})
export class DetailsTabComponent {
	@Input() product: any = {};
	@Input() newDescriptionArr: any[] = [];


	@Output() _expandSummaryItem = new EventEmitter<any>();
	expandSummaryItem(item) {
		this._expandSummaryItem.emit(item);
	}

	@Output() _viewImageDetail = new EventEmitter<any>();
	viewImageDetail(item){
		this._viewImageDetail.emit(item);
	}

	@Output() _viewVideoDetail = new EventEmitter<any>();
	viewVideoDetail(item){
		this._viewVideoDetail.emit(item);
	}

	@Output() _viewPrintDetail = new EventEmitter<any>();
	viewPrintDetail(item){
		this._viewPrintDetail.emit(item);
	}

	@Output() _viewLogoDetail = new EventEmitter<any>();
	viewLogoDetail(item){
		this._viewLogoDetail.emit(item);
	}
}
