import { Component, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'booking-component',
  encapsulation: ViewEncapsulation.None,
	templateUrl: 'booking-component.html',
})
export class BookingTabComponent {
	@Input() product: any = {};
	@Input() showBookingSettings: boolean;

	@Output() clickOpenLink = new EventEmitter<any>();

	openLink(link){
		this.clickOpenLink.emit(link);
	}
}
