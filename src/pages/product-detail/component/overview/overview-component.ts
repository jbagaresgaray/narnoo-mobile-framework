import { Component, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'overview-component',
  encapsulation: ViewEncapsulation.None,
	templateUrl: 'overview-component.html',
})
export class OverviewTabComponent {
	@Input() product: any = {};
	@Input() features: any = {};
	@Input() newSummaryArr: any[] = [];


	@Output() clickViewDetails = new EventEmitter<any>();
	viewDetails() {
		this.clickViewDetails.emit();
	}

	@Output() clickExpandSummaryItem = new EventEmitter<any>();
	expandSummaryItem(item) {
		this.clickExpandSummaryItem.emit(item);
	}
}
