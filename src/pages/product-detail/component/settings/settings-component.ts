import { Component, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'settings-component',
  encapsulation: ViewEncapsulation.None,
	templateUrl: 'settings-component.html',
})
export class SettingsTabComponent {
	@Input() product: any = {};
	@Input() showBookingSettings: boolean;
}
