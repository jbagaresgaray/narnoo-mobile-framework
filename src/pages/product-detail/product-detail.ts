import {
  Component,
  ViewChild,
  ViewEncapsulation,
  NgZone,
  OnInit
} from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  AlertController,
  ActionSheetController,
  Platform,
  ToastController,
  ModalController,
  Content,
  App,
  ViewController,
  PopoverController,
  Events
} from "ionic-angular";
import { CacheService, Cache } from "ionic-cache-observable";
import { Observable, Subscription } from "rxjs";
import "rxjs/add/operator/finally";
import * as $ from "jquery";
import * as _ from "lodash";

import { InAppBrowser } from "@ionic-native/in-app-browser";
import { SocialSharing } from "@ionic-native/social-sharing";

import { FileUploadPage } from "../../pages/file-upload/file-upload";
import { ImageDetailPage } from "../../pages/image-detail/image-detail";
import { VideoDetailPage } from "../../pages/video-detail/video-detail";
import { PrintDetailPage } from "../../pages/print-detail/print-detail";
import { LogoDetailPage } from "../../pages/logo-detail/logo-detail";
import { ProductsEntryPage } from "../../pages/products-entry/products-entry";

import { ProductsServicesProvider } from "../../providers/services/products";
import { StorageProvider } from "../../providers/storage/storage";
import { LoadingProvider } from "../../providers/loading/loading";

declare let safari: any;

@Component({
  template: `
    <ion-list>
      <ion-list-header>Product Feature</ion-list-header>
      <button ion-item (click)="setFeature('image')">Feature Image</button>
      <button ion-item (click)="setFeature('video')">Feature Video</button>
      <button ion-item (click)="setFeature('print')">Feature Print Item</button>
      <button ion-item (click)="setFeature('logo')">Feature Logo</button>
    </ion-list>
  `
})
export class ProductDetailPopoverPage implements OnInit {
  business: any = {};
  product: any = {};
  constructor(
    public events: Events,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public toastCtrl: ToastController,
    public modalCtrl: ModalController,
    public storageService: StorageProvider
  ) {
    this.product = navParams.get("product");
  }

  async ngOnInit() {
    const business = await this.storageService.getStorage(
      this.storageService.storageKey.CURRENT_BUSINESS
    );
    this.business = !_.isEmpty(business) ? JSON.parse(business) || {} : {};
  }

  setFeature(type) {
    let title: string;
    let action: string;
    if (this.business.type == 1 && this.business.role !== 1) {
      let toast = this.toastCtrl.create({
        message: "Invalid Access!!!",
        duration: 3000
      });
      toast.present();
      return;
    }

    if (type == "image") {
      title = "Set Feature Image";
      action = "feature_image";
    } else if (type == "video") {
      title = "Set Feature Video";
      action = "feature_video";
    } else if (type == "print") {
      title = "Set Feature Print Item";
      action = "feature_print";
    } else if (type == "logo") {
      title = "Set Feature Logo";
      action = "feature_logo";
    }

    let modal = this.modalCtrl.create(FileUploadPage, {
      action: action,
      title: title,
      params: this.product
    });
    modal.onDidDismiss(resp => {
      console.log("resp: ", resp);
      if (resp == "save") {
        this.events.publish("productDetailsInitData");
      }
    });
    modal.present().then(() => {
      this.close();
    });
  }

  close() {
    this.viewCtrl.dismiss();
  }
}
@IonicPage()
@Component({
  selector: "page-product-detail",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "product-detail.html"
})
export class ProductDetailPage {
  token: string;
  action: string;
  connect_params: any = {};
  business: any = {};
  item: any = {};
  product: any = {};
  features: any = {};
  fakeArr: any[] = [];

  newDescriptionArr: any[] = [];
  newSummaryArr: any[] = [];

  pageProduct: number = 0;
  perPage: number = 10;

  imageAction: string = "overview";

  showPreview: boolean = false;
  showContent: boolean = true;

  summaryEnglish: string;
  summaryChinese: string;
  summaryJapanese: string;

  descriptionEnglish: string;
  descriptionChinese: string;
  descriptionJapanese: string;

  showBookingSettings: boolean = true;
  showGalleryContent: boolean = false;
  isSafari: boolean;

  @ViewChild(Content) content: Content;

  productArr$: Observable<any>;
  private productCache: Cache<any>;
  public refreshSubscription: Subscription;

  constructor(
    public app: App,
    public events: Events,
    public zone: NgZone,
    public platform: Platform,
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public actionSheetCtrl: ActionSheetController,
    public popoverCtrl: PopoverController,
    public toastCtrl: ToastController,
    public modalCtrl: ModalController,
    private iab: InAppBrowser,
    private socialSharing: SocialSharing,
    public products: ProductsServicesProvider,
    public storageService: StorageProvider,
    public cacheService: CacheService,
    public loader: LoadingProvider
  ) {
    this.item = navParams.get("product");
    this.action = navParams.get("action");
    this.connect_params = navParams.get("params");

    console.log("this.connect_params =", this.connect_params);
    console.log("this.action =", this.action);
    console.log("item: ", this.item);

    this.isSafari =
      /constructor/i.test(window["HTMLElement"]) ||
      (function(p) {
        return p.toString() === "[object SafariRemoteNotification]";
      })(!window["safari"] || safari.pushNotification);
    this.showContent = true;
    for (var i = 0; i < 12; ++i) {
      this.fakeArr.push(i);
    }

    events.unsubscribe("productDetailsInitData");
    events.subscribe("productDetailsInitData", () => {
      console.log("productDetailsInitData");
      this.initData();
    });
  }

  private initData(event?: any) {
    const productResponse = (data: any) => {
      if (data && data.success) {
        const product = data.data;
        console.log("product: ", product);

        this.zone.run(() => {
          this.product = product;
          if (this.platform.is("ios")) {
            if (this.isSafari) {
              if (this.product.dateCreated) {
                this.product.dateCreated = new Date(
                  this.product.dateCreated.replace(/-/g, "/")
                );
              }
              if (this.product.dataModified) {
                this.product.dataModified = new Date(
                  this.product.dataModified.replace(/-/g, "/")
                );
              }
            } else {
              if (this.product.dateCreated) {
                this.product.dateCreated = new Date(this.product.dateCreated);
              }
              if (this.product.dataModified) {
                this.product.dataModified = new Date(this.product.dataModified);
              }
            }
          }

          if (this.product.description) {
            let descriptionArr = this.product.description.description;
            let summaryArr = this.product.description.summary;

            if (!_.isEmpty(descriptionArr)) {
              _.each(descriptionArr, (row: any) => {
                let arr: any = Object.keys(row).map(key => ({
                  key,
                  value: row[key],
                  expanded: false
                }));
                this.newDescriptionArr.push(arr[0]);
              });
            }

            if (!_.isEmpty(summaryArr)) {
              _.each(summaryArr, (row: any) => {
                let arr: any = Object.keys(row).map(key => ({
                  key,
                  value: row[key],
                  expanded: false
                }));
                this.newSummaryArr.push(arr[0]);
              });
            }
          }

          this.features = this.product.features;
          if (this.features) {
            // ==================================================================
            // ==================================================================
            // ==================================================================
            this.features.disabledAccess =
              this.features.disabledAccess == 1 ? true : false;
            this.features.giftShop = this.features.giftShop == 1 ? true : false;
            this.features.storageLockers =
              this.features.storageLockers == 1 ? true : false;
            this.features.visitorInformation =
              this.features.visitorInformation == 1 ? true : false;
            this.features.dinnerIncluded =
              this.features.dinnerIncluded == 1 ? true : false;
            this.features.internetAccess =
              this.features.internetAccess == 1 ? true : false;
            this.features.media = this.features.media == 1 ? true : false;
            this.features.functionHire =
              this.features.functionHire == 1 ? true : false;
            this.features.hotelPickupService =
              this.features.hotelPickupService == 1 ? true : false;
            this.features.mealProvided =
              this.features.mealProvided == 1 ? true : false;
            this.features.guidedTours =
              this.features.guidedTours == 1 ? true : false;
            this.features.weddingServices =
              this.features.weddingServices == 1 ? true : false;
            this.features.breakfestIncluded =
              this.features.breakfestIncluded == 1 ? true : false;
            this.features.cafe = this.features.cafe == 1 ? true : false;
            this.features.languageTranslation =
              this.features.languageTranslation == 1 ? true : false;
            this.features.conferenceServices =
              this.features.conferenceServices == 1 ? true : false;
            this.features.lunchIncluded =
              this.features.lunchIncluded == 1 ? true : false;
            // ==================================================================
            // ==================================================================
            // ==================================================================

            this.features["24HourDesk"] =
              this.features["24HourDesk"] == 1 ? true : false;
            this.features.bar = this.features.bar == 1 ? true : false;
            this.features.conciergeService =
              this.features.conciergeService == 1 ? true : false;
            this.features.conferenceServices =
              this.features.conferenceServices == 1 ? true : false;
            this.features.expressCheckIn =
              this.features.expressCheckIn == 1 ? true : false;
            this.features.expressCheckOut =
              this.features.expressCheckOut == 1 ? true : false;
            this.features.fitnessCentre =
              this.features.fitnessCentre == 1 ? true : false;
            this.features.kitchenFacilities =
              this.features.kitchenFacilities == 1 ? true : false;
            this.features.laundryFacilities =
              this.features.laundryFacilities == 1 ? true : false;
            this.features.meetingFacilities =
              this.features.meetingFacilities == 1 ? true : false;
            this.features.nonSmoking =
              this.features.nonSmoking == 1 ? true : false;
            this.features.parkingAccess =
              this.features.parkingAccess == 1 ? true : false;
            this.features.restaurant =
              this.features.restaurant == 1 ? true : false;
            this.features.roomService =
              this.features.roomService == 1 ? true : false;
            this.features.safeDeposit =
              this.features.safeDeposit == 1 ? true : false;
            this.features.swimmingPool =
              this.features.swimmingPool == 1 ? true : false;
            this.features.tourBookingService =
              this.features.tourBookingService == 1 ? true : false;
            this.features.wifiAccess =
              this.features.wifiAccess == 1 ? true : false;
          }
        });
      }
      this.showContent = false;

      if (event) {
        event.complete();

        setTimeout(() => {
          $(".progressive-image").each(function() {
            var image = new Image();
            var previewImage = $(this).find(".loadingImage");
            var newImage = $(this).find(".overlay");

            image.onload = function() {
              newImage.css("background-image", "url(" + image.src + ")");
              newImage.css("opacity", "1");
            };
            image.onerror = function() {
              newImage.css("background-image", "url(./assets/img/thumb.jpg)");
              newImage.css("opacity", "1");
            };
            image.src = previewImage.data("image");
          });
        }, 600);
      }
    };

    const errorResponse = error => {
      this.showContent = false;
      // loading.dismiss();
      console.log("error: ", error);
      if (event) {
        event.complete();
      }
    };

    this.showContent = true;
    this.newDescriptionArr = [];
    this.newSummaryArr = [];
    let dataObservable: any;
    let cacheKey: any;
    this.product = {};

    if (this.action == "connect") {
      dataObservable = Observable.fromPromise(
        this.products.operator_products_detail(
          this.connect_params.id,
          this.item.productId
        )
      );

      cacheKey =
        this.storageService.cacheKey.PRODUCT_DETAIL +
        "_" +
        this.business.id +
        "_" +
        this.connect_params.id +
        "_" +
        this.item.productId;
    } else {
      dataObservable = Observable.fromPromise(
        this.products.product_details(this.item.productId)
      );
      cacheKey =
        this.storageService.cacheKey.PRODUCT_DETAIL +
        "_" +
        this.business.id +
        "_" +
        this.item.productId;
    }

    this.cacheService.register(cacheKey, dataObservable).subscribe(cache => {
      this.productCache = cache;
      this.zone.run(() => {
        this.productArr$ = cache.get$;

        this.productCache.refresh().subscribe(
          () => {
            this.loader.start();
          },
          () => {
            this.loader.complete();
          },
          () => {
            this.loader.complete();
          }
        );
      });
    });

    this.productArr$.subscribe(productResponse, errorResponse);
  }

  private async initBusiness() {
    const business = await this.storageService.getStorage(
      this.storageService.storageKey.CURRENT_BUSINESS
    );
    this.business = !_.isEmpty(business) ? JSON.parse(business) || {} : {};
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad ProductDetailPage");
    this.initBusiness().then(() => {
      if (this.action == "connect") {
        if (this.connect_params.type == "operator") {
          this.showBookingSettings = false;
        } else if (
          this.connect_params.type != "operator" &&
          (this.business.role !== 3 && this.business.role !== 4)
        ) {
          this.showBookingSettings = true;
        }
      } else {
        if (this.business.role !== 3 && this.business.role !== 4) {
          this.showBookingSettings = true;
        }
      }
      this.initData();
    });
  }

  doRefresh(refresher: any) {
    if (this.productCache) {
      this.showContent = false;
      this.zone.run(() => {
        this.initData(refresher);
      });
    } else {
      console.warn("Cache has not been initialized.");
      if (refresher) {
        refresher.cancel();
      }
    }
  }

  presentPopover(myEvent) {
    let popover = this.popoverCtrl.create(ProductDetailPopoverPage, {
      product: this.product
    });
    popover.present({
      ev: myEvent
    });
  }

  segmentSelected(ev) {
    /*if (this.imageAction == 'gallery') {
			this.content.resize();
		}*/
    if (this.content) {
      this.content.resize();
      this.content.scrollToTop();
    }

    setTimeout(() => {
      $(".progressive-image").each(function() {
        console.log("progressive-image: ");
        const image = new Image();
        const previewImage = $(this).find(".loadingImage");
        const newImage = $(this).find(".overlay");

        image.onload = function() {
          newImage.css("background-image", "url(" + image.src + ")");
          newImage.css("opacity", "1");
        };
        image.onerror = function() {
          newImage.css("background-image", "url(./assets/img/thumb.jpg)");
          newImage.css("opacity", "1");
        };
        image.src = previewImage.data("image");
      });
    }, 600);
  }

  expandSummaryItem(item) {
    return (item.expanded = !item.expanded);
  }

  viewDetails() {
    this.imageAction = "details";
    this.content.scrollToTop();
  }

  updateProduct() {
    if (this.action == "connect") {
      this.alertCtrl
        .create({
          title: "Information",
          message: "This will open Operator preferred Booking platform!",
          buttons: ["OK"]
        })
        .present();
      return;
    } else {
      if (this.business.role !== 1) {
        const toast = this.toastCtrl.create({
          message: "Invalid Access!!!",
          duration: 3000
        });
        toast.present();
        return;
      }

      let modal = this.modalCtrl.create(ProductsEntryPage, {
        product: this.product,
        action: "update"
      });
      modal.onDidDismiss((resp: any) => {
        if (resp == "save") {
          this.initData();
        }
      });
      modal.present();
    }
  }

  viewImageDetail(item: any) {
    if (item.error) {
      this.alertCtrl
        .create({
          title: "WARNING",
          message: item.msg,
          buttons: ["OK"]
        })
        .present();
      return;
    }

    if (this.action == "connect") {
      this.navCtrl.push(ImageDetailPage, {
        image: item,
        action: "connect",
        params: this.connect_params
      });
    } else {
      this.navCtrl.push(ImageDetailPage, {
        image: item
      });
    }
  }

  viewVideoDetail(item: any) {
    if (item.error) {
      this.alertCtrl
        .create({
          title: "WARNING",
          message: item.msg,
          buttons: ["OK"]
        })
        .present();
      return;
    }

    if (this.action == "connect") {
      this.navCtrl.push(VideoDetailPage, {
        video: item,
        action: "connect",
        params: this.connect_params
      });
    } else {
      this.navCtrl.push(VideoDetailPage, {
        video: item
      });
    }
  }

  viewPrintDetail(item: any) {
    if (item.error) {
      this.alertCtrl
        .create({
          title: "WARNING",
          message: item.msg,
          buttons: ["OK"]
        })
        .present();
      return;
    }

    if (this.action == "connect") {
      this.navCtrl.push(PrintDetailPage, {
        print: item,
        action: "connect",
        params: this.connect_params
      });
    } else {
      this.navCtrl.push(PrintDetailPage, {
        print: item
      });
    }
  }

  viewLogoDetail(item: any) {
    if (item.error) {
      this.alertCtrl
        .create({
          title: "WARNING",
          message: item.msg,
          buttons: ["OK"]
        })
        .present();
      return;
    }

    if (this.action == "connect") {
      this.navCtrl.push(LogoDetailPage, {
        logo: item,
        action: "connect",
        params: this.connect_params
      });
    } else {
      this.navCtrl.push(LogoDetailPage, {
        logo: item
      });
    }
  }

  uploadFile() {
    let title: string;
    if (this.business.role !== 1) {
      const toast = this.toastCtrl.create({
        message: "Invalid Access!!!",
        duration: 3000
      });
      toast.present();
      return;
    }

    if (this.imageAction === "gallery") {
      title = "Upload Image";
    }

    let modal = this.modalCtrl.create(FileUploadPage, {
      action: this.imageAction,
      title: title,
      params: this.product
    });
    modal.onDidDismiss(resp => {
      if (this.imageAction === "gallery" && resp == "save") {
        this.initData();
      }
    });
    modal.present();
  }

  openLink(link) {
    if (link) {
      this.iab.create(link, "_blank");
    } else {
      let alert = this.alertCtrl.create({
        title: "Invalid URL",
        subTitle: "Invalid URL format",
        buttons: ["OK"]
      });
      alert.present();
    }
  }

  private shareProduct() {
    if (!this.business.paid) {
      this.alertCtrl
        .create({
          title: "WARNING",
          message: "This feature is only available to premium users only.",
          buttons: ["OK"]
        })
        .present();
      return;
    }

    if (!this.platform.is("cordova")) {
      this.alertCtrl
        .create({
          title: "WARNING",
          message:
            "Social Sharing is only available on actual device/simulator",
          buttons: ["OK"]
        })
        .present();
      return;
    }

    const loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    loading.present();
    this.products.share_url_product(this.product.productId).then(
      (data: any) => {
        if (data && data.success) {
          console.log("data: ", data);
          this.socialSharing
            .share(
              "This link allows you to view the details of this product. You can send the link to whom ever you need and this will allow them access to the product information.",
              "Share",
              null,
              data.data
            )
            .then(() => {
              // Success!
            })
            .catch(() => {
              // Error!
            });
          loading.dismiss();
        } else {
          loading.dismiss();
        }
      },
      error => {
        console.log("error: ", error);
        loading.dismiss();
        if (error) {
          if (!error.success) {
            this.alertCtrl
              .create({
                title: "ERROR",
                subTitle: error.message,
                buttons: ["OK"]
              })
              .present();
          }
        }
      }
    );
  }

  presentActionSheet() {
    if (this.business.role !== 1) {
      const toast = this.toastCtrl.create({
        message: "Invalid Access!!!",
        duration: 3000
      });
      toast.present();
      return;
    }

    const actionSheet = this.actionSheetCtrl.create({
      title: "Choose action for product?",
      buttons: [
        {
          text: "Update",
          role: "destructive",
          handler: () => {
            setTimeout(() => {
              this.updateProduct();
            }, 300);
          }
        },
        {
          text: "Share",
          handler: () => {
            setTimeout(() => {
              this.shareProduct();
            }, 300);
          }
        },
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        }
      ]
    });
    actionSheet.present();
  }
}
