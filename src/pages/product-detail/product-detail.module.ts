import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProductDetailPage, ProductDetailPopoverPage } from './product-detail';
import { IonicImageViewerModule } from 'ionic-img-viewer';
import { IonicImageLoader } from "ionic-image-loader";

import { PipesModule } from '../../pipes/pipes.module';
import { ComponentsModule } from '../../components/components.module';

import { OverviewTabComponent } from './component/overview/overview-component';
import { DetailsTabComponent } from './component/details/details-component';
import { GalleryTabComponent } from './component/gallery/gallery-component';
import { BookingTabComponent } from './component/booking/booking-component';
import { SettingsTabComponent } from './component/settings/settings-component';

@NgModule({
	declarations: [
		ProductDetailPage, ProductDetailPopoverPage,
		OverviewTabComponent,
		DetailsTabComponent,
		GalleryTabComponent,
		BookingTabComponent,
		SettingsTabComponent
	],
	imports: [
		IonicPageModule.forChild(ProductDetailPage),
		IonicImageViewerModule,
		PipesModule,
    ComponentsModule,
    IonicImageLoader
	],
	entryComponents: [
		ProductDetailPopoverPage,
		OverviewTabComponent,
		DetailsTabComponent,
		GalleryTabComponent,
		BookingTabComponent,
		SettingsTabComponent
	]
})
export class ProductDetailPageModule { }
