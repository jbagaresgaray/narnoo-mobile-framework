import { Component, ViewChild, ViewEncapsulation } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  Events,
  AlertController,
  ActionSheetController,
  LoadingController,
  Content,
  ToastController
} from "ionic-angular";
import * as _ from "lodash";

import { InAppBrowser } from "@ionic-native/in-app-browser";

import { UtilitiesServicesProvider } from "../../providers/services/utilities";
import { ConnectServicesProvider } from "../../providers/services/connect";
import { UserServicesProvider } from "../../providers/services/users";
import { BusinessServicesProvider } from "../../providers/services/business";

import { environment } from "../../environments/environment";

@IonicPage()
@Component({
  selector: "page-createbusiness",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "createbusiness.html"
})
export class CreatebusinessPage {
  users: any = {};

  step: any;
  stepCondition: any;
  stepDefaultCondition: any;
  currentStep: any;
  business: any = {};
  isSameAddress: boolean = false;
  needAccess: boolean = false;
  showSubcats: boolean = false;

  distributorCat: any[] = [];
  operatorCat: any[] = [];
  platformsArr: any[] = [];
  subCategoryArr: any[] = [];
  countriesArr: any[] = [];

  isAgree: boolean = false;
  isSearching: boolean = false;
  isRequesting: boolean = false;

  @ViewChild(Content) content: Content;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public evts: Events,
    public alertCtrl: AlertController,
    public actionSheetCtrl: ActionSheetController,
    public utilities: UtilitiesServicesProvider,
    public loadingCtrl: LoadingController,
    public connect: ConnectServicesProvider,
    public toastCtrl: ToastController,
    public services: UserServicesProvider,
    public businesses: BusinessServicesProvider,
    private iab: InAppBrowser
  ) {
    this.step = 1;
    this.stepCondition = false;
    this.stepDefaultCondition = this.stepCondition;

    this.evts.subscribe("step:changed", step => {
      this.currentStep = step;
      this.stepCondition = this.stepDefaultCondition;
      console.log("this.currentStep: ", this.currentStep);
    });

    this.evts.subscribe("step:next", () => {
      console.log("Next pressed: ", this.business);
      if (this.content) {
        this.content.scrollToTop();
      }
    });
    this.evts.subscribe("step:back", () => {
      console.log("Back pressed: ", this.business);
      if (this.content) {
        this.content.scrollToTop();
      }
    });
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad CreatebusinessPage");
    this.utilities.category().then((data: any) => {
      if (data && data.success) {
        this.operatorCat = data.data;
      }
    });

    this.utilities.distributor_category().then((data: any) => {
      if (data && data.success) {
        this.distributorCat = data.data;
      }
    });

    this.utilities.booking_platforms().then((data: any) => {
      if (data && data.success) {
        this.platformsArr = data.data;
      }
    });

    this.utilities.getCountries().then(
      (data: any) => {
        if (data && data.success) {
          this.countriesArr = data.data;
        }
      },
      error => {
        console.log("error: ", error);
      }
    );
  }

  populateSubCategory(category) {
    this.utilities.subcategory(category).then(
      (data: any) => {
        if (data && data.success) {
          this.subCategoryArr = data.data;
          this.showSubcats = true;
          this.stepCondition = true;
        }
      },
      error => {
        console.log("error: ", error);
      }
    );
  }

  viewTerms(ev) {
    console.log("viewTerms: ", environment.AppTerms);

    ev.preventDefault();
    ev.stopPropagation();
    this.iab.create(environment.AppTerms, "_blank", {
      toolbarposition: "top",
      toolbar: "yes",
      location: "no"
    });
  }

  createButtons(dataArr: any, type: string) {
    let buttons = [];
    for (let index in dataArr) {
      let button = {
        text: type === "opt" ? dataArr[index].title : dataArr[index].name,
        handler: () => {
          let category =
            type === "opt" ? dataArr[index].title : dataArr[index].name;
          console.log("click icon " + category);

          this.business.type = type === "opt" ? "Operator" : "Distributor";
          this.business.categoryObj = dataArr[index];
          this.business.category = category;

          console.log("this.business ", this.business);
          return true;
        }
      };
      buttons.push(button);
    }
    buttons.push({
      text: "Cancel",
      role: "cancel",
      handler: () => {
        console.log("Cancel clicked");
      }
    });
    return buttons;
  }

  onFinish() {
    let ctrl = this;
    let loading = this.loadingCtrl.create({
      content: "Saving...",
      duration: 2000
    });
    console.log("saveBusiness: ", this.business);
    loading.present();
    this.businesses.create_business(this.business).then(
      (data: any) => {
        if (data && data.success) {
          let alert = ctrl.alertCtrl.create({
            message: data.data,
            title: "Record Saved!!",
            buttons: [
              {
                text: "Ok"
              }
            ]
          });
          alert.present();
          alert.onDidDismiss(() => {
            ctrl.navCtrl.pop();
          });
        } else {
          let alert = ctrl.alertCtrl.create({
            message: data.message,
            title: "Error!",
            buttons: [
              {
                text: "Ok"
              }
            ]
          });
          alert.present();
        }
        loading.dismiss();
      },
      error => {
        console.log("error: ", error);
      }
    );
  }

  toggleAddress(e) {
    // this.isSameAddress = !this.isSameAddress;
    if (this.isSameAddress) {
      this.business.physicalNumber = this.business.postalNumber;
      this.business.physicalStreet = this.business.postalStreet;
      this.business.physicalSuburb = this.business.postalSuburb;
      this.business.physicalState = this.business.postalState;
      this.business.physicalPostcode = this.business.postalPostcode;
      this.business.physicalCountry = this.business.postalCountry;
    }
  }

  showOperators() {
    this.stepCondition = false;
    let actionSheet = this.actionSheetCtrl.create({
      title: "Operator business type",
      buttons: this.createButtons(this.operatorCat, "opt")
    });
    actionSheet.present();
    actionSheet.onDidDismiss(() => {
      // this.stepCondition = !this.stepCondition;
      console.log("this.business.category: ", this.business.categoryObj);
      this.populateSubCategory(this.business.categoryObj.id);
    });
  }

  showDistributor() {
    this.stepCondition = false;
    let actionSheet = this.actionSheetCtrl.create({
      title: "Distributor business type",
      buttons: this.createButtons(this.distributorCat, "dist")
    });
    actionSheet.present();
    actionSheet.onDidDismiss(() => {
      this.stepCondition = true;
      // console.log('this.business.category: ', this.business.categoryObj);
      // this.populateSubCategory(this.business.categoryObj.id);
    });
  }

  requestAccess(business) {
    console.log("business: ", business);
    let ctrl = this;

    function requestAccess() {
      ctrl.isRequesting = true;
      ctrl.services
        .request_access({ accountId: business.id, type: business.type })
        .then(
          (data: any) => {
            if (data && data.success) {
              /*let toast = ctrl.toastCtrl.create({
						message: data.data,
						duration: 1500
					});
					toast.present();*/
              let alert = ctrl.alertCtrl.create({
                title: "Success!",
                subTitle: data.message,
                buttons: ["OK"]
              });
              alert.present();
              ctrl.isRequesting = false;
            } else {
              /*let toast = ctrl.toastCtrl.create({
						message: data.message,
						duration: 1500
					});
					toast.present();*/
              let alert = ctrl.alertCtrl.create({
                title: "Warning!",
                subTitle: data.message,
                buttons: ["OK"]
              });
              alert.present();
              ctrl.isRequesting = false;
            }
          },
          error => {
            console.log("error: ", error);
            ctrl.isRequesting = false;
          }
        );
    }

    let confirm = this.alertCtrl.create({
      title: "Request Access",
      message: "Do you want to request access to this business account?",
      buttons: [
        {
          text: "Yes",
          handler: () => {
            requestAccess();
          }
        },
        {
          text: "No",
          handler: () => {
            console.log("Agree clicked");
          }
        }
      ]
    });
    confirm.present();
  }

  updateCucumber() {
    /*if(!this.isAgree){
			this.stepCondition = false;
		}else{
			this.stepCondition = true;
		}*/
    this.isAgree = !this.isAgree;

    this.validateBusinessName();
  }

  validateBusinessName() {
    let ctrl = this;

    function checkBusinessName() {
      ctrl.isSearching = true;
      ctrl.connect
        .search_businesses({
          name: ctrl.business.name
        })
        .then(
          (data: any) => {
            if (data && data.success) {
              if (_.isArray(data.data) && data.data[0] === false) {
                ctrl.stepCondition = true;
                ctrl.needAccess = false;
                ctrl.isSearching = false;
              } else if (_.isArray(data.data) && data.data[0] !== false) {
                ctrl.needAccess = true;
                ctrl.stepCondition = false;
                ctrl.isSearching = false;

                ctrl.business = data.data[0];
              } else {
                ctrl.needAccess = true;
                ctrl.stepCondition = false;
                ctrl.isSearching = false;
              }
            } else {
              ctrl.stepCondition = true;
              ctrl.isSearching = false;
            }
          },
          error => {
            ctrl.needAccess = true;
            ctrl.stepCondition = false;
            ctrl.isSearching = false;
          }
        );
    }

    if (!this.isAgree) {
      this.stepCondition = false;
    } else {
      if (this.business.name && this.business.name.trim() !== "") {
        // this.stepCondition = true;
        setTimeout(() => {
          checkBusinessName();
        }, 1000);
      } else {
        this.stepCondition = false;
      }
    }
  }

  valildateBusDetails(e) {
    console.log("valildateBusDetails");
    if (this.showSubcats) {
      if (
        this.business.subCategory &&
        this.business.bookingPlatform &&
        this.business.businessContact &&
        this.business.url &&
        this.business.email &&
        this.business.phone
      ) {
        this.stepCondition = true;
      } else {
        this.stepCondition = false;
      }
    } else {
      if (
        this.business.businessContact &&
        this.business.url &&
        this.business.email &&
        this.business.phone
      ) {
        this.stepCondition = true;
      } else {
        this.stepCondition = false;
      }
    }
    console.log("this.stepCondition: ", this.stepCondition);
  }

  valildatePostDetails(e) {
    if (
      this.business.postalNumber &&
      this.business.postalStreet &&
      this.business.postalSuburb &&
      this.business.postalState &&
      this.business.postalPostcode &&
      this.business.postalCountry
    ) {
      this.stepCondition = true;
    } else {
      this.stepCondition = false;
    }
  }

  valildatePhyDetails(e) {
    if (
      this.business.physicalNumber &&
      this.business.physicalStreet &&
      this.business.physicalSuburb &&
      this.business.physicalState &&
      this.business.physicalPostcode &&
      this.business.physicalCountry
    ) {
      this.stepCondition = true;
      this.onFinish();
    } else {
      this.stepCondition = false;
    }
  }
}
