import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreatebusinessPage } from './createbusiness';

import { ComponentsModule } from '../../components/components.module';

@NgModule({
	declarations: [
		CreatebusinessPage,
	],
	imports: [
		ComponentsModule,
		IonicPageModule.forChild(CreatebusinessPage),
	],
})
export class CreatebusinessPageModule { }
