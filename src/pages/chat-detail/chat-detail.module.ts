import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChatDetailPage } from './chat-detail';

import { CommonModule } from '@angular/common';
import { PipesModule } from '../../pipes/pipes.module';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    ChatDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(ChatDetailPage),
    PipesModule,
    ComponentsModule,
    CommonModule
  ],
})
export class ChatDetailPageModule {}
