import { Component, ViewEncapsulation, NgZone } from "@angular/core";
import {
  App,
  NavController,
  LoadingController,
  AlertController,
  ToastController,
  Events
} from "ionic-angular";
import { CacheService, Cache } from "ionic-cache-observable";
import { Observable, Subscription } from "rxjs";
import { map, catchError } from "rxjs/operators";
import "rxjs/add/operator/finally";
import * as _ from "lodash";

// import { InAppBrowser } from '@ionic-native/in-app-browser';

import { CreatebusinessPage } from "../createbusiness/createbusiness";
import { SearchPage } from "../search/search";
import { LoginPage } from "../login/login";
import { BusinesstabsPage } from "../businesstabs/businesstabs";

import { UserServicesProvider } from "../../providers/services/users";
import { UtilitiesServicesProvider } from "../../providers/services/utilities";

import { StorageProvider } from "../../providers/storage/storage";
import { LoadingProvider } from "../../providers/loading/loading";

@Component({
  selector: "page-home",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "home.html"
})
export class HomePage {
  businessArr: any[] = [];
  showContent: boolean = false;
  fakeArr: any[] = [];

  user: any = {};

  businessArr$: Observable<any>;
  public cache: Cache<any>;
  public refreshSubscription: Subscription;

  constructor(
    public app: App,
    public zone: NgZone,
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public events: Events,
    public storageService: StorageProvider,
    public cacheService: CacheService,
    public loader: LoadingProvider,
    public services: UserServicesProvider,
    public utilities: UtilitiesServicesProvider // private iab: InAppBrowser
  ) {
    for (var i = 0; i < 12; i++) {
      this.fakeArr.push(i);
    }
  }

  ionViewDidLoad() {
    this.events.publish("showNotifications", 0);
  }

  ionViewWillEnter() {
    this.initAppUser().then(() => {
      this.initData();
    });
  }

  ionViewWillLeave() {
    if (this.refreshSubscription) {
      this.refreshSubscription.unsubscribe();
    }
  }

  private async initAppUser() {
    const user = await this.storageService.getStorage(
      this.storageService.storageKey.CURRENT_USER
    );
    this.zone.run(() => {
      this.user = !_.isEmpty(user) ? JSON.parse(user) || {} : {};
    });
  }

  private initData(refresher?: any) {
    this.showContent = false;
    this.businessArr = [];

    const dataObservable = Observable.fromPromise(this.services.businesses());
    const cacheKey =
      this.storageService.cacheKey.USERS_BUSINESSES + "_" + this.user.uid;

    this.cacheService.register(cacheKey, dataObservable).subscribe(cache => {
      this.businessArr = [];
      this.cache = cache;
      this.zone.run(() => {
        this.businessArr$ = cache.get$;

        this.cache.refresh().subscribe(
          () => {
            this.loader.start();
          },
          () => {
            this.loader.complete();
          },
          () => {
            this.loader.complete();
          }
        );
      });
    });

    this.businessArr$.subscribe(
      (item: any) => {
        const businessArr = item.accounts;
        _.each(businessArr, (row: any) => {
          if (row.type === 1) {
            row.businessType = "Operator";
          } else if (row.type === 2) {
            row.businessType = "Distributor";
          }

          if (row.role === 1) {
            row.businessRole = "Administrator";
          } else if (row.role === 2) {
            row.businessRole = "Staff";
          } else if (row.role === 3) {
            row.businessRole = "Trade";
          } else if (row.role === 4) {
            row.businessRole = "Media";
          }
        });
        this.zone.run(() => {
          this.businessArr = businessArr;
          this.showContent = true;
        });

        if (refresher) {
          refresher.complete();
        }
      },
      error => {
        console.log("error: ", error);
        this.showContent = true;
        if (refresher) {
          refresher.complete();
        }
        if (error && !error.success && error.error == "Token has expired") {
          this.navCtrl.setRoot(
            LoginPage,
            {},
            { animate: true, direction: "back" }
          );
        }
      }
    );
  }

  doRefresh(refresher: any) {
    if (this.cache) {
      this.zone.run(() => {
        this.initData(refresher);
      });
    } else {
      console.warn("Cache has not been initialized.");
      if (refresher) {
        refresher.cancel();
      }
    }
  }

  createBusiness() {
    this.navCtrl.push(CreatebusinessPage);
  }

  searchBusiness() {
    this.navCtrl.push(SearchPage);
  }

  errorAvatarHandler(event) {
    event.target.src = this.utilities.radomizeAvatar();
  }

  gotoProfile(item: any) {
    // if (item.role == 1 || item.role == 2) {
    // 	this.iab.create('https://portal.narnoo.com/landing/authenticate?token=' + item.token, '_blank');
    // 	return;
    // } else {

    this.storageService.setStorage(
      this.storageService.storageKey.BUSINESS_TOKENS,
      item.token
    );
    this.storageService.setStorage(
      this.storageService.storageKey.CURRENT_BUSINESS,
      JSON.stringify(item)
    );

    const loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    loading.present();
    setTimeout(() => {
      loading.dismiss();
      this.zone.run(() => {
        this.navCtrl
          .setRoot(
            BusinesstabsPage,
            {
              opentab: 0,
              item: item
            },
            { animate: true, direction: "forward" }
          )
          .then(() => {
            this.events.publish("generateFilters");
          });
      });
    }, 600);
  }

  unlinkBusiness(business) {
    const loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    loading.present();
    const requestAccess = () => {
      this.services
        .user_unlink({
          businessId: business.id,
          businessType: business.type
        })
        .then(
          (data: any) => {
            if (data && data.success) {
              loading.dismiss();
              const toast = this.toastCtrl.create({
                message: data.data,
                duration: 1500
              });
              toast.present();
              toast.onDidDismiss(() => {
                this.initData();
              });
            } else {
              loading.dismiss();
              let toast = this.toastCtrl.create({
                message: data.message,
                duration: 1500
              });
              toast.present();
            }
          },
          error => {
            loading.dismiss();
          }
        );
    };

    const confirm = this.alertCtrl.create({
      title: "Unlink Account",
      message: "Are you sure to unlink?",
      buttons: [
        {
          text: "Yes",
          handler: () => {
            requestAccess();
          }
        },
        {
          text: "No",
          handler: () => {
            console.log("Agree clicked");
          }
        }
      ]
    });
    confirm.present();
  }

  testToast() {
    const toast = this.toastCtrl.create({
      message: "Network online!",
      position: "top",
      dismissOnPageChange: true,
      duration: 5000,
      cssClass: "offline-toast-notif-success"
    });
    toast.present();
  }
}
