import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LocationPage, LocationModalPage } from './location';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
	declarations: [
		LocationPage,
		LocationModalPage
	],
	imports: [
		ComponentsModule,
		IonicPageModule.forChild(LocationPage),
	],
	entryComponents: [
		LocationModalPage
	]
})
export class LocationPageModule { }
