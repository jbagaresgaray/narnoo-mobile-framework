import { Component, ViewEncapsulation, NgZone } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ViewController,
  ModalController,
  AlertController,
  ToastController,
  LoadingController
} from "ionic-angular";
import * as _ from "lodash";
import { CacheService, Cache } from "ionic-cache-observable";
import { Observable, Subscription } from "rxjs";
import "rxjs/add/operator/finally";

import { LocationsServicesProvider } from "../../providers/services/locations";
import { StorageProvider } from "../../providers/storage/storage";
import { LoadingProvider } from "../../providers/loading/loading";

@IonicPage()
@Component({
  selector: "page-location-modal",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "location.modal.html"
})
export class LocationModalPage {
  locationId: any;
  item: any = {};
  business: any = {};

  constructor(
    public zone: NgZone,
    public viewCtrl: ViewController,
    public navParams: NavParams,
    public toastCtrl: ToastController,
    public locations: LocationsServicesProvider,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public storageService: StorageProvider
  ) {
    this.locationId = this.navParams.get("id");
    this.item = this.navParams.get("item");
    console.log("this.locationId: ", this.locationId);
  }

  async ionViewDidLoad() {
    const business = await this.storageService.getStorage(
      this.storageService.storageKey.CURRENT_BUSINESS
    );
    this.business = !_.isEmpty(business) ? JSON.parse(business) || {} : {};
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  saveLocation() {
    if (this.business.role !== 1) {
      this.toastCtrl
        .create({
          message: "Invalid Access!!!",
          duration: 3000
        })
        .present();
      return;
    }

    const loader = this.loadingCtrl.create({
      content: "Saving..."
    });
    loader.present();
    console.log("this.locationId: ", this.locationId);

    if (this.locationId) {
      this.locations
        .edit_location({
          id: this.locationId,
          name: this.item.name
        })
        .then(
          (data: any) => {
            if (data && data.success) {
              console.log("data: ", data);
              loader.dismiss();
              let alert = this.alertCtrl.create({
                title: "SUCCESS",
                message: "Location was updated successfully",
                buttons: ["OK"]
              });
              alert.onDidDismiss(() => {
                this.viewCtrl.dismiss("save");
              });
              alert.present();
            } else {
              loader.dismiss();
              let toast = this.alertCtrl.create({
                title: "WARNING",
                message: data.message,
                buttons: ["OK"]
              });
              toast.present();
            }
          },
          error => {
            console.log("error: ", error);
            loader.dismiss();
          }
        );
    } else {
      this.locations
        .add_location({
          name: this.item.name
        })
        .then(
          (data: any) => {
            if (data && data.success) {
              console.log("data: ", data);
              loader.dismiss();
              let alerty = this.alertCtrl.create({
                title: "SUCCESS",
                message: "Location was added successfully",
                buttons: ["OK"]
              });
              alerty.onDidDismiss(() => {
                this.viewCtrl.dismiss("save");
              });
              alerty.present();
            } else {
              loader.dismiss();
              let alerty = this.alertCtrl.create({
                title: "WARNING",
                message: data.message,
                buttons: ["OK"]
              });
              alerty.present();
            }
          },
          error => {
            loader.dismiss();
            console.log("error: ", error);
          }
        );
    }
  }
}
@Component({
  selector: "page-location",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "location.html"
})
export class LocationPage {
  business: any = {};
  showContent: boolean = false;
  showContentError: boolean = false;
  contentErr: any = {};

  locationsArr: any[] = [];
  fakeArr: any[] = [];

  locationArr$: Observable<any>;
  private locationCache: Cache<any>;
  public refreshSubscription: Subscription;

  constructor(
    public zone: NgZone,
    public navCtrl: NavController,
    public navParams: NavParams,
    public locations: LocationsServicesProvider,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public storageService: StorageProvider,
    public loader: LoadingProvider,
    public cacheService: CacheService
  ) {
    for (var i = 0; i < 12; ++i) {
      this.fakeArr.push(i);
    }
  }

  private initializeData(refresher?: any) {
    this.showContent = false;

    const dataObservable = Observable.fromPromise(
      this.locations.get_locations()
    );
    const cacheKey =
      this.storageService.cacheKey.LOCATION + "_" + this.business.id;
    this.cacheService.register(cacheKey, dataObservable).subscribe(cache => {
      this.locationCache = cache;
      this.zone.run(() => {
        this.locationArr$ = cache.get$;
        this.locationCache.refresh().subscribe(
          () => {
            this.loader.start();
          },
          () => {
            this.loader.complete();
          },
          () => {
            this.loader.complete();
          }
        );
      });
    });

    this.locationArr$.subscribe(
      (data: any) => {
        if (data && data.success) {
          if (_.isArray(data.data) && data.data[0] !== false) {
            const locationsArr = data.data;
            this.zone.run(() => {
              this.locationsArr = locationsArr;
            });
          }
        }
        console.log("this.locationsArr: ", this.locationsArr);
        this.showContent = true;
        this.showContentError = false;
        if (refresher) {
          refresher.complete();
        }
      },
      error => {
        console.log("error: ", error);
        this.showContent = true;
        this.showContentError = true;
        if (error && !error.success) {
          this.contentErr = error;
        }
        if (refresher) {
          refresher.complete();
        }
      }
    );
  }

  async ionViewDidLoad() {
    console.log("ionViewDidLoad LocationPage");
    const business = await this.storageService.getStorage(
      this.storageService.storageKey.CURRENT_BUSINESS
    );
    this.business = !_.isEmpty(business) ? JSON.parse(business) || {} : {};

    this.initializeData();
  }

  deleteLocation(item, slidingItem) {
    if (this.business.role !== 1) {
      const toast = this.toastCtrl.create({
        message: "Invalid Access!!! Administrator access only",
        duration: 3000
      });
      toast.present();
      return;
    }

    slidingItem.close();

    const deleteLocation = () => {
      let loader = this.loadingCtrl.create({
        content: "Deleting..."
      });
      loader.present();
      this.locations.delete_locations(item.id).then(
        (data: any) => {
          if (data && data.success) {
            console.log("data: ", data);

            loader.dismiss();
            this.initializeData();
          } else {
            loader.dismiss();
            const toast = this.toastCtrl.create({
              message: data.message,
              duration: 2000
            });
            toast.present();
          }
        },
        error => {
          console.log("error: ", error);
        }
      );
    };

    const confirm = this.alertCtrl.create({
      title: "Delete this location?",
      message: "Are you sure to delete this location?",
      buttons: [
        {
          text: "No",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Yes",
          handler: () => {
            console.log("Agree clicked");
            deleteLocation();
          }
        }
      ]
    });
    confirm.present();
  }

  updateLocation(item, slidingItem) {
    if (this.business.role !== 1) {
      const toast = this.toastCtrl.create({
        message: "Invalid Access!!! Administrator access only",
        duration: 3000
      });
      toast.present();
      return;
    }

    slidingItem.close();
    const modal = this.modalCtrl.create(LocationModalPage, {
      id: item.id,
      item: item
    });
    modal.onDidDismiss(resp => {
      console.log("resp: ", resp);
      if (resp == "save") {
        this.initializeData();
      }
    });
    modal.present();
  }

  addLocation() {
    if (this.business.role !== 1) {
      const toast = this.toastCtrl.create({
        message: "Invalid Access!!! Administrator access only",
        duration: 3000
      });
      toast.present();
      return;
    }

    const modal = this.modalCtrl.create(LocationModalPage, {
      item: {}
    });
    modal.onDidDismiss(resp => {
      console.log("resp: ", resp);
      if (resp == "save") {
        this.initializeData();
      }
    });
    modal.present();
  }

  doRefresh(refresher: any) {
    if (this.locationCache) {
      this.showContent = false;
      this.zone.run(() => {
        this.initializeData(refresher);
      });
    } else {
      console.warn("Cache has not been initialized.");
      if (refresher) {
        refresher.cancel();
      }
    }
  }
}
