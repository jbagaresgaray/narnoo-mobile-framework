import { Component, ViewChild, ViewEncapsulation, NgZone } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  AlertController,
  LoadingController,
  ActionSheetController,
  Events,
  Content,
  TextInput,
  ToastController
} from "ionic-angular";
import * as _ from "lodash";

import { UtilitiesServicesProvider } from "../../providers/services/utilities";
import { NewsFeedServicesProvider } from "../../providers/services/newsfeed";
import { StorageProvider } from "../../providers/storage/storage";

@IonicPage()
@Component({
  selector: "page-notification-details",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "notification-details.html"
})
export class NotificationDetailsPage {
  token: string;
  business: any = {};

  notification: any = {};
  status: string = "";
  message: string = "";
  notifStatus: string = "";
  editorMsg = "";

  @ViewChild(Content) content: Content;
  @ViewChild("chat_input") messageInput: TextInput;

  showMessaging: boolean = true;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public events: Events,
    public zone: NgZone,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    public actionSheetCtrl: ActionSheetController,
    public utilities: UtilitiesServicesProvider,
    public newsfeeds: NewsFeedServicesProvider,
    public storageService: StorageProvider
  ) {
    this.notification = navParams.get("notification");
    this.status = navParams.get("status");

    if (this.notification) {
      this.reformatNotification();
    }

    console.log("this.notification: ", this.notification);
    console.log("this.status: ", this.status);

    this.newsfeeds.notificationData$.subscribe((data: any) => {
      console.log("activity feed notificationData: ", data);
      if (data && data.success) {
        this.zone.run(() => {
          this.newsfeeds.totalNotification = data.data.total;
          this.events.publish("showNotifications", data.data.total);
        });
      } else {
        this.zone.run(() => {
          this.newsfeeds.totalNotification = 0;
          this.events.publish("showNotifications", 0);
        });
      }
    });
  }

  private reformatNotification() {
    if (!_.isEmpty(this.notification.toMessage)) {
      if (_.isBoolean(this.notification.toMessage)) {
        this.message = "";
        this.showMessaging = true;
      } else {
        this.message = this.notification.toMessage;
        this.showMessaging = false;
      }
    }

    if (this.notification.status) {
      this.notifStatus = "Approved";
    } else {
      this.notifStatus = "Rejected";
    }
  }

  private acknowledgeNotification(status) {
    const loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    loading.present();
    this.newsfeeds
      .create_app_notification_acknowledge({
        id: this.notification.id,
        status: status
      })
      .then(
      (data: any) => {
        console.log("data: ", data);
        this.notification.acknowledged = true;
        this.notification.status = status;

        this.reformatNotification();
        this.newsfeeds.notificationCache$.refresh().subscribe();

        const alert = this.alertCtrl.create({
          title: "SUCCESS",
          subTitle: data.data,
          buttons: ["OK"]
        });
        loading.dismiss().then(() => {
          alert.present();
        });
      },
      (error: any) => {
        loading.dismiss();
        console.log("app_notification_acknowledge ERROR: ", error);
      }
      );
  }

  async ionViewDidLoad() {
    console.log("ionViewDidLoad NotificationDetailsPage");
    const business = await this.storageService.getStorage(
      this.storageService.storageKey.CURRENT_BUSINESS
    );
    this.business = !_.isEmpty(business) ? JSON.parse(business) || {} : {};
  }

  errorAvatarHandler(event) {
    event.target.src = this.utilities.radomizeAvatar();
  }

  approvedNotification() {
    if (this.business.role !== 1) {
      this.toastCtrl
        .create({
          message: "Invalid Access!!!",
          duration: 3000
        })
        .present();
      return;
    }

    this.acknowledgeNotification(true);
  }

  rejectNotification() {
    if (this.business.role !== 1) {
      this.toastCtrl
        .create({
          message: "Invalid Access!!!",
          duration: 3000
        })
        .present();
      return;
    }

    const loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    loading.present();
    this.newsfeeds
      .create_app_notification_acknowledge({
        id: this.notification.id,
        status: false
      })
      .then(
      (data: any) => {
        console.log("data: ", data);
        const alert = this.alertCtrl.create({
          title: "SUCCESS",
          subTitle: data.data,
          buttons: ["OK"]
        });
        alert.onDidDismiss(() => {
          this.notification.status = false;

          this.reformatNotification();
          this.newsfeeds.notificationCache$.refresh().subscribe();
        });
        loading.dismiss().then(() => {
          alert.present();
        });
      },
      (error: any) => {
        loading.dismiss();
        console.log("app_notification_acknowledge ERROR: ", error);
      }
      );
  }

  deleteNotification() {
    if (this.business.role !== 1) {
      this.toastCtrl
        .create({
          message: "Invalid Access!!!",
          duration: 3000
        })
        .present();
      return;
    }

    const deleteNotification = () => {
      const loading = this.loadingCtrl.create({
        dismissOnPageChange: true
      });
      loading.present();
      this.newsfeeds
        .delete_notification({
          id: this.notification.id
        })
        .then(
        (data: any) => {
          this.newsfeeds.notificationCache$.refresh().subscribe();

          if (data && data.success) {
            let alert = this.alertCtrl.create({
              title: "SUCCESS",
              subTitle: data.message,
              buttons: ["OK"]
            });
            alert.onDidDismiss(() => {
              this.navCtrl.pop();
            });
            loading.dismiss().then(() => {
              alert.present();
            });
          }
        },
        error => {
          console.log("error: ", error);
          loading.dismiss();
        }
        );
    };

    const alert = this.alertCtrl.create({
      title: "Delete this notification?",
      message: "Are you sure to delete this notification?",
      buttons: [
        {
          text: "Cancel",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Confirm",
          handler: () => {
            console.log("Agree clicked");
            deleteNotification();
          }
        }
      ]
    });
    alert.present();
  }

  showStatus() {
    if (this.business.role !== 1) {
      this.toastCtrl
        .create({
          message: "Invalid Access!!!",
          duration: 3000
        })
        .present();
      return;
    }

    const updateApproved = () => {
      const loading = this.loadingCtrl.create({
        dismissOnPageChange: true
      });
      loading.present();
      this.newsfeeds
        .update_app_notification_acknowledge({
          id: this.notification.id,
          status: true
        })
        .then(
        (data: any) => {
          console.log("data: ", data);
          this.notification.status = true;
          this.reformatNotification();

          const alert = this.alertCtrl.create({
            title: "SUCCESS",
            subTitle: data.data,
            buttons: ["OK"]
          });
          alert.onDidDismiss(() => {
            this.newsfeeds.notificationCache$.refresh().subscribe();
          });
          loading.dismiss().then(() => {
            alert.present();
          });
        },
        (error: any) => {
          loading.dismiss();
          console.log("update_app_notification_acknowledge ERROR: ", error);
        }
        );
    };

    const actionSheet = this.actionSheetCtrl.create({
      title: "Update Status",
      buttons: [
        {
          text: "Approved",
          handler: () => {
            updateApproved();
          }
        },
        {
          text: "Reject",
          role: "destructive",
          handler: () => {
            console.log("Destructive clicked");
            this.acknowledgeNotification(false);
          }
        },
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        }
      ]
    });
    actionSheet.present();
  }

  sendMessage() {
    if (this.business.role !== 1) {
      this.toastCtrl
        .create({
          message: "Invalid Access!!!",
          duration: 3000
        })
        .present();
      return;
    }

    const loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    loading.onDidDismiss(() => { });
    loading.present();

    const _sendMessage = () => {
      this.newsfeeds
        .create_notification_message({
          id: this.notification.id,
          message: this.message
        })
        .then(
        (data: any) => {
          if (data && data.success) {
            this.alertCtrl
              .create({
                title: "SUCCESS",
                subTitle: "Notification has been sent",
                buttons: ["OK"]
              })
              .present()
              .then(() => {
                this.notification.toMessage = this.message;

                this.reformatNotification();
                this.newsfeeds.notificationCache$.refresh().subscribe();
              });
          } else if (data && !data.success) {
            this.alertCtrl
              .create({
                title: "WARNING",
                subTitle: data.message,
                buttons: ["OK"]
              })
              .present();
          }
          loading.dismiss();
        },
        error => {
          console.log("error: ", error);
          loading.dismiss();
        }
        );
    }

    _sendMessage();
  }
}
