import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { PipesModule } from "../../pipes/pipes.module";
import { ComponentsModule } from "../../components/components.module";

import { NotificationDetailsPage } from "./notification-details";

@NgModule({
	declarations: [NotificationDetailsPage],
	imports: [
		PipesModule,
		ComponentsModule,
		IonicPageModule.forChild(NotificationDetailsPage)
	]
})
export class NotificationDetailsPageModule {}
