import { Component, ViewChild, ViewEncapsulation, NgZone } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  ActionSheetController,
  ToastController,
  Platform,
  AlertController,
  Content,
  ModalController
} from "ionic-angular";
import { CacheService, Cache } from "ionic-cache-observable";
import { Observable, Subscription } from "rxjs";
import "rxjs/add/operator/finally";
import * as _ from "lodash";

import { ImageModalPage } from "../../pages/image-modal/image-modal";
import { ImageDetailPage } from "../../pages/image-detail/image-detail";

import { AlbumsServicesProvider } from "../../providers/services/albums";
import { StorageProvider } from "../../providers/storage/storage";
import { LoadingProvider } from "../../providers/loading/loading";

@IonicPage()
@Component({
  selector: "page-album-gallery",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "album-gallery.html"
})
export class AlbumGalleryPage {
  token: string;
  imagesArr: any[] = [];
  imagesLoadingArr: any[] = [];
  albumId: any;
  title: string;

  albumsArr$: Observable<any>;
  private albumCache: Cache<any>;
  public refreshSubscription: Subscription;

  business: any = {};
  action: string;
  connect_params: any = {};

  showPreview: boolean = false;
  showContent: boolean = false;
  showCheckbox: boolean = false;
  showUpload: boolean = false;

  @ViewChild(Content) content: Content;
  constructor(
    public zone: NgZone,
    public platform: Platform,
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public albums: AlbumsServicesProvider,
    public actionSheetCtrl: ActionSheetController,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public storageService: StorageProvider,
    public cacheService: CacheService,
    public loader: LoadingProvider
  ) {
    for (var i = 0; i < 12; ++i) {
      this.imagesLoadingArr.push(i);
    }

    this.albumId = navParams.get("id");
    this.title = navParams.get("album");
    this.action = navParams.get("action");
    this.connect_params = navParams.get("params");
    console.log("params: ", this.connect_params);
    console.log("action: ", this.action);

    if (this.business.role == 1) {
      if (this.action == "connect") {
        this.showUpload = false;
      } else {
        this.showUpload = true;
      }
    } else {
      this.showUpload = false;
    }
  }

  private async initBusiness() {
    const business = await this.storageService.getStorage(
      this.storageService.storageKey.CURRENT_BUSINESS
    );
    this.business = !_.isEmpty(business) ? JSON.parse(business) || {} : {};
  }

  private initData(ev?: any) {
    this.showContent = false;

    const successResponse = (data: any) => {
      if (data && data.success) {
        this.zone.run(() => {
          this.imagesArr = data.data.images;
          _.each(this.imagesArr, (row: any) => {
            row.selected = false;
          });
        });
        console.log("this.imagesArr: ", this.imagesArr);
      }
      this.showContent = true;
      if (ev) {
        ev.complete();
      }
    };

    const errorResponse = error => {
      console.log("Error: ", error);
      this.showContent = true;
      if (ev) {
        ev.complete();
      }
    };

    let dataObservable: any;
    let cacheKey: any;
    if (this.action == "connect") {
      if (this.connect_params.type == "operator") {
        dataObservable = Observable.fromPromise(
          this.albums.album_operator_images(
            this.connect_params.id,
            this.albumId
          )
        );
        cacheKey =
          this.storageService.cacheKey.ALBUMS_GALLERY +
          "_" +
          this.business.id +
          "_operator_" +
          this.connect_params.id +
          "_" +
          this.albumId;
      } else if (this.connect_params.type == "distributor") {
        dataObservable = Observable.fromPromise(
          this.albums.album_distributor_images(
            this.connect_params.id,
            this.albumId
          )
        );
        cacheKey =
          this.storageService.cacheKey.ALBUMS_GALLERY +
          "_" +
          this.business.id +
          "_distributor_" +
          this.connect_params.id +
          "_" +
          this.albumId;
      }
    } else {
      dataObservable = Observable.fromPromise(
        this.albums.album_images(this.albumId)
      );
      cacheKey =
        this.storageService.cacheKey.ALBUMS_GALLERY +
        "_" +
        this.business.id +
        "_" +
        this.albumId;
    }

    this.cacheService.register(cacheKey, dataObservable).subscribe(cache => {
      this.albumCache = cache;
      this.zone.run(() => {
        this.albumsArr$ = cache.get$;

        this.albumCache.refresh().subscribe(
          () => {
            this.loader.start();
          },
          () => {
            this.loader.complete();
          },
          () => {
            this.loader.complete();
          }
        );
      });
    });

    this.albumsArr$.subscribe(successResponse, errorResponse);
  }

  ionViewDidLoad() {
    this.initBusiness().then(() => {
      this.initData();
    });
  }

  doRefresh(refresher: any) {
    if (this.albumCache) {
      this.showContent = false;
      this.zone.run(() => {
        this.initData(refresher);
      });
    } else {
      console.warn("Cache has not been initialized.");
      if (refresher) {
        refresher.cancel();
      }
    }
  }

  playSlideShow() {
    if (_.size(this.imagesArr) < 1) {
      const alert = this.alertCtrl.create({
        title: "WARNING",
        message: "Unable to display Slideshows. No images on the album found!"
      });
      alert.present();
      return;
    }

    this.showPreview = !this.showPreview;
    this.content.resize();
  }

  viewImageDetail(item) {
    if (this.showCheckbox) {
      _.each(this.imagesArr, (row: any) => {
        if (item.id == row.id) {
          row.selected = !row.selected;
        }
      });
      return;
    }

    const params: any = {};
    params.image = item;
    if (this.action == "connect") {
      params.action = "connect";
      params.params = this.connect_params;
    }
    this.navCtrl.push(ImageDetailPage, params);
  }

  uploadFile() {
    if (this.action == "connect") {
      const alert = this.alertCtrl.create({
        title: "WARNING",
        message: "Invalid Access! Business owner can update only.",
        buttons: ["OK"]
      });
      alert.present();
      return;
    }

    const modal = this.modalCtrl.create(ImageModalPage, {
      media: "albums",
      params: {
        albumId: this.albumId
      },
      mode: "albums"
    });
    modal.onDidDismiss(resp => {
      if (resp == "save") {
        this.initData();
      }
    });
    modal.present();
  }

  selectClick() {
    if (this.action == "connect") {
      const alert = this.alertCtrl.create({
        title: "WARNING",
        message: "Invalid Access! Business owner can update only.",
        buttons: ["OK"]
      });
      alert.present();
      return;
    }

    this.showCheckbox = true;
  }

  cancelClick() {
    if (this.action == "connect") {
      const alert = this.alertCtrl.create({
        title: "WARNING",
        message: "Invalid Access! Business owner can update only.",
        buttons: ["OK"]
      });
      alert.present();
      return;
    }

    this.showCheckbox = false;
  }

  deleteSelected() {
    let selectedDatas: any[] = [];
    selectedDatas = _.filter(this.imagesArr, (row: any) => {
      return row.selected;
    });

    const removeAlbum = () => {
      const loading = this.loadingCtrl.create({
        dismissOnPageChange: true,
        content: "Deleting..."
      });
      loading.present();
      selectedDatas = _.map(selectedDatas, (row: any) => {
        return {
          id: row.id
        };
      });
      this.albums
        .album_remove_image({
          albumId: this.albumId,
          image: selectedDatas
        })
        .then(
          (data: any) => {
            if (data && data.success) {
              setTimeout(() => {
                this.initData();
                this.showCheckbox = false;
              }, 1000);
            }
            loading.dismiss();
          },
          error => {
            console.log("error: ", error);
            loading.dismiss();
            if (error && !error.success) {
              const toast = this.toastCtrl.create({
                message: error.message
              });
              toast.present();
            }
          }
        );
    };

    const confirm = this.alertCtrl.create({
      title: "Delete Selected Image?",
      message: "Are you sure to delete selected images?",
      buttons: [
        {
          text: "No",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Yes",
          handler: () => {
            removeAlbum();
          }
        }
      ]
    });
    confirm.present();
  }
}
