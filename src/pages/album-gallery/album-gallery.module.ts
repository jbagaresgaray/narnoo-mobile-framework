import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AlbumGalleryPage } from './album-gallery';

import { IonicImageViewerModule } from 'ionic-img-viewer';
import { PipesModule } from '../../pipes/pipes.module';
import { ComponentsModule } from '../../components/components.module';
import { IonicImageLoader } from "ionic-image-loader";

@NgModule({
  declarations: [
    AlbumGalleryPage,
  ],
  imports: [
    IonicPageModule.forChild(AlbumGalleryPage),
    IonicImageViewerModule,
    PipesModule,
    ComponentsModule,
    IonicImageLoader
  ],
})
export class AlbumGalleryPageModule {}
