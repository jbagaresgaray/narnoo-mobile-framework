import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { ImageDetailPage, ImageDetailPopoverPage } from "./image-detail";
import { IonicImageLoader } from "ionic-image-loader";
import { IonicImageViewerModule } from "ionic-img-viewer";
import { CacheModule } from "ionic-cache-observable";

import { PipesModule } from "../../pipes/pipes.module";
import { ComponentsModule } from "../../components/components.module";

@NgModule({
  declarations: [ImageDetailPage, ImageDetailPopoverPage],
  imports: [
    IonicPageModule.forChild(ImageDetailPage),
    IonicPageModule.forChild(ImageDetailPopoverPage),
    IonicImageLoader,
    PipesModule,
    ComponentsModule,
    IonicImageViewerModule,
    CacheModule
  ]
})
export class ImageDetailPageModule {}
