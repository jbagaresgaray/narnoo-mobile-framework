import { Component, NgZone, ViewEncapsulation, OnInit } from "@angular/core";
import {
  App,
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  AlertController,
  Platform,
  ActionSheetController,
  ModalController,
  PopoverController,
  ViewController,
  Events,
  ToastController
} from "ionic-angular";
import * as _ from "lodash";
import * as $ from "jquery";
import { CacheService, Cache } from "ionic-cache-observable";
import { Observable, Subscription } from "rxjs";
import "rxjs/add/operator/finally";

import { FileTransfer, FileTransferObject } from "@ionic-native/file-transfer";
import { LocalNotifications } from "@ionic-native/local-notifications";
import { InAppBrowser } from "@ionic-native/in-app-browser";
import { SocialSharing } from "@ionic-native/social-sharing";

import { ImageDetailInfoPage } from "../../pages/image-detail-info/image-detail-info";
import { AlbumsModalPage } from "../../pages/albums-modal/albums-modal";
import { ProductsModalPage } from "../../pages/products-modal/products-modal";
import { CollectionsModalPage } from "../../pages/collections-modal/collections-modal";
import { ChatPage } from "../../pages/chat/chat";

import { ImagesServicesProvider } from "../../providers/services/images";
import { AlbumsServicesProvider } from "../../providers/services/albums";
import { ProductsServicesProvider } from "../../providers/services/products";

import { environment } from "../../environments/environment";
import { StorageProvider } from "../../providers/storage/storage";
import { LoadingProvider } from "../../providers/loading/loading";

declare let safari: any;

@Component({
  template: `
    <ion-list>
      <button ion-item (click)="downloadImage()">
        <ion-icon name="ios-download-outline" item-start></ion-icon>
        Download
      </button>
      <button
        ion-item
        (click)="shareNow()"
        *ngIf="
          action != 'connect' &&
          action != 'notif' &&
          (business.role == 1 || business.role == 2)
        "
      >
        <ion-icon name="md-share" item-start></ion-icon>
        Share
      </button>
    </ion-list>
  `
})
export class ImageDetailPopoverPage implements OnInit {
  business: any = {};
  showDownload: boolean;
  action: string;

  constructor(
    public params: NavParams,
    public viewCtrl: ViewController,
    public events: Events,
    public storageService: StorageProvider
  ) {
    this.showDownload = params.get("showDownload");
    this.action = params.get("action");
  }

  async ngOnInit() {
    const business = await this.storageService.getStorage(
      this.storageService.storageKey.CURRENT_BUSINESS
    );
    this.business = !_.isEmpty(business) ? JSON.parse(business) || {} : {};
  }

  close() {
    this.viewCtrl.dismiss();
  }

  viewChat() {
    this.events.publish("ImageDetailPage:viewChat");
    this.viewCtrl.dismiss();
  }

  downloadImage() {
    this.events.publish("ImageDetailPage:downloadImage");
    this.viewCtrl.dismiss();
  }

  shareNow() {
    this.events.publish("ImageDetailPage:shareNow");
    this.viewCtrl.dismiss();
  }
}
@IonicPage()
@Component({
  selector: "page-image-detail",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "image-detail.html"
})
export class ImageDetailPage {
  action: string;

  item: any = {};
  params: any = {};
  business: any = {};
  connect_params: any = {};
  notif_business: any = {};

  showDownload: boolean;
  showError: boolean = false;
  showLoading: boolean = true;
  isSafari: boolean;
  showRelatedImages: boolean = false;

  errContent: string = "";
  callback: any;

  relatedImagesArr: any[] = [];

  storageDirectory: string = "";
  fileTransfer: FileTransferObject;

  mediaArr$: Observable<any>;
  private mediaCache: Cache<any>;
  public refreshSubscription: Subscription;

  relatedMediaArr$: Observable<any>;
  private relatedMediaCache: Cache<any>;

  constructor(
    public app: App,
    public events: Events,
    public zone: NgZone,
    public platform: Platform,
    public navCtrl: NavController,
    public navParams: NavParams,
    public images: ImagesServicesProvider,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public actionSheetCtrl: ActionSheetController,
    private transfer: FileTransfer,
    public albums: AlbumsServicesProvider,
    public products: ProductsServicesProvider,
    public modalCtrl: ModalController,
    public popoverCtrl: PopoverController,
    public toastCtrl: ToastController,
    private localNotifications: LocalNotifications,
    private iab: InAppBrowser,
    private socialSharing: SocialSharing,
    public storageService: StorageProvider,
    public cacheService: CacheService,
    public loader: LoadingProvider
  ) {
    this.isSafari =
      /constructor/i.test(window["HTMLElement"]) ||
      (function(p) {
        return p.toString() === "[object SafariRemoteNotification]";
      })(!window["safari"] || safari.pushNotification);

    this.action = navParams.get("action");
    this.connect_params = navParams.get("params");
    this.callback = navParams.get("callback");
    this.params = navParams.get("image");
    this.notif_business = navParams.get("business");

    console.log("this.action: ", this.action);
    console.log("this.connect_params: ", this.connect_params);

    this.item.markets = {};

    if (platform.is("cordova")) {
      if (this.platform.is("ios")) {
        this.storageDirectory =
          window["cordova"].file.documentsDirectory + environment.AlbumName;
      } else if (this.platform.is("android")) {
        this.storageDirectory =
          window["cordova"].file.externalRootDirectory + environment.AlbumName;
      }
    }

    // ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //
    // ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //
    // ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //

    events.unsubscribe("ImageDetailPage:viewChat");
    events.unsubscribe("ImageDetailPage:downloadImage");
    events.unsubscribe("ImageDetailPage:shareNow");

    events.subscribe("ImageDetailPage:downloadImage", () => {
      this.downloadImage();
    });

    events.subscribe("ImageDetailPage:viewChat", () => {
      this.viewChat();
    });

    events.subscribe("ImageDetailPage:shareNow", () => {
      this.shareNow();
    });
  }

  private async initBusiness() {
    const business = await this.storageService.getStorage(
      this.storageService.storageKey.CURRENT_BUSINESS
    );
    this.business = !_.isEmpty(business) ? JSON.parse(business) || {} : {};
  }

  private initData(refresher?: any) {
    const successResponse = (data: any) => {
      if (data && data.success) {
        const item = data.data;

        this.zone.run(() => {
          this.item = item;
          this.item.imageId = item.id;
          this.item.imageSize = parseFloat(this.item.imageSize);
          if (_.isEmpty(this.item.caption)) {
            this.item.caption = "No caption available";
          }

          if (this.platform.is("cordova")) {
            if (this.platform.is("ios")) {
              if (this.item.uploadedAt) {
                this.item.uploadedAt = new Date(
                  this.item.uploadedAt.replace(/-/g, "/")
                );
              }
            }
          } else {
            if (this.platform.is("ios")) {
              if (this.isSafari) {
                if (this.item.uploadedAt) {
                  this.item.uploadedAt = new Date(
                    this.item.uploadedAt.replace(/-/g, "/")
                  );
                }
              } else {
                if (this.item.uploadedAt) {
                  this.item.uploadedAt = new Date(this.item.uploadedAt);
                }
              }
            }
          }

          if (this.item.markets) {
            this.item.markets.australia =
              this.item.markets.australia == 1 ? true : false;
            this.item.markets.france =
              this.item.markets.france == 1 ? true : false;
            this.item.markets.germany =
              this.item.markets.germany == 1 ? true : false;
            this.item.markets.greaterChina =
              this.item.markets.greaterChina == 1 ? true : false;
            this.item.markets.india =
              this.item.markets.india == 1 ? true : false;
            this.item.markets.japan =
              this.item.markets.japan == 1 ? true : false;
            this.item.markets.korea =
              this.item.markets.korea == 1 ? true : false;
            this.item.markets.newZealand =
              this.item.markets.newZealand == 1 ? true : false;
            this.item.markets.northAmerica =
              this.item.markets.northAmerica == 1 ? true : false;
            this.item.markets.singapore =
              this.item.markets.singapore == 1 ? true : false;
            this.item.markets.unitedKindom =
              this.item.markets.unitedKindom == 1 ? true : false;
          } else {
            this.item.markets = {};
          }
        });
      }
      this.showError = false;
      this.showLoading = false;

      if (refresher) {
        refresher.complete();
      }

      console.log("relatedImages 1: ", this.business.role);
      this.relatedImages();

      setTimeout(() => {
        $(".progressive-image").each(function() {
          var image = new Image();
          var previewImage = $(this).find(".loadingImage");
          var newImage = $(this).find(".overlay");
          image.src = previewImage.data("image");
          image.onload = function() {
            newImage.css("background-image", "url(" + image.src + ")");
            newImage.css("opacity", "1");
          };
          image.onerror = function() {
            newImage.css("background-image", "url(./assets/img/thumb.jpg)");
            newImage.css("opacity", "1");
          };
        });
      }, 1000);
    };

    const errorResponse = error => {
      console.log("errorResponse: ", error);
      this.showError = true;
      this.showLoading = false;
      if (refresher) {
        refresher.complete();
      }
      if (error) {
        this.errContent = error.message || "No image media found!";
      }
    };

    let dataObservable: any;
    let cacheKey: any;
    this.showLoading = true;

    if (this.action == "connect") {
      if (this.connect_params.type == "operator") {
        dataObservable = Observable.fromPromise(
          this.images.image_operator_image(
            this.connect_params.id,
            this.params.id
          )
        );
        cacheKey =
          this.storageService.cacheKey.MEDIA_IMAGE_DETAIL +
          "_" +
          this.business.id +
          "_operator_" +
          this.connect_params.id +
          "_" +
          this.params.id;
      } else if (this.connect_params.type == "distributor") {
        dataObservable = Observable.fromPromise(
          this.images.image_distributor_image(
            this.connect_params.id,
            this.params.id
          )
        );
        cacheKey =
          this.storageService.cacheKey.MEDIA_IMAGE_DETAIL +
          "_" +
          this.business.id +
          "_distributor_" +
          this.connect_params.id +
          "_" +
          this.params.id;
      }
    } else if (this.action == "notif") {
      dataObservable = Observable.fromPromise(
        this.images.image_detail(this.params.id)
      );
      cacheKey =
        this.storageService.cacheKey.MEDIA_IMAGE_DETAIL +
        "_" +
        this.business.id +
        "_" +
        this.params.id;
    } else {
      dataObservable = Observable.fromPromise(
        this.images.image_detail(this.params.id)
      );
      cacheKey =
        this.storageService.cacheKey.MEDIA_IMAGE_DETAIL +
        "_" +
        this.business.id +
        "_" +
        this.params.id;
    }

    this.cacheService.register(cacheKey, dataObservable).subscribe(cache => {
      this.mediaCache = cache;
      this.zone.run(() => {
        this.mediaArr$ = cache.get$;

        this.mediaCache.refresh().subscribe(
          () => {
            this.loader.start();
          },
          () => {
            this.loader.complete();
          },
          () => {
            this.loader.complete();
          }
        );
      });
    });

    this.mediaArr$.subscribe(successResponse, errorResponse);
  }

  private relatedImages() {
    console.log("relatedImages");
    this.showRelatedImages = false;

    let dataObservable: any;
    let cacheKey: any;
    if (this.action == "connect") {
      dataObservable = Observable.fromPromise(
        this.images.connected_business_related_images(
          this.params.id,
          this.connect_params.type,
          this.connect_params.id
        )
      ).subscribe(x => {}, error => console.log("error: ", error));
      cacheKey =
        this.storageService.cacheKey.MEDIA_IMAGES_RELATED +
        "_" +
        this.business.id +
        "_" +
        this.connect_params.type +
        "_" +
        this.connect_params.id +
        "_" +
        this.params.id;
    } else {
      dataObservable = Observable.fromPromise(
        this.images.related_images(this.params.id)
      ).subscribe(x => {}, error => console.log("error: ", error));
      cacheKey =
        this.storageService.cacheKey.MEDIA_IMAGES_RELATED +
        "_" +
        this.params.id;
    }

    this.cacheService.register(cacheKey, dataObservable).subscribe(cache => {
      this.zone.run(() => {
        this.relatedMediaCache = cache;
        this.relatedMediaArr$ = cache.get$;
      });
    });

    this.relatedMediaArr$.subscribe(
      (data: any) => {
        console.log("related_images: ", data);
        if (data && data.success) {
          this.relatedImagesArr = data.data.images;
        }
        if (!_.isEmpty(this.relatedImagesArr)) {
          this.showRelatedImages = true;
        }
      },
      error => {
        console.log("error: ", error);
        this.showRelatedImages = false;
      }
    );
  }

  ionViewDidLoad() {
    this.initBusiness().then(() => {
      this.initData();
    });
  }

  ionViewWillLeave() {
    if (this.fileTransfer) {
      this.fileTransfer.abort();
    }
  }

  doRefresh(refresher: any) {
    if (this.mediaCache) {
      this.showLoading = true;

      this.zone.run(() => {
        this.initData(refresher);
      });
    } else {
      console.warn("Cache has not been initialized.");
      if (refresher) {
        refresher.cancel();
      }
    }
  }

  presentPopover(ev) {
    const popover = this.popoverCtrl.create(ImageDetailPopoverPage, {
      showDownload: this.business.paid,
      action: this.action
    });
    popover.present({ ev: ev });
  }

  presentActionSheet() {
    let buttons = [];

    if (this.business.role !== 1) {
      const toast = this.toastCtrl.create({
        message: "Invalid Access!!!",
        duration: 3000
      });
      toast.present();
      return;
    }

    if (this.business.type == 1) {
      if (this.business.role == 1) {
        buttons = [
          {
            text: "Edit",
            handler: () => {
              this.updateImageDetail();
            }
          },
          {
            text: "Delete",
            role: "destructive",
            handler: () => {
              console.log("Archive clicked");
              setTimeout(() => {
                this.deleteImage();
              }, 300);
            }
          },
          {
            text: "Add to Album",
            handler: () => {
              setTimeout(() => {
                this.addToAlbumImage();
              }, 300);
            }
          },
          {
            text: "Add to Product Gallery",
            handler: () => {
              setTimeout(() => {
                this.addToProductImage();
              }, 300);
            }
          },
          {
            text: "Set as Product Feature Image",
            handler: () => {
              setTimeout(() => {
                this.addToProductFeature();
              }, 300);
            }
          },
          {
            text: "Set as Business Feature image",
            handler: () => {
              setTimeout(() => {
                this.setImageAsBusinessFeature();
              }, 300);
            }
          },
          {
            text: "Add to Collection's media",
            handler: () => {
              setTimeout(() => {
                this.addImageToCollection();
              }, 300);
            }
          }
        ];
      }
    } else if (this.business.type == 2) {
      if (this.business.role == 1) {
        buttons = [
          {
            text: "Edit",
            handler: () => {
              this.updateImageDetail();
            }
          },
          {
            text: "Add to Album",
            handler: () => {
              setTimeout(() => {
                this.addToAlbumImage();
              }, 300);
            }
          },
          {
            text: "Add to Product Gallery",
            handler: () => {
              setTimeout(() => {
                this.addToProductImage();
              }, 300);
            }
          },
          {
            text: "Set as Product Feature Image",
            handler: () => {
              setTimeout(() => {
                this.addToProductFeature();
              }, 300);
            }
          },
          {
            text: "Set as Business Feature image",
            handler: () => {
              setTimeout(() => {
                this.setImageAsBusinessFeature();
              }, 300);
            }
          },
          {
            text: "Add to Collection's media",
            handler: () => {
              setTimeout(() => {
                this.addImageToCollection();
              }, 300);
            }
          }
        ];
      }
    }

    buttons.push({
      text: "Cancel",
      role: "cancel",
      handler: () => {
        console.log("Cancel clicked");
      }
    });

    const actionSheet = this.actionSheetCtrl.create({
      title: "Select action for image",
      buttons: buttons
    });
    actionSheet.present();
  }

  viewChat() {
    const modal = this.modalCtrl.create(ChatPage);
    modal.present();
  }

  shareNow() {
    if (!this.business.paid) {
      this.alertCtrl
        .create({
          title: "WARNING",
          message: "This feature is only available to premium users only.",
          buttons: ["OK"]
        })
        .present();
      return;
    }

    if (!this.platform.is("cordova")) {
      this.alertCtrl
        .create({
          title: "WARNING",
          message:
            "Social Sharing is only available on actual device/simulator",
          buttons: ["OK"]
        })
        .present();
      return;
    }

    const loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    loading.present();
    this.images.share_url_image(this.item.imageId).then(
      (data: any) => {
        if (data && data.success) {
          console.log("data: ", data);
          this.socialSharing
            .share(
              "This link allows you to download the original version of this image. " +
                "You can send the link to whom ever you need and this will allow them access to the original file.",
              "Share",
              null,
              data.data
            )
            .then(() => {
              // Success!
            })
            .catch(() => {
              // Error!
            });
          loading.dismiss();
        } else {
          loading.dismiss();
        }
      },
      error => {
        console.log("error: ", error);
        loading.dismiss();
      }
    );
  }

  openLink(link) {
    if (link) {
      link = link.startsWith("//") ? "http:" + link : link;
      this.iab.create(link, "_blank");
    } else {
      const alert = this.alertCtrl.create({
        title: "Invalid URL",
        subTitle: "Invalid URL format",
        buttons: ["OK"]
      });
      alert.present();
    }
  }

  viewImageDetail(item: any) {
    console.log("item: ", item);
    let params: any = {
      image: item
    };

    if (this.action == "connect") {
      params.action = "connect";
      params.params = this.connect_params;
    }
    this.navCtrl.push(ImageDetailPage, params);
  }

  private updateImageDetail() {
    if (this.business.role !== 1) {
      let toast = this.toastCtrl.create({
        message: "Invalid Access!!!",
        duration: 3000
      });
      toast.present();
      return;
    }
    this.app.getRootNav().push(ImageDetailInfoPage, {
      image: this.item,
      callback: this.getImageDetails
    });
  }

  private getImageDetails = resp => {
    return new Promise((resolve, reject) => {
      if (resp == "save") {
        this.initData();
        resolve();
      }
    });
  };

  private addToAlbumImage() {
    if (this.business.role !== 1) {
      this.toastCtrl
        .create({
          message: "Invalid Access!!!",
          duration: 3000
        })
        .present();
      return;
    }

    let modal = this.modalCtrl.create(AlbumsModalPage, {
      image: this.item
    });
    modal.present();
  }

  private addToProductImage() {
    if (this.business.role !== 1) {
      this.toastCtrl
        .create({
          message: "Invalid Access!!!",
          duration: 3000
        })
        .present();
      return;
    }

    let modal = this.modalCtrl.create(ProductsModalPage, {
      image: this.item,
      action: "add_image"
    });
    modal.present();
  }

  private addToProductFeature() {
    if (this.business.role !== 1) {
      this.toastCtrl
        .create({
          message: "Invalid Access!!!",
          duration: 3000
        })
        .present();
      return;
    }

    let modal = this.modalCtrl.create(ProductsModalPage, {
      image: this.item,
      action: "feature",
      type: "image"
    });
    modal.present();
  }

  private setImageAsBusinessFeature() {
    let loading = this.loadingCtrl.create({
      dismissOnPageChange: true,
      content: "Setting..."
    });
    loading.present();
    this.images.image_feature({ id: this.item.imageId }).then(
      (data: any) => {
        if (data && data.success) {
          let alert = this.alertCtrl.create({
            title: "SUCCESS",
            message: data.data,
            buttons: ["OK"]
          });
          alert.present();
        }
        loading.dismiss();
      },
      (error: any) => {
        loading.dismiss();
        console.log("error: ", error);
      }
    );
  }

  private addImageToCollection() {
    if (this.business.role !== 1) {
      this.toastCtrl
        .create({
          message: "Invalid Access!!!",
          duration: 3000
        })
        .present();
      return;
    }

    let modal = this.modalCtrl.create(CollectionsModalPage, {
      type: "image",
      params: this.item
    });
    modal.present();
  }

  private deleteImage() {
    if (this.business.role !== 1) {
      this.toastCtrl
        .create({
          message: "Invalid Access!!!",
          duration: 3000
        })
        .present();
      return;
    }

    let loading = this.loadingCtrl.create({
      dismissOnPageChange: true,
      content: "Deleting..."
    });
    loading.present();
    this.images.image_delete([this.item.id]).then(
      (data: any) => {
        if (data && data.success) {
          console.log("image_delete: ", data);
          let alert = this.alertCtrl.create({
            title: "SUCCESS",
            message: "Image successfully deleted!",
            buttons: ["OK"]
          });
          alert.onDidDismiss(() => {
            this.callback("refresh").then(() => {
              this.navCtrl.pop();
            });
          });
          alert.present();
        }
        loading.dismiss();
      },
      (error: any) => {
        loading.dismiss();
        console.log("error: ", error);
      }
    );
  }

  private downloadImage() {
    let ctrl = this;
    let perc: any;

    if (!this.business.paid) {
      this.alertCtrl
        .create({
          title: "WARNING",
          message: "This feature is only available to premium users only.",
          buttons: ["OK"]
        })
        .present();
      return;
    }

    const loading = ctrl.loadingCtrl.create({
      content: "Downloading ...",
      dismissOnPageChange: true
    });
    loading.present();

    const alert = ctrl.alertCtrl.create({
      title: "Success",
      subTitle: "Image successfully downloaded",
      buttons: ["OK"]
    });

    const download = (dataurl, filename) => {
      var a = document.createElement("a");
      a.href = dataurl;
      a.setAttribute("download", filename);
      var b = document.createEvent("MouseEvents");
      b.initEvent("click", false, true);
      a.dispatchEvent(b);
      return false;
    };

    const downloadNative = (image, filename) => {
      ctrl.platform.ready().then(() => {
        ctrl.fileTransfer = ctrl.transfer.create();
        const newDir = ctrl.storageDirectory + "/" + filename;
        console.log("newDir: ", newDir);

        ctrl.fileTransfer
          .download(image, newDir)
          .then(
            entry => {
              console.log("entry: ", entry);
              console.log("entry.toURL(); ", entry.toURL());

              if (entry) {
                const alertSuccess = ctrl.alertCtrl.create({
                  title: `Download Succeeded!`,
                  subTitle: "Download successfully",
                  buttons: ["Ok"]
                });

                loading.dismiss();
                alertSuccess.present();
              }
            },
            error => {
              console.log("fileTransfer error: ", error);
              ctrl.fileTransfer.abort();
              const alertFailure = ctrl.alertCtrl.create({
                title: `Download Failed!`,
                subTitle: `${image} was not successfully downloaded. Error code: ${
                  error.code
                }`,
                buttons: ["Ok"]
              });
              loading.dismiss();
              alertFailure.present();
            }
          )
          .catch(error => {
            ctrl.fileTransfer.abort();
            loading.dismiss();
          });

        ctrl.fileTransfer.onProgress(progressEvent => {
          if (progressEvent.lengthComputable) {
            perc = Math.floor(
              (progressEvent.loaded / progressEvent.total) * 100
            );
            console.log("Progress: ", perc + "% loaded...");

            ctrl.zone.run(() => {
              loading.setContent("Downloading " + perc + "% ...");

              if (ctrl.platform.is("android")) {
                ctrl.localNotifications.schedule({
                  title: "Downloading " + filename,
                  text: "Downloading " + perc + "% ...",
                  sound: null,
                  progressBar: { value: perc }
                });
              }
            });
          }
        });
      });
    };

    const getFilename = url => {
      url = url
        .split("/")
        .pop()
        .replace(/\#(.*?)$/, "")
        .replace(/\?(.*?)$/, "");
      url = url.split("."); // separates filename and extension
      return { filename: url[0] || "", ext: url[1] || "" };
    };

    this.images.image_download(this.item.id).then(
      (data: any) => {
        if (data && data.success) {
          let filename =
            getFilename(data.data).filename + "." + getFilename(data.data).ext;
          if (!this.platform.is("cordova")) {
            download(data.data, filename);
            loading.dismiss();
            alert.present();
          } else {
            downloadNative(data.data, filename);
          }
        }
      },
      error => {
        loading.dismiss();
        if (error && !error.success) {
          this.alertCtrl
            .create({
              title: "WARNING",
              message: "Error while downloading file. " + error.message,
              buttons: ["OK"]
            })
            .present();
          return;
        }
      }
    );
  }
}
