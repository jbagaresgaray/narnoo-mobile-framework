import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BusinessProfileSettingsPage } from './business-profile-settings';
import { IonTagsInputModule } from "ionic-tags-input";
import { ComponentsModule } from '../../components/components.module';

import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    BusinessProfileSettingsPage,
  ],
  imports: [
    IonicPageModule.forChild(BusinessProfileSettingsPage),
    IonTagsInputModule,
    ComponentsModule,
    PipesModule
  ],
})
export class BusinessProfileSettingsPageModule {}
