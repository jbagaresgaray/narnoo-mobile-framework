import { Component, ViewEncapsulation, NgZone } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  AlertController,
  ToastController
} from "ionic-angular";
import * as async from "async";
import * as _ from "lodash";
import { CacheService, Cache } from "ionic-cache-observable";
import { Observable, Subscription } from "rxjs";
import "rxjs/add/operator/finally";

import { BusinessServicesProvider } from "../../providers/services/business";
import { ConnectServicesProvider } from "../../providers/services/connect";

import { BusinessProfileEntryPage } from "../../pages/business-profile-entry/business-profile-entry";
import { FollowersFollowingPage } from "../../pages/followers-following/followers-following";
import { StorageProvider } from "../../providers/storage/storage";
import { LoadingProvider } from "../../providers/loading/loading";

@IonicPage()
@Component({
  selector: "page-business-profile-settings",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "business-profile-settings.html"
})
export class BusinessProfileSettingsPage {
  tags: any[] = ["Ionic", "Angular", "TypeScript"];
  imageAction: any = "info";

  info: any = {};
  business: any = {};
  imageUrl: string = "assets/img/background.jpg";
  showLoading: boolean = true;
  showFollowers: boolean = false;
  showFollowing: boolean = false;

  followerCount: number = 0;
  followingCount: number = 0;
  followingArr: any[] = [];

  businessArr$: Observable<any>;
  private businessCache: Cache<any>;
  public refreshSubscription: Subscription;

  followersArr$: Observable<any>;
  private followersCache: Cache<any>;

  followingArr$: Observable<any>;
  private followingCache: Cache<any>;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public zone: NgZone,
    public businessService: BusinessServicesProvider,
    public connect: ConnectServicesProvider,
    public storageService: StorageProvider,
    public cacheService: CacheService,
    public loader: LoadingProvider
  ) {
    this.imageUrl = "assets/img/background.jpg";
  }

  private async initBusiness() {
    const business = await this.storageService.getStorage(
      this.storageService.storageKey.CURRENT_BUSINESS
    );
    this.business = !_.isEmpty(business) ? JSON.parse(business) || {} : {};
  }

  private initData(refresher?: any) {
    this.showLoading = true;
    this.info = {};

    const dataObservable = Observable.fromPromise(
      this.businessService.business_profile()
    );
    const cacheKey =
      this.storageService.cacheKey.BUSINESS_PROFILE + "_" + this.business.id;
    this.cacheService.register(cacheKey, dataObservable).subscribe(cache => {
      this.businessCache = cache;
      this.zone.run(() => {
        this.businessArr$ = cache.get$;

        this.businessCache.refresh().subscribe(
          () => {
            this.loader.start();
          },
          () => {
            this.loader.complete();
          },
          () => {
            this.loader.complete();
          }
        );
      });
    });

    const successResponse = (data: any) => {
      if (data && data.success) {
        this.zone.run(() => {
          const info = data.data;
          const keywords = info.keywords;
          if (keywords) {
            info.tags = keywords.split(",");
          } else {
            info.tags = null;
          }
          this.info = info;
        });
        console.log("this.info : ", this.info);
      }
      this.showLoading = false;
      if (refresher) {
        refresher.complete();
      }
    };

    const errorResponse = error => {
      console.log("error: ", error);
      this.showLoading = false;
      if (refresher) {
        refresher.complete();
      }
    };

    this.businessArr$.subscribe(successResponse, errorResponse);
  }

  private initBusinessConnection() {
    this.showFollowers = false;
    this.showFollowing = false;

    const dataObservable1 = Observable.fromPromise(
      this.connect.connect_followers()
    );
    const cacheKey1 =
      this.storageService.cacheKey.BUSINESS_PROFILE_FOLLOWERS +
      "_" +
      this.business.id;
    this.cacheService.register(cacheKey1, dataObservable1).subscribe(cache => {
      this.followersCache = cache;
      this.connect.connect_followersCache = cache;
      this.zone.run(() => {
        this.followersArr$ = cache.get$;
        this.connect.connect_followers$ = cache.get$;
      });
    });

    this.followersArr$.subscribe(
      (data: any) => {
        if (data && data.success) {
          let result = data.data.data;
          this.followerCount = _.size(result);
        }
        this.showFollowers = true;
      },
      error => {
        console.log("error: ", error);
        this.showFollowers = true;
      }
    );

    const dataObservable2 = Observable.fromPromise(
      this.connect.connect_following()
    );
    const cacheKey2 =
      this.storageService.cacheKey.BUSINESS_PROFILE_FOLLOWING +
      "_" +
      this.business.id;
    this.cacheService.register(cacheKey2, dataObservable2).subscribe(cache => {
      this.followingCache = cache;
      this.connect.connect_followingCache = cache;
      this.zone.run(() => {
        this.followingArr$ = cache.get$;
        this.connect.connect_following$ = cache.get$;
      });
    });

    this.followingArr$.subscribe(
      (data: any) => {
        if (data && data.success) {
          let result = data.data.data;
          this.followingCount = _.size(result);
          this.followingArr = result;
        }
        this.showFollowing = true;
      },
      error => {
        console.log("error: ", error);
        this.showFollowing = true;
      }
    );
  }

  ionViewDidLoad() {
    this.initBusiness().then(() => {
      this.initData();
      this.initBusinessConnection();
    });
  }

  doRefresh(refresher: any) {
    if (this.businessCache) {
      this.showLoading = true;
      this.info = {};
      this.zone.run(() => {
        this.initData(refresher);
      });
    } else {
      console.warn("Cache has not been initialized.");
      if (refresher) {
        refresher.cancel();
      }
    }
  }

  updateProfile() {
    if (this.showLoading) {
      return;
    }

    if (this.business.role !== 1) {
      this.toastCtrl
        .create({
          message: "Invalid Access!!!",
          duration: 3000
        })
        .present();
      return;
    }

    this.navCtrl.push(BusinessProfileEntryPage, {
      profile: _.cloneDeep(this.info)
    });
  }

  viewFollowers() {
    if (this.showLoading) {
      return;
    }

    this.navCtrl.push(FollowersFollowingPage, {
      mode: "followers",
      title: "Followers",
      following: this.followingArr
    });
  }

  viewFollowing() {
    if (this.showLoading) {
      return;
    }

    this.navCtrl.push(FollowersFollowingPage, {
      mode: "following",
      title: "Following"
    });
  }
}
