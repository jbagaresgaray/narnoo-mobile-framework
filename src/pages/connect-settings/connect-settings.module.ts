import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConnectSettingsPage } from './connect-settings';

@NgModule({
  declarations: [
    ConnectSettingsPage,
  ],
  imports: [
    IonicPageModule.forChild(ConnectSettingsPage),
  ],
})
export class ConnectSettingsPageModule {}
