import { Component, ViewEncapsulation } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  AlertController,
  ToastController
} from "ionic-angular";
import * as _ from "lodash";

import { ConnectServicesProvider } from "../../providers/services/connect";

@IonicPage()
@Component({
  selector: "page-connect-settings",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "connect-settings.html"
})
export class ConnectSettingsPage {
  connect_params: any = {};
  action: string;
  business: any = {};

  connectSettings: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public connects: ConnectServicesProvider
  ) {
    this.connect_params = navParams.get("params");
    this.action = navParams.get("action");

    if (this.connect_params.details) {
      this.business = this.connect_params.details.business;
    } else {
      this.business = this.connect_params.name;
    }
    console.log("this.connect_params: ", this.connect_params);
    console.log("this.business: ", this.business);

    this.connectSettings = "";
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad ConnectSettingsPage");
  }

  saveEntry() {
    console.log("this.connectSettings: ", this.connectSettings);
    if (this.business.role !== 1) {
      this.toastCtrl
        .create({
          message: "Invalid Access!!!",
          duration: 3000
        })
        .present();
      return;
    }

    if (_.isEmpty(this.connectSettings)) {
      this.alertCtrl
        .create({
          message: "Please select a role!",
          buttons: ["OK"]
        })
        .present();
      return;
    }

    const loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    loading.present();
    this.connects
      .connect_edit({
        type: this.connect_params.details.type,
        id: this.connect_params.details.id,
        role: this.connectSettings
      })
      .then(
        (data: any) => {
          if (data && data.success) {
            const alert = this.alertCtrl.create({
              title: "SUCCESS",
              message: data.message,
              buttons: ["OK"]
            });
            alert.onDidDismiss(() => {
              this.navCtrl.pop();
            });
            alert.present();
            return;
          } else if (data && !data.success) {
            this.alertCtrl
              .create({
                title: "WARNING",
                message: data.message,
                buttons: ["OK"]
              })
              .present();
            return;
          }
          loading.dismiss();
        },
        error => {
          console.log("connectSettings Error: ", error);
        }
      );
  }
}
