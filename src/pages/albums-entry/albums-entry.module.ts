import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { ReactiveFormsModule } from "@angular/forms";

import { AlbumsEntryPage } from "./albums-entry";

@NgModule({
  declarations: [AlbumsEntryPage],
  imports: [ReactiveFormsModule, IonicPageModule.forChild(AlbumsEntryPage)]
})
export class AlbumsEntryPageModule {}
