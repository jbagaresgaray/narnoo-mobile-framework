import { Component, ViewEncapsulation, NgZone } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ViewController,
  LoadingController,
  AlertController
} from "ionic-angular";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from "@angular/forms";
import * as _ from "lodash";

import { AlbumsServicesProvider } from "../../providers/services/albums";
import { StorageProvider } from "../../providers/storage/storage";

@IonicPage()
@Component({
  selector: "page-albums-entry",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "albums-entry.html"
})
export class AlbumsEntryPage {
  business: any = {};
  album: any = {};
  albumsForm: FormGroup;

  isSaving = false;
  submitted = false;

  constructor(
    public zone: NgZone,
    private formBuilder: FormBuilder,
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public albums: AlbumsServicesProvider,
    public storageService: StorageProvider
  ) {
    this.albumsForm = this.formBuilder.group({
      title: ["", Validators.required]
    });
  }

  get f(): any {
    return this.albumsForm.controls;
  }

  private validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  async ionViewDidLoad() {
    console.log("ionViewDidLoad AlbumsEntryPage");
    const business = await this.storageService.getStorage(
      this.storageService.storageKey.CURRENT_BUSINESS
    );
    this.business = !_.isEmpty(business) ? JSON.parse(business) || {} : {};
  }

  close() {
    this.viewCtrl.dismiss();
  }

  saveEntry() {
    this.submitted = true;
    this.isSaving = true;

    if (this.albumsForm.invalid) {
      this.isSaving = false;
      this.validateAllFormFields(this.albumsForm);
      return;
    }

    const loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    loading.present();
    this.albums
      .album_create(this.albumsForm.value)
      .then((data: any) => {
        if (data && data.success) {
          const alert = this.alertCtrl.create({
            title: "Success",
            message: data.message || "Album successfully created!",
            buttons: ["OK"]
          });
          alert.onDidDismiss(() => {
            this.viewCtrl.dismiss("save");
          });
          alert.present();
        } else {
          const alert = this.alertCtrl.create({
            title: "Warning",
            message: data.message,
            buttons: ["OK"]
          });
          alert.present();
        }
        loading.dismiss();
        this.submitted = false;
        this.isSaving = false;
      })
      .catch((error: any) => {
        console.log("error: ", error);
        loading.dismiss();
        this.submitted = false;
        this.isSaving = false;

        if (error && !error.success) {
          const alert = this.alertCtrl.create({
            title: "Error",
            message: error.message,
            buttons: ["OK"]
          });
          alert.present();
        }
      });
  }
}
