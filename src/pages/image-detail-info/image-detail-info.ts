import { Component, ViewEncapsulation, NgZone } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  AlertController,
  ToastController
} from "ionic-angular";
import * as async from "async";
import * as _ from "lodash";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from "@angular/forms";

import { ImagesServicesProvider } from "../../providers/services/images";
import { StorageProvider } from "../../providers/storage/storage";

@IonicPage()
@Component({
  selector: "page-image-detail-info",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "image-detail-info.html"
})
export class ImageDetailInfoPage {
  image: any = {};
  item: any = {};
  business: any = {};
  tags: any[] = ["Ionic", "Angular", "TypeScript"];
  callback: any;

  isSaving = false;
  submitted = false;
  disabledMarket: boolean = false;

  imageForm: FormGroup;

  constructor(
    public zone: NgZone,
    private formBuilder: FormBuilder,
    public navCtrl: NavController,
    public navParams: NavParams,
    public images: ImagesServicesProvider,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public storageService: StorageProvider
  ) {
    this.callback = navParams.get("callback");
    this.item = navParams.get("image");
    this.image.markets = {};

    if (this.business.role == 3 && this.business.role == 4) {
      this.disabledMarket = true;
    }

    this.imageForm = this.formBuilder.group({
      imageId: [""],
      caption: ["", Validators.required],
      privilege: ["", Validators.required]
    });
  }

  get f(): any {
    return this.imageForm.controls;
  }

  private validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  private initData() {
    const loading = this.loadingCtrl.create();
    loading.present();
    this.images
      .image_detail(this.item.id)
      .then(
        (data: any) => {
          if (data && data.success) {
            this.zone.run(() => {
              this.image = data.data;
              this.image.imageId = data.data.id;

              this.imageForm.patchValue(this.image);

              if (this.image.markets) {
                this.image.markets.australia =
                  this.image.markets.australia == 1 ? true : false;
                this.image.markets.france =
                  this.image.markets.france == 1 ? true : false;
                this.image.markets.germany =
                  this.image.markets.germany == 1 ? true : false;
                this.image.markets.greaterChina =
                  this.image.markets.greaterChina == 1 ? true : false;
                this.image.markets.india =
                  this.image.markets.india == 1 ? true : false;
                this.image.markets.japan =
                  this.image.markets.japan == 1 ? true : false;
                this.image.markets.korea =
                  this.image.markets.korea == 1 ? true : false;
                this.image.markets.newZealand =
                  this.image.markets.newZealand == 1 ? true : false;
                this.image.markets.northAmerica =
                  this.image.markets.northAmerica == 1 ? true : false;
                this.image.markets.singapore =
                  this.image.markets.singapore == 1 ? true : false;
                this.image.markets.unitedKindom =
                  this.image.markets.unitedKindom == 1 ? true : false;
              } else {
                this.image.markets = {};
              }

              if (this.image) {
                this.imageForm.patchValue({
                  imageId: this.image.imageId
                });
              }
            });
          }
          console.log("this.image: ", this.image);
          loading.dismiss();
        },
        error => {
          loading.dismiss();
        }
      )
      .catch(error => {
        console.log("catch: ", error);
        loading.dismiss();
      });
  }

  async ionViewDidLoad() {
    const business = await this.storageService.getStorage(
      this.storageService.storageKey.CURRENT_BUSINESS
    );
    this.business = !_.isEmpty(business) ? JSON.parse(business) || {} : {};

    this.initData();
  }

  save() {
    console.log("this.image: ", this.imageForm.value);

    this.submitted = true;
    this.isSaving = true;

    if (this.imageForm.invalid) {
      this.isSaving = false;
      this.validateAllFormFields(this.imageForm);
      return;
    }

    const updateImage = () => {
      const loading = this.loadingCtrl.create({
        content: "Saving...",
        dismissOnPageChange: true
      });
      loading.present();

      async.waterfall(
        [
          callback => {
            if (this.disabledMarket) {
              const toast = this.toastCtrl.create({
                message: "Invalid Access to Image Market!",
                position: "top",
                duration: 2000
              });
              toast.present();
              callback();
            } else {
              this.image.markets.imageId = this.image.imageId;
              this.images
                .image_market_edit(this.image.markets)
                .then(
                  (data: any) => {
                    if (data && data.success) {
                      const toast = this.toastCtrl.create({
                        message: "Success! " + data.data,
                        position: "top",
                        duration: 2000
                      });
                      toast.present();
                      callback();
                    } else {
                      loading.dismiss();
                      const alert = this.alertCtrl.create({
                        title: "Error!",
                        subTitle: data.message,
                        buttons: ["OK"]
                      });
                      alert.onDidDismiss(() => {
                        return;
                      });
                      alert.present();
                    }
                  },
                  error => {
                    loading.dismiss();
                    callback();
                  }
                );
            }
          },
          callback => {
            this.images.image_edit(this.imageForm.value).then(
              (data: any) => {
                if (data && data.success) {
                  loading.dismiss();
                  const alert = this.alertCtrl.create({
                    title: "Success!",
                    subTitle: data.data,
                    buttons: ["OK"]
                  });
                  alert.onDidDismiss(() => {
                    callback();
                  });
                  alert.present();
                } else {
                  loading.dismiss();
                  const alert = this.alertCtrl.create({
                    title: "Error!",
                    subTitle: data.message,
                    buttons: ["OK"]
                  });
                  alert.onDidDismiss(() => {
                    return;
                  });
                  alert.present();
                }
              },
              error => {
                loading.dismiss();
                callback();
              }
            );
          }
        ],
        () => {
          this.submitted = false;
          this.isSaving = false;
          this.callback("save").then(() => {
            this.navCtrl.pop();
          });
        }
      );
    };

    const confirm = this.alertCtrl.create({
      title: "Save Entry?",
      message: "Do you want to save the entry?",
      buttons: [
        {
          text: "Cancel",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Confirm",
          handler: () => {
            console.log("Agree clicked");
            updateImage();
          }
        }
      ]
    });
    confirm.present();
  }
}
