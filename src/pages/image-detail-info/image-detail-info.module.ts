import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReactiveFormsModule } from "@angular/forms";
import { IonTagsInputModule } from "ionic-tags-input";

import { ImageDetailInfoPage } from './image-detail-info';


@NgModule({
  declarations: [
    ImageDetailInfoPage,
  ],
  imports: [
    IonicPageModule.forChild(ImageDetailInfoPage),
    IonTagsInputModule,
    ReactiveFormsModule
  ],
})
export class ImageDetailInfoPageModule {}
