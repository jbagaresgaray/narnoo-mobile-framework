import { Component, ViewEncapsulation, NgZone } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  AlertController,
  ModalController,
  ToastController
} from "ionic-angular";
import * as md5 from "crypto-md5";
import * as _ from "lodash";
import { CacheService, Cache } from "ionic-cache-observable";
import { Observable, Subscription } from "rxjs";
import "rxjs/add/operator/finally";

import { BusinessUserDetailPage } from "../../pages/business-user-detail/business-user-detail";
import { BusinessUserEntryPage } from "../../pages/business-user-entry/business-user-entry";

import { UserServicesProvider } from "../../providers/services/users";
import { UtilitiesServicesProvider } from "../../providers/services/utilities";
import { StorageProvider } from "../../providers/storage/storage";
import { LoadingProvider } from "../../providers/loading/loading";

@IonicPage()
@Component({
  selector: "page-business-users",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "business-users.html"
})
export class BusinessUsersPage {
  contactsArr: any[] = [];
  contactsArrCopy: any[] = [];
  fakeArr: any[] = [];

  showContent: boolean = false;
  business: any = {};

  usersArr$: Observable<any>;
  private usersCache: Cache<any>;
  public refreshSubscription: Subscription;

  constructor(
    public zone: NgZone,
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController,
    public users: UserServicesProvider,
    public utilities: UtilitiesServicesProvider,
    public storageService: StorageProvider,
    public cacheService: CacheService,
    public loader: LoadingProvider
  ) {
    for (var i = 0; i < 12; ++i) {
      this.fakeArr.push(i);
    }
  }

  private async initBusiness() {
    const business = await this.storageService.getStorage(
      this.storageService.storageKey.CURRENT_BUSINESS
    );
    this.business = !_.isEmpty(business) ? JSON.parse(business) || {} : {};
  }

  private initializeData(refresher?: any) {
    this.showContent = false;

    const dataObservable = Observable.fromPromise(this.users.user_list());
    const cacheKey =
      this.storageService.cacheKey.USERS_LIST + "_" + this.business.id;
    this.cacheService.register(cacheKey, dataObservable).subscribe(cache => {
      this.usersCache = cache;
      this.zone.run(() => {
        this.usersArr$ = cache.get$;

        this.usersCache.refresh().subscribe(
          () => {
            this.loader.start();
          },
          () => {
            this.loader.complete();
          },
          () => {
            this.loader.complete();
          }
        );
      });
    });

    this.usersArr$.subscribe(
      (data: any) => {
        if (data && data.success) {
          _.each(data.data, (row: any) => {
            row.gravatar =
              "https://www.gravatar.com/avatar/" +
              md5(row.email.toLowerCase(), "hex");
          });

          this.zone.run(() => {
            this.contactsArr = data.data;
            this.contactsArrCopy = _.cloneDeep(data.data);
          });
          this.showContent = true;
        }
        if (refresher) {
          refresher.complete();
        }
      },
      () => {
        this.showContent = true;
        if (refresher) {
          refresher.complete();
        }
      }
    );
  }

  ionViewDidLoad() {
    this.initBusiness().then(() => {
      this.initializeData();
    });
  }

  getItems(ev: any) {
    this.showContent = false;
    const val = ev.target.value;
    if (val && val.trim() != "") {
      this.contactsArr = this.contactsArrCopy.filter((item: any) => {
        return item.firstName.toLowerCase().indexOf(val.toLowerCase()) > -1;
      });
      this.showContent = true;
    } else {
      this.contactsArr = this.contactsArrCopy;
    }
  }

  onCancel(ev) {
    this.contactsArr = this.contactsArrCopy;
  }

  doRefresh(refresher: any) {
    if (this.usersCache) {
      this.showContent = false;
      this.contactsArr = [];
      this.contactsArrCopy = [];

      this.zone.run(() => {
        this.initializeData(refresher);
      });
    } else {
      console.warn("Cache has not been initialized.");
      if (refresher) {
        refresher.cancel();
      }
    }
  }

  errorAvatarHandler(event) {
    event.target.src = this.utilities.radomizeAvatar();
  }

  revokeUser(item, slidingItem) {
    if (this.business.role !== 1) {
      this.toastCtrl
        .create({
          message: "Invalid Access!!!",
          duration: 3000
        })
        .present();
      return;
    }

    slidingItem.close();
    const confirm = this.alertCtrl.create({
      title: "Revoke user?",
      message: "Are you sure to revoke this User?",
      buttons: [
        {
          text: "No",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Yes",
          handler: () => {
            this.users.revoke_access(item).then((data: any) => {
              if (data && data.success) {
                const alert = this.alertCtrl.create({
                  title: "Success",
                  subTitle: data.data,
                  buttons: ["OK"]
                });
                alert.present();
              } else {
                const alert = this.alertCtrl.create({
                  title: "Warning",
                  subTitle: data.message,
                  buttons: ["OK"]
                });
                alert.present();
              }
            });
          }
        }
      ]
    });
    confirm.present();
  }

  deleteUser(item, slidingItem) {
    if (this.business.role !== 1) {
      this.toastCtrl
        .create({
          message: "Invalid Access!!!",
          duration: 3000
        })
        .present();
      return;
    }

    slidingItem.close();
    const confirm = this.alertCtrl.create({
      title: "Delete user?",
      message: "Are you sure to delete this User?",
      buttons: [
        {
          text: "No",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Yes",
          handler: () => {
            /*this.users.revoke_access(item).then((data: any)=>{
							if(data && data.success){
								const alert = this.alertCtrl.create({
									title: 'Success',
									subTitle: data.data,
									buttons:['OK']
								});
								alert.present();
							}else{
								const alert = this.alertCtrl.create({
									title: 'Warning',
									subTitle: data.message,
									buttons:['OK']
								});
								alert.present();
							}
						});*/
          }
        }
      ]
    });
    confirm.present();
  }

  createUser() {
    if (this.business.role !== 1) {
      this.toastCtrl
        .create({
          message: "Invalid Access!!!",
          duration: 3000
        })
        .present();
      return;
    }

    const profileModal = this.modalCtrl.create(BusinessUserEntryPage, {
      action: "create"
    });
    profileModal.onDidDismiss(resp => {
      if (resp == "save") {
        this.initializeData();
      }
    });
    profileModal.present();
  }

  updateUser(item, slidingItem) {
    if (this.business.role !== 1) {
      this.toastCtrl
        .create({
          message: "Invalid Access!!!",
          duration: 3000
        })
        .present();
      return;
    }

    slidingItem.close();
    const profileModal = this.modalCtrl.create(BusinessUserEntryPage, {
      action: "update",
      staff: item
    });
    profileModal.onDidDismiss(resp => {
      if (resp == "save") {
        this.initializeData();
      }
    });
    profileModal.present();
  }

  gotoDetail(item) {
    this.navCtrl.push(BusinessUserDetailPage, { item: item });
  }
}
