import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { BusinessUsersPage } from "./business-users";

import { ComponentsModule } from "../../components/components.module";
import { IonicImageLoader } from "ionic-image-loader";

@NgModule({
  declarations: [BusinessUsersPage],
  imports: [
    ComponentsModule,
    IonicImageLoader,
    IonicPageModule.forChild(BusinessUsersPage)
  ]
})
export class BusinessUsersPageModule {}
