import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VideoDetailPage, VideoDetailPopoverPage } from './video-detail';

import { PipesModule } from '../../pipes/pipes.module';
import { ComponentsModule } from '../../components/components.module';
import { IonicImageLoader } from "ionic-image-loader";

@NgModule({
	declarations: [
		VideoDetailPage,
		VideoDetailPopoverPage
	],
	imports: [
		IonicPageModule.forChild(VideoDetailPage),
		IonicPageModule.forChild(VideoDetailPopoverPage),
		PipesModule,
    ComponentsModule,
    IonicImageLoader
	],
})
export class VideoDetailPageModule { }
