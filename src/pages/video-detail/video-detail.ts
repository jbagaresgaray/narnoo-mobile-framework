import { Component, ViewEncapsulation, NgZone, OnInit } from "@angular/core";
import {
  Platform,
  IonicPage,
  NavController,
  NavParams,
  ModalController,
  ActionSheetController,
  App,
  LoadingController,
  AlertController,
  ToastController,
  ViewController,
  Events,
  PopoverController
} from "ionic-angular";
import * as _ from "lodash";
import * as $ from "jquery";
import { CacheService, Cache } from "ionic-cache-observable";
import { Observable, Subscription } from "rxjs";
import "rxjs/add/operator/finally";

import { VideosServicesProvider } from "../../providers/services/videos";

import { VideosPlayerPage } from "../../pages/videos-player/videos-player";
import { ProductsModalPage } from "../../pages/products-modal/products-modal";
import { CollectionsModalPage } from "../../pages/collections-modal/collections-modal";
import { VideoDetailInfoPage } from "../../pages/video-detail-info/video-detail-info";
import { ChatPage } from "../../pages/chat/chat";
import { FileUploadPage } from "../../pages/file-upload/file-upload";

import { InAppBrowser } from "@ionic-native/in-app-browser";
import { SocialSharing } from "@ionic-native/social-sharing";
import { FileTransfer, FileTransferObject } from "@ionic-native/file-transfer";
import { LocalNotifications } from "@ionic-native/local-notifications";

import { environment } from "../../environments/environment";
import { StorageProvider } from "../../providers/storage/storage";
import { LoadingProvider } from "../../providers/loading/loading";

declare const safari: any;

@Component({
  template: `
    <ion-list>
      <button ion-item (click)="downloadImage()">
        <ion-icon
          ios="ios-download-outline"
          md="ios-download-outline"
          item-start
        ></ion-icon>
        Download
      </button>
      <button
        ion-item
        (click)="shareNow()"
        *ngIf="
          action != 'connect' &&
          action != 'notif' &&
          (business.role == 1 || business.role == 2)
        "
      >
        <ion-icon name="md-share" item-start></ion-icon>
        Share
      </button>
    </ion-list>
  `
})
export class VideoDetailPopoverPage implements OnInit {
  showDownload: boolean;
  action: any;
  business: any = {};

  constructor(
    public params: NavParams,
    public viewCtrl: ViewController,
    public events: Events,
    public storageService: StorageProvider
  ) {
    this.showDownload = params.get("showDownload");
    this.action = params.get("action");
  }

  async ngOnInit() {
    const business = await this.storageService.getStorage(
      this.storageService.storageKey.CURRENT_BUSINESS
    );
    this.business = !_.isEmpty(business) ? JSON.parse(business) || {} : {};
  }

  close() {
    this.viewCtrl.dismiss();
  }

  downloadImage() {
    this.events.publish("VideoDetailPage:downloadImage");
    this.viewCtrl.dismiss();
  }

  viewChat() {
    this.events.publish("VideoDetailPage:viewChat");
    this.viewCtrl.dismiss();
  }

  shareNow() {
    this.events.publish("VideoDetailPage:shareNow");
    this.viewCtrl.dismiss();
  }
}
@IonicPage()
@Component({
  selector: "page-video-detail",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "video-detail.html"
})
export class VideoDetailPage {
  item: any = {};
  params: any = {};
  business: any = {};
  notif_business: any = {};
  isSafari: boolean = false;
  showLoading: boolean = true;
  showError: boolean = false;

  action: string;
  connect_params: any = {};
  callback: any;
  errContent: string = "";

  storageDirectory: string = "";
  fileTransfer: FileTransferObject;

  mediaArr$: Observable<any>;
  private mediaCache: Cache<any>;
  public refreshSubscription: Subscription;

  constructor(
    public platform: Platform,
    public app: App,
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public actionSheetCtrl: ActionSheetController,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public events: Events,
    public popoverCtrl: PopoverController,
    public zone: NgZone,
    public videos: VideosServicesProvider,
    private iab: InAppBrowser,
    private socialSharing: SocialSharing,
    private transfer: FileTransfer,
    private localNotifications: LocalNotifications,
    public storageService: StorageProvider,
    public cacheService: CacheService,
    public loader: LoadingProvider
  ) {
    this.isSafari =
      /constructor/i.test(window["HTMLElement"]) ||
      (function(p) {
        return p.toString() === "[object SafariRemoteNotification]";
      })(!window["safari"] || safari.pushNotification);

    this.action = navParams.get("action");
    this.connect_params = navParams.get("params");
    this.params = navParams.get("video");
    this.notif_business = navParams.get("business");
    this.callback = navParams.get("callback");

    this.item = {};

    if (platform.is("cordova")) {
      if (this.platform.is("ios")) {
        this.storageDirectory =
          window["cordova"].file.documentsDirectory + environment.AlbumName;
      } else if (this.platform.is("android")) {
        this.storageDirectory =
          window["cordova"].file.externalRootDirectory + environment.AlbumName;
      }
    }

    // ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //
    // ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //
    // ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //

    events.unsubscribe("VideoDetailPage:viewChat");
    events.unsubscribe("VideoDetailPage:shareNow");
    events.unsubscribe("VideoDetailPage:downloadImage");

    events.subscribe("VideoDetailPage:downloadImage", () => {
      this.downloadImage();
    });

    events.subscribe("VideoDetailPage:viewChat", () => {
      this.viewChat();
    });

    events.subscribe("VideoDetailPage:shareNow", () => {
      this.shareNow();
    });
  }

  private initData(refresher?: any) {
    const successResponse = (data: any) => {
      if (data && data.success) {
        const item = data.data;

        this.zone.run(() => {
          this.item = item;
          if (_.isEmpty(this.item.caption)) {
            this.item.caption = "No caption available";
          }
          console.log("video: ", this.item);

          if (this.platform.is("cordova")) {
            if (this.platform.is("ios")) {
              if (this.item.uploadedAt) {
                this.item.uploadedAt = new Date(
                  this.item.uploadedAt.replace(/-/g, "/")
                );
              }
            }
          } else {
            if (this.platform.is("ios")) {
              if (this.isSafari) {
                if (this.item.uploadedAt) {
                  this.item.uploadedAt = new Date(
                    this.item.uploadedAt.replace(/-/g, "/")
                  );
                }
              } else {
                if (this.item.uploadedAt) {
                  this.item.uploadedAt = new Date(this.item.uploadedAt);
                }
              }
            }
          }
        });
      }
      this.showLoading = false;
      this.showError = false;
      if (refresher) {
        refresher.complete();
      }
    };

    const errorResponse = error => {
      this.showLoading = false;
      this.showError = true;
      if (refresher) {
        refresher.complete();
      }
      if (error) {
        this.errContent = error.message || "No video media found!";
      }
    };

    let dataObservable: any;
    let cacheKey: any;
    this.showLoading = true;

    if (this.action == "connect") {
      this.showLoading = true;
      if (this.connect_params.type == "operator") {
        dataObservable = Observable.fromPromise(
          this.videos.video_operator_detail(
            this.params.id,
            this.connect_params.id
          )
        );
        cacheKey =
          this.storageService.cacheKey.MEDIA_VIDEO_DETAIL +
          "_" +
          this.business.id +
          "_operator_" +
          "_" +
          this.connect_params.id +
          "_" +
          this.params.id;
      } else if (this.connect_params.type == "distributor") {
        dataObservable = Observable.fromPromise(
          this.videos.video_distributor_detail(
            this.params.id,
            this.connect_params.id
          )
        );
        cacheKey =
          this.storageService.cacheKey.MEDIA_VIDEO_DETAIL +
          "_" +
          this.business.id +
          "_distributor_" +
          "_" +
          this.connect_params.id +
          "_" +
          this.params.id;
      }
    } else if (this.action == "notif") {
      dataObservable = Observable.fromPromise(
        this.videos.video_detail(this.params.id)
      );
      cacheKey =
        this.storageService.cacheKey.MEDIA_VIDEO_DETAIL +
        "_" +
        this.business.id +
        "_" +
        this.params.id;
    } else {
      dataObservable = Observable.fromPromise(
        this.videos.video_detail(this.params.id)
      );
      cacheKey =
        this.storageService.cacheKey.MEDIA_VIDEO_DETAIL +
        "_" +
        this.business.id +
        "_" +
        this.params.id;
    }

    this.cacheService.register(cacheKey, dataObservable).subscribe(cache => {
      this.mediaCache = cache;
      this.zone.run(() => {
        this.mediaArr$ = cache.get$;

        this.mediaCache.refresh().subscribe(
          () => {
            this.loader.start();
          },
          () => {
            this.loader.complete();
          },
          () => {
            this.loader.complete();
          }
        );
      });
    });

    this.mediaArr$.subscribe(successResponse, errorResponse);
  }

  private async initBusiness() {
    const business = await this.storageService.getStorage(
      this.storageService.storageKey.CURRENT_BUSINESS
    );
    this.business = !_.isEmpty(business) ? JSON.parse(business) || {} : {};
  }

  async ionViewDidLoad() {
    this.isSafari =
      /constructor/i.test(window["HTMLElement"]) ||
      (function(p) {
        return p.toString() === "[object SafariRemoteNotification]";
      })(!window["safari"] || safari.pushNotification);

    this.initBusiness().then(() => {
      this.initData();
    });
  }

  ionViewDidLeave() {
    console.log("ionViewDidLeave");
    $("video").each((videoIndex, video) => {
      video.pause();
    });
  }

  ionViewWillUnload() {
    console.log("ionViewWillUnload");
    $("video").each((videoIndex, video) => {
      video.pause();
    });
  }

  doRefresh(refresher: any) {
    if (this.mediaCache) {
      this.showLoading = true;
      this.zone.run(() => {
        this.initData(refresher);
      });
    } else {
      console.warn("Cache has not been initialized.");
      if (refresher) {
        refresher.cancel();
      }
    }
  }

  presentPopover(ev) {
    const popover = this.popoverCtrl.create(VideoDetailPopoverPage, {
      showDownload: this.business.paid,
      action: this.action
    });
    popover.present({ ev: ev });
  }

  presentActionSheet() {
    if (this.business.role !== 1) {
      const toast = this.toastCtrl.create({
        message: "Invalid Access!!!",
        duration: 3000
      });
      toast.present();
      return;
    }

    const actionSheetButtons: any[] = [
      {
        text: "Add to Product as Feature",
        handler: () => {
          console.log("Archive clicked");
          this.addToProductFeature();
        }
      },
      {
        text: "Add Video to collection",
        handler: () => {
          console.log("Archive clicked");
          this.addVideoToCollection();
        }
      },
      {
        text: "Delete",
        role: "destructive",
        handler: () => {
          console.log("Archive clicked");
          setTimeout(() => {
            this.deleteVideo();
          }, 300);
        }
      },
      {
        text: "Cancel",
        role: "cancel",
        handler: () => {
          console.log("Cancel clicked");
        }
      }
    ];

    if (this.business.role == 1 || this.business.role == 2) {
      actionSheetButtons.unshift(
        {
          text: "Edit Details",
          handler: () => {
            console.log("Destructive clicked");
            this.updateVideoDetail();
          }
        },
        {
          text: "Update Thumbnail",
          handler: () => {
            console.log("Destructive clicked");
            this.updateVideoThumbnail();
          }
        },
        {
          text: "Update File",
          handler: () => {
            console.log("Destructive clicked");
            this.updateVideoFile();
          }
        }
      );
    }

    const actionSheet = this.actionSheetCtrl.create({
      title: "Select action for video",
      buttons: actionSheetButtons
    });
    actionSheet.present();
  }

  private addToProductFeature() {
    if (this.business.role !== 1) {
      const toast = this.toastCtrl.create({
        message: "Invalid Access!!!",
        duration: 3000
      });
      toast.present();
      return;
    }

    const modal = this.modalCtrl.create(ProductsModalPage, {
      image: this.item,
      action: "feature",
      type: "video"
    });
    modal.present();
  }

  private addVideoToCollection() {
    if (this.business.role == 3 || this.business.role == 4) {
      const toast = this.toastCtrl.create({
        message: "Invalid Access!!!",
        duration: 3000
      });
      toast.present();
      return;
    }

    const modal = this.modalCtrl.create(CollectionsModalPage, {
      type: "video",
      params: this.item
    });
    modal.present();
  }

  private updateVideoDetail() {
    if (this.business.role !== 1) {
      const toast = this.toastCtrl.create({
        message: "Invalid Access!!!",
        duration: 3000
      });
      toast.present();
      return;
    }

    this.app.getRootNav().push(VideoDetailInfoPage, {
      video: this.item
    });
  }

  private updateVideoFile() {
    if (this.business.role == 3 || this.business.role == 4) {
      const toast = this.toastCtrl.create({
        message: "Invalid Access!!!",
        duration: 3000
      });
      toast.present();
      return;
    }

    const modal = this.modalCtrl.create(FileUploadPage, {
      action: "update_video_policy",
      title: "Update Video File",
      params: {
        videoId: this.item.id
      }
    });
    modal.onDidDismiss(resp => {
      if (resp && resp == "save") {
        setTimeout(() => {
          this.initData();
        }, 300);
      }
    });
    modal.present();
  }

  private updateVideoThumbnail() {
    if (this.business.role == 3 || this.business.role == 4) {
      const toast = this.toastCtrl.create({
        message: "Invalid Access!!!",
        duration: 3000
      });
      toast.present();
      return;
    }

    const modal = this.modalCtrl.create(FileUploadPage, {
      action: "update_video_thumbnail_policy",
      title: "Update Video Thumbnail",
      params: {
        videoId: this.item.id
      }
    });
    modal.onDidDismiss(resp => {
      if (resp && resp == "save") {
        setTimeout(() => {
          this.initData();
        }, 300);
      }
    });
    modal.present();
  }

  private deleteVideo() {
    if (this.business.role == 3 || this.business.role == 4) {
      const toast = this.toastCtrl.create({
        message: "Invalid Access!!!",
        duration: 3000
      });
      toast.present();
      return;
    }

    const loading = this.loadingCtrl.create({
      dismissOnPageChange: true,
      content: "Deleting..."
    });
    loading.present();
    this.videos.videos_delete([this.item.id]).then(
      (data: any) => {
        if (data && data.success) {
          console.log("print_delete: ", data);
          const alert = this.alertCtrl.create({
            title: "SUCCESS",
            message: "Video successfully deleted!",
            buttons: ["OK"]
          });
          alert.onDidDismiss(() => {
            this.callback("refresh").then(() => {
              this.navCtrl.pop();
            });
          });
          alert.present();
        }
        loading.dismiss();
      },
      (error: any) => {
        loading.dismiss();
        console.log("error: ", error);
      }
    );
  }

  fullScreen() {
    console.log("fullScreen");
    const profileModal = this.modalCtrl.create(VideosPlayerPage, {
      video: this.item
    });
    profileModal.present();
  }

  viewChat() {
    const modal = this.modalCtrl.create(ChatPage);
    modal.present();
  }

  openLink(link) {
    if (link) {
      link = link.startsWith("//") ? "http:" + link : link;
      this.iab.create(link, "_blank");
    } else {
      const alert = this.alertCtrl.create({
        title: "Invalid URL",
        subTitle: "Invalid URL format",
        buttons: ["OK"]
      });
      alert.present();
    }
  }

  shareNow() {
    if (!this.business.paid) {
      this.alertCtrl
        .create({
          title: "WARNING",
          message: "This feature is only available to premium users only.",
          buttons: ["OK"]
        })
        .present();
      return;
    }

    if (!this.platform.is("cordova")) {
      this.alertCtrl
        .create({
          title: "WARNING",
          message:
            "Social Sharing is only available on actual device/simulator",
          buttons: ["OK"]
        })
        .present();
      return;
    }

    const loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    loading.present();
    this.videos.share_url_video(this.item.id).then(
      (data: any) => {
        if (data && data.success) {
          console.log("data: ", data);
          this.socialSharing
            .share(
              "This link allows you to download the original version of this image. " +
                "You can send the link to whom ever you need and this will allow them access to the original file.",
              "Share",
              null,
              data.data
            )
            .then(() => {
              // Success!
            })
            .catch(() => {
              // Error!
            });
          loading.dismiss();
        } else {
          loading.dismiss();
        }
      },
      error => {
        console.log("error: ", error);
        loading.dismiss();
      }
    );
  }

  private downloadImage() {
    const ctrl = this;
    let perc: any;

    if (!this.business.paid) {
      this.alertCtrl
        .create({
          title: "WARNING",
          message: "This feature is only available to premium users only.",
          buttons: ["OK"]
        })
        .present();
      return;
    }

    const loading = ctrl.loadingCtrl.create({
      content: "Downloading ...",
      dismissOnPageChange: true
    });
    loading.present();

    const alert = ctrl.alertCtrl.create({
      title: "Success",
      subTitle: "Image successfully downloaded",
      buttons: ["OK"]
    });

    const download = (dataurl, filename) => {
      var a = document.createElement("a");
      a.href = dataurl;
      a.setAttribute("download", filename);
      var b = document.createEvent("MouseEvents");
      b.initEvent("click", false, true);
      a.dispatchEvent(b);
      return false;
    };

    const downloadNative = (image, filename) => {
      ctrl.platform.ready().then(() => {
        ctrl.fileTransfer = ctrl.transfer.create();
        const newDir = ctrl.storageDirectory + "/" + filename;

        ctrl.fileTransfer
          .download(image, newDir)
          .then(
            entry => {
              if (entry) {
                const alertSuccess = ctrl.alertCtrl.create({
                  title: `Download Succeeded!`,
                  subTitle: "Download successfully",
                  buttons: ["Ok"]
                });

                loading.dismiss();
                alertSuccess.present();
              }
            },
            error => {
              console.log("fileTransfer error: ", error);
              ctrl.fileTransfer.abort();
              const alertFailure = ctrl.alertCtrl.create({
                title: `Download Failed!`,
                subTitle: `${image} was not successfully downloaded. Error code: ${
                  error.code
                }`,
                buttons: ["Ok"]
              });
              loading.dismiss();
              alertFailure.present();
            }
          )
          .catch(error => {
            ctrl.fileTransfer.abort();
            loading.dismiss();
          });

        ctrl.fileTransfer.onProgress(progressEvent => {
          if (progressEvent.lengthComputable) {
            perc = Math.floor(
              (progressEvent.loaded / progressEvent.total) * 100
            );
            console.log("Progress: ", perc + "% loaded...");

            ctrl.zone.run(() => {
              loading.setContent("Downloading " + perc + "% ...");

              if (ctrl.platform.is("android")) {
                ctrl.localNotifications.schedule({
                  title: "Downloading " + filename,
                  text: "Downloading " + perc + "% ...",
                  sound: null,
                  progressBar: { value: perc }
                });
              }
            });
          }
        });
      });
    };

    const getFilename = url => {
      url = url
        .split("/")
        .pop()
        .replace(/\#(.*?)$/, "")
        .replace(/\?(.*?)$/, "");
      url = url.split("."); // separates filename and extension
      return { filename: url[0] || "", ext: url[1] || "" };
    };

    this.videos.video_download(this.item.id).then(
      (data: any) => {
        if (data && data.success) {
          const filename =
            getFilename(data.data).filename + "." + getFilename(data.data).ext;
          if (!this.platform.is("cordova")) {
            download(data.data, filename);
            loading.dismiss();
            alert.present();
          } else {
            downloadNative(data.data, filename);
          }
        }
      },
      error => {
        loading.dismiss();
        if (error && !error.success) {
          this.alertCtrl
            .create({
              title: "WARNING",
              message: "Error while downloading file. " + error.message,
              buttons: ["OK"]
            })
            .present();
          return;
        }
      }
    );
  }
}
