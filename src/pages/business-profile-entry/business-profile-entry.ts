import { Component, ViewChild, ElementRef, NgZone } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  Events,
  Content,
  AlertController,
  ActionSheetController,
  LoadingController
} from "ionic-angular";
import * as async from "async";
import * as _ from "lodash";
import {
  FormGroup,
} from "@angular/forms";

import { UtilitiesServicesProvider } from "../../providers/services/utilities";
import { ConnectServicesProvider } from "../../providers/services/connect";
import { UserServicesProvider } from "../../providers/services/users";
import { BusinessServicesProvider } from "../../providers/services/business";
import { StorageProvider } from "../../providers/storage/storage";

declare const google: any;

@IonicPage()
@Component({
  selector: "page-business-profile-entry",
  templateUrl: "business-profile-entry.html"
})
export class BusinessProfileEntryPage {
  users: any = {};
  business: any = {};
  token: string;
  map: any;

  isSaving = false;
  submitted = false;

  inpuForm: FormGroup;

  isSameAddress: boolean = false;
  needAccess: boolean = false;
  showSubcats: boolean = false;
  isCollapsedOperator: boolean = false;
  isCollapsedDistributor: boolean = false;
  isShowOperator: boolean = false;
  isShowDistributor: boolean = false;

  distributorCat: any[] = [];
  operatorCat: any[] = [];
  platformsArr: any[] = [];
  subCategoryArr: any[] = [];
  countriesArr: any[] = [];

  isSearching: boolean = false;
  isRequesting: boolean = false;

  @ViewChild(Content) content: Content;
  @ViewChild("map") mapElement: ElementRef;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public evts: Events,
    public alertCtrl: AlertController,
    public actionSheetCtrl: ActionSheetController,
    public loadingCtrl: LoadingController,
    public zone: NgZone,
    public connect: ConnectServicesProvider,
    public utilities: UtilitiesServicesProvider,
    public services: UserServicesProvider,
    public businesses: BusinessServicesProvider,
    public storageService: StorageProvider
  ) {}

  private initData(ev?: any) {
    const loading = this.loadingCtrl.create();
    loading.present();

    this.business = this.navParams.get("profile");

    async.parallel(
      [
        callback => {
          this.utilities.getCountries().then(
            (data: any) => {
              if (data && data.success) {
                this.countriesArr = data.data;
              }
              callback();
            },
            error => {
              console.log("error: ", error);
              callback();
            }
          );
        },
        callback => {
          this.utilities.category().then(
            (data: any) => {
              if (data && data.success) {
                this.operatorCat = data.data;
                console.log("operatorCat: ", this.operatorCat);
                callback();
              } else {
                callback();
              }
            },
            error => {
              callback();
            }
          );
        },
        callback => {
          this.utilities.distributor_category().then(
            (data: any) => {
              if (data && data.success) {
                this.distributorCat = data.data;
                console.log("distributorCat: ", this.distributorCat);
                callback();
              } else {
                callback();
              }
            },
            error => {
              callback();
            }
          );
        },
        callback => {
          this.utilities.booking_platforms().then(
            (data: any) => {
              if (data && data.success) {
                this.platformsArr = data.data;
                console.log("platformsArr: ", this.platformsArr);
                callback();
              } else {
                callback();
              }
            },
            error => {
              callback();
            }
          );
        }
      ],
      () => {
        this.business.businessContact = this.business.contact;
        /* this.business.postalNumber = this.business.postcode;
			this.business.postalStreet = this.business.location;
			this.business.postalSuburb = this.business.suburb;
			this.business.postalState = this.business.state;
			this.business.postalPostcode = this.business.postcode;
			this.business.postalCountry = this.business.country; */

        let keywords = this.business.keywords;
        console.log("keywords: ", keywords);
        if (keywords) {
          this.business.tags = keywords.split(",");
        } else {
          this.business.tags = [];
        }

        console.log("this.business : ", this.business);

        if (this.business.type == "operator") {
          this.showOperators();
          let category: any = _.find(this.operatorCat, {
            title: this.business.category
          });
          if (category) {
            this.business.category = category.id;
            this.selectCategory(this.business.category, "opt");
          }
        } else if (this.business.type == "distributor") {
          this.showDistributor();

          let category: any = _.find(this.distributorCat, {
            title: this.business.category
          });
          if (category) {
            this.business.category = category.id;
            this.selectCategory(this.business.category, "dist");
          }
        }

        this.showSubcats = true;

        this.loadInteractiveMap(
          this.business.latitude,
          this.business.longitude
        );

        loading.dismiss();

        if (ev) {
          ev.complete();
        }
      }
    );
  }

  async ionViewDidLoad() {
    console.log("ionViewDidLoad CreatebusinessPage");
  }

  ionViewWillEnter() {
    console.log("ionViewWillEnter CreatebusinessPage");
    this.initData();
  }

  doRefresh(ev) {
    this.initData(ev);
  }

  showOperators() {
    this.isShowOperator = true;
    this.isShowDistributor = false;
  }

  showDistributor() {
    this.isShowOperator = false;
    this.isShowDistributor = true;
  }

  selectCategory(index, type) {
    console.log("selectCategory: ", index);
    if (type === "opt") {
      const category = _.find(this.operatorCat, { id: index });
      console.log("category 1: ", category);
      if (category) {
        this.populateSubCategory(category.id);
      }
    } else if (type === "dist") {
      const category = _.find(this.distributorCat, { id: index });
      console.log("category 2: ", category);
      if (category) {
        this.populateSubCategory(category.id);
      }
    }
  }

  populateSubCategory(category) {
    const loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    loading.present();
    this.utilities.subcategory(category).then(
      (data: any) => {
        if (data && data.success) {
          this.subCategoryArr = data.data;
          this.showSubcats = true;
          console.log("this.subCategoryArr: ", this.subCategoryArr);

          if (this.business.subCategory) {
            const subcategory = _.find(this.subCategoryArr, {
              title: this.business.subCategory
            });
            if (subcategory) {
              this.business.subCategory = subcategory.id;
            }
          }
        }
        loading.dismiss();
      },
      error => {
        console.log("error: ", error);
        loading.dismiss();
      }
    );
  }

  valildateBusDetails(e) {
    console.log("valildateBusDetails");
    if (this.showSubcats) {
      if (
        this.business.subCategory &&
        this.business.bookingPlatform &&
        this.business.businessContact &&
        this.business.url &&
        this.business.email &&
        this.business.phone
      ) {
      } else {
      }
    } else {
      if (
        this.business.businessContact &&
        this.business.url &&
        this.business.email &&
        this.business.phone
      ) {
      } else {
      }
    }
  }

  valildatePostDetails(e) {
    if (
      this.business.postalNumber &&
      this.business.postalStreet &&
      this.business.postalSuburb &&
      this.business.postalState &&
      this.business.postalPostcode &&
      this.business.postalCountry
    ) {
    } else {
    }
  }

  valildatePhyDetails(e) {
    if (
      this.business.physicalNumber &&
      this.business.physicalStreet &&
      this.business.physicalSuburb &&
      this.business.physicalState &&
      this.business.physicalPostcode &&
      this.business.physicalCountry
    ) {
    } else {
    }
  }

  toggleAddress(e) {
    // this.isSameAddress = !this.isSameAddress;
    if (this.isSameAddress) {
      this.business.physicalNumber = this.business.postalNumber;
      this.business.physicalStreet = this.business.postalStreet;
      this.business.physicalSuburb = this.business.postalSuburb;
      this.business.physicalState = this.business.postalState;
      this.business.physicalPostcode = this.business.postalPostcode;
      this.business.physicalCountry = this.business.postalCountry;
    } else {
      this.business.physicalNumber = null;
      this.business.physicalStreet = null;
      this.business.physicalSuburb = null;
      this.business.physicalState = null;
      this.business.physicalPostcode = null;
      this.business.physicalCountry = null;
    }
  }

  validateBusinessName() {
    const checkBusinessName = () => {
      this.isSearching = true;
      this.connect
        .search_businesses({
          name: this.business.name
        })
        .then(
          (data: any) => {
            if (data && data.success) {
              if (_.isArray(data.data) && data.data[0] === false) {
                this.needAccess = false;
                this.isSearching = false;
              } else if (_.isArray(data.data) && data.data[0] !== false) {
                this.needAccess = true;
                this.isSearching = false;

                this.business = data.data[0];
              } else {
                this.needAccess = true;
                this.isSearching = false;
              }
            } else {
              this.isSearching = false;
            }
          },
          error => {
            this.needAccess = true;
            this.isSearching = false;
          }
        );
    };

    if (this.business.name && this.business.name.trim() !== "") {
      setTimeout(() => {
        checkBusinessName();
      }, 1000);
    } else {
      return;
    }
  }

  loadInteractiveMap(latitude, longitude) {
    const handleEvent = event => {
      let markerlatlong: any = event.latLng;
      console.log("latt1", markerlatlong.lat());
      console.log("long1", markerlatlong.lng());
      this.business.latitude = markerlatlong.lat();
      this.business.longitude = markerlatlong.lng();
    };

    if (typeof google === "object" && typeof google.maps === "object") {
      let mapOptions2 = {
        center: new google.maps.LatLng(latitude, longitude),
        zoom: 6,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };
      this.zone.runOutsideAngular(() => {
        this.map = new google.maps.Map(
          this.mapElement.nativeElement,
          mapOptions2
        );
        let marker = new google.maps.Marker({
          animation: google.maps.Animation.DROP,
          position: this.map.getCenter(),
          draggable: true,
          optimized: false,
          zIndex: 5
        });
        marker.setMap(this.map);
        marker.addListener("drag", handleEvent);
        marker.addListener("dragend", handleEvent);
      });
    }
  }

  onFinish() {
    const loading = this.loadingCtrl.create({
      content: "Saving...",
      duration: 2000
    });
    this.business.businessContact = this.business.contact;
    console.log("saveBusiness: ", this.business);

    loading.present();
    this.businesses.business_profile_edit(this.business).then(
      (data: any) => {
        if (data && data.success) {
          const alert = this.alertCtrl.create({
            message: data.data,
            title: "Record Saved!!",
            buttons: [
              {
                text: "Ok"
              }
            ]
          });
          alert.present();
          alert.onDidDismiss(() => {
            this.navCtrl.pop();
          });
        } else {
          this.alertCtrl
            .create({
              message: data.message,
              title: "Error!",
              buttons: [
                {
                  text: "Ok"
                }
              ]
            })
            .present();
        }
        loading.dismiss();
      },
      error => {
        console.log("error: ", error);
        this.alertCtrl
          .create({
            title: "Error",
            message: "An unknown error occurred!",
            buttons: ["OK"]
          })
          .present();
      }
    );
  }
}
