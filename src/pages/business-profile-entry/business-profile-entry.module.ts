import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BusinessProfileEntryPage } from './business-profile-entry';
import { IonTagsInputModule } from "ionic-tags-input";
import { ComponentsModule } from '../../components/components.module';

@NgModule({
	declarations: [
		BusinessProfileEntryPage
	],
	imports: [
		ComponentsModule,
		IonTagsInputModule,
		IonicPageModule.forChild(BusinessProfileEntryPage),
	],
})
export class BusinessProfileEntryPageModule { }
