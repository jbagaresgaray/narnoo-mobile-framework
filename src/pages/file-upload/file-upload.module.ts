import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FileUploadPage } from './file-upload';
import { NgProgressModule } from '@ngx-progressbar/core';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    FileUploadPage,
  ],
  imports: [
    IonicPageModule.forChild(FileUploadPage),
    NgProgressModule,
    PipesModule,
  ],
})
export class FileUploadPageModule {}
