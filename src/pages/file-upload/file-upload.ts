import { Component, ViewChild, NgZone, ViewEncapsulation } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import {
  IonicPage,
  NavParams,
  ActionSheetController,
  AlertController,
  ViewController,
  LoadingController,
  ToastController,
  normalizeURL,
  Platform,
  Events
} from "ionic-angular";
import { NgProgressComponent, NgProgress } from "@ngx-progressbar/core";
import xml2js from "xml2js";
import _ from "lodash";

import { Camera, CameraOptions } from "@ionic-native/camera";
import { FileChooser } from "@ionic-native/file-chooser";
import { File, FileEntry, IFile } from "@ionic-native/file";
// import { IOSFilePicker } from "@ionic-native/file-picker";

import { environment } from "../../environments/environment";

import { ImagesServicesProvider } from "../../providers/services/images";
import { PrintServicesProvider } from "../../providers/services/print";
import { VideosServicesProvider } from "../../providers/services/videos";
import { LogoServicesProvider } from "../../providers/services/logos";
import { ProductsServicesProvider } from "../../providers/services/products";
import { UploadServicesProvider } from "../../providers/services/upload";
import { StorageProvider } from "../../providers/storage/storage";

@IonicPage()
@Component({
  selector: "page-file-upload",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "file-upload.html"
})
export class FileUploadPage {
  action: string;
  title: string;
  params: any = {};
  business: any = {};

  accessKey: string;
  action_uri: string;
  file_name: string;
  policy: string;
  signature: string;
  file_key: string;
  contentType: string;
  successActionStatus: string;
  acl: string;

  uploadMode: string = "gallery";
  pageImage: number = 0;
  pagePrint: number = 0;
  pageVideo: number = 0;
  pageLogo: number = 0;
  perPage: number = 20;
  showContent: boolean = false;
  showCheckbox: boolean = false;

  showUploadButton: boolean = false;
  showSetFeatureButton: boolean = false;
  showUpdateFileButton: boolean = false;
  isOnline: boolean = true;

  public isUploading = false;
  public isImageAdded = false;
  public uploadingProgress = {};
  public uploadingHandler = {};

  imagesArr: any[] = [];
  fakeArr: any[] = [];

  public images: any = [];

  public filesToUpload: Array<File> = [];
  protected imagesValue: Array<any>;
  protected filesValue: Array<any>;

  @ViewChild(NgProgressComponent) progressBar: NgProgressComponent;

  constructor(
    public navParams: NavParams,
    private actionSheetCtrl: ActionSheetController,
    private alertCtrl: AlertController,
    public progress: NgProgress,
    private sanitization: DomSanitizer,
    public viewCtrl: ViewController,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public zone: NgZone,
    public platform: Platform,
    public events: Events,
    public imageService: ImagesServicesProvider,
    public printService: PrintServicesProvider,
    public uploadService: UploadServicesProvider,
    public productsService: ProductsServicesProvider,
    public videoService: VideosServicesProvider,
    public logoService: LogoServicesProvider,
    private fileChooser: FileChooser,
    private camera: Camera,
    private file: File, // private filePicker: IOSFilePicker
    public storageService: StorageProvider
  ) {
    this.action = navParams.get("action");
    this.title = navParams.get("title");
    this.params = navParams.get("params");
    console.log("this.params: ", this.params);

    for (var i = 0; i < 12; ++i) {
      this.fakeArr.push(i);
    }

    events.unsubscribe("app:online");
    events.unsubscribe("app:offline");

    events.subscribe("app:online", () => {
      this.isOnline = true;
    });
    events.subscribe("app:offline", () => {
      this.isOnline = false;
    });
  }

  public abort() {
    if (!this.isUploading) return;

    this.isUploading = false;
    if (this.uploadingHandler) {
      for (let key in this.uploadingHandler) {
        this.uploadingHandler[key].abort();
      }
    }
  }

  // ======================================================================
  protected removeImage(image) {
    if (this.isUploading) return;

    this.confirm("Are you sure to remove it?").then(value => {
      if (value) {
        this.removeFromArray(this.imagesValue, image);
        _.remove(this.images, {
          uuid: image.uuid
        });
        _.remove(this.filesToUpload, (row: any) => {
          return row.uuid == image.uuid;
        });
        if (!_.isEmpty(this.filesToUpload)) {
          this.isImageAdded = true;
        } else {
          this.isImageAdded = false;
        }
      }
    });
  }

  protected removeFile(file) {
    if (this.isUploading) return;

    this.confirm("Are you sure to remove it?").then(value => {
      if (value) {
        _.remove(this.images, {
          uuid: file.uuid
        });
        _.remove(this.filesToUpload, (row: any) => {
          return row.uuid == file.uuid;
        });

        if (!_.isEmpty(this.filesToUpload)) {
          this.isImageAdded = true;
        } else {
          this.isImageAdded = false;
        }
      }
    });
  }

  protected showAddImage() {
    let uuid = parseInt(new Date().getTime().toString());

    if (this.business.role !== 1) {
      this.toastCtrl
        .create({
          message: "Invalid Access!!!",
          duration: 3000
        })
        .present();
      return;
    }

    if (!window["cordova"]) {
      let input = document.createElement("input");
      input.type = "file";
      input.name = this.file_name;
      input.accept = "image/x-png,image/gif,image/jpeg";
      input.click();
      input.onchange = () => {
        let blob = window.URL.createObjectURL(input.files[0]);
        let blob2: any = input.files[0];
        blob2.uuid = uuid;

        if (this.action === "media") {
          if (blob2.size < environment.imageFileSize) {
            this.alertCtrl
              .create({
                title: "Upload file too small",
                message:
                  "Your proposed upload file is smaller than the minimum allowed size of 800kb",
                buttons: ["OK"]
              })
              .present();
            return;
          }
        } else if (this.action == "logos") {
          if (blob2.size < environment.logoFileSize) {
            this.alertCtrl
              .create({
                title: "Upload file too small",
                message:
                  "Your proposed upload file is smaller than the minimum allowed size of 100kb",
                buttons: ["OK"]
              })
              .present();
            return;
          }
        } else if (this.action == "gallery") {
          if (this.uploadMode == "upload") {
            if (blob2.size < environment.imageFileSize) {
              this.alertCtrl
                .create({
                  title: "Upload file too small",
                  message:
                    "Your proposed upload file is smaller than the minimum allowed size of 800kb",
                  buttons: ["OK"]
                })
                .present();
              return;
            }
          }
        }

        this.images.push({
          url: blob,
          uuid: uuid
        });
        this.filesToUpload.push(blob2);
        this.trustImages();
      };
    } else {
      new Promise((resolve, reject) => {
        let actionSheet = this.actionSheetCtrl.create({
          title: "Add a photo",
          buttons: [
            {
              text: "From photo library",
              handler: () => {
                resolve(this.camera.PictureSourceType.PHOTOLIBRARY);
              }
            },
            {
              text: "From camera",
              handler: () => {
                resolve(this.camera.PictureSourceType.CAMERA);
              }
            },
            {
              text: "Cancel",
              role: "cancel",
              handler: () => {
                reject();
              }
            }
          ]
        });
        actionSheet.present();
      }).then(sourceType => {
        if (!this.platform.is("cordova")) return;

        let options: CameraOptions = {
          quality: 100,
          sourceType: sourceType as number,
          saveToPhotoAlbum: false,
          correctOrientation: true
        };
        this.camera.getPicture(options).then(imagePath => {
          this.file.resolveLocalFilesystemUrl(imagePath).then(
            (entry: FileEntry) => {
              entry.file(
                (meta: IFile) => {
                  console.log("file: ", meta);
                  if (this.action === "media") {
                    if (meta.size < environment.imageFileSize) {
                      this.alertCtrl
                        .create({
                          title: "Upload file too small",
                          message:
                            "Your proposed upload file is smaller than the minimum allowed size of 800kb",
                          buttons: ["OK"]
                        })
                        .present();
                      return;
                    }
                  } else if (this.action === "logos") {
                    if (meta.size < environment.logoFileSize) {
                      this.alertCtrl
                        .create({
                          title: "Upload file too small",
                          message:
                            "Your proposed upload file is smaller than the minimum allowed size of 100kb",
                          buttons: ["OK"]
                        })
                        .present();
                      return;
                    }
                  } else if (this.action == "gallery") {
                    if (this.uploadMode == "upload") {
                      if (meta.size < environment.imageFileSize) {
                        this.alertCtrl
                          .create({
                            title: "Upload file too small",
                            message:
                              "Your proposed upload file is smaller than the minimum allowed size of 800kb",
                            buttons: ["OK"]
                          })
                          .present();
                        return;
                      }
                    }
                  } else if (this.action == "update_video_thumbnail_policy") {
                    if (meta.size < environment.videoThumbnailSize) {
                      this.alertCtrl
                        .create({
                          title: "Upload file too small",
                          message:
                            "Your proposed upload file is smaller than the minimum allowed size of 800kb",
                          buttons: ["OK"]
                        })
                        .present();
                      return;
                    }
                  }

                  if (this.platform.is("ios")) {
                    this.images.push({
                      url: normalizeURL(imagePath),
                      file: meta,
                      size: meta.size,
                      name: meta.name,
                      uuid: uuid
                    });
                  } else {
                    this.images.push({
                      url: imagePath,
                      file: meta,
                      size: meta.size,
                      name: meta.name,
                      uuid: uuid
                    });
                  }
                  this.filesToUpload.push(imagePath);
                  this.trustImages();
                },
                error => {
                  console.error("error getting fileentry file!", error);
                }
              );
            },
            error => {
              console.error("error getting file! ", error);
            }
          );
        });
      });
    }
  }

  protected showAddFile() {
    let uuid = parseInt(new Date().getTime().toString());

    if (this.business.role !== 1) {
      this.toastCtrl
        .create({
          message: "Invalid Access!!!",
          duration: 3000
        })
        .present();
      return;
    }

    if (!this.platform.is("cordova")) {
      let input = document.createElement("input");
      input.type = "file";
      input.name = this.file_name;
      if (this.action == "print" || this.action == "update_print_policy") {
        input.accept = "application/pdf";
      } else if (
        this.action == "videos" ||
        this.action == "update_video_policy"
      ) {
        input.accept = "video/*";
      }
      input.click();
      input.onchange = () => {
        let blob = window.URL.createObjectURL(input.files[0]);
        let blob2: any = input.files[0];
        blob2.uuid = uuid;

        if (this.action === "update_print_policy" || this.action === "print") {
          if (blob2.size < environment.printFileSize) {
            // 500KB
            this.alertCtrl
              .create({
                title: "Upload file too small",
                message:
                  "Your proposed upload file is smaller than the minimum allowed size of 500kb",
                buttons: ["OK"]
              })
              .present();
            return;
          }
        }

        if (this.action === "update_video_policy" || this.action === "videos") {
          if (blob2.size < environment.videoThumbnailSize) {
            // 500KB
            this.alertCtrl
              .create({
                title: "Upload file too small",
                message:
                  "Your proposed upload file is smaller than the minimum allowed size of 800kb",
                buttons: ["OK"]
              })
              .present();
            return;
          }
        }

        this.images.push({
          url: blob,
          uuid: uuid,
          size: blob2.size,
          name: blob2.name
        });
        this.filesToUpload.push(blob2);
        this.trustImages();
      };
    } else {
      if (this.platform.is("android")) {
        this.fileChooser
          .open()
          .then((uri: any) => {
            console.log("fileChooser Android: ", uri);
            this.file.resolveLocalFilesystemUrl(uri).then(
              (entry: FileEntry) => {
                entry.file(
                  (meta: IFile) => {
                    console.log("file: ", meta);

                    if (
                      this.action === "update_print_policy" ||
                      this.action === "print"
                    ) {
                      if (meta.size < environment.printFileSize) {
                        // 500KB
                        this.alertCtrl
                          .create({
                            title: "Upload file too small",
                            message:
                              "Your proposed upload file is smaller than the minimum allowed size of 500kb",
                            buttons: ["OK"]
                          })
                          .present();
                        return;
                      }
                    }

                    if (
                      this.action === "update_video_policy" ||
                      this.action === "videos"
                    ) {
                      if (meta.size < environment.videoThumbnailSize) {
                        // 500KB
                        this.alertCtrl
                          .create({
                            title: "Upload file too small",
                            message:
                              "Your proposed upload file is smaller than the minimum allowed size of 800kb",
                            buttons: ["OK"]
                          })
                          .present();
                        return;
                      }
                    }

                    this.images.push({
                      url: uri,
                      file: meta,
                      size: meta.size,
                      name: meta.name,
                      uuid: uuid
                    });
                    this.filesToUpload.push(uri);
                    console.log("images: ", this.images);
                    console.log("filesToUpload: ", this.filesToUpload);

                    this.trustImages();
                  },
                  error => {
                    console.error("error getting fileentry file!", error);
                  }
                );
              },
              error => {
                console.error("error getting file! ", error);
              }
            );
          })
          .catch(error => {
            console.log("fileChooser error: ", error);
          });
      } else if (this.platform.is("ios")) {
        let options: CameraOptions = {
          quality: 100,
          sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
          correctOrientation: true,
          mediaType: this.camera.MediaType.VIDEO
        };
        this.camera.getPicture(options).then((uri: any) => {
          this.file.resolveLocalFilesystemUrl(uri).then(
            (entry: FileEntry) => {
              entry.file(
                (meta: IFile) => {
                  console.log("file: ", meta);

                  if (
                    this.action === "update_video_policy" ||
                    this.action === "videos"
                  ) {
                    if (meta.size < 800000) {
                      // 500KB
                      this.alertCtrl
                        .create({
                          title: "Upload file too small",
                          message:
                            "Your proposed upload file is smaller than the minimum allowed size of 800kb",
                          buttons: ["OK"]
                        })
                        .present();
                      return;
                    }
                  }

                  this.images.push({
                    url: normalizeURL(uri),
                    file: meta,
                    size: meta.size,
                    name: meta.name,
                    uuid: uuid
                  });
                  this.filesToUpload.push(uri);
                  this.trustImages();
                },
                error => {
                  console.error("error getting fileentry file!", error);
                }
              );
            },
            error => {
              console.error("error getting file! ", error);
            }
          );
        });
      }
    }
  }

  private uploadImage(targetPath: any) {
    let vm = this;
    return new Promise((resolve, reject) => {
      console.log("targetPath: ", targetPath);

      let formData: FormData = new FormData();
      let xhr2: XMLHttpRequest = new XMLHttpRequest();

      if (this.platform.is("cordova")) {
        let img: any = _.find(vm.imagesValue, {
          url: this.platform.is("ios") ? normalizeURL(targetPath) : targetPath
        });
        console.log("img: ", img);
        vm.uploadingProgress[img.uuid] = 0;
        vm.file.resolveLocalFilesystemUrl(targetPath).then(
          (entry: FileEntry) => {
            entry.file(
              (meta: IFile) => {
                if (
                  this.action == "update_video_thumbnail_policy" ||
                  this.action == "update_print_policy"
                ) {
                  formData.append(
                    "key",
                    vm.file_key.split("/")[0] + "/" + vm.file_key.split("/")[1]
                  );
                } else {
                  formData.append(
                    "key",
                    vm.file_key.split("/")[0] + "/" + meta.name
                  );
                }

                formData.append("acl", vm.acl);
                formData.append("Content-Type", meta.type);
                formData.append("Policy", vm.policy);
                formData.append("X-Amz-Signature", vm.signature);
                formData.append("AWSAccessKeyId", vm.accessKey);
                formData.append("Signature", vm.signature);
                formData.append(
                  "success_action_status",
                  vm.successActionStatus
                );

                var reader = new FileReader();
                reader.onloadend = function(evt: any) {
                  var blob = new Blob([new Uint8Array(evt.target.result)], {
                    type: meta.type
                  });
                  console.log("blob: ", blob);
                  formData.append(vm.file_name, blob);

                  vm.uploadingHandler[img.uuid] = xhr2;
                  xhr2.timeout = 900000;
                  xhr2.onreadystatechange = () => {
                    if (xhr2.readyState === 4) {
                      if (xhr2.status === parseInt(vm.successActionStatus)) {
                        resolve(xhr2.response);
                      } else {
                        askRetry(xhr2.response);
                      }
                    }
                  };

                  xhr2.upload.onprogress = event => {
                    let progress = (event.loaded * 100) / event.total;
                    vm.zone.run(() => {
                      vm.uploadingProgress[img.uuid] = progress;
                      console.log(
                        "vm.uploadingProgress[img.uuid]: ",
                        vm.uploadingProgress[img.uuid]
                      );
                    });
                  };
                  xhr2.open("POST", encodeURI(vm.action_uri), true);
                  xhr2.send(formData);
                };
                reader.readAsArrayBuffer(meta);
              },
              error => {
                console.error("error getting fileentry file!", error);
              }
            );
          },
          error => {
            console.error("error getting file! ", error);
          }
        );
      } else {
        this.uploadingProgress[targetPath.uuid] = 0;

        console.log("vm.file_key.split: ", vm.file_key.split("/"));
        if (
          this.action == "update_video_thumbnail_policy" ||
          this.action == "update_print_policy"
        ) {
          formData.append(
            "key",
            vm.file_key.split("/")[0] + "/" + vm.file_key.split("/")[1]
          );
        } else {
          formData.append(
            "key",
            vm.file_key.split("/")[0] + "/" + targetPath.name
          );
        }

        formData.append("acl", this.acl);
        formData.append("Content-Type", targetPath.type);
        formData.append("Policy", this.policy);
        formData.append("X-Amz-Signature", this.signature);
        formData.append("AWSAccessKeyId", this.accessKey);
        formData.append("Signature", this.signature);
        formData.append("success_action_status", this.successActionStatus);
        formData.append(this.file_name, targetPath);

        this.uploadingHandler[targetPath.uuid] = xhr2;
        xhr2.onreadystatechange = () => {
          if (xhr2.readyState === 4) {
            if (xhr2.status === parseInt(this.successActionStatus)) {
              resolve(xhr2.response);
            } else {
              askRetry(xhr2.response);
            }
          }
        };

        xhr2.upload.onprogress = event => {
          let progress = (event.loaded * 100) / event.total;
          this.zone.run(() => {
            this.uploadingProgress[targetPath.uuid] = progress;
          });
          console.log("this.uploadingProgress[targetPath]: ", progress);
        };
        xhr2.open("POST", encodeURI(this.action_uri), true);
        xhr2.send(formData);
      }

      let askRetry = resp => {
        // might have been aborted
        if (!this.isUploading) return reject(null);
        this.confirm("Do you wish to retry?", "Upload failed").then(res => {
          if (!res) {
            this.isUploading = false;
            for (let key in this.uploadingHandler) {
              console.log("key: ", key);
              this.uploadingHandler[key].abort();
            }
            return reject(resp);
            // return;
          } else {
            if (!this.isUploading) return reject(null);
            this.uploadImage(targetPath).then(resolve, reject);
          }
        });
      };
    });
  }

  private removeFromArray<T>(array: Array<T>, item: T) {
    let index: number = array.indexOf(item);
    console.log("removeFromArray index: ", index);
    if (index !== -1) {
      array.splice(index, 1);
    }
  }

  private confirm(text, title = "", yes = "Yes", no = "No") {
    return new Promise(resolve => {
      this.alertCtrl
        .create({
          title: title,
          message: text,
          buttons: [
            {
              text: no,
              role: "cancel",
              handler: () => {
                resolve(false);
              }
            },
            {
              text: yes,
              handler: () => {
                resolve(true);
              }
            }
          ]
        })
        .present();
    });
  }

  private trustImages() {
    this.zone.run(() => {
      this.imagesValue = this.images.map(val => {
        return {
          url: val.url,
          uuid: val.uuid,
          sanitized: this.sanitization.bypassSecurityTrustStyle(
            "url(" + val.url + ")"
          )
        };
      });

      if (!_.isEmpty(this.filesToUpload)) {
        this.isImageAdded = true;
      } else {
        this.isImageAdded = false;
      }
      this.validateUploadButton();
    });
  }

  private parseXML(data) {
    return new Promise(resolve => {
      var parser = new xml2js.Parser({
        trim: true
      });

      parser.parseString(data, function(err, result) {
        console.log("result: ", result);
        resolve(result);
      });
    });
  }

  private uploadImages(): Promise<Array<any>> {
    if (this.business.role !== 1) {
      this.toastCtrl
        .create({
          message: "Invalid Access!!!",
          duration: 3000
        })
        .present();
      return;
    }

    return new Promise((resolve, reject) => {
      this.isUploading = true;
      Promise.all(
        this.filesToUpload.map((image: any) => {
          return this.uploadImage(image);
        })
      )
        .then(() => {
          resolve();
          this.isUploading = false;
          this.progress.complete();

          let uploadLength: number = 0;
          if (
            this.action == "update_print_policy" ||
            this.action == "update_video_policy" ||
            this.action == "print" ||
            this.action == "videos"
          ) {
            uploadLength = _.size(this.images);
          } else if (
            this.action == "update_video_thumbnail_policy" ||
            this.action == "media" ||
            this.action == "logos"
          ) {
            uploadLength = _.size(this.imagesValue);
          } else if (this.action == "gallery" && this.uploadMode == "upload") {
            uploadLength = _.size(this.imagesValue);
          }

          setTimeout(() => {
            this.viewCtrl.dismiss({
              status: "save",
              uploaded: uploadLength
            });
          }, 300);
        })
        .catch((reason: any) => {
          console.log("reasons: ", reason);
          if (reason) {
            this.parseXML(reason).then((data: any) => {
              console.log("parseXML: ", data);
              if (data && data.Error) {
                let alert = this.alertCtrl.create({
                  title: "Error",
                  message: data.Error.Message[0],
                  buttons: ["OK"]
                });
                alert.present();
              } else {
                this.alertCtrl
                  .create({
                    title: "Error",
                    message: reason.body || reason.exception,
                    buttons: ["OK"]
                  })
                  .present();
              }
            });
          }
          this.isUploading = false;
          this.uploadingProgress = {};
          this.uploadingHandler = {};
          // reject(reason);
        });
    });
  }

  private addImagesToProduct(selectedDatas) {
    let loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    loading.present();
    this.isUploading = true;
    this.productsService
      .product_add_image({
        productId: this.params.productId,
        image: selectedDatas
      })
      .then(
        (data: any) => {
          if (data && data.success) {
            console.log("data: ", data);
            const toast = this.alertCtrl.create({
              title: "SUCCESS",
              message: data.data,
              buttons: ["OK"]
            });
            toast.onDidDismiss(() => {
              this.viewCtrl.dismiss("save");
            });
            toast.present();
          } else if (data && !data.success) {
            const toast = this.alertCtrl.create({
              title: "WARNING",
              message: data.data,
              buttons: ["OK"]
            });
            toast.present();
          }
          loading.dismiss();
          this.isUploading = false;
        },
        error => {
          console.log("error: ", error);
          loading.dismiss();
          this.isUploading = false;
          if (error && !error.success) {
            const toast = this.alertCtrl.create({
              title: "SUCCESS",
              message: error.message,
              buttons: ["OK"]
            });
            toast.present();
          }
        }
      );
  }

  private featureProductItem(type, mediaId) {
    let loading = this.loadingCtrl.create({
      dismissOnPageChange: true,
      content: "Updating..."
    });
    loading.present();
    this.isUploading = true;
    this.productsService
      .product_set_feature_item({
        productId: this.params.productId,
        mediaId: mediaId,
        type: type
      })
      .then(
        (data: any) => {
          if (data && data.success) {
            const alert = this.alertCtrl.create({
              title: "SUCCESS",
              message: data.data,
              buttons: ["OK"]
            });
            alert.onDidDismiss(() => {
              this.viewCtrl.dismiss("save");
            });
            alert.present();
          }
          this.isUploading = false;
          loading.dismiss();
        },
        (error: any) => {
          console.log("error: ", error);
          this.isUploading = false;
          loading.dismiss();
          if (error && !error.success) {
            this.alertCtrl
              .create({
                title: "ERROR",
                message: error.message,
                buttons: ["OK"]
              })
              .present();
          }
        }
      );
  }

  private featureUtilsItems(action) {
    let selectedDatas: any = _.find(this.imagesArr, (row: any) => {
      return row.selected === true;
    });
    let _action: string;

    if (action == "feature_image") {
      _action = "image";
    } else if (action == "feature_video") {
      _action = "video";
    } else if (action == "feature_print") {
      _action = "print";
    } else if (action == "feature_logo") {
      _action = "logo";
    }

    if (!this.isImageAdded) {
      this.alertCtrl
        .create({
          title: "WARNING",
          message: "Please select image to feature!",
          buttons: ["OK"]
        })
        .present();
      return;
    }

    if (_.isEmpty(selectedDatas)) {
      this.alertCtrl
        .create({
          title: "WARNING",
          message: "Please select image to feature!",
          buttons: ["OK"]
        })
        .present();
      return;
    }

    const confirm = this.alertCtrl.create({
      title: this.title + "?",
      message: "Are you sure to add the selected " + _action + "?",
      buttons: [
        {
          text: "No",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Yes",
          handler: () => {
            this.featureProductItem(_action, selectedDatas.id);
          }
        }
      ]
    });
    confirm.present();
  }

  uploadFiles() {
    console.log("uploadFiles");
    let vm = this;

    if (!this.isOnline) {
      this.alertCtrl
        .create({
          title: "WARNING",
          message: "No connection available.",
          buttons: ["OK"]
        })
        .present();
      return;
    }

    function addImagesToProduct() {
      let selectedDatas = _.filter(vm.imagesArr, (row: any) => {
        return row.selected === true;
      });
      selectedDatas = _.map(selectedDatas, (row: any) => {
        return row.id;
      });
      if (_.isEmpty(selectedDatas)) {
        vm.alertCtrl
          .create({
            title: "WARNING",
            message: "Unable to upload when no images are selected!",
            buttons: ["OK"]
          })
          .present();
        return;
      }

      const confirm = vm.alertCtrl.create({
        title: "Add Images to product?",
        message: "Are you sure to add the selected " + vm.title + "?",
        buttons: [
          {
            text: "No",
            handler: () => {
              console.log("Disagree clicked");
            }
          },
          {
            text: "Yes",
            handler: () => {
              vm.addImagesToProduct(selectedDatas);
            }
          }
        ]
      });
      confirm.present();
    }

    if (this.action === "media") {
      this.uploadImages();
    } else if (this.action === "logos") {
      this.uploadImages();
    } else if (this.action === "print" && this.platform.is("android")) {
      this.uploadImages();
    } else if (this.action === "print" && this.platform.is("ios")) {
      this.alertCtrl
        .create({
          title: "Upload not allowed",
          message: "Print file upload not allowed at this platform!",
          buttons: ["OK"]
        })
        .present();
      return;
    } else if (this.action === "videos") {
      this.uploadImages();
    } else if (this.action == "update_print_policy") {
      this.uploadImages();
    } else if (this.action == "update_video_policy") {
      this.uploadImages();
    } else if (this.action == "update_video_thumbnail_policy") {
      this.uploadImages();
    } else if (this.action == "gallery" && this.uploadMode == "upload") {
      this.uploadImages();
    } else if (this.action == "gallery" && this.uploadMode == "gallery") {
      addImagesToProduct();
    } else if (this.action == "feature_image") {
      this.featureUtilsItems("feature_image");
    } else if (this.action == "feature_video") {
      this.featureUtilsItems("feature_video");
    } else if (this.action == "feature_print") {
      this.featureUtilsItems("feature_print");
    } else if (this.action == "feature_logo") {
      this.featureUtilsItems("feature_logo");
    }
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  async ionViewDidLoad() {
    const business = await this.storageService.getStorage(
      this.storageService.storageKey.CURRENT_BUSINESS
    );
    this.business = !_.isEmpty(business) ? JSON.parse(business) || {} : {};
  }

  ionViewWillLeave() {
    console.log("FileUploadPage ionViewWillLeave");
    this.abort();
  }

  ionViewDidEnter() {
    console.log("FileUploadPage ionViewDidEnter: ", this.action);
    this.title = this.navParams.get("title");

    if (!this.isOnline) {
      this.alertCtrl
        .create({
          title: "WARNING",
          message: "No connection available.",
          buttons: ["OK"]
        })
        .present();
      return;
    }

    let loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    if (this.action === "media") {
      this.generateImagePolicy(loading);
    } else if (this.action === "logos") {
      this.generateLogoPolicy(loading);
    } else if (this.action === "print") {
      this.generatePrintPolicy(loading);
    } else if (this.action === "videos") {
      this.generateVideoPolicy(loading);
    } else if (this.action == "gallery") {
      if (this.uploadMode == "upload") {
        this.generateImagePolicy(loading);
      } else if (this.uploadMode == "gallery") {
        this.generateImageList();
      }
    } else if (this.action == "feature_image") {
      this.generateImageList();
    } else if (this.action == "feature_video") {
      this.generateVideoList();
    } else if (this.action == "feature_print") {
      this.generatePrintList();
    } else if (this.action == "feature_logo") {
      this.generateLogoList();
    } else if (this.action == "update_print_policy") {
      this.generateUpdatePrintPolicy(this.params.printId, loading);
    } else if (this.action == "update_video_policy") {
      this.generateUpdateVideoPolicy(this.params.videoId, loading);
    } else if (this.action == "update_video_thumbnail_policy") {
      this.generateUpdateVideoThumbnailPolicy(this.params.videoId, loading);
    }

    if (
      this.action == "gallery" ||
      this.action == "feature_image" ||
      this.action == "feature_video" ||
      this.action == "feature_print" ||
      this.action == "feature_logo"
    ) {
      this.showSetFeatureButton = true;
    } else {
      this.showSetFeatureButton = false;
    }
  }

  segmentChanged() {
    if (this.uploadMode == "upload") {
      let loading = this.loadingCtrl.create({
        dismissOnPageChange: true
      });
      this.generateImagePolicy(loading);
    }
  }

  checkChanged(item) {
    _.each(this.imagesArr, (row: any) => {
      if (this.action == "gallery") {
        if (item.id == row.id && this.showCheckbox) {
          row.selected = !row.selected;
        }
      } else {
        if (item.id == row.id && this.showCheckbox) {
          row.selected = !row.selected;
        } else {
          row.selected = false;
        }
      }
    });
    if (
      _.size(
        _.filter(this.imagesArr, (row: any) => {
          return row.selected === true;
        })
      ) > 0
    ) {
      this.isImageAdded = true;
    } else {
      this.isImageAdded = false;
    }
  }

  selectClick() {
    this.showCheckbox = true;
  }

  cancelClick() {
    this.showCheckbox = false;
  }

  doInfinite(ev) {
    if (this.action == "gallery" || this.action == "feature_image") {
      this.pageImage = this.pageImage + 1;
      this.generateImageList(ev);
    } else if (this.action == "feature_video") {
      this.pageVideo = this.pageVideo + 1;
      this.generateVideoList(ev);
    } else if (this.action == "feature_print") {
      this.pagePrint = this.pagePrint + 1;
      this.generatePrintList(ev);
    } else if (this.action == "feature_logo") {
      this.pageLogo = this.pageLogo + 1;
      this.generateLogoList(ev);
    } else {
      ev.complete();
    }
  }

  private validateUploadButton() {
    if (
      this.isImageAdded &&
      (this.action != "gallery" &&
        this.action != "feature_image" &&
        this.action != "feature_video" &&
        this.action != "feature_print" &&
        this.action != "feature_logo" &&
        this.action != "update_print_policy" &&
        this.action != "update_video_policy")
    ) {
      this.showUploadButton = true;
    } else {
      this.showUploadButton = false;
    }

    if (
      this.isImageAdded &&
      (this.action == "update_print_policy" ||
        this.action == "update_video_policy")
    ) {
      this.showUpdateFileButton = true;
    } else {
      this.showUpdateFileButton = false;
    }
  }

  private generateImagePolicy(loading?: any) {
    if (loading) {
      loading.present();
    }
    this.uploadService
      .upload_image_policy()
      .then(
        (data: any) => {
          if (data && data.success) {
            console.log("upload_image_policy: ", data.data);
            this.accessKey = data.data.accessKey;
            this.action_uri = data.data.action_uri;
            this.file_name = data.data.name;
            this.file_key = data.data.key;
            this.policy = data.data.policy;
            this.signature = data.data.signature;
            this.contentType = data.data.contentType;
            this.successActionStatus = data.data.successActionStatus;
            this.acl = data.data.acl;

            if (loading) {
              loading.dismiss();
            }
          } else {
            if (loading) {
              loading.dismiss();
            }
          }
        },
        error => {
          console.error("error: ", error);
          if (loading) {
            loading.dismiss();
          }
        }
      )
      .catch(error => {
        console.error("error: ", error);
        if (loading) {
          loading.dismiss();
        }
      });
  }

  private generatePrintPolicy(loading?: any) {
    if (loading) {
      loading.present();
    }
    this.uploadService
      .upload_print_policy()
      .then(
        (data: any) => {
          if (data && data.success) {
            this.accessKey = data.data.accessKey;
            this.action_uri = data.data.action_uri;
            this.file_name = data.data.name;
            this.file_key = data.data.key;
            this.policy = data.data.policy;
            this.signature = data.data.signature;
            this.contentType = data.data.contentType;
            this.successActionStatus = data.data.successActionStatus;
            this.acl = data.data.acl;

            if (loading) {
              loading.dismiss();
            }
          } else {
            if (loading) {
              loading.dismiss();
            }
          }
        },
        error => {
          console.error("error: ", error);
          if (loading) {
            loading.dismiss();
          }
        }
      )
      .catch(error => {
        console.error("error: ", error);
        if (loading) {
          loading.dismiss();
        }
      });
  }

  private generateLogoPolicy(loading?: any) {
    if (loading) {
      loading.present();
    }
    this.uploadService
      .upload_logo_policy()
      .then(
        (data: any) => {
          if (data && data.success) {
            this.accessKey = data.data.accessKey;
            this.action_uri = data.data.action_uri;
            this.file_name = data.data.name;
            this.file_key = data.data.key;
            this.policy = data.data.policy;
            this.signature = data.data.signature;
            this.contentType = data.data.contentType;
            this.successActionStatus = data.data.successActionStatus;
            this.acl = data.data.acl;

            console.log("upload_logo_policy: ", this.accessKey);

            if (loading) {
              loading.dismiss();
            }
          } else {
            if (loading) {
              loading.dismiss();
            }
          }
        },
        error => {
          console.error("error: ", error);
          if (loading) {
            loading.dismiss();
          }
        }
      )
      .catch(error => {
        console.error("error: ", error);
        if (loading) {
          loading.dismiss();
        }
      });
  }

  private generateVideoPolicy(loading?: any) {
    if (loading) {
      loading.present();
    }
    this.uploadService
      .upload_video_policy()
      .then(
        (data: any) => {
          if (data && data.success) {
            console.log("upload_video_policy: ", data);
            this.accessKey = data.data.accessKey;
            this.action_uri = data.data.action_uri;
            this.file_name = data.data.name;
            this.file_key = data.data.key;
            this.policy = data.data.policy;
            this.signature = data.data.signature;
            this.contentType = data.data.contentType;
            this.successActionStatus = data.data.successActionStatus;
            this.acl = data.data.acl;

            if (loading) {
              loading.dismiss();
            }
          } else {
            if (loading) {
              loading.dismiss();
            }
          }
        },
        error => {
          console.error("error: ", error);
          if (loading) {
            loading.dismiss();
          }
        }
      )
      .catch(error => {
        console.error("error: ", error);
        if (loading) {
          loading.dismiss();
        }
      });
  }

  private generateUpdatePrintPolicy(printId: any, loading?: any) {
    if (loading) {
      loading.present();
    }
    this.uploadService
      .update_print_policy(printId)
      .then(
        (data: any) => {
          if (data && data.success) {
            console.log("update_print_policy: ", data);
            this.accessKey = data.data.accessKey;
            this.action_uri = data.data.action_uri;
            this.file_name = data.data.name;
            this.file_key = data.data.key;
            this.policy = data.data.policy;
            this.signature = data.data.signature;
            this.contentType = data.data.contentType;
            this.successActionStatus = data.data.successActionStatus;
            this.acl = data.data.acl;

            if (loading) {
              loading.dismiss();
            }
          } else {
            if (loading) {
              loading.dismiss();
            }
          }
        },
        error => {
          console.error("error: ", error);
          if (loading) {
            loading.dismiss();
          }
        }
      )
      .catch(error => {
        console.error("error: ", error);
        if (loading) {
          loading.dismiss();
        }
      });
  }

  private generateUpdateVideoPolicy(videoId: any, loading?: any) {
    if (loading) {
      loading.present();
    }
    this.uploadService
      .update_video_policy(videoId)
      .then(
        (data: any) => {
          if (data && data.success) {
            console.log("update_video_policy: ", data);
            this.accessKey = data.data.accessKey;
            this.action_uri = data.data.action_uri;
            this.file_name = data.data.name;
            this.file_key = data.data.key;
            this.policy = data.data.policy;
            this.signature = data.data.signature;
            this.contentType = data.data.contentType;
            this.successActionStatus = data.data.successActionStatus;
            this.acl = data.data.acl;

            if (loading) {
              loading.dismiss();
            }
          } else {
            if (loading) {
              loading.dismiss();
            }
          }
        },
        error => {
          console.error("error: ", error);
          if (loading) {
            loading.dismiss();
          }
        }
      )
      .catch(error => {
        console.error("error: ", error);
        if (loading) {
          loading.dismiss();
        }
      });
  }

  private generateUpdateVideoThumbnailPolicy(videoId: any, loading?: any) {
    if (loading) {
      loading.present();
    }
    this.uploadService
      .update_video_thumbnail_policy(videoId)
      .then(
        (data: any) => {
          if (data && data.success) {
            console.log("update_video_thumbnail_policy: ", data);
            this.accessKey = data.data.accessKey;
            this.action_uri = data.data.action_uri;
            this.file_name = data.data.name;
            this.file_key = data.data.key;
            this.policy = data.data.policy;
            this.signature = data.data.signature;
            this.contentType = data.data.contentType;
            this.successActionStatus = data.data.successActionStatus;
            this.acl = data.data.acl;

            if (loading) {
              loading.dismiss();
            }
          } else {
            if (loading) {
              loading.dismiss();
            }
          }
        },
        error => {
          console.error("error: ", error);
          if (loading) {
            loading.dismiss();
          }
        }
      )
      .catch(error => {
        console.error("error: ", error);
        if (loading) {
          loading.dismiss();
        }
      });
  }

  private generateImageList(ev?: any) {
    this.showContent = false;
    this.imageService
      .image_list({
        page: this.pageImage,
        total: this.perPage
      })
      .then(
        (data: any) => {
          if (data && !_.isArray(data) && data.success) {
            console.log("data: ", data);
            this.zone.run(() => {
              this.pageImage = parseInt(data.data.currentPage);
              for (let i = 0; i < _.size(data.data.images); i++) {
                data.data.images[i].selected = false;
                this.imagesArr.push(data.data.images[i]);
              }
            });
          }
          this.showContent = true;
          if (ev) {
            ev.complete();
          }
        },
        error => {
          this.showContent = true;
          if (ev) {
            ev.complete();
          }
        }
      );
  }

  private generateVideoList(ev?: any) {
    this.showContent = false;
    this.videoService
      .video_list({
        page: this.pageImage,
        total: this.perPage
      })
      .then(
        (data: any) => {
          if (data && !_.isArray(data) && data.success) {
            console.log("data: ", data);
            this.zone.run(() => {
              this.pageImage = parseInt(data.data.currentPage);
              for (let i = 0; i < _.size(data.data.videos); i++) {
                data.data.videos[i].selected = false;
                this.imagesArr.push(data.data.videos[i]);
              }
            });
          }
          this.showContent = true;
          if (ev) {
            ev.complete();
          }
        },
        error => {
          this.showContent = true;
          if (ev) {
            ev.complete();
          }
        }
      );
  }

  private generatePrintList(ev?: any) {
    this.showContent = false;
    this.printService
      .brochure_list({
        page: this.pageImage,
        total: this.perPage
      })
      .then(
        (data: any) => {
          if (data && !_.isArray(data) && data.success) {
            console.log("data: ", data);
            this.zone.run(() => {
              this.pageImage = parseInt(data.data.currentPage);
              for (let i = 0; i < _.size(data.data.prints); i++) {
                data.data.prints[i].selected = false;
                this.imagesArr.push(data.data.prints[i]);
              }
            });
          }
          this.showContent = true;
          if (ev) {
            ev.complete();
          }
        },
        error => {
          this.showContent = true;
          if (ev) {
            ev.complete();
          }
        }
      );
  }

  private generateLogoList(ev?: any) {
    this.showContent = false;
    this.logoService
      .logos_list({
        page: this.pageImage,
        total: this.perPage
      })
      .then(
        (data: any) => {
          if (data && !_.isArray(data) && data.success) {
            console.log("data: ", data);
            this.zone.run(() => {
              this.pageImage = parseInt(data.data.currentPage);
              for (let i = 0; i < _.size(data.data.logos); i++) {
                data.data.logos[i].selected = false;
                this.imagesArr.push(data.data.logos[i]);
              }
            });
          }
          this.showContent = true;
          if (ev) {
            ev.complete();
          }
        },
        error => {
          this.showContent = true;
          if (ev) {
            ev.complete();
          }
        }
      );
  }
}
