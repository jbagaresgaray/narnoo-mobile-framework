import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController } from 'ionic-angular';

import { LoginServicesProvider } from '../../providers/services/login';


@IonicPage()
@Component({
	selector: 'page-forgot_password',
	templateUrl: 'forgot_password.html',
})
export class ForgotPasswordPage {
	users: any = {};

	constructor(public navCtrl: NavController, public navParams: NavParams, public toastCtrl: ToastController, public alertCtrl: AlertController, public services: LoginServicesProvider) {
		this.users.email = '';
	}

	backToLogin() {
		this.navCtrl.pop();
	}

	forgotPassword() {
		this.services.forgot_password(this.users.email).then((data: any) => {
			if (data && data.success) {
				let alert = this.alertCtrl.create({
					title: 'Success',
					subTitle: data.message,
					buttons: ['OK']
				});
				alert.present().then(() => {
					this.navCtrl.pop();
				});
			} else {
				let toast = this.toastCtrl.create({
					message: data.message,
					duration: 2000
				});
				toast.present();
			}
		}, (error) => {
			let toast = this.toastCtrl.create({
				message: error.message,
				duration: 2000
			});
			toast.present();
		});
	}
}
