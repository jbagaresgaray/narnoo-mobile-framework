import { Component, ViewEncapsulation, OnInit, NgZone } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ModalController,
  ToastController,
  App,
  ViewController,
  PopoverController,
  AlertController,
  ItemSliding,
  LoadingController,
  ActionSheetController,
  Events
} from "ionic-angular";
import { CacheService, Cache } from "ionic-cache-observable";
import { Observable, Subscription } from "rxjs";
import "rxjs/add/operator/finally";
import * as _ from "lodash";

import { ProductDetailPage } from "../product-detail/product-detail";
import { ChatPage } from "../chat/chat";
import { BusinessProfileSettingsPage } from "../business-profile-settings/business-profile-settings";
import { ProductsEntryPage } from "../products-entry/products-entry";
import { NotificationsPage } from "../notifications/notifications";

import { ProductsServicesProvider } from "../../providers/services/products";
import { NewsFeedServicesProvider } from "../../providers/services/newsfeed";

import { StorageProvider } from "../../providers/storage/storage";
import { LoadingProvider } from "../../providers/loading/loading";

@Component({
  template: `
    <ion-list>
      <button
        ion-item
        (click)="createProduct()"
        *ngIf="business.role == 1 || business.role == 2"
      >
        <ion-icon name="ios-add-outline" item-start></ion-icon>
        Create Product
      </button>
    </ion-list>
  `
})
export class ProductsTabPopoverPage implements OnInit {
  business: any = {};

  constructor(
    public app: App,
    public params: NavParams,
    public viewCtrl: ViewController,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController,
    public storageService: StorageProvider
  ) {}

  async ngOnInit() {
    const business = await this.storageService.getStorage(
      this.storageService.storageKey.CURRENT_BUSINESS
    );
    this.business = !_.isEmpty(business) ? JSON.parse(business) || {} : {};
    console.log("this.business: ", this.business);
  }

  close() {
    this.viewCtrl.dismiss();
  }

  viewChat() {
    this.viewCtrl.dismiss();
    const modal = this.modalCtrl.create(ChatPage);
    modal.present();
  }

  createProduct() {
    this.app.getRootNav().push(ProductsEntryPage, {
      action: "create"
    });
  }
}
@IonicPage()
@Component({
  selector: "page-product-tab",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "product-tab.html"
})
export class ProductTabPage {
  action: any;
  connect_params: any = {};

  business: any = {};
  productArr: any[] = [];
  productArrCopy: any[] = [];
  fakeArr: any[] = [];

  showContentProducts: boolean = false;
  showContentProductsErr: boolean = false;
  productsContentErr: any = {};

  pageProduct: number = 1;
  totalPageProduct: number = 0;
  perPage: number = 20;
  totalNotification: number = 0;

  productArr$: Observable<any>;
  public productCache: Cache<any>;
  public refreshSubscription: Subscription;

  constructor(
    public app: App,
    public events: Events,
    public zone: NgZone,
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController,
    public popoverCtrl: PopoverController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public actionSheetCtrl: ActionSheetController,
    public products: ProductsServicesProvider,
    public newsfeeds: NewsFeedServicesProvider,
    public storageService: StorageProvider,
    public loader: LoadingProvider,
    public cacheService: CacheService
  ) {
    this.action = navParams.get("action");
    this.connect_params = navParams.get("params");

    for (var i = 0; i < 12; ++i) {
      this.fakeArr.push(i);
    }

    events.unsubscribe("showNotifications");
    events.subscribe("showNotifications", count => {
      this.totalNotification = count;
    });

    try {
      this.newsfeeds.notificationData$.subscribe((data: any) => {
        console.log("activity feed notificationData: ", data);
        if (data && data.success) {
          this.zone.run(() => {
            this.newsfeeds.totalNotification = data.data.total;
            this.totalNotification = data.data.total;
          });
        } else {
          this.zone.run(() => {
            this.newsfeeds.totalNotification = 0;
            this.totalNotification = 0;
          });
        }
      });
    } catch (error) {
      this.zone.run(() => {
        this.newsfeeds.totalNotification = 0;
        this.totalNotification = 0;
      });
      this.events.publish("initNotifications");
    }
  }

  async ionViewDidLoad() {
    this.pageProduct = 1;
    this.initBusiness().then(() => {
      this.initData();
    });
  }

  private async initBusiness() {
    const business = await this.storageService.getStorage(
      this.storageService.storageKey.CURRENT_BUSINESS
    );
    this.business = !_.isEmpty(business) ? JSON.parse(business) || {} : {};
  }

  private initData(refresher?: any) {
    this.pageProduct = 1;
    this.showContentProducts = false;
    this.productArr = [];
    this.productArrCopy = [];

    let dataObservable: any;
    let cacheKey: any;

    if (this.action == "connect") {
      dataObservable = Observable.fromPromise(
        this.products.operator_products(this.connect_params.id, {
          page: this.pageProduct,
          total: this.perPage
        })
      );
      cacheKey =
        this.storageService.cacheKey.CONNECTED_PRODUCT_LIST +
        "_" +
        this.business.id +
        "_" +
        this.connect_params.type +
        "_" +
        this.connect_params.id +
        "_" +
        this.pageProduct;
    } else {
      dataObservable = Observable.fromPromise(
        this.products.product_list({
          page: this.pageProduct,
          total: this.perPage
        })
      );
      cacheKey =
        this.storageService.cacheKey.CONNECTED_PRODUCT_LIST +
        "_" +
        this.business.id +
        "_" +
        this.pageProduct;
    }

    this.cacheService.register(cacheKey, dataObservable).subscribe(cache => {
      this.productCache = cache;
      this.zone.run(() => {
        this.productArr$ = cache.get$;
      });
      this.productCache.refresh().subscribe(()=>{
        this.loader.start();
      },()=>{
        this.loader.complete();
      },()=>{
        this.loader.complete();
      });
    });

    this.productArr$.subscribe(
      (item: any) => {
        this.productArr = [];
        this.productArrCopy = [];

        const _data = item.data;
        const products = _data.products;
        this.pageProduct = parseInt(_data.currentPage);
        this.totalPageProduct = parseInt(_data.totalPages);

        for (let i = 0; i < _.size(products); i++) {
          products[i].selected = false;
          this.productArr.push(products[i]);
        }
        this.productArrCopy = _.cloneDeep(this.productArr);
        this.showContentProducts = true;

        if(refresher){
          refresher.complete();
        }
      },
      error => {
        this.showContentProducts = true;
        this.showContentProductsErr = true;
        if(refresher){
          refresher.complete();
        }
        if (error && !error.success) {
          this.productsContentErr = error;
        }
      }
    );
  }

  doRefresh(refresher: any) {
    if (this.productCache) {
      this.pageProduct = 1;
      this.showContentProducts = false;
      this.productArr = [];
      this.productArrCopy = [];

      this.zone.run(() => {
        this.initData(refresher);
      });
    } else {
      console.warn("Cache has not been initialized.");
      if (refresher) {
        refresher.cancel();
      }
    }
  }

  doInfinite(refresher: any) {
    if (this.refreshSubscription) {
      console.warn("Already refreshing.");
      refresher.cancel();
      return;
    }

    this.pageProduct = this.pageProduct + 1;
    if (this.pageProduct <= this.totalPageProduct) {
      this.initData();
    } else {
      refresher.complete();
    }
  }

  gotoDetails(item) {
    this.navCtrl.push(ProductDetailPage, {
      product: item,
      action: this.action,
      params: this.connect_params
    });
  }

  presentPopover(ev) {
    if (this.business.role !== 1) {
      const toast = this.toastCtrl.create({
        message: "Invalid Access!!!",
        duration: 3000
      });
      toast.present();
      return;
    }

    const popover = this.popoverCtrl.create(ProductsTabPopoverPage);
    popover.present({ ev: ev });
  }

  viewChat() {
    const modal = this.modalCtrl.create(ChatPage);
    modal.present();
  }

  viewProfile() {
    this.app.getRootNav().push(BusinessProfileSettingsPage);
  }

  viewNotifications() {
    this.app.getRootNav().push(NotificationsPage);
  }

  updateProduct(item: any, slidingItem: ItemSliding) {
    slidingItem.close();

    if (this.business.role !== 1) {
      const toast = this.toastCtrl.create({
        message: "Invalid Access!!!",
        duration: 3000
      });
      toast.present();
      return;
    }

    const _updateProduct = () => {
      const modal = this.modalCtrl.create(ProductsEntryPage, {
        product: item,
        action: "update"
      });
      modal.onDidDismiss((resp: any) => {
        if (resp == "save") {
          this.doRefresh(null);
        }
      });
      modal.present();
    };

    const _markProductPrimary = () => {
      const loading = this.loadingCtrl.create({
        dismissOnPageChange: true
      });
      loading.present();
      this.products
        .product_update_primary({
          productId: item.productId
        })
        .then(
          (data: any) => {
            if (data && data.success) {
              this.productArr = [];
              this.productArrCopy = [];
              this.doRefresh(null);
            }
            loading.dismiss();
          },
          error => {
            loading.dismiss();
          }
        );
    };

    const buttonsArr: any[] = [
      {
        text: "Update Product",
        handler: () => {
          _updateProduct();
        }
      },
      {
        text: "Cancel",
        role: "cancel",
        handler: () => {
          console.log("Cancel clicked");
        }
      }
    ];

    if (!item.primary) {
      buttonsArr.unshift({
        text: "Set As Primary",
        handler: () => {
          _markProductPrimary();
        }
      });
    }

    const actionSheet = this.actionSheetCtrl.create({
      title: "Choose Product Action",
      buttons: buttonsArr
    });
    actionSheet.present();
  }

  deleteProduct(item: any, slidingItem: ItemSliding) {
    slidingItem.close();

    const _deleteProduct = () => {
      const loading = this.loadingCtrl.create({
        dismissOnPageChange: true,
        content: "Deleting..."
      });
      loading.present();
      this.products
        .product_delete({
          productId: item.productId
        })
        .then(
          (data: any) => {
            if (data && data.success) {
              this.productArr = [];
              this.productArrCopy = [];
              this.doRefresh(null);
            }
            loading.dismiss();
          },
          error => {
            console.log("error: ", error);
            loading.dismiss();
          }
        );
    };

    const confirm = this.alertCtrl.create({
      title: "Delete Product?",
      message: "Delete selected product?",
      buttons: [
        {
          text: "Cancel",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Confirm",
          handler: () => {
            console.log("Agree clicked");
            _deleteProduct();
          }
        }
      ]
    });
    confirm.present();
  }

  createProduct() {
    if (this.business.role == 3 && this.business.role == 4) {
      const toast = this.toastCtrl.create({
        message: "Invalid Access!!!",
        duration: 3000
      });
      toast.present();
      return;
    }

    /* this.app.getRootNav().push(ProductsEntryPage, {
      action: 'create'
    }); */
    const modal = this.modalCtrl.create(ProductsEntryPage, {
      action: "create"
    });
    modal.onDidDismiss((resp: any) => {
      if (resp == "save") {
        this.doRefresh(null);
      }
    });
    modal.present();
  }
}
