import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CacheModule } from "ionic-cache-observable";
import { IonicImageLoader } from "ionic-image-loader";

import { PipesModule } from '../../pipes/pipes.module';
import { ComponentsModule } from '../../components/components.module';

import { ProductTabPage, ProductsTabPopoverPage } from './product-tab';

@NgModule({
	declarations: [
		ProductTabPage,
		ProductsTabPopoverPage
	],
	imports: [
		CacheModule,
		IonicImageLoader,
		IonicPageModule.forChild(ProductTabPage),
		PipesModule,
		ComponentsModule
	],
	entryComponents: [
		ProductsTabPopoverPage
	]
})
export class ProductTabPageModule { }
