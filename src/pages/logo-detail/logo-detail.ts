import { Component, ViewEncapsulation, NgZone, OnInit } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  AlertController,
  Platform,
  ActionSheetController,
  ModalController,
  PopoverController,
  ViewController,
  Events,
  ToastController
} from "ionic-angular";
import * as _ from "lodash";
import * as $ from "jquery";
import { CacheService, Cache } from "ionic-cache-observable";
import { Observable, Subscription } from "rxjs";
import "rxjs/add/operator/finally";

import { FileTransfer, FileTransferObject } from "@ionic-native/file-transfer";
import { LocalNotifications } from "@ionic-native/local-notifications";
import { InAppBrowser } from "@ionic-native/in-app-browser";
import { SocialSharing } from "@ionic-native/social-sharing";

import { ProductsModalPage } from "../../pages/products-modal/products-modal";
import { ChatPage } from "../../pages/chat/chat";

import { LogoServicesProvider } from "../../providers/services/logos";

import { environment } from "../../environments/environment";
import { StorageProvider } from "../../providers/storage/storage";
import { LoadingProvider } from "../../providers/loading/loading";

declare const safari: any;

@Component({
  template: `
    <ion-list>
      <button ion-item (click)="downloadImage()" [disabled]="showLoading">
        <ion-icon
          ios="ios-download-outline"
          md="ios-download-outline"
          item-start
        ></ion-icon>
        Download
      </button>
      <button
        ion-item
        (click)="shareNow()"
        *ngIf="
          action != 'connect' &&
          action != 'notif' &&
          (business.role == 1 || business.role == 2)
        "
      >
        <ion-icon name="md-share" item-start></ion-icon>
        Share
      </button>
    </ion-list>
  `
})
export class LogoDetailPopoverPage implements OnInit {
  showDownload: boolean;
  showLoading: boolean;
  action: any;
  business: any = {};

  constructor(
    public params: NavParams,
    public viewCtrl: ViewController,
    public events: Events,
    public storageService: StorageProvider
  ) {
    this.showDownload = params.get("showDownload");
    this.action = params.get("action");
    this.showLoading = params.get("showLoading");
  }

  async ngOnInit() {
    const business = await this.storageService.getStorage(
      this.storageService.storageKey.CURRENT_BUSINESS
    );
    this.business = !_.isEmpty(business) ? JSON.parse(business) || {} : {};
  }

  close() {
    this.viewCtrl.dismiss();
  }

  viewChat() {
    this.events.publish("LogoDetailPage:viewChat");
    this.viewCtrl.dismiss();
  }

  downloadImage() {
    this.events.publish("LogoDetailPage:downloadImage");
    this.viewCtrl.dismiss();
  }

  shareNow() {
    this.events.publish("LogoDetailPage:shareNow");
    this.viewCtrl.dismiss();
  }
}
@IonicPage()
@Component({
  selector: "page-logo-detail",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "logo-detail.html"
})
export class LogoDetailPage {
  item: any = {};
  params: any = {};
  business: any = {};
  notif_business: any = {};
  showDownload: boolean;
  showLoading: boolean = true;
  showError: boolean = false;
  isSafari: boolean = false;
  action: any;
  connect_params: any = {};
  errContent: string = "";
  callback: any;

  storageDirectory: string = "";
  fileTransfer: FileTransferObject;

  mediaArr$: Observable<any>;
  private mediaCache: Cache<any>;
  public refreshSubscription: Subscription;

  constructor(
    public platform: Platform,
    public navCtrl: NavController,
    public navParams: NavParams,
    public logos: LogoServicesProvider,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public actionSheetCtrl: ActionSheetController,
    private transfer: FileTransfer,
    private localNotifications: LocalNotifications,
    public zone: NgZone,
    public modalCtrl: ModalController,
    public popoverCtrl: PopoverController,
    public events: Events,
    private iab: InAppBrowser,
    private socialSharing: SocialSharing,
    public storageService: StorageProvider,
    public cacheService: CacheService,
    public loader: LoadingProvider
  ) {
    this.action = navParams.get("action");
    this.connect_params = navParams.get("params");
    this.callback = navParams.get("callback");
    this.params = navParams.get("logo");
    this.notif_business = navParams.get("business");

    this.isSafari =
      /constructor/i.test(window["HTMLElement"]) ||
      (function(p) {
        return p.toString() === "[object SafariRemoteNotification]";
      })(!window["safari"] || safari.pushNotification);

    if (platform.is("cordova")) {
      if (this.platform.is("ios")) {
        this.storageDirectory = window["cordova"].file.documentsDirectory;
      } else if (this.platform.is("android")) {
        this.storageDirectory =
          window["cordova"].file.externalRootDirectory + environment.AlbumName;
      }
    }

    // ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //
    // ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //
    // ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //

    events.unsubscribe("LogoDetailPage:viewChat");
    events.unsubscribe("LogoDetailPage:downloadImage");
    events.unsubscribe("LogoDetailPage:shareNow");

    events.subscribe("LogoDetailPage:downloadImage", () => {
      this.downloadImage();
    });

    events.subscribe("LogoDetailPage:viewChat", () => {
      this.viewChat();
    });

    events.subscribe("LogoDetailPage:shareNow", () => {
      this.shareNow();
    });
  }

  private async initBusiness() {
    const business = await this.storageService.getStorage(
      this.storageService.storageKey.CURRENT_BUSINESS
    );
    this.business = !_.isEmpty(business) ? JSON.parse(business) || {} : {};
  }

  private initData(refresher?: any) {
    const successResponse = (data: any) => {
      if (data && data.success) {
        const item = data.data;

        this.zone.run(() => {
          this.item = item;
          console.log("logo: ", this.item);

          if (this.platform.is("cordova")) {
            if (this.platform.is("ios")) {
              if (this.item.uploadedAt) {
                this.item.uploadedAt = new Date(
                  this.item.uploadedAt.replace(/-/g, "/")
                );
              }
            }
          } else {
            if (this.platform.is("ios")) {
              if (this.isSafari) {
                if (this.item.uploadedAt) {
                  this.item.uploadedAt = new Date(
                    this.item.uploadedAt.replace(/-/g, "/")
                  );
                }
              } else {
                if (this.item.uploadedAt) {
                  this.item.uploadedAt = new Date(this.item.uploadedAt);
                }
              }
            }
          }
        });
      }
      this.showLoading = false;
      this.showError = false;
      if (refresher) {
        refresher.complete();
      }
      setTimeout(() => {
        $(".progressive-image").each(function() {
          const image = new Image();
          const previewImage = $(this).find(".loadingImage");
          const newImage = $(this).find(".overlay");

          image.onload = function() {
            newImage.css("background-image", "url(" + image.src + ")");
            newImage.css("opacity", "1");
          };
          image.onerror = function() {
            newImage.css("background-image", "url(./assets/img/thumb.jpg)");
            newImage.css("opacity", "1");
          };
          image.src = previewImage.data("image");
        });
      }, 1000);
    };

    const errorResponse = error => {
      this.showLoading = false;
      this.showError = true;
      if (refresher) {
        refresher.complete();
      }
      if (error) {
        this.errContent = error.message || "No logo media found!";
      }
    };

    let dataObservable: any;
    let cacheKey: any;
    this.showLoading = true;

    if (this.action == "connect") {
      if (this.connect_params.type == "operator") {
        dataObservable = Observable.fromPromise(
          this.logos.logos_operator_detail(
            this.connect_params.id,
            this.params.id
          )
        );
        cacheKey =
          this.storageService.cacheKey.MEDIA_LOGO_DETAIL +
          "_" +
          this.business.id +
          "_operator_" +
          this.connect_params.id +
          "_" +
          this.params.id;
      } else if (this.connect_params.type == "distributor") {
        dataObservable = Observable.fromPromise(
          this.logos.logos_distributor_detail(
            this.connect_params.id,
            this.params.id
          )
        );
        cacheKey =
          this.storageService.cacheKey.MEDIA_LOGO_DETAIL +
          "_" +
          this.business.id +
          "_distributor_" +
          this.connect_params.id +
          "_" +
          this.params.id;
      }
    } else if (this.action == "notif") {
      dataObservable = Observable.fromPromise(
        this.logos.logos_detail(this.params.id)
      );
      cacheKey =
        this.storageService.cacheKey.MEDIA_LOGO_DETAIL + "_" + this.params.id;
    } else {
      dataObservable = Observable.fromPromise(
        this.logos.logos_detail(this.params.id)
      );
      cacheKey =
        this.storageService.cacheKey.MEDIA_LOGO_DETAIL +
        "_" +
        this.business.id +
        "_" +
        this.params.id;
    }

    this.cacheService.register(cacheKey, dataObservable).subscribe(cache => {
      this.mediaCache = cache;
      this.zone.run(() => {
        this.mediaArr$ = cache.get$;

        this.mediaCache.refresh().subscribe(
          () => {
            this.loader.start();
          },
          () => {
            this.loader.complete();
          },
          () => {
            this.loader.complete();
          }
        );
      });
    });

    this.mediaArr$.subscribe(successResponse, errorResponse);
  }

  async ionViewDidLoad() {
    this.initBusiness().then(() => {
      this.initData();
    });
  }

  ionViewWillLeave() {
    if (this.fileTransfer) {
      this.fileTransfer.abort();
    }
  }

  doRefresh(refresher: any) {
    if (this.mediaCache) {
      this.showLoading = true;
      this.zone.run(() => {
        this.initData(refresher);
      });
    } else {
      console.warn("Cache has not been initialized.");
      if (refresher) {
        refresher.cancel();
      }
    }
  }

  presentPopover(ev) {
    const popover = this.popoverCtrl.create(LogoDetailPopoverPage, {
      showDownload: this.business.paid,
      action: this.action,
      showLoading: this.showDownload
    });
    popover.present({ ev: ev });
  }

  updateImageDetail() {
    if (this.business.role !== 1) {
      const toast = this.toastCtrl.create({
        message: "Invalid Access!!!",
        duration: 3000
      });
      toast.present();
      return;
    }

    /* this.navCtrl.push(ImageDetailInfoPage, {
    image: this.item
    }) */
  }

  presentActionSheet() {
    if (this.business.role !== 1) {
      const toast = this.toastCtrl.create({
        message: "Invalid Access!!!",
        duration: 3000
      });
      toast.present();
      return;
    }

    const actionSheet = this.actionSheetCtrl.create({
      title: "Select action for Logo",
      buttons: [
        {
          text: "Add to Product as Feature",
          handler: () => {
            console.log("Archive clicked");
            this.addToProductFeature();
          }
        },
        {
          text: "Set As Business Main Logo Feature",
          handler: () => {
            console.log("setFeature clicked");
            this.setFeature();
          }
        },
        {
          text: "Delete",
          role: "destructive",
          handler: () => {
            console.log("Archive clicked");
            setTimeout(() => {
              this.deleteLogo();
            }, 300);
          }
        },
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        }
      ]
    });
    actionSheet.present();
  }

  private setFeature() {
    if (this.business.role !== 1) {
      const toast = this.toastCtrl.create({
        message: "Invalid Access!!!",
        duration: 3000
      });
      toast.present();
      return;
    }

    const loading = this.loadingCtrl.create({
      content: "Setting as feature..."
    });
    loading.present();
    this.logos.logos_feature(this.item).then(
      (data: any) => {
        if (data && data.success) {
          loading.dismiss();
          const alert = this.alertCtrl.create({
            title: "Success",
            subTitle: data.data,
            buttons: ["OK"]
          });
          alert.present();
        }
      },
      error => {
        console.log("error: ", error);
        loading.dismiss();
      }
    );
  }

  private addToProductFeature() {
    if (this.business.role !== 1) {
      const toast = this.toastCtrl.create({
        message: "Invalid Access!!!",
        duration: 3000
      });
      toast.present();
      return;
    }

    const modal = this.modalCtrl.create(ProductsModalPage, {
      image: this.item,
      action: "feature",
      type: "logo"
    });
    modal.present();
  }

  private deleteLogo() {
    if (this.business.role !== 1) {
      const toast = this.toastCtrl.create({
        message: "Invalid Access!!!",
        duration: 3000
      });
      toast.present();
      return;
    }

    const loading = this.loadingCtrl.create({
      dismissOnPageChange: true,
      content: "Deleting..."
    });
    loading.present();
    this.logos.logos_delete([this.item.id]).then(
      (data: any) => {
        if (data && data.success) {
          console.log("logos_delete: ", data);
          const alert = this.alertCtrl.create({
            title: "SUCCESS",
            message: "Logo successfully deleted!",
            buttons: ["OK"]
          });
          alert.onDidDismiss(() => {
            this.callback("refresh").then(() => {
              this.navCtrl.pop();
            });
          });
          alert.present();
        }
        loading.dismiss();
      },
      (error: any) => {
        loading.dismiss();
        console.log("error: ", error);
      }
    );
  }

  downloadImage() {
    const ctrl = this;

    if (!this.business.paid) {
      this.alertCtrl
        .create({
          title: "WARNING",
          message: "This feature is only available to premium users only.",
          buttons: ["OK"]
        })
        .present();
      return;
    }

    const loading = ctrl.loadingCtrl.create({
      content: "Downloading ...",
      dismissOnPageChange: true
    });
    loading.present();

    const alert = ctrl.alertCtrl.create({
      title: "Success",
      subTitle: "Logo successfully downloaded",
      buttons: ["OK"]
    });

    let perc: any;

    const download = (dataurl, filename) => {
      const a = document.createElement("a");
      a.href = dataurl;
      a.setAttribute("download", filename);
      const b = document.createEvent("MouseEvents");
      b.initEvent("click", false, true);
      a.dispatchEvent(b);
      return false;
    };

    const downloadNative = (image, filename) => {
      ctrl.platform.ready().then(() => {
        ctrl.fileTransfer = ctrl.transfer.create();
        const newDir = ctrl.storageDirectory + "/" + filename;
        console.log("newDir: ", newDir);

        ctrl.fileTransfer
          .download(image, newDir)
          .then(
            entry => {
              console.log("entry: ", entry);
              console.log("entry.toURL(); ", entry.toURL());

              if (entry) {
                const alertSuccess = ctrl.alertCtrl.create({
                  title: `Download Succeeded!`,
                  subTitle: "Download successfully",
                  buttons: ["Ok"]
                });

                loading.dismiss();
                alertSuccess.present();
              }
            },
            error => {
              ctrl.fileTransfer.abort();
              const alertFailure = ctrl.alertCtrl.create({
                title: `Download Failed!`,
                subTitle: `${image} was not successfully downloaded. Error code: ${
                  error.code
                }`,
                buttons: ["Ok"]
              });
              loading.dismiss();
              alertFailure.present();
            }
          )
          .catch(error => {
            ctrl.fileTransfer.abort();
            loading.dismiss();
          });

        ctrl.fileTransfer.onProgress(progressEvent => {
          if (progressEvent.lengthComputable) {
            perc = Math.floor(
              (progressEvent.loaded / progressEvent.total) * 100
            );
            console.log("Progress: ", perc + "% loaded...");

            ctrl.zone.run(() => {
              loading.setContent("Downloading " + perc + "% ...");

              if (ctrl.platform.is("android")) {
                ctrl.localNotifications.schedule({
                  title: "Downloading " + filename,
                  text: "Downloading " + perc + "% ...",
                  sound: null,
                  progressBar: { value: perc }
                });
              }
            });
          }
        });
      });
    };

    const getFilename = url => {
      url = url
        .split("/")
        .pop()
        .replace(/\#(.*?)$/, "")
        .replace(/\?(.*?)$/, "");
      url = url.split("."); // separates filename and extension
      return { filename: url[0] || "", ext: url[1] || "" };
    };

    this.logos.logos_download(this.item.id).then(
      (data: any) => {
        if (data && data.success) {
          console.log("logos_download: ", data.data);
          let file: any;
          if (data.data && data.data.length > 0) {
            file = data.data[0];
          } else {
            file = data.data;
          }

          const filename =
            getFilename(file).filename + "." + getFilename(file).ext;
          if (!this.platform.is("cordova")) {
            download(
              file,
              getFilename(file).filename + "." + getFilename(file).ext
            );
            loading.dismiss();
            alert.present();
          } else {
            downloadNative(file, filename);
          }
        }
      },
      error => {
        loading.dismiss();
        if (error && !error.success) {
          this.alertCtrl
            .create({
              title: "WARNING",
              message: "Error while downloading file. " + error.message,
              buttons: ["OK"]
            })
            .present();
          return;
        }
      }
    );
  }

  viewChat() {
    const modal = this.modalCtrl.create(ChatPage);
    modal.present();
  }

  openLink(link) {
    if (link) {
      link = link.startsWith("//") ? "http:" + link : link;
      this.iab.create(link, "_blank");
    } else {
      const alert = this.alertCtrl.create({
        title: "Invalid URL",
        subTitle: "Invalid URL format",
        buttons: ["OK"]
      });
      alert.present();
    }
  }

  shareNow() {
    if (!this.business.paid) {
      this.alertCtrl
        .create({
          title: "WARNING",
          message: "This feature is only available to premium users only.",
          buttons: ["OK"]
        })
        .present();
      return;
    }

    if (!this.platform.is("cordova")) {
      this.alertCtrl
        .create({
          title: "WARNING",
          message:
            "Social Sharing is only available on actual device/simulator",
          buttons: ["OK"]
        })
        .present();
      return;
    }

    const loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    loading.present();
    this.logos.share_url_logo(this.item.id).then(
      (data: any) => {
        if (data && data.success) {
          console.log("data: ", data);
          this.socialSharing
            .share(
              "This link allows you to download the original version of this image. " +
                "You can send the link to whom ever you need and this will allow them access to the original file.",
              "Share",
              null,
              data.data
            )
            .then(() => {
              // Success!
            })
            .catch(() => {
              // Error!
            });
          loading.dismiss();
        } else {
          loading.dismiss();
        }
      },
      error => {
        console.log("error: ", error);
        loading.dismiss();
      }
    );
  }
}
