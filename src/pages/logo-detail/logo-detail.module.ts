import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LogoDetailPage,LogoDetailPopoverPage } from './logo-detail';

import { IonicImageViewerModule } from 'ionic-img-viewer';
import { IonicImageLoader } from "ionic-image-loader";
import { PipesModule } from '../../pipes/pipes.module';
import { ComponentsModule } from '../../components/components.module';
@NgModule({
  declarations: [
    LogoDetailPage,
    LogoDetailPopoverPage
  ],
  imports: [
    IonicPageModule.forChild(LogoDetailPage),
    IonicPageModule.forChild(LogoDetailPopoverPage),
    IonicImageViewerModule,
    PipesModule,
    ComponentsModule,
    IonicImageLoader
  ],
})
export class LogoDetailPageModule {}
