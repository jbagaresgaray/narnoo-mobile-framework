import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { ReactiveFormsModule } from "@angular/forms";

import { StaffDirectoryEntryPage } from "./staff-directory-entry";

@NgModule({
  declarations: [StaffDirectoryEntryPage],
  imports: [
    ReactiveFormsModule,
    IonicPageModule.forChild(StaffDirectoryEntryPage)
  ]
})
export class StaffDirectoryEntryPageModule {}
