import { Component, ViewEncapsulation } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ViewController,
  AlertController,
  LoadingController
} from "ionic-angular";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from "@angular/forms";

import { StaffServicesProvider } from "../../providers/services/staff";

@IonicPage()
@Component({
  selector: "page-staff-directory-entry",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "staff-directory-entry.html"
})
export class StaffDirectoryEntryPage {
  token: string;
  action: string;
  item: any = {};
  type: any;

  isSaving = false;
  submitted = false;

  inputForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public staffs: StaffServicesProvider
  ) {
    this.action = navParams.get("action");
    this.type = navParams.get("type");
    this.item = navParams.get("item") || {};
    console.log("this.item: ", this.item);

    this.inputForm = this.formBuilder.group({
      name: ["", Validators.required],
      title: ["", Validators.required],
      email: ["", Validators.required],
      phone: [""]
    });
  }

  get f(): any {
    return this.inputForm.controls;
  }

  private validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad StaffDirectoryEntryPage");
    if (this.item) {
      this.inputForm.patchValue(this.item);
    }
  }

  close() {
    this.viewCtrl.dismiss();
  }

  saveStaff() {
    this.submitted = true;
    this.isSaving = true;

    if (this.inputForm.invalid) {
      this.isSaving = false;
      this.validateAllFormFields(this.inputForm);
      return;
    }

    const saveStaffEntry = () => {
      const loading = this.loadingCtrl.create({
        dismissOnPageChange: true
      });
      loading.present();
      if (this.action === "add") {
        this.staffs.staff_add(this.inputForm.value).then(
          (data: any) => {
            if (data && data.success) {
              this.submitted = false;
              this.isSaving = false;

              loading.dismiss();
              const alert = this.alertCtrl.create({
                title: "Staff Entry",
                subTitle: "Staff successfully saved",
                buttons: ["OK"]
              });
              alert.present();
              alert.onDidDismiss(() => {
                this.viewCtrl.dismiss("save");
              });
            } else if (data && !data.success) {
              this.submitted = false;
              this.isSaving = false;

              loading.dismiss();
              const alert = this.alertCtrl.create({
                title: "WARNING",
                subTitle: data.message,
                buttons: ["OK"]
              });
              alert.present();
            }
          },
          error => {
            console.log("eRror: ", error);
            this.submitted = false;
            this.isSaving = false;

            loading.dismiss();
            if (error && !error.success) {
              const alert = this.alertCtrl.create({
                title: "WARNING",
                subTitle: error.message,
                buttons: ["OK"]
              });
              alert.present();
            }
          }
        );
      } else {
        this.staffs.staff_edit(this.item).then(
          (data: any) => {
            if (data && data.success) {
              this.submitted = false;
              this.isSaving = false;

              loading.dismiss();
              const alert = this.alertCtrl.create({
                title: "Staff Entry",
                subTitle: "Staff successfully saved",
                buttons: ["OK"]
              });
              alert.present();
              alert.onDidDismiss(() => {
                this.viewCtrl.dismiss();
              });
            } else if (data && !data.success) {
              this.submitted = false;
              this.isSaving = false;

              loading.dismiss();
              const alert = this.alertCtrl.create({
                title: "WARNING",
                subTitle: data.message,
                buttons: ["OK"]
              });
              alert.present();
            }
          },
          error => {
            console.log("eRror: ", error);
            loading.dismiss();
            this.submitted = false;
            this.isSaving = false;

            if (error && !error.success) {
              const alert = this.alertCtrl.create({
                title: "WARNING",
                subTitle: error.message,
                buttons: ["OK"]
              });
              alert.present();
            }
          }
        );
      }
    };

    const confirm = this.alertCtrl.create({
      title: "Save Entry?",
      message: "Do you want to save the entry?",
      buttons: [
        {
          text: "Cancel",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Confirm",
          handler: () => {
            console.log("Agree clicked");
            saveStaffEntry();
          }
        }
      ]
    });
    confirm.present();
  }
}
