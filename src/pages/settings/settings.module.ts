import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { IonicImageLoader } from "ionic-image-loader";
import { CacheModule } from "ionic-cache-observable";

import { SettingsPage, SettingsTabPopoverPage } from "./settings";

import { PipesModule } from "../../pipes/pipes.module";
import { ComponentsModule } from "../../components/components.module";

@NgModule({
  declarations: [SettingsPage, SettingsTabPopoverPage],
  imports: [
    PipesModule,
    ComponentsModule,
    IonicImageLoader,
    CacheModule,
    IonicPageModule.forChild(SettingsPage)
  ],
  entryComponents: [SettingsTabPopoverPage]
})
export class SettingsPageModule {}
