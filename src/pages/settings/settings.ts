import { Component, ViewEncapsulation, NgZone } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ModalController,
  ToastController,
  App,
  ViewController,
  PopoverController,
  Events
} from "ionic-angular";
import * as _ from "lodash";

import { StaffDirectoryPage } from "../../pages/staff-directory/staff-directory";
import { BusinessUsersPage } from "../../pages/business-users/business-users";
import { BusinessProfileSettingsPage } from "../../pages/business-profile-settings/business-profile-settings";
import { ChatPage } from "../../pages/chat/chat";
import { LocationPage } from "../../pages/location/location";
import { CreatorsPage } from "../../pages/creators/creators";
import { SocialLinksPage } from "../../pages/social-links/social-links";
import { NotificationsPage } from "../notifications/notifications";

import { NewsFeedServicesProvider } from "../../providers/services/newsfeed";
import { StorageProvider } from "../../providers/storage/storage";

@Component({
  template: `
    <ion-list>
      <button ion-item (click)="viewChat()">
        <ion-icon name="ios-chatbubbles-outline" item-start></ion-icon>
        Chat Now
      </button>
    </ion-list>
  `
})
export class SettingsTabPopoverPage {
  business: any = {};

  constructor(
    public app: App,
    public params: NavParams,
    public viewCtrl: ViewController,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController,
    public storageService: StorageProvider
  ) {
    this.initBusiness();
  }

  private async initBusiness() {
    const business = await this.storageService.getStorage(
      this.storageService.storageKey.CURRENT_BUSINESS
    );
    this.business = !_.isEmpty(business) ? JSON.parse(business) || {} : {};
  }

  close() {
    this.viewCtrl.dismiss();
  }

  viewChat() {
    this.viewCtrl.dismiss();
    const modal = this.modalCtrl.create(ChatPage);
    modal.present();
  }
}
@IonicPage()
@Component({
  selector: "page-settings",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "settings.html"
})
export class SettingsPage {
  business: any = {};
  totalNotification: number = 0;

  constructor(
    public app: App,
    public events: Events,
    public zone: NgZone,
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController,
    public popoverCtrl: PopoverController,
    public newsfeeds: NewsFeedServicesProvider,
    public storageService: StorageProvider
  ) {
    events.unsubscribe("showNotifications");
    events.subscribe("showNotifications", count => {
      this.totalNotification = count;
    });

    try {
      this.newsfeeds.notificationData$.subscribe((data: any) => {
        console.log("activity feed notificationData: ", data);
        if (data && data.success) {
          this.zone.run(() => {
            this.newsfeeds.totalNotification = data.data.total;
            this.totalNotification = data.data.total;
          });
        } else {
          this.zone.run(() => {
            this.newsfeeds.totalNotification = 0;
            this.totalNotification = 0;
          });
        }
      });
    } catch (error) {
      this.zone.run(() => {
        this.newsfeeds.totalNotification = 0;
        this.totalNotification = 0;
      });
      this.events.publish("initNotifications");
    }
  }

  private async initBusiness() {
    const business = await this.storageService.getStorage(
      this.storageService.storageKey.CURRENT_BUSINESS
    );
    this.business = !_.isEmpty(business) ? JSON.parse(business) || {} : {};
  }

  ionViewDidLoad() {
    this.initBusiness();
  }

  presentPopover(ev) {
    const popover = this.popoverCtrl.create(SettingsTabPopoverPage);
    popover.present({ ev: ev });
  }

  viewChat() {
    const modal = this.modalCtrl.create(ChatPage);
    modal.present();
  }

  openStaff() {
    this.app.getRootNav().push(StaffDirectoryPage);
  }

  openBusinessUsers() {
    this.app.getRootNav().push(BusinessUsersPage);
  }

  openBusinessProfile() {
    this.app.getRootNav().push(BusinessProfileSettingsPage);
  }

  openLocations() {
    this.app.getRootNav().push(LocationPage);
  }

  openCreators() {
    this.app.getRootNav().push(CreatorsPage);
  }

  viewSocialLinks() {
    this.app.getRootNav().push(SocialLinksPage);
  }

  viewNotifications() {
    this.app.getRootNav().push(NotificationsPage);
  }
}
