import { Component, ViewEncapsulation, NgZone } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  ToastController,
  AlertController
} from "ionic-angular";

import { ConnectServicesProvider } from "../../providers/services/connect";
import { UserServicesProvider } from "../../providers/services/users";

@IonicPage()
@Component({
  selector: "page-search",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "search.html"
})
export class SearchPage {
  searchArr: any[] = [];
  fakeArr: any[] = [];
  showContent: boolean = true;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public connect: ConnectServicesProvider,
    public services: UserServicesProvider,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController
  ) {
    for (var i = 0; i < 12; ++i) {
      this.fakeArr.push(i);
    }
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad SearchPage");
  }

  getItems(q: string) {
    let params: any = {};
    params.name = q;
    /*{
		name: params.name,
		id: params.id,
		type: params.type
	}*/
    this.showContent = false;
    this.connect.search_businesses(params).then((data: any) => {
      if (data && data.success) {
        let result = data.data;
        if (result && result[0] !== false) {
          this.searchArr = result;
          this.showContent = true;
        }
      }
    });
  }

  requestAccess(business) {
    console.log("business: ", business);
    let ctrl = this;

    function requestAccess() {
      let loading = ctrl.loadingCtrl.create({
        content: "Requesting..."
      });
      loading.present();
      ctrl.services
        .request_access({ accountId: business.id, type: business.type })
        .then((data: any) => {
          if (data && data.success) {
            loading.dismiss();
            /*let toast = ctrl.toastCtrl.create({
						message: data.data,
						duration: 1500
					});
					toast.present();*/
            let alert = ctrl.alertCtrl.create({
              title: "Success!",
              subTitle: data.message,
              buttons: ["OK"]
            });
            alert.present();
          } else {
            loading.dismiss();
            /*let toast = ctrl.toastCtrl.create({
						message: data.message,
						duration: 1500
					});
					toast.present();*/
            let alert = ctrl.alertCtrl.create({
              title: "Warning!",
              subTitle: data.message,
              buttons: ["OK"]
            });
            alert.present();
          }
        });
    }

    let confirm = this.alertCtrl.create({
      title: "Request Access",
      message: "Do you want to request access to this business account?",
      buttons: [
        {
          text: "Yes",
          handler: () => {
            requestAccess();
          }
        },
        {
          text: "No",
          handler: () => {
            console.log("Agree clicked");
          }
        }
      ]
    });
    confirm.present();
  }
}
