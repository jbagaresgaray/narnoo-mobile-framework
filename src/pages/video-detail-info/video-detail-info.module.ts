import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { ReactiveFormsModule } from "@angular/forms";

import { VideoDetailInfoPage } from "./video-detail-info";

@NgModule({
  declarations: [VideoDetailInfoPage],
  imports: [ReactiveFormsModule, IonicPageModule.forChild(VideoDetailInfoPage)]
})
export class VideoDetailInfoPageModule {}
