import { Component, ViewEncapsulation } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  AlertController,
  LoadingController
} from "ionic-angular";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from "@angular/forms";

import { VideosServicesProvider } from "../../providers/services/videos";

@IonicPage()
@Component({
  selector: "page-video-detail-info",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "video-detail-info.html"
})
export class VideoDetailInfoPage {
  item: any = {};

  isSaving = false;
  submitted = false;

  videoForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public videos: VideosServicesProvider
  ) {
    this.item = navParams.get("video");
    this.item.privacy = this.item.privilege;

    this.videoForm = this.formBuilder.group({
      caption: ["", Validators.required],
      privacy: ["", Validators.required]
    });
  }

  get f(): any {
    return this.videoForm.controls;
  }

  private validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad VideoDetailInfoPage");
    this.item = this.navParams.get("video");
    if (this.item) {
      this.videoForm.patchValue(this.item);
    }
  }

  save() {
    this.submitted = true;
    this.isSaving = true;

    if (this.videoForm.invalid) {
      this.isSaving = false;
      this.validateAllFormFields(this.videoForm);
      return;
    }

    const saveVideoEntry = () => {
      const loading = this.loadingCtrl.create({
        dismissOnPageChange: true
      });
      loading.present();
      this.videos.video_edit(this.item).then(
        (data: any) => {
          if (data && data.success) {
            this.submitted = false;
            this.isSaving = false;

            const alert = this.alertCtrl.create({
              title: "Success!",
              subTitle: data.data,
              buttons: ["OK"]
            });
            alert.onDidDismiss(() => {
              this.navCtrl.pop();
            });

            loading.dismiss().then(() => {
              alert.present();
            });
          } else {
            this.submitted = false;
            this.isSaving = false;

            const alert = this.alertCtrl.create({
              title: "Error!",
              subTitle: data.message,
              buttons: ["OK"]
            });
            loading.dismiss().then(() => {
              alert.present();
            });
          }
        },
        (error: any) => {
          console.log("error: ", error);
          loading.dismiss();
          this.submitted = false;
          this.isSaving = false;
        }
      );
    };

    const confirm = this.alertCtrl.create({
      title: "Save Entry?",
      message: "Do you want to save the entry?",
      buttons: [
        {
          text: "Cancel",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Confirm",
          handler: () => {
            console.log("Agree clicked");
            saveVideoEntry();
          }
        }
      ]
    });
    confirm.present();
  }
}
