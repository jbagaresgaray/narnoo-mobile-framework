import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CollectionsModalPage } from './collections-modal';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    CollectionsModalPage,
  ],
  imports: [
  	PipesModule,
    IonicPageModule.forChild(CollectionsModalPage),
  ],
})
export class CollectionsModalPageModule {}
