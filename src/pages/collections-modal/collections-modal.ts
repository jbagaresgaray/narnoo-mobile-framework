import { Component, ViewEncapsulation } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ViewController,
  LoadingController,
  AlertController,
  ToastController
} from "ionic-angular";
import * as _ from "lodash";
import * as async from "async";

import { CollectionsServicesProvider } from "../../providers/services/collections";

@IonicPage()
@Component({
  selector: "page-collections-modal",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "collections-modal.html"
})
export class CollectionsModalPage {
  business: any = {};
  collectionsArr: any[] = [];
  fakeArr: any[] = [];

  showContent: boolean = false;
  selected: any = {};
  type: string;
  action: string;
  params: any = {};

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public collections: CollectionsServicesProvider
  ) {
    this.type = navParams.get("type");
    this.action = navParams.get("action");
    this.params = navParams.get("params");

    for (let i = 0; i < 12; ++i) {
      this.fakeArr.push(i);
    }
  }

  initData(ev?: any) {
    this.collections.collecton_list().then(
      (data: any) => {
        if (data && data.success) {
          this.collectionsArr = data.data.data;
          _.each(this.collectionsArr, (row: any) => {
            row.checked = false;
          });
          this.showContent = true;
        }
        if (ev) {
          ev.complete();
        }
      },
      error => {
        this.showContent = true;
        if (ev) {
          ev.complete();
        }
      }
    );
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad CollectionsModalPage");
    this.initData();
  }

  doRefresh(ev) {
    this.showContent = false;
    this.initData(ev);
  }

  checkChanged(item) {
    this.selected = item;
    _.each(this.collectionsArr, (row: any) => {
      if (item.id == row.id) {
        row.checked = true;
      } else {
        row.checked = false;
      }
    });
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  private addToCollection() {
    if (this.business.role !== 1) {
      this.toastCtrl
        .create({
          message: "Invalid Access!!!",
          duration: 3000
        })
        .present();
      return;
    }

    let selectedDatas: any[] = [];
    selectedDatas = _.filter(this.collectionsArr, (row: any) => {
      return row.checked;
    });
    console.log("selectedDatas: ", selectedDatas);

    if (_.isEmpty(selectedDatas)) {
      this.alertCtrl
        .create({
          title: "WARNING",
          message: "No collections are selected",
          buttons: ["OK"]
        })
        .present();
      return;
    }

    let loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    loading.present();
    async.eachSeries(
      selectedDatas,
      (item: any, callback) => {
        this.collections
          .collection_add_media(
            {
              id: item.id,
              media: this.type,
              mediaId: this.params.id
            }
          )
          .then(
            (data: any) => {
              if (data && data.success) {
                const toast = this.toastCtrl.create({
                  message: data.data,
                  duration: 1000,
                  showCloseButton: true,
                  dismissOnPageChange: true
                });
                toast.present();
              }
              callback();
            },
            error => {
              console.log("error: ", error);
              if (error && !error.success) {
                const toast = this.toastCtrl.create({
                  message: error.message
                });
                toast.present();
              }
              callback();
            }
          );
      },
      () => {
        loading.dismiss();
        setTimeout(() => {
          this.viewCtrl.dismiss("save");
        }, 1000);
      }
    );
  }

  saveData() {
    const confirm = this.alertCtrl.create({
      title: "Add  to Collection?",
      message: "Add the selected data to collection?",
      buttons: [
        {
          text: "Cancel",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Confirm",
          handler: () => {
            this.addToCollection();
          }
        }
      ]
    });
    confirm.present();
  }
}
