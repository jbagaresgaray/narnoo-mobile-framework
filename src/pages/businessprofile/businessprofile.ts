import {
  Component,
  ViewChild,
  NgZone,
  ElementRef,
  ViewEncapsulation,
  OnInit
} from "@angular/core";
import { HttpEventType } from "@angular/common/http";
import {
  IonicPage,
  NavController,
  NavParams,
  ModalController,
  LoadingController,
  AlertController,
  Platform,
  App,
  Content,
  PopoverController,
  Events,
  ViewController,
  ToastController,
  ActionSheetController
} from "ionic-angular";
import { CacheService, Cache } from "ionic-cache-observable";
import { Observable, Subscription } from "rxjs";
import { tap } from "rxjs/operators";
import "rxjs/add/operator/finally";
import * as _ from "lodash";
import * as $ from "jquery";

import { SocialSharing } from "@ionic-native/social-sharing";
import { InAppBrowser } from "@ionic-native/in-app-browser";

import { VideosPlayerPage } from "../videos-player/videos-player";
import { ActivityDetailPage } from "../activity-detail/activity-detail";
import { ChatPage } from "../chat/chat";
import { BusinessProfileSettingsPage } from "../business-profile-settings/business-profile-settings";
import { ImagesTabPage } from "../images-tab/images-tab";
import { CreateFeedPage } from "../create-feed/create-feed";
import { NotificationsPage } from "../notifications/notifications";

import { ImageDetailPage } from "../image-detail/image-detail";
import { LogoDetailPage } from "../logo-detail/logo-detail";
import { PrintDetailPage } from "../print-detail/print-detail";
import { VideoDetailPage } from "../video-detail/video-detail";
import { ProductDetailPage } from "../product-detail/product-detail";

import { NewsFeedServicesProvider } from "../../providers/services/newsfeed";
import { ImagesServicesProvider } from "../../providers/services/images";
import { PrintServicesProvider } from "../../providers/services/print";
import { LogoServicesProvider } from "../../providers/services/logos";
import { StorageProvider } from "../../providers/storage/storage";
import { LoadingProvider } from "../../providers/loading/loading";

@Component({
  encapsulation: ViewEncapsulation.None,
  template: `
    <ion-list>
      <button ion-item (click)="viewChat()">
        <ion-icon name="ios-chatbubbles-outline" item-start></ion-icon>
        Chat Now
      </button>
    </ion-list>
  `
})
export class BusinessProfilePopoverPage implements OnInit {
  business: any = {};

  constructor(
    public app: App,
    public params: NavParams,
    public viewCtrl: ViewController,
    public modalCtrl: ModalController,
    public storageService: StorageProvider
  ) { }

  async ngOnInit() {
    const business = await this.storageService.getStorage(
      this.storageService.storageKey.CURRENT_BUSINESS
    );
    this.business = !_.isEmpty(business) ? JSON.parse(business) || {} : {};
  }

  close() {
    this.viewCtrl.dismiss();
  }

  viewChat() {
    this.viewCtrl.dismiss();
    let modal = this.modalCtrl.create(ChatPage);
    modal.present();
  }
}

@IonicPage()
@Component({
  selector: "page-businessprofile",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "businessprofile.html"
})
export class BusinessprofilePage {
  business: any = {};
  token: string;
  _shout: any = {};
  actions: string = "products";
  retryMsg: string = "";

  feedsArr: any[] = [];
  fakeArr: any[] = [];

  showContent: boolean = false;
  showPostContent: boolean = false;
  isPosting: boolean = false;
  showRetry: boolean = false;
  isRetry: boolean = false;
  isLimit: boolean = false;
  isOnline: boolean = true;

  totalNotification: number = 0;
  percentDone: number = 0;
  perPage: number = 50;
  pageFeed: number = 1;
  totalPageFeed: number = 0;
  hasNextPage: boolean = false;
  nextPage: string = "";

  feedsArr$: Observable<any>;
  feedsNextArr$: Observable<any>;
  private cache: Cache<any>;
  private pageCache: Cache<any>;
  public refreshSubscription: Subscription;
  public paginationSubscription: Subscription;

  @ViewChild(Content) content: Content;
  @ViewChild("mediaVideo") mediaVideo: ElementRef;

  constructor(
    public platform: Platform,
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public loadingCtrl: LoadingController,
    public events: Events,
    public alertCtrl: AlertController,
    public app: App,
    public popoverCtrl: PopoverController,
    public actionSheetCtrl: ActionSheetController,
    public toastCtrl: ToastController,
    public zone: NgZone,
    public storageService: StorageProvider,
    public loader: LoadingProvider,
    public cacheService: CacheService,
    public newsfeed: NewsFeedServicesProvider,
    public images: ImagesServicesProvider,
    public print: PrintServicesProvider,
    public logos: LogoServicesProvider,
    private socialSharing: SocialSharing,
    private iab: InAppBrowser
  ) {
    for (var i = 0; i < 20; ++i) {
      this.fakeArr.push(i);
    }

    events.unsubscribe("showNotifications");
    events.subscribe("showNotifications", total => {
      this.totalNotification = total;
    });

    events.unsubscribe("app:online");
    events.unsubscribe("app:offline");
    events.subscribe("app:online", () => {
      this.isOnline = true;
    });
    events.subscribe("app:offline", () => {
      this.isOnline = false;
    });

    try {
      this.newsfeed.notificationData$.subscribe((data: any) => {
        console.log("activity feed notificationData: ", data);
        if (data && data.success) {
          this.zone.run(() => {
            this.newsfeed.totalNotification = data.data.total;
            this.totalNotification = data.data.total;
          });
        } else {
          this.zone.run(() => {
            this.newsfeed.totalNotification = 0;
            this.totalNotification = 0;
          });
        }
      });
    } catch (error) {
      this.zone.run(() => {
        this.newsfeed.totalNotification = 0;
        this.totalNotification = 0;
      });
      this.events.publish("initNotifications");
    }
  }

  ionViewDidLoad() {
    this.initBusiness().then(() => {
      setTimeout(() => {
        this.zone.run(() => {
          this.initData();
        });
      }, 400);
    });

    this._shout = this.business.shout;

    if (!_.isEmpty(this._shout)) {
      this.isPosting = true;
      this.showRetry = true;
      this.retryMsg = "";
    }

    this.showContent = false;
    this.showPostContent = false;
    this.pageFeed = 1;
    this.feedsArr = [];
  }

  ionViewWillEnter() {
    this.initBusiness();
    this._shout = this.business.shout;

    if (!_.isEmpty(this._shout)) {
      this.isPosting = true;
    }

    this.reloadImages();
  }

  ionViewDidLeave() {
    console.log("ionViewDidLeave");
    $("video").each((videoIndex, video) => {
      video.pause();
    });
  }

  ionViewWillUnload() {
    console.log("ionViewWillUnload");
    $("video").each((videoIndex, video) => {
      video.pause();
    });
  }

  private async initBusiness() {
    const business = await this.storageService.getStorage(
      this.storageService.storageKey.CURRENT_BUSINESS
    );
    this.business = !_.isEmpty(business) ? JSON.parse(business) || {} : {};
  }

  private async initData(refresher?: any) {
    const successResponse = (item: any) => {
      console.log("successResponse: ", item);
      if (item && item.success) {
        if (item.currentPage) {
          this.pageFeed = parseInt(item.currentPage);
        }
        this.nextPage = item.nextPage;
        this.hasNextPage = _.isEmpty(item.nextPage) ? false : true;

        this.zone.run(() => {
          let feedsArr: any[] = [];
          _.each(item.data, (row: any) => {
            if (_.isObject(row)) {
              if (row["itemType"] == "images") {
                row["moreCount"] = _.size(row["media"]) - 4;
                row["disableShare"] = true;
                if (this.business.paid) {
                  row["disableDownload"] = false;
                } else {
                  row["disableDownload"] = true;
                }
              } else if (row["itemType"] == "image") {
                row["disableShare"] = true;
                if (this.business.paid) {
                  row["disableDownload"] = false;
                } else {
                  row["disableDownload"] = true;
                }
              } else if (row["itemType"] == "shout") {
                row["disableDownload"] = true;
                row["disableShare"] = false;
              } else {
                row["disableDownload"] = true;
                row["disableShare"] = true;
              }
              feedsArr.push(row);
            }
            const arrayOfObjAfter = _.map(
              _.uniq(
                _.map(feedsArr, function(obj) {
                  return JSON.stringify(obj);
                })
              ),
              function(obj) {
                return JSON.parse(obj);
              }
            );
            this.feedsArr = arrayOfObjAfter;
          });

          this.content.resize();
          this.showContent = true;
          this.showPostContent = true;
          this.reloadImages();
        });
      }

      if (refresher) {
        refresher.complete();
      }
    };

    const errorResponse = error => {
      this.showContent = true;
      this.showPostContent = true;
      this.content.resize();
      this.loader.complete();
      console.log("error: ", error);

      if (refresher) {
        refresher.complete();
      }
    };

    if (this.hasNextPage) {
      const dataObservable = Observable.fromPromise(
        this.newsfeed.feed_next_page({
          next: this.nextPage
        })
      );

      const pageCacheKey =
        this.storageService.cacheKey.NEXT_FEED_ACTIVITY +
        "_" +
        this.business.id +
        "_" +
        this.nextPage;

      this.cacheService
        .register(pageCacheKey, dataObservable)
        .subscribe(cache => {
          this.pageCache = cache;
          this.zone.run(() => {
            this.feedsNextArr$ = cache.get$;
          });
          this.pageCache.refresh().subscribe(()=>{
            this.loader.start();
          },()=>{
            this.loader.complete();
          },()=>{
            this.loader.complete();
          });
        });
    } else {
      this.zone.run(() => {
        this.showContent = false;
        this.pageFeed = 1;
        this.feedsArr = [];
        this.nextPage = null;
        this.hasNextPage = false;
      });

      const dataObservable = Observable.fromPromise(
        this.newsfeed.feed_activity({
          page: this.pageFeed,
          limit: this.perPage
        })
      );

      const cacheKey =
        this.storageService.cacheKey.FEED_ACTIVITY + "_" + this.business.id;

      this.cacheService.register(cacheKey, dataObservable).subscribe(cache => {
        this.cache = cache;
        this.zone.run(() => {
          this.feedsArr$ = cache.get$;
          
          this.cache.refresh().subscribe(()=>{
            this.loader.start();
          },()=>{
            this.loader.complete();
          },()=>{
            this.loader.complete();
          });
        });
      });

      this.feedsArr$.subscribe(successResponse, errorResponse, () => {
        this.reloadImages();
      });
    }
  }

  private reloadImages() {
    setTimeout(() => {
      $(".progressive-image").each(function() {
        var image = new Image();
        var previewImage = $(this).find(".loadingImage");
        var newImage = $(this).find(".overlay");

        image.onload = function() {
          newImage.css("background-image", "url(" + image.src + ")");
          newImage.css("opacity", "1");
        };
        image.src = previewImage.data("image");
      });
    }, 300);
  }

  doInfinite(refresher: any) {
    if (this.paginationSubscription) {
      console.warn("Already doInfinite refreshing.");
      refresher.complete();
      return;
    }

    const refreshData = () => {
      this.zone.run(() => {
        this.paginationSubscription = this.feedsNextArr$.subscribe(
          (data: any) => {
            if (refresher) {
              refresher.complete();
            }
          },
          err => {
            console.error("Something went wrong!", err);
            if (refresher) {
              refresher.complete();
            }

            throw err;
          },
          () => {
            this.reloadImages();
            if (refresher) {
              refresher.complete();
            }
            this.paginationSubscription = null;
          }
        );
      });
    };

    if (this.pageCache) {
      if (this.hasNextPage) {
        refreshData();
      } else {
        refresher.complete();
      }
    } else {
      this.initData();
      refreshData();
    }
  }

  doRefresh(refresher: any) {
    if (this.cache) {
      this.zone.run(() => {
        this.showContent = false;
        this.pageFeed = 1;
        this.feedsArr = [];
        this.nextPage = null;
        this.hasNextPage = false;

        this.initData(refresher);
      });
    } else {
      console.warn("Cache has not been initialized.");
      if (refresher) {
        refresher.cancel();
      }
    }
  }

  presentPopover(ev) {
    const popover = this.popoverCtrl.create(BusinessProfilePopoverPage);
    popover.present({ ev: ev });
  }

  checkBlur() {
    console.log("checkBlur");
  }

  checkFocus(isLink: boolean) {
    console.log("checkFocus");
    if (this.business.role !== 1) {
      const toast = this.toastCtrl.create({
        message: "Invalid Access!!!",
        duration: 3000
      });
      toast.present();
      return;
    }

    const modal = this.modalCtrl.create(CreateFeedPage, {
      isLink: isLink
    });
    modal.onDidDismiss(resp => {
      console.log("resp: ", resp);
      if (resp && resp.status == "save") {
        this._shout = {
          shout: resp.message,
          link: resp.link,
          timestamp: resp.timestamp
        };
        console.log("_shout: ", this._shout);
        this.business.shout = this._shout;
        this.storageService.setStorage(
          this.storageService.storageKey.CURRENT_BUSINESS,
          JSON.stringify(this.business)
        );

        if (this.isOnline) {
          this.zone.run(() => {
            this.showRetry = false;
          });
          this.createPost(resp.message, resp.link);
        } else {
          this.showRetry = true;
          this.isRetry = true;
          this.retryMsg = "No connection available.";
        }
      }
    });
    modal.present();
  }

  fullScreen(item) {
    const profileModal = this.modalCtrl.create(VideosPlayerPage, {
      video: item.media
    });
    profileModal.present();
  }

  viewChat() {
    const modal = this.modalCtrl.create(ChatPage);
    modal.present();
  }

  viewProfile() {
    // this.navCtrl.push(BusinessProfileSettingsPage);
    this.app.getRootNav().push(BusinessProfileSettingsPage);
  }

  viewNotifications() {
    this.app.getRootNav().push(NotificationsPage);
  }

  private viewImageDetail(media: any) {
    console.log("media: ", media);
    media.media.id = media.objectId;
    this.app.getRootNav().push(ImageDetailPage, {
      image: media.media
    });
  }

  private viewImages() {
    this.app.getRootNav().push(ImagesTabPage);
  }

  private viewVideoDetail(media: any) {
    media.media.id = media.objectId;
    this.app.getRootNav().push(VideoDetailPage, {
      video: media.media
    });
  }

  private viewLogoDetail(media: any) {
    media.media.id = media.objectId;
    this.app.getRootNav().push(LogoDetailPage, {
      logo: media.media
    });
  }

  private viewPrintDetail(media: any) {
    media.media.id = media.objectId;
    this.app.getRootNav().push(PrintDetailPage, {
      print: media.media
    });
  }

  private viewProductDetails(media: any) {
    const options: any = {};
    media.media.productId = media.objectId;
    options.product = media.media;

    if (media.businessId !== _.toString(this.business.id)) {
      options.action = "connect";
      options.params = {
        id: media.businessId,
        type: media.businessType === 1 ? "operator" : "distributor"
      };
    }
    console.log("options: ", options);

    this.navCtrl.push(ProductDetailPage, options);
  }

  viewFeedDetails(itemType: string, media: any) {
    console.log("itemType: ", itemType);
    if (itemType === "video") {
      this.viewVideoDetail(media);
    } else if (itemType == "product") {
      this.viewProductDetails(media);
    } else if (itemType == "image") {
      this.viewImageDetail(media);
    } else if (itemType == "images") {
      this.viewImages();
    } else if (itemType == "logo") {
      this.viewLogoDetail(media);
    } else if (itemType == "brochure") {
      this.viewPrintDetail(media);
    }
  }

  private downloadImage(item: any) {
    if (!this.business.paid) {
      this.alertCtrl
        .create({
          title: "WARNING",
          message: "This feature is only available to premium users only.",
          buttons: ["OK"]
        })
        .present();
      return;
    }

    if (item.businessType === 1) {
      item.businessType = "Operator";
    } else if (item.businessType === 2) {
      item.businessType = "Distributor";
    }

    if (
      item.itemType === "image" ||
      item.itemType === "following" ||
      item.itemType === "joined"
    ) {
      this.navCtrl.push(ActivityDetailPage, {
        type: "image",
        item: item,
        business: {
          id: item.businessId,
          businessType: item.businessType
        }
      });
    } else if (item.itemType === "brochure") {
      this.navCtrl.push(ActivityDetailPage, {
        type: "brochure",
        item: item,
        business: {
          id: item.businessId,
          businessType: item.businessType
        }
      });
    } else if (item.itemType === "logo") {
      this.navCtrl.push(ActivityDetailPage, {
        type: "logo",
        item: item,
        business: {
          id: item.businessId,
          businessType: item.businessType
        }
      });
    } else if (item.itemType === "video") {
      this.navCtrl.push(ActivityDetailPage, {
        type: "video",
        item: item,
        business: {
          id: item.businessId,
          businessType: item.businessType
        }
      });
    } else if (item.itemType === "album") {
      this.navCtrl.push(ActivityDetailPage, {
        type: "album",
        item: item,
        business: {
          id: item.businessId,
          businessType: item.businessType
        }
      });
    }
  }

  createPost(shout: string, link: string) {
    if (this.business.role !== 1) {
      const toast = this.toastCtrl.create({
        message: "Invalid Access!!!",
        duration: 3000
      });
      toast.present();
      return;
    }

    this.isPosting = true;
    this.percentDone = 0;

    this.zone.run(() => {
      this.showRetry = false;
      this.isRetry = false;
      this.isLimit = false;
      this.content.resize();
    });

    const successResponse = () => {
      setTimeout(() => {
        this.showPostContent = false;
        this.pageFeed = 1;
        this.feedsArr = [];

        this.isPosting = false;
        this.isRetry = false;
        this.isLimit = false;
        this.nextPage = null;

        this.doRefresh(null);

        this.business.shout = null;
        this.storageService.setStorage(
          this.storageService.storageKey.CURRENT_BUSINESS,
          JSON.stringify(this.business)
        );
        this.content.resize();
      }, 2000);
    };

    this.storageService
      .getStorage(this.storageService.storageKey.BUSINESS_TOKENS)
      .then(token => {
        this.newsfeed
          .postFeed(token, {
            message: shout,
            link: link
          })
          .subscribe(
          (event: any) => {
            if (event) {
              if (event.type === HttpEventType.UploadProgress) {
                this.percentDone = Math.round(
                  (100 * event.loaded) / event.total
                );
              } else if (event.type === HttpEventType.Response) {
                const data: any = event.body;
                if (data && data.success) {
                  successResponse();
                } else if (data && !data.success) {
                  this.showRetry = true;
                  this.isLimit = true;
                  this.isRetry = false;

                  this.retryMsg = "Monthly shout out quota reach.";
                  this.alertCtrl
                    .create({
                      title: "WARNING",
                      message: data.message,
                      buttons: ["OK"]
                    })
                    .present();
                  return;
                }
              }
            }
          },
          error => {
            console.log("error: ", error);
            this.showRetry = true;
            this.isRetry = true;
            this.isLimit = false;
            this.retryMsg = "Will retry your shout.";
          }
          );
      });
  }

  deleteCacheShout() {
    const loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    loading.present();

    this.business.shout = null;
    this.storageService.setStorage(
      this.storageService.storageKey.CURRENT_BUSINESS,
      JSON.stringify(this.business)
    );

    setTimeout(() => {
      this._shout = {};

      this.isPosting = false;
      this.isLimit = false;
      this.isRetry = false;

      loading.dismiss().then(() => {
        this.content.resize();
      });
    }, 600);
  }

  deleteShout(shout: any) {
    if (this.business.role !== 1) {
      const toast = this.toastCtrl.create({
        message: "Invalid Access!!!",
        duration: 3000
      });
      toast.present();
      return;
    }

    const _deleteShout = () => {
      const loading = this.loadingCtrl.create({
        dismissOnPageChange: true,
        content: "Deleting..."
      });
      loading.present();
      this.newsfeed.remotePostedFeed(shout.objectId).then(
        (data: any) => {
          if (data && data.success) {
            const index = _.findIndex(this.feedsArr, (row: any) => {
              return row.objectId == shout.objectId;
            });
            console.log("index: ", index);
            this.zone.run(() => {
              // _.remove(this.feedsArr, (row: any) => {
              //   return row.objectId == shout.objectId;
              // });
              this.feedsArr.splice(index, 1);
            });
            this.doRefresh(null);
          }
          loading.dismiss();
        },
        error => {
          loading.dismiss();
          console.log("error: ", error);
        }
      );
    };

    const confirm = this.alertCtrl.create({
      title: "Remove this post?",
      message: "Are you sure you want to add remote this post?",
      buttons: [
        {
          text: "Cancel",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Remove",
          handler: () => {
            _deleteShout();
          }
        }
      ]
    });
    confirm.present();
  }

  flagShout(shout: any) {
    const _flagShout = () => {
      const loading = this.loadingCtrl.create({
        dismissOnPageChange: true
      });
      loading.present();
      this.newsfeed.flagPostedFeed(shout.objectId).then(
        (data: any) => {
          if (data && data.success) {
            this.doRefresh(null);
          }
          loading.dismiss();
        },
        error => {
          loading.dismiss();
          console.log("error: ", error);
        }
      );
    };

    const confirm = this.alertCtrl.create({
      title: "Flag this post?",
      message: "Are you sure you want to add flag on this post?",
      buttons: [
        {
          text: "Cancel",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Flag",
          handler: () => {
            _flagShout();
          }
        }
      ]
    });
    confirm.present();
  }

  shareActivity(activity: any) {
    if (!this.platform.is("cordova")) {
      this.alertCtrl
        .create({
          title: "WARNING",
          message: "Social Sharing is only available on an actual device!",
          buttons: ["OK"]
        })
        .present();
      return;
    }

    const post: any = activity.media;
    const messageShout = this.business.business + ": " + post.message;

    const successResponse = (data: any) => {
      console.log("Share Success: ", data);
    };

    const errorResponse = (error: any) => {
      console.log("Share Error: ", error);
    };

    const actionSheet = this.actionSheetCtrl.create({
      title: "Share your Shout",
      buttons: [
        {
          text: "Share with Facebook",
          handler: () => {
            this.socialSharing
              .shareViaFacebook(messageShout, post.screenshot, post.link)
              .then(successResponse)
              .catch(errorResponse);
          }
        },
        {
          text: "Share with Twitter",
          handler: () => {
            this.socialSharing
              .shareViaTwitter(messageShout, post.screenshot, post.link)
              .then(successResponse)
              .catch(errorResponse);
          }
        },
        {
          text: "Share with Instagram",
          handler: () => {
            this.socialSharing
              .shareViaInstagram(messageShout, post.screenshot)
              .then(successResponse)
              .catch(errorResponse);
          }
        },
        {
          text: "Share with WhatsApp",
          handler: () => {
            this.socialSharing
              .shareViaWhatsApp(messageShout, post.screenshot, post.link)
              .then(successResponse)
              .catch(errorResponse);
          }
        },
        {
          text: "More ...",
          handler: () => {
            this.socialSharing
              .share(messageShout, "SHARE", null, post.link)
              .then(successResponse)
              .catch(errorResponse);
          }
        },
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        }
      ]
    });
    actionSheet.present();
  }

  moreAction(type: any, shout: any) {
    console.log("item: ", shout);
    console.log("itemType: ", type);

    let actionButtons: any[] = [];

    if (type == "shout") {
      if (_.isEqual(this.business.id, parseInt(shout.businessId))) {
        actionButtons.unshift({
          text: "Delete Shout",
          role: "destructive",
          handler: () => {
            console.log("Destructive clicked");
            this.deleteShout(shout);
          }
        });
      }

      actionButtons.push({
        text: "Flag Shout",
        handler: () => {
          console.log("Archive clicked");
          this.flagShout(shout);
        }
      });
    } else if (type == "image") {
      actionButtons = [
        {
          text: "Download",
          handler: () => {
            console.log("Archive clicked");
            this.downloadImage(shout);
          }
        }
      ];
    } else {
    }

    actionButtons.push({
      text: "Cancel",
      role: "cancel",
      handler: () => {
        console.log("Cancel clicked");
      }
    });
    const actionSheet = this.actionSheetCtrl.create({
      buttons: actionButtons
    });
    actionSheet.present();
  }

  viewExternalLink(media: any) {
    console.log("viewExternalLink: ", media);
    if (!_.isEmpty(media.link)) {
      const browser = this.iab.create(media.link, "_blank");
      browser.on("loadstart").subscribe(event => { });
      browser.on("loadstop").subscribe(event => { });
      browser.on("loaderror").subscribe(event => { });
    } else {
      this.alertCtrl
        .create({
          title: "WARNING",
          subTitle: "No link is available!",
          buttons: ["OK"]
        })
        .present();
    }
  }
}
