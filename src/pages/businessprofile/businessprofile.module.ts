import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { IonicImageViewerModule } from "ionic-img-viewer";
import { InViewportModule } from "ng-in-viewport";
import { IonicImageLoader } from "ionic-image-loader";
import { CacheModule } from "ionic-cache-observable";

import { PipesModule } from "../../pipes/pipes.module";
import { ComponentsModule } from "../../components/components.module";
import { DirectivesModule } from "../../directives/directives.module";

import {
  BusinessprofilePage,
  BusinessProfilePopoverPage
} from "./businessprofile";

@NgModule({
  declarations: [BusinessprofilePage, BusinessProfilePopoverPage],
  imports: [
    IonicPageModule.forChild(BusinessprofilePage),
    IonicImageViewerModule,
    InViewportModule,
    PipesModule,
    ComponentsModule,
    DirectivesModule,
    IonicImageLoader,
    CacheModule
  ],
  entryComponents: [BusinessProfilePopoverPage]
})
export class BusinessprofilePageModule { }
