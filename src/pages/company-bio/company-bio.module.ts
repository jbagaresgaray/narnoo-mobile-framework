import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CompanyBioPage } from './company-bio';

@NgModule({
  declarations: [
    CompanyBioPage
  ],
  imports: [
    IonicPageModule.forChild(CompanyBioPage),
  ],
})
export class CompanyBioPageModule {}
