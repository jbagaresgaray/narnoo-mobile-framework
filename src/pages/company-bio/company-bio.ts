import { Component, ViewEncapsulation } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  ModalController,
  ToastController
} from "ionic-angular";
import * as _ from "lodash";

import { CompanyBioEntryPage } from "../../pages/company-bio-entry/company-bio-entry";
import { BusinessServicesProvider } from "../../providers/services/business";
import { StorageProvider } from "../../providers/storage/storage";

@IonicPage()
@Component({
  selector: "page-company-bio",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "company-bio.html"
})
export class CompanyBioPage {
  biog: any[] = [];
  imagesLoadingArr: any[] = [];
  token: string;

  business: any = {};

  descriptionArr: any[] = [];
  summaryArr: any[] = [];

  showLoading: boolean = true;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public businessServices: BusinessServicesProvider,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController,
    public storageService: StorageProvider
  ) {
    for (var i = 0; i < 12; ++i) {
      this.imagesLoadingArr.push(i);
    }
  }

  ionViewDidLoad() {
    this.initBusiness().then(() => {
      this.initData();
    });
  }

  private async initBusiness() {
    const business = await this.storageService.getStorage(
      this.storageService.storageKey.CURRENT_BUSINESS
    );
    this.business = !_.isEmpty(business) ? JSON.parse(business) || {} : {};
  }

  private initData(loading?: any) {
    this.showLoading = true;
    this.businessServices.business_biography().then(
      (data: any) => {
        if (data && data.success) {
          const biog = data.data;

          const result = _.filter(biog, { size: "description" });
          _.each(result, (row: any) => {
            row.selected = false;
            if (row.language !== "english" && this.business.paid) {
              row.paid = true;
            } else {
              row.paid = false;
            }
          });
          this.descriptionArr = result;
          console.log("this.descriptionArr: ", this.descriptionArr);

          const result2 = _.filter(biog, { size: "summary" });
          _.each(result2, (row: any) => {
            row.selected = false;
            if (row.language !== "english" && this.business.paid) {
              row.paid = true;
            } else {
              row.paid = false;
            }
          });
          this.summaryArr = result2;
          console.log("this.summaryArr: ", this.summaryArr);
        }
        this.showLoading = false;
        if (loading) {
          loading.complete();
        }
      },
      error => {
        this.showLoading = false;
        if (loading) {
          loading.complete();
        }
      }
    );
  }

  doRefresh(ev) {
    this.initData(ev);
  }

  selectDescription(item: any) {
    _.each(this.descriptionArr, (row: any) => {
      if (item.id == row.id) {
        row.selected = !item.selected;
      } else {
        row.selected = false;
      }
    });
  }

  selectSummary(item: any) {
    _.each(this.summaryArr, (row: any) => {
      if (item.id == row.id) {
        row.selected = !item.selected;
      } else {
        row.selected = false;
      }
    });
  }

  updateBio(item, type) {
    if (this.business.role !== 1) {
      this.toastCtrl
        .create({
          message: "Invalid Access!!!",
          duration: 3000
        })
        .present();
      return;
    }

    const modal = this.modalCtrl.create(CompanyBioEntryPage, {
      item: item,
      type: type,
      action: "update"
    });
    modal.onDidDismiss(resp => {
      if (resp == "save") {
        this.initData();
      }
    });
    modal.present();
  }
}
