import { Component, ViewEncapsulation, NgZone } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ViewController,
  Platform,
  Events
} from "ionic-angular";
import * as _ from "lodash";
import { CacheService, Cache } from "ionic-cache-observable";
import { Observable } from "rxjs";
import "rxjs/add/operator/finally";

import { ProductTabPage } from "./../product-tab/product-tab";
import { ImagesTabPage } from "./../images-tab/images-tab";
import { BusinessprofilePage } from "./../businessprofile/businessprofile";
import { SettingsPage } from "../settings/settings";
import { ConnectedBusinessPage } from "../connected-business/connected-business";

import { NewsFeedServicesProvider } from "../../providers/services/newsfeed";
import { StorageProvider } from "../../providers/storage/storage";

@IonicPage()
@Component({
  selector: "page-businesstabs",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "businesstabs.html"
})
export class BusinesstabsPage {
  productTabRoot = ProductTabPage;
  imagesTabRoot = ImagesTabPage;
  profileTabRoot = BusinessprofilePage;
  settingsTabRoot = SettingsPage;
  followersTabRoot = ConnectedBusinessPage;

  seltabix: number;
  item: any = {};
  business: any = {};
  token: string;

  tabsPlacement: string = "bottom";
  tabsLayout: string = "icon-top";

  totalNotification: number = 0;

  constructor(
    public zone: NgZone,
    public navCtrl: NavController,
    public np: NavParams,
    public viewCtrl: ViewController,
    public platform: Platform,
    public events: Events,
    public newsfeeds: NewsFeedServicesProvider,
    public storage: StorageProvider,
    public cacheService: CacheService
  ) {
    this.seltabix = np.get("opentab");
    this.item = np.get("item");
    this.business = JSON.parse(localStorage.getItem("business")) || {};
    this.token = localStorage.getItem("bus.token");

    /*if (!this.platform.is('mobile')) {
      this.tabsPlacement = 'top';
      this.tabsLayout = 'icon-left';
    }*/

    this.totalNotification = 0;
    this.events.unsubscribe("showNotifications");
    this.events.subscribe("showNotifications", count => {
      this.totalNotification = count;
    });

    events.subscribe("initNotifications", () => {
      console.log("initNotifications");
      this.initBusiness().then(() => {
        this.initNotificationData();
      });
    });
  }

  private async initBusiness() {
    const business = await this.storage.getStorage(
      this.storage.storageKey.CURRENT_BUSINESS
    );
    this.business = !_.isEmpty(business) ? JSON.parse(business) || {} : {};
  }

  private async initNotificationData() {
    const dataObservable = Observable.fromPromise(
      this.newsfeeds.app_notifications_latest()
    );
    const cacheKey =
      this.storage.cacheKey.NOTIFICATION_LATEST + "_" + this.business.id;
    this.cacheService.register(cacheKey, dataObservable).subscribe(cache => {
      this.newsfeeds.notificationCache$ = cache;
      this.zone.run(() => {
        this.newsfeeds.notificationData$ = cache.get$;
      });
    });

    this.newsfeeds.notificationData$.subscribe(
      data => {
        if (data && data.success) {
          this.newsfeeds.totalNotification = data.data.total;
          this.totalNotification = data.data.total;
        } else {
          this.newsfeeds.totalNotification = 0;
          this.totalNotification = 0;
        }
        this.events.publish(
          "showNotifications",
          this.newsfeeds.totalNotification
        );
      },
      error => {
        console.log("error: ", error);
      }
    );
  }

  ionViewDidLoad() {
    this.seltabix = this.np.get("opentab");
    this.item = this.np.get("item");
    this.totalNotification = 0;
  }

  ionViewWillEnter() {
    this.totalNotification = 0;

    this.initBusiness().then(() => {
      this.initNotificationData();
    });
  }

  ionViewDidEnter() {
    this.viewCtrl.showBackButton(false);
  }
}
