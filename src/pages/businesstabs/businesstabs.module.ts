import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BusinesstabsPage } from './businesstabs';

@NgModule({
  declarations: [
    BusinesstabsPage
  ],
  imports: [
    IonicPageModule.forChild(BusinesstabsPage)
  ]
})
export class BusinesstabsPageModule {}
