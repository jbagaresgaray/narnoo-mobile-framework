import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { IonicImageLoader } from "ionic-image-loader";
import { CacheModule } from "ionic-cache-observable";

import { CreateFeedPage } from "./create-feed";

import { PipesModule } from "../../pipes/pipes.module";
import { ComponentsModule } from "../../components/components.module";

@NgModule({
  declarations: [CreateFeedPage],
  imports: [
    PipesModule,
    ComponentsModule,
    IonicImageLoader,
    CacheModule,
    IonicPageModule.forChild(CreateFeedPage)
  ]
})
export class CreateFeedPageModule {}
