import {
  Component,
  ViewChild,
  ElementRef,
  ChangeDetectorRef,
  ViewEncapsulation
} from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  ViewController,
  Platform,
  Events,
  AlertController
} from "ionic-angular";
import * as _ from "lodash";

import { NewsFeedServicesProvider } from "../../providers/services/newsfeed";
import { StorageProvider } from "../../providers/storage/storage";

// import { Keyboard } from "@ionic-native/keyboard";

@IonicPage()
@Component({
  selector: "page-create-feed",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "create-feed.html"
})
export class CreateFeedPage {
  @ViewChild("myInput") myInput: ElementRef;
  business: any = {};

  inputPost: string = "";
  inputLink: string = "";
  limitText: number = 280;
  maxLength: number = 280;

  isOnline: boolean = true;
  showLink: boolean = false;

  constructor(
    public platform: Platform,
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public viewCtrl: ViewController,
    private cdRef: ChangeDetectorRef,
    public events: Events,
    public alertCtrl: AlertController,
    public newsfeeds: NewsFeedServicesProvider,
    public storageService: StorageProvider
  ) // private keyboard: Keyboard
  {
    this.showLink = navParams.get("isLink");

    events.unsubscribe("app:online");
    events.unsubscribe("app:offline");

    events.subscribe("app:online", () => {
      this.isOnline = true;
    });
    events.subscribe("app:offline", () => {
      this.isOnline = false;
    });
  }

  private async initBusiness() {
    const business = await this.storageService.getStorage(
      this.storageService.storageKey.CURRENT_BUSINESS
    );
    this.business = !_.isEmpty(business) ? JSON.parse(business) || {} : {};
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad CreateFeedPage");
    this.initBusiness();
  }

  /*ionViewDidEnter() {
    this.platform.ready().then(() => {
      this.keyboard.disableScroll(true);
    });
  }

  ionViewWillLeave() {
    this.platform.ready().then(() => {
      this.keyboard.disableScroll(false);
    });
  }
*/
  addActivity() {}

  dismiss() {
    this.viewCtrl.dismiss();
  }

  resize() {
    this.myInput.nativeElement.style.height =
      this.myInput.nativeElement.scrollHeight + "px";
  }

  change(value) {
    //manually launch change detection
    this.cdRef.detectChanges();
    if (this.maxLength >= value.length) {
      this.limitText = this.maxLength - value.length;
    } else {
      this.inputPost = value.substr(0, value.length - 1);

      if (this.maxLength < value.length) {
        this.inputPost = value.substr(
          0,
          value.length - (value.length - this.maxLength)
        );
      }
    }
  }

  createPost() {
    if (!this.isOnline) {
      this.alertCtrl
        .create({
          title: "WARNING",
          message: "No connection available.",
          buttons: ["OK"]
        })
        .present();
      return;
    }

    const shout: any = {
      status: "save",
      message: this.inputPost,
      timestamp: new Date().toISOString()
    };

    if (this.showLink) {
      shout.link = this.inputLink;
    }

    const loading = this.loadingCtrl.create({
      dismissOnPageChange: true,
      content: "Posting..."
    });
    loading.present();
    setTimeout(() => {
      loading.dismiss().then(() => {
        this.viewCtrl.dismiss(shout);
      });
    }, 600);

    /* this.newsfeeds.postFeed(this.token, {
      message: this.inputPost,
      link: this.inputPost
    }).then((data: any) => {
      if (data && data.success) {
      loading.dismiss().then(() => {
        this.viewCtrl.dismiss('save');
      });
      }
    }, (error) => {
      console.log('error: ', error);
    }); */
  }
}
