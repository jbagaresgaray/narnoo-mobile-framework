import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";

import { ImageModalPage } from "./image-modal";
import { PipesModule } from "../../pipes/pipes.module";
import { ComponentsModule } from "../../components/components.module";

@NgModule({
  declarations: [ImageModalPage],
  imports: [
    PipesModule,
    ComponentsModule,
    IonicPageModule.forChild(ImageModalPage)
  ]
})
export class ImageModalPageModule {}
