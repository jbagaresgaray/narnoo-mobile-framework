import { Component, ViewEncapsulation, NgZone } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ViewController,
  LoadingController,
  AlertController,
  ToastController
} from "ionic-angular";

import * as async from "async";
import * as _ from "lodash";

import { ImagesServicesProvider } from "../../providers/services/images";
import { PrintServicesProvider } from "../../providers/services/print";
import { VideosServicesProvider } from "../../providers/services/videos";

import { CollectionsServicesProvider } from "../../providers/services/collections";
import { AlbumsServicesProvider } from "../../providers/services/albums";
import { ChannelsServicesProvider } from "../../providers/services/channels";
import { StorageProvider } from "../../providers/storage/storage";

@IonicPage()
@Component({
  selector: "page-image-modal",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "image-modal.html"
})
export class ImageModalPage {
  params: any = {};
  business: any = {};
  media: string;
  title: string;
  selected: any = {};
  mode: string;

  imagesArr: any[] = [];
  printsArr: any[] = [];
  videosArr: any[] = [];
  fakeArr: any[] = [];

  showContent: boolean = false;
  showCheckbox: boolean = false;

  pageImage: number = 0;
  pagePrint: number = 0;
  pageVideo: number = 0;
  perPage: number = 20;

  constructor(
    public zone: NgZone,
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public images: ImagesServicesProvider,
    public prints: PrintServicesProvider,
    public videos: VideosServicesProvider,
    public collections: CollectionsServicesProvider,
    public albums: AlbumsServicesProvider,
    public channels: ChannelsServicesProvider,
    public storageService: StorageProvider
  ) {
    this.params = navParams.get("params");
    this.media = navParams.get("media");
    this.mode = navParams.get("mode");
    console.log("params: ", this.params);

    for (var i = 0; i < 12; ++i) {
      this.fakeArr.push(i);
    }
  }

  initData(ev?: any) {
    this.showContent = false;
    if (this.media == "image" || this.media == "albums") {
      this.images
        .image_list({
          page: this.pageImage,
          total: this.perPage
        })
        .then(
          (data: any) => {
            if (data && !_.isArray(data) && data.success) {
              console.log("data: ", data);
              this.pageImage = parseInt(data.data.currentPage);
              for (let i = 0; i < _.size(data.data.images); i++) {
                data.data.images[i].selected = false;
                this.imagesArr.push(data.data.images[i]);
              }
            }
            this.showContent = true;
            if (ev) {
              ev.complete();
            }
          },
          error => {
            this.showContent = true;
            if (ev) {
              ev.complete();
            }
          }
        );
    } else if (this.media == "print") {
      this.prints
        .brochure_list({
          page: this.pagePrint,
          total: this.perPage
        })
        .then(
          (data: any) => {
            if (data && data.success && data.data[0] != false) {
              this.pagePrint = parseInt(data.data.currentPage);
              for (let i = 0; i < _.size(data.data.prints); i++) {
                data.data.prints[i].selected = false;
                this.printsArr.push(data.data.prints[i]);
              }
            }
            this.showContent = true;
            if (ev) {
              ev.complete();
            }
          },
          error => {
            this.showContent = true;
            if (ev) {
              ev.complete();
            }
          }
        );
    } else if (this.media == "video") {
      this.videos
        .video_list({
          page: this.pageVideo,
          total: this.perPage
        })
        .then(
          (data: any) => {
            if (data && data.success && data.data[0] != false) {
              this.pageVideo = parseInt(data.data.currentPage);
              for (let i = 0; i < _.size(data.data.videos); i++) {
                data.data.videos[i].selected = false;
                this.videosArr.push(data.data.videos[i]);
              }
            }
            this.showContent = true;
            if (ev) {
              ev.complete();
            }
          },
          error => {
            this.showContent = true;
            if (ev) {
              ev.complete();
            }
          }
        );
    }
  }

  async ionViewDidLoad() {
    console.log("ionViewDidLoad ImageModalPage");

    const business = await this.storageService.getStorage(
      this.storageService.storageKey.CURRENT_BUSINESS
    );
    this.business = !_.isEmpty(business) ? JSON.parse(business) || {} : {};

    if (this.media == "image") {
      this.title = "Media Images";
    } else if (this.media == "print") {
      this.title = "Media Prints";
    } else if (this.media == "video") {
      this.title = "Media Videos";
    } else if (this.media == "albums") {
      this.title = "Album Media Images";
    }
  }

  ionViewWillEnter() {
    console.log("ionViewWillEnter ImageModalPage");
    this.pageImage = 0;
    this.perPage = 20;

    this.initData();
  }

  checkChanged(item) {
    this.selected = item;
    if (this.media == "image") {
      _.each(this.imagesArr, (row: any) => {
        if (item.id == row.id && this.showCheckbox) {
          row.selected = !row.selected;
        }
      });
    } else if (this.media == "print") {
      _.each(this.printsArr, (row: any) => {
        if (item.id == row.id && this.showCheckbox) {
          row.selected = !row.selected;
        }
      });
    } else if (this.media == "video") {
      _.each(this.videosArr, (row: any) => {
        if (item.id == row.id && this.showCheckbox) {
          row.selected = !row.selected;
        }
      });
    } else if (this.media == "albums") {
      _.each(this.imagesArr, (row: any) => {
        if (item.id == row.id && this.showCheckbox) {
          row.selected = !row.selected;
        }
      });
    }
  }

  doRefresh(ev) {
    this.perPage = 20;
    if (this.media == "image" || this.media == "albums") {
      this.pageImage = 0;
      this.imagesArr = [];
    } else if (this.media == "print") {
      this.pagePrint = 0;
      this.printsArr = [];
    } else if (this.media == "video") {
      this.pageVideo = 0;
      this.videosArr = [];
    }
    this.initData(ev);
  }

  doInfinite(ev) {
    if (this.media == "image" || this.media == "albums") {
      this.pageImage = this.pageImage + 1;
    } else if (this.media == "print") {
      this.pagePrint = this.pagePrint + 1;
    } else if (this.media == "video") {
      this.pageVideo = this.pageVideo + 1;
    }
    this.initData(ev);
  }

  selectClick() {
    this.showCheckbox = true;
  }

  cancelClick() {
    this.showCheckbox = false;
    if (this.media == "image") {
      _.each(this.imagesArr, (row: any) => {
        row.selected = false;
      });
    } else if (this.media == "print") {
      _.each(this.printsArr, (row: any) => {
        row.selected = false;
      });
    } else if (this.media == "video") {
      _.each(this.videosArr, (row: any) => {
        row.selected = false;
      });
    } else if (this.media == "albums") {
      _.each(this.imagesArr, (row: any) => {
        row.selected = false;
      });
    }
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  addImage() {
    let selectedDatas: any[] = [];

    if (this.business.role !== 1) {
      this.toastCtrl
        .create({
          message: "Invalid Access!!!",
          duration: 3000
        })
        .present();
      return;
    }

    if (!this.showCheckbox) {
      return;
    }

    if (this.media == "image") {
      selectedDatas = _.filter(this.imagesArr, row => {
        return row.selected == true;
      });
    } else if (this.media == "print") {
      selectedDatas = _.filter(this.printsArr, row => {
        return row.selected == true;
      });
    } else if (this.media == "video") {
      selectedDatas = _.filter(this.videosArr, row => {
        return row.selected == true;
      });
    } else if (this.media == "albums") {
      selectedDatas = _.filter(this.imagesArr, row => {
        return row.selected == true;
      });
    }
    console.log("selectedDatas: ", selectedDatas);

    if (_.isEmpty(selectedDatas)) {
      return;
    }

    const addImageToCollection = () => {
      const loading = this.loadingCtrl.create({
        dismissOnPageChange: true
      });
      loading.present();
      async.eachSeries(
        selectedDatas,
        (item: any, callback) => {
          this.collections
            .collection_add_media({
              id: this.params.id,
              media: this.media,
              mediaId: item.id
            })
            .then(
              (data: any) => {
                if (data && data.success) {
                  console.log("data: ", data);
                  const toast = this.toastCtrl.create({
                    message: data.data,
                    duration: 1000,
                    showCloseButton: true,
                    dismissOnPageChange: true
                  });
                  toast.present();
                }
                callback();
              },
              error => {
                console.log("error: ", error);
                if (error && !error.success) {
                  const toast = this.toastCtrl.create({
                    message: error.message
                  });
                  toast.present();
                }
                callback();
              }
            );
        },
        () => {
          loading.dismiss();
          setTimeout(() => {
            this.viewCtrl.dismiss("save");
          }, 1000);
        }
      );
    };

    const addImageToAlbums = () => {
      selectedDatas = _.map(selectedDatas, (row: any) => {
        return {
          id: row.id
        };
      });
      console.log("selectedDatas: 2", selectedDatas);

      const loading = this.loadingCtrl.create({
        dismissOnPageChange: true
      });
      loading.present();
      this.albums
        .album_add_image({
          albumId: this.params.albumId,
          image: selectedDatas
        })
        .then(
          (data: any) => {
            if (data && data.success) {
              setTimeout(() => {
                this.viewCtrl.dismiss("save");
              }, 1000);
            }
            loading.dismiss();
          },
          error => {
            console.log("error: ", error);
            loading.dismiss();
            if (error && !error.success) {
              const toast = this.toastCtrl.create({
                message: error.message
              });
              toast.present();
            }
          }
        );
    };

    const addVideoToCollections = () => {
      const loading = this.loadingCtrl.create({
        dismissOnPageChange: true
      });
      loading.present();
      async.eachSeries(
        selectedDatas,
        (item: any, callback) => {
          this.channels
            .channel_add_video({
              id: this.params.id,
              videoId: item.id
            })
            .then(
              (data: any) => {
                if (data && data.success) {
                  console.log("data: ", data);
                  const toast = this.toastCtrl.create({
                    message: data.data,
                    duration: 1000,
                    showCloseButton: true,
                    dismissOnPageChange: true
                  });
                  toast.present();
                }
                callback();
              },
              error => {
                console.log("error: ", error);
                if (error && !error.success) {
                  const toast = this.toastCtrl.create({
                    message: error.message
                  });
                  toast.present();
                }
                callback();
              }
            );
        },
        () => {
          loading.dismiss();
          setTimeout(() => {
            this.viewCtrl.dismiss("save");
          }, 1000);
        }
      );
    };

    const confirm = this.alertCtrl.create({
      title: "Add " + this.title + "?",
      message: "Are you sure to add the selected " + this.title + "?",
      buttons: [
        {
          text: "No",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Yes",
          handler: () => {
            if (this.media !== "albums") {
              if (this.mode == "collection") {
                addImageToCollection();
              } else if (this.mode == "channel") {
                addVideoToCollections();
              }
            } else if (this.media == "albums") {
              if (this.mode == "albums") {
                addImageToAlbums();
              }
            }
          }
        }
      ]
    });
    confirm.present();
  }
}
