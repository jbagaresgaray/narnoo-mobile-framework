import { Component, ViewEncapsulation } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  ViewController,
  AlertController
} from "ionic-angular";

import { RolesServicesProvider } from "../../providers/services/roles";

@IonicPage()
@Component({
  selector: "page-user-privacy",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "user-privacy.html"
})
export class UserPrivacyPage {
  notification: any = {};

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public viewCtrl: ViewController,
    public alertCtrl: AlertController,
    public privacy: RolesServicesProvider
  ) {}

  private initData() {
    const loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    loading.present();
    this.privacy.privacy_settings().then(
      (data: any) => {
        if (data && data.success) {
          this.notification = data.data;
        }
        console.log("this.notification: ", this.notification);
        loading.dismiss();
      },
      (error: any) => {
        console.log("Error: ", error);
        loading.dismiss();
      }
    );
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad CreateFeedPage");
    setTimeout(() => {
      this.initData();
    }, 600);
  }

  saveEntry() {
    const savePrivacySettings = () => {
      const loading = this.loadingCtrl.create({
        dismissOnPageChange: true
      });
      loading.present();
      this.privacy.privacy_edit(this.notification).then(
        (data: any) => {
          if (data && data.success) {
            const alert = this.alertCtrl.create({
              title: "SUCCESS",
              message: data.data,
              buttons: ["OK"]
            });
            alert.onDidDismiss(() => {
              this.navCtrl.pop();
            });
            alert.present();
          }
          loading.dismiss();
        },
        (error: any) => {
          console.log("Error: ", error);
          loading.dismiss();
          if (error && !error.success) {
            this.alertCtrl
              .create({
                title: "ERROR",
                message: error.error,
                buttons: ["OK"]
              })
              .present();
          }
        }
      );
    };

    const confirm = this.alertCtrl.create({
      title: "Update user privacy?",
      message: "Save changes on privacy settings?",
      buttons: [
        {
          text: "Cancel",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Confirm",
          handler: () => {
            console.log("Agree clicked");
            savePrivacySettings();
          }
        }
      ]
    });
    confirm.present();
  }
}
