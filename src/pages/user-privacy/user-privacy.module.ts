import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserPrivacyPage } from './user-privacy';

@NgModule({
  declarations: [
    UserPrivacyPage,
  ],
  imports: [
    IonicPageModule.forChild(UserPrivacyPage),
  ],
})
export class UserPrivacyPageModule {}
