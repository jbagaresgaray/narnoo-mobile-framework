import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BusinessUserDetailPage } from './business-user-detail';

import { ComponentsModule } from '../../components/components.module';

@NgModule({
	declarations: [
		BusinessUserDetailPage,
	],
	imports: [
		ComponentsModule,
		IonicPageModule.forChild(BusinessUserDetailPage),
	],
})
export class BusinessUserDetailPageModule { }
