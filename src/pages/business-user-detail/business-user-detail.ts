import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  AlertController,
  LoadingController,
  ModalController,
  ToastController
} from "ionic-angular";
import * as _ from "lodash";

import { BusinessUserEntryPage } from "../../pages/business-user-entry/business-user-entry";
import { StaffDirectoryEntryPage } from "../../pages/staff-directory-entry/staff-directory-entry";

import { StaffServicesProvider } from "../../providers/services/staff";
import { UtilitiesServicesProvider } from "../../providers/services/utilities";

@IonicPage()
@Component({
  selector: "page-business-user-detail",
  templateUrl: "business-user-detail.html"
})
export class BusinessUserDetailPage {
  posts: any[] = [];
  imageUrl: string = "assets/img/bg.png";
  user: any = {};
  business: any = {};

  action: string;
  title: string;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController,
    public staffs: StaffServicesProvider,
    public utilities: UtilitiesServicesProvider
  ) {
    this.business = JSON.parse(localStorage.getItem("business")) || {};

    this.user = navParams.get("item");
    if (_.isEmpty(this.user.name)) {
      this.user.name = this.user.firstName + " " + this.user.lastName;
    }
    console.log("user: ", this.user);

    this.action = navParams.get("action");
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad BusinessUserDetailPage");
    this.initializeData();
  }

  initializeData() {
    if (this.action == "staff") {
      this.title = "Staff Profile";
    } else {
      this.title = "Profile Page";
    }
  }

  errorAvatarHandler(event) {
    event.target.src = this.utilities.radomizeAvatar();
  }

  private reloadUsers = resp => {
    console.log("resp: ", resp);
    return new Promise((resolve, reject) => {
      if (resp == "save") {
        this.navCtrl.pop();
        resolve();
      }
    });
  };

  editUser() {
    if (this.business.role !== 1) {
      this.toastCtrl
        .create({
          message: "Invalid Access!!!",
          duration: 3000
        })
        .present();
      return;
    }

    if (this.action == "staff") {
      const profileModal = this.modalCtrl.create(StaffDirectoryEntryPage, {
        action: "update",
        type: "staff",
        item: this.user
      });
      profileModal.present();
      profileModal.onDidDismiss(status => {
        console.log("status: ", status);
        if (status) {
          this.initializeData();
        }
      });
    } else {
      this.navCtrl.push(BusinessUserEntryPage, {
        action: "update",
        item: this.user,
        callback: this.reloadUsers
      });
    }
  }

  deleteUser() {
    if (this.business.role !== 1) {
      this.toastCtrl
        .create({
          message: "Invalid Access!!!",
          duration: 3000
        })
        .present();
      return;
    }
    const deleteUser = () => {
      const loading = this.loadingCtrl.create({
        content: "Deleting..."
      });
      loading.present();

      setTimeout(() => {
        loading.dismiss();
      }, 1000);
    };

    const deleteStaff = () => {
      let loading = this.loadingCtrl.create({
        content: "Deleting..."
      });
      loading.present();
      this.staffs.staff_remove(this.user).then((data: any) => {
        if (data && data.success) {
          loading.dismiss();
          this.navCtrl.pop();
        }
      });
    };

    const confirm = this.alertCtrl.create({
      title: this.action == "staff" ? "Delete Staff?" : "Delete User?",
      message:
        this.action == "staff"
          ? "Are you sure to delete this Staff?"
          : "Are you sure to delete this user?",
      buttons: [
        {
          text: "No",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Yes",
          handler: () => {
            console.log("Agree clicked");

            if (this.action == "staff") {
              deleteStaff();
            } else {
              deleteUser();
            }
          }
        }
      ]
    });
    confirm.present();
  }
}
