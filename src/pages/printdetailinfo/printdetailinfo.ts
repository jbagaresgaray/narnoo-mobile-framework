import { Component, ViewEncapsulation, NgZone } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  AlertController,
  LoadingController
} from "ionic-angular";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from "@angular/forms";

import { PrintServicesProvider } from "../../providers/services/print";

@IonicPage()
@Component({
  selector: "page-printdetailinfo",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "printdetailinfo.html"
})
export class PrintdetailinfoPage {
  item: any = {};
  token: string;
  callback: any;

  isSaving = false;
  submitted = false;

  printForm: FormGroup;

  constructor(
    public zone: NgZone,
    private formBuilder: FormBuilder,
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public prints: PrintServicesProvider
  ) {
    this.callback = this.navParams.get("callback");
    this.item = this.navParams.get("print");
    this.item.privacy = this.item.privilege;

    this.printForm = this.formBuilder.group({
      brochure_caption: ["", Validators.required],
      caption: [""],
      validity: [""],
      validityDate: ["", Validators.required],
      type: ["", Validators.required],
      privacy: ["", Validators.required]
    });
  }

  get f(): any {
    return this.printForm.controls;
  }

  private validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad PrintdetailinfoPage");
    this.item = this.navParams.get("print");
    if (this.item) {
      this.printForm.patchValue(this.item);
    }
  }

  save() {
    this.submitted = true;
    this.isSaving = true;

    if (this.printForm.invalid) {
      this.isSaving = false;
      this.validateAllFormFields(this.printForm);
      return;
    }

    const saveEntry = () => {
      const loading = this.loadingCtrl.create({
        dismissOnPageChange: true
      });
      loading.present();

      this.printForm.patchValue({
        caption: this.item.brochure_caption,
        validity: this.item.validityDate
      });

      this.prints.edit_print(this.printForm.value).then(
        (data: any) => {
          if (data && data.success) {
            this.submitted = false;
            this.isSaving = false;

            const alert = this.alertCtrl.create({
              title: "Success!",
              message: data.data,
              buttons: ["OK"]
            });
            alert.onDidDismiss(() => {
              this.callback("save").then(() => {
                this.navCtrl.pop();
              });
            });

            loading.dismiss().then(() => {
              alert.present();
            });
          } else {
            this.submitted = false;
            this.isSaving = false;

            const alert = this.alertCtrl.create({
              title: "Error!",
              message: data.message,
              buttons: ["OK"]
            });
            loading.dismiss().then(() => {
              alert.present();
            });
          }
        },
        error => {
          console.log("error: ", error);
          loading.dismiss();
          this.submitted = false;
          this.isSaving = false;
        }
      );
    };

    const confirm = this.alertCtrl.create({
      title: "Save Entry?",
      message: "Do you want to save the entry?",
      buttons: [
        {
          text: "Cancel",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Confirm",
          handler: () => {
            console.log("Agree clicked");
            saveEntry();
          }
        }
      ]
    });
    confirm.present();
  }
}
