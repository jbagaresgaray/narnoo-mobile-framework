import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { ReactiveFormsModule } from "@angular/forms";
import { IonicImageLoader } from "ionic-image-loader";

import { PrintdetailinfoPage } from "./printdetailinfo";

@NgModule({
  declarations: [PrintdetailinfoPage],
  imports: [
    IonicPageModule.forChild(PrintdetailinfoPage),
    IonicImageLoader,
    ReactiveFormsModule
  ]
})
export class PrintdetailinfoPageModule {}
