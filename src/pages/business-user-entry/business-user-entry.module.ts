import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { ReactiveFormsModule } from "@angular/forms";

import { BusinessUserEntryPage } from "./business-user-entry";

@NgModule({
  declarations: [BusinessUserEntryPage],
  imports: [
    ReactiveFormsModule,
    IonicPageModule.forChild(BusinessUserEntryPage)
  ]
})
export class BusinessUserEntryPageModule {}
