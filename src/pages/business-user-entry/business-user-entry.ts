import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  AlertController,
  ViewController,
  ToastController
} from "ionic-angular";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from "@angular/forms";
import * as _ from "lodash";

import { UserServicesProvider } from "../../providers/services/users";
import { StorageProvider } from "../../providers/storage/storage";

@IonicPage()
@Component({
  selector: "page-business-user-entry",
  templateUrl: "business-user-entry.html"
})
export class BusinessUserEntryPage {
  token: string;
  item: any = {};
  business: any = {};
  action: string;
  callback: any;

  isSaving = false;
  submitted = false;

  inputForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public viewCtrl: ViewController,
    public toastCtrl: ToastController,
    public users: UserServicesProvider,
    public storageService: StorageProvider
  ) {
    this.item = navParams.get("item");
    this.action = navParams.get("action");
    this.callback = this.navParams.get("callback");
    console.log("this.action: ", this.action);
    console.log("this.callback: ", this.callback);

    this.inputForm = this.formBuilder.group({
      email: ["", Validators.required],
      role: ["", Validators.required],
      id: ""
    });

    if (this.item) {
      this.inputForm.patchValue(this.item);
    }
  }

  get f(): any {
    return this.inputForm.controls;
  }

  private validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  async ionViewDidLoad() {
    console.log("ionViewDidLoad BusinessUserEntryPage");
    const business = await this.storageService.getStorage(
      this.storageService.storageKey.CURRENT_BUSINESS
    );
    this.business = !_.isEmpty(business) ? JSON.parse(business) || {} : {};

    if (this.item) {
      this.inputForm.patchValue(this.item);
    }
  }

  ionViewWillLoad() {
    this.viewCtrl.showBackButton(false);
  }

  save() {
    if (this.business.role !== 1) {
      this.toastCtrl
        .create({
          message: "Invalid Access!!!",
          duration: 3000
        })
        .present();
      return;
    }

    this.submitted = true;
    this.isSaving = true;

    if (this.inputForm.invalid) {
      this.isSaving = false;
      this.validateAllFormFields(this.inputForm);
      return;
    }
    console.log("this.inputForm.value: ", this.inputForm.value);

    const saveUserEntry = () => {
      const loading = this.loadingCtrl.create({
        dismissOnPageChange: true
      });
      loading.present();
      this.users.add_user(this.inputForm.value).then(
        (data: any) => {
          if (data && data.success) {
            this.submitted = false;
            this.isSaving = false;

            const alert = this.alertCtrl.create({
              title: "Success!",
              subTitle: data.data,
              buttons: ["OK"]
            });
            alert.onDidDismiss(() => {
              // this.navCtrl.pop();
              this.viewCtrl.dismiss("save");
            });

            loading.dismiss().then(() => {
              alert.present();
            });
          } else {
            this.submitted = false;
            this.isSaving = false;

            const alert = this.alertCtrl.create({
              title: "Error!",
              subTitle: data.message,
              buttons: ["OK"]
            });
            loading.dismiss().then(() => {
              alert.present();
            });
          }
        },
        error => {
          console.log("error: ", error);
          this.submitted = false;
          this.isSaving = false;
          loading.dismiss();
        }
      );
    };

    const updateUserEntry = () => {
      const loading = this.loadingCtrl.create({
        dismissOnPageChange: true
      });
      loading.present();
      this.users.edit_access(this.inputForm.value).then(
        (data: any) => {
          if (data && data.success) {
            this.submitted = false;
            this.isSaving = false;

            const alert = this.alertCtrl.create({
              title: "Success!",
              subTitle: data.data,
              buttons: ["OK"]
            });
            alert.onDidDismiss(() => {
              this.callback("save").then(() => {
                // this.viewCtrl.dismiss("save");
                this.navCtrl.pop();
              });
            });

            loading.dismiss().then(() => {
              alert.present();
            });
          } else {
            this.submitted = false;
            this.isSaving = false;

            const alert = this.alertCtrl.create({
              title: "Error!",
              subTitle: data.message,
              buttons: ["OK"]
            });
            loading.dismiss().then(() => {
              alert.present();
            });
          }
        },
        error => {
          console.log("error: ", error);
          this.submitted = false;
          this.isSaving = false;
          loading.dismiss();
        }
      );
    };

    const confirm = this.alertCtrl.create({
      title: "Save Entry?",
      message: "Do you want to save the entry?",
      buttons: [
        {
          text: "Cancel",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Confirm",
          handler: () => {
            console.log("Agree clicked");
            if (this.action == "create") {
              saveUserEntry();
            } else {
              updateUserEntry();
            }
          }
        }
      ]
    });
    confirm.present();
  }

  close() {
    this.viewCtrl.dismiss();
  }
}
