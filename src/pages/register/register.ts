import { Component } from "@angular/core";
import {
  NavController,
  NavParams,
  ToastController,
  AlertController
} from "ionic-angular";

import { InAppBrowser } from "@ionic-native/in-app-browser";

import { RegisterServicesProvider } from "../../providers/services/register";

import { environment } from "../../environments/environment";

@Component({
  selector: "page-register",
  templateUrl: "register.html"
})
export class RegisterPage {
  users: any = {};
  isAgree: boolean = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public services: RegisterServicesProvider,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public iab: InAppBrowser
  ) {
    /*this.users = {
      firstName: 'Philip Cesar',
      lastName: 'Garay',
      email: 'philipgaray2@gmail.com',
      password: '12345678',
      confirmation: '12345678'
    };*/
  }

  updateCucumber() {
    this.isAgree = !this.isAgree;
  }

  registerUser() {
    this.services.user(this.users).then(
      (data: any) => {
        if (data) {
          if (data.success) {
            let alert = this.alertCtrl.create({
              title: "Success",
              subTitle: data.message,
              buttons: ["OK"]
            });
            alert.present().then(() => {
              this.navCtrl.pop();
            });
          } else {
            let toast = this.toastCtrl.create({
              message: data.message,
              duration: 2000
            });
            toast.present();
          }
        }
      },
      error => {
        let toast = this.toastCtrl.create({
          message: error.message,
          duration: 2000
        });
        toast.present();
      }
    );
  }

  viewTerms(event) {
    console.log("viewTerms: ", environment.AppTerms);
    event.preventDefault();
    event.stopPropagation();

    this.iab.create(environment.AppTerms, "_blank", {
      toolbarposition: "top",
      toolbar: "yes",
      location: "no"
    });
  }
}
