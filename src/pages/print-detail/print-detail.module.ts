import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { IonicImageLoader } from "ionic-image-loader";
import { PipesModule } from "../../pipes/pipes.module";
import { IonicImageViewerModule } from "ionic-img-viewer";

import { ComponentsModule } from "../../components/components.module";

import { PrintDetailPage, PrintDetailPopoverPage } from "./print-detail";

@NgModule({
  declarations: [PrintDetailPage, PrintDetailPopoverPage],
  imports: [
    IonicPageModule.forChild(PrintDetailPage),
    IonicPageModule.forChild(PrintDetailPopoverPage),
    IonicImageLoader,
    IonicImageLoader,
    PipesModule,
    ComponentsModule,
    IonicImageViewerModule
  ]
})
export class PrintDetailPageModule {}
