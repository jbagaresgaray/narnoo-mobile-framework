import { Component, NgZone, ViewEncapsulation, OnInit } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  AlertController,
  Platform,
  ActionSheetController,
  ModalController,
  ViewController,
  Events,
  PopoverController,
  ToastController
} from "ionic-angular";
import * as $ from "jquery";
import * as _ from "lodash";
import { CacheService, Cache } from "ionic-cache-observable";
import { Observable, Subscription } from "rxjs";
import "rxjs/add/operator/finally";

import { PrintServicesProvider } from "../../providers/services/print";

import { PrintdetailinfoPage } from "../../pages/printdetailinfo/printdetailinfo";
import { ProductsModalPage } from "../../pages/products-modal/products-modal";
import { CollectionsModalPage } from "../../pages/collections-modal/collections-modal";
import { ChatPage } from "../../pages/chat/chat";
import { FileUploadPage } from "../../pages/file-upload/file-upload";

import { FileTransfer, FileTransferObject } from "@ionic-native/file-transfer";
import { File } from "@ionic-native/file";
import { LocalNotifications } from "@ionic-native/local-notifications";
import { FileOpener } from "@ionic-native/file-opener";
import { SocialSharing } from "@ionic-native/social-sharing";

import { environment } from "../../environments/environment";
import { StorageProvider } from "../../providers/storage/storage";
import { LoadingProvider } from "../../providers/loading/loading";
declare let safari: any;

@Component({
  template: `
    <ion-list>
      <button ion-item (click)="downloadImage()">
        <ion-icon
          ios="ios-download-outline"
          md="ios-download-outline"
          item-start
        ></ion-icon>
        Download
      </button>
      <button
        ion-item
        (click)="shareNow()"
        *ngIf="
          action != 'connect' &&
          action != 'notif' &&
          (business.role == 1 || business.role == 2)
        "
      >
        <ion-icon name="md-share" item-start></ion-icon>
        Share
      </button>
    </ion-list>
  `
})
export class PrintDetailPopoverPage implements OnInit {
  showDownload: boolean;
  action: any;
  business: any = {};

  constructor(
    public events: Events,
    public params: NavParams,
    public viewCtrl: ViewController,
    public storageService: StorageProvider
  ) {
    this.showDownload = params.get("showDownload");
    this.action = params.get("action");
  }

  async ngOnInit() {
    const business = await this.storageService.getStorage(
      this.storageService.storageKey.CURRENT_BUSINESS
    );
    this.business = !_.isEmpty(business) ? JSON.parse(business) || {} : {};
  }

  close() {
    this.viewCtrl.dismiss();
  }

  viewChat() {
    this.events.publish("PrintDetailPage:viewChat");
    this.viewCtrl.dismiss();
  }

  downloadImage() {
    this.events.publish("PrintDetailPage:downloadImage");
    this.viewCtrl.dismiss();
  }

  shareNow() {
    this.events.publish("PrintDetailPage:shareNow");
    this.viewCtrl.dismiss();
  }
}
@IonicPage()
@Component({
  selector: "page-print-detail",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "print-detail.html"
})
export class PrintDetailPage {
  item: any = {};
  business: any = {};
  notif_business: any = {};
  showDownload: boolean;
  showLoading: boolean = true;
  showError: boolean = false;
  isSafari: boolean;

  params: any = {};
  action: string;
  connect_params: any = {};
  callback: any;
  contentErr: string = "";
  storageDirectory: string = "";
  fileTransfer: FileTransferObject;

  mediaArr$: Observable<any>;
  private mediaCache: Cache<any>;
  public refreshSubscription: Subscription;

  constructor(
    public platform: Platform,
    public navCtrl: NavController,
    public navParams: NavParams,
    public print: PrintServicesProvider,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public actionSheetCtrl: ActionSheetController,
    private transfer: FileTransfer,
    public zone: NgZone,
    public modalCtrl: ModalController,
    public popoverCtrl: PopoverController,
    public events: Events,
    public toastCtrl: ToastController,
    private file: File,
    private localNotifications: LocalNotifications,
    private fileOpener: FileOpener,
    private socialSharing: SocialSharing,
    public storageService: StorageProvider,
    public cacheService: CacheService,
    public loader: LoadingProvider
  ) {
    this.isSafari =
      /constructor/i.test(window["HTMLElement"]) ||
      (function(p) {
        return p.toString() === "[object SafariRemoteNotification]";
      })(!window["safari"] || safari.pushNotification);

    this.params = navParams.get("print");
    this.action = navParams.get("action");
    this.connect_params = navParams.get("params");
    this.callback = navParams.get("callback");
    this.notif_business = navParams.get("business");

    if (platform.is("cordova")) {
      if (this.platform.is("ios")) {
        this.storageDirectory =
          window["cordova"].file.documentsDirectory + environment.AlbumName;
      } else if (this.platform.is("android")) {
        this.storageDirectory =
          window["cordova"].file.externalRootDirectory + environment.AlbumName;
      }
    }

    // ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //
    // ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //
    // ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //

    events.unsubscribe("PrintDetailPage:downloadImage");
    events.unsubscribe("PrintDetailPage:viewChat");
    events.unsubscribe("PrintDetailPage:shareNow");

    events.subscribe("PrintDetailPage:downloadImage", () => {
      this.downloadImage();
    });

    events.subscribe("PrintDetailPage:viewChat", () => {
      this.viewChat();
    });

    events.subscribe("PrintDetailPage:shareNow", () => {
      this.shareNow();
    });
  }

  private initData(refresher?: any) {
    const successResponse = (data: any) => {
      if (data && data.success) {
        const item = data.data;

        this.zone.run(() => {
          this.item = item;
          this.item.size = parseFloat(this.item.size);
          console.log("this.item: ", this.item);

          if (_.isEmpty(this.item.brochure_caption)) {
            this.item.brochure_caption = "No caption available";
          }

          if (this.platform.is("cordova")) {
            if (this.platform.is("ios")) {
              if (this.item.uploadedAt) {
                this.item.uploadedAt = new Date(
                  this.item.uploadedAt.replace(/-/g, "/")
                );
              }
            }
          } else {
            if (this.platform.is("ios")) {
              if (this.isSafari) {
                if (this.item.uploadedAt) {
                  this.item.uploadedAt = new Date(
                    this.item.uploadedAt.replace(/-/g, "/")
                  );
                }
              } else {
                if (this.item.uploadedAt) {
                  this.item.uploadedAt = new Date(this.item.uploadedAt);
                }
              }
            }
          }
        });
      }
      this.showLoading = false;
      this.showError = false;
      if (refresher) {
        refresher.complete();
      }
      setTimeout(() => {
        $(".progressive-image").each(function() {
          const image = new Image();
          const previewImage = $(this).find(".loadingImage");
          const newImage = $(this).find(".overlay");

          image.onload = function() {
            newImage.css("background-image", "url(" + image.src + ")");
            newImage.css("opacity", "1");
          };
          image.onerror = function() {
            newImage.css("background-image", "url(./assets/img/thumb.jpg)");
            newImage.css("opacity", "1");
            console.log("complete");
          };
          image.src = previewImage.data("image");
        });
      }, 1000);
    };

    const errorResponse = error => {
      this.showLoading = false;
      this.showError = true;
      if (refresher) {
        refresher.complete();
      }
      if (error) {
        this.contentErr = error.message || "No print media found!";
      }
    };

    let dataObservable: any;
    let cacheKey: any;
    this.showLoading = true;

    if (this.action == "connect") {
      if (this.connect_params.type == "operator") {
        dataObservable = Observable.fromPromise(
          this.print.brochure_operator_detail(
            this.params.id,
            this.connect_params.id
          )
        );
        cacheKey =
          this.storageService.cacheKey.MEDIA_LOGO_DETAIL +
          "_" +
          this.business.id +
          "_operator_" +
          "_" +
          this.connect_params.id +
          "_" +
          this.params.id;
      } else if (this.connect_params.type == "distributor") {
        dataObservable = Observable.fromPromise(
          this.print.brochure_distributor_detail(
            this.params.id,
            this.connect_params.id
          )
        );
        cacheKey =
          this.storageService.cacheKey.MEDIA_LOGO_DETAIL +
          "_" +
          this.business.id +
          "_distributor_" +
          "_" +
          this.connect_params.id +
          "_" +
          this.params.id;
      }
    } else if (this.action == "notif") {
      dataObservable = Observable.fromPromise(
        this.print.brochure_detail(this.params.id)
      );
      cacheKey =
        this.storageService.cacheKey.MEDIA_LOGO_DETAIL +
        "_" +
        this.business.id +
        "_" +
        this.params.id;
    } else {
      dataObservable = Observable.fromPromise(
        this.print.brochure_detail(this.params.id)
      );
      cacheKey =
        this.storageService.cacheKey.MEDIA_LOGO_DETAIL +
        "_" +
        this.business.id +
        "_" +
        this.params.id;
    }

    this.cacheService.register(cacheKey, dataObservable).subscribe(cache => {
      this.mediaCache = cache;
      this.zone.run(() => {
        this.mediaArr$ = cache.get$;

        this.mediaCache.refresh().subscribe(
          () => {
            this.loader.start();
          },
          () => {
            this.loader.complete();
          },
          () => {
            this.loader.complete();
          }
        );
      });
    });

    this.mediaArr$.subscribe(successResponse, errorResponse);
  }

  private async initBusiness() {
    const business = await this.storageService.getStorage(
      this.storageService.storageKey.CURRENT_BUSINESS
    );
    this.business = !_.isEmpty(business) ? JSON.parse(business) || {} : {};
  }

  async ionViewDidLoad() {
    console.log("ionViewDidLoad PrintDetailPage");
    this.initBusiness().then(() => {
      this.showDownload = this.business.paid;
      this.initData();
    });
  }

  ionViewWillLeave() {
    if (this.fileTransfer) {
      this.fileTransfer.abort();
    }
  }

  presentPopover(ev) {
    const popover = this.popoverCtrl.create(PrintDetailPopoverPage, {
      showDownload: this.business.paid,
      action: this.action
    });
    popover.present({ ev: ev });
  }

  updatePrintInfo() {
    if (this.business.role !== 1) {
      const toast = this.toastCtrl.create({
        message: "Invalid Access!!!",
        duration: 3000
      });
      toast.present();
      return;
    }

    this.navCtrl.push(PrintdetailinfoPage, {
      print: this.item,
      callback: this.getPrintDetails
    });
  }

  updatePrintFile() {
    if (this.business.role !== 1) {
      const toast = this.toastCtrl.create({
        message: "Invalid Access!!!",
        duration: 3000
      });
      toast.present();
      return;
    }

    const modal = this.modalCtrl.create(FileUploadPage, {
      action: "update_print_policy",
      title: "Update Brochure File",
      params: {
        printId: this.item.id
      }
    });
    modal.onDidDismiss(resp => {
      if (resp && resp == "save") {
        this.getPrintDetails(resp);
      }
    });
    modal.present();
  }

  private getPrintDetails = resp => {
    return new Promise((resolve, reject) => {
      if (resp == "save") {
        this.initData();
        resolve();
      }
    });
  };

  doRefresh(refresher: any) {
    if (this.mediaCache) {
      this.showLoading = true;
      this.zone.run(() => {
        this.initData(refresher);
      });
    } else {
      console.warn("Cache has not been initialized.");
      if (refresher) {
        refresher.cancel();
      }
    }
  }

  presentActionSheet() {
    if (this.business.role !== 1) {
      const toast = this.toastCtrl.create({
        message: "Invalid Access!!!",
        duration: 3000
      });
      toast.present();
      return;
    }

    let actionSheetButtons: any[] = [
      {
        text: "Delete",
        role: "destructive",
        handler: () => {
          console.log("Archive clicked");
          setTimeout(() => {
            this.deletePrint();
          }, 300);
        }
      },
      {
        text: "Cancel",
        role: "cancel",
        handler: () => {
          console.log("Cancel clicked");
        }
      }
    ];

    if (this.business.type == 1) {
      actionSheetButtons.unshift({
        text: "Add to Product as Feature",
        handler: () => {
          console.log("Archive clicked");
          this.addToProductFeature();
        }
      });
    }

    actionSheetButtons.unshift({
      text: "Add Brochure to collection",
      handler: () => {
        console.log("Archive clicked");
        this.addPrintToCollection();
      }
    });

    if (this.business.role == 1) {
      actionSheetButtons.unshift(
        {
          text: "Edit Details",
          handler: () => {
            console.log("Destructive clicked");
            this.updatePrintInfo();
          }
        },
        {
          text: "Update File",
          handler: () => {
            console.log("Destructive clicked");
            this.updatePrintFile();
          }
        }
      );
    }

    let actionSheet = this.actionSheetCtrl.create({
      title: "Select action for brochure",
      buttons: actionSheetButtons
    });
    actionSheet.present();
  }

  private addToProductFeature() {
    if (this.business.role !== 1) {
      const toast = this.toastCtrl.create({
        message: "Invalid Access!!!",
        duration: 3000
      });
      toast.present();
      return;
    }

    let modal = this.modalCtrl.create(ProductsModalPage, {
      image: this.item,
      action: "feature",
      type: "print"
    });
    modal.present();
  }

  private addPrintToCollection() {
    if (this.business.role !== 1) {
      const toast = this.toastCtrl.create({
        message: "Invalid Access!!!",
        duration: 3000
      });
      toast.present();
      return;
    }

    let modal = this.modalCtrl.create(CollectionsModalPage, {
      type: "print",
      params: this.item
    });
    modal.present();
  }

  private deletePrint() {
    if (this.business.role !== 1) {
      const toast = this.toastCtrl.create({
        message: "Invalid Access!!!",
        duration: 3000
      });
      toast.present();
      return;
    }

    let loading = this.loadingCtrl.create({
      dismissOnPageChange: true,
      content: "Deleting..."
    });
    loading.present();
    this.print.print_delete([this.item.id]).then(
      (data: any) => {
        if (data && data.success) {
          console.log("print_delete: ", data);
          let alert = this.alertCtrl.create({
            title: "SUCCESS",
            message: "Print successfully deleted!",
            buttons: ["OK"]
          });
          alert.onDidDismiss(() => {
            this.callback("refresh").then(() => {
              this.navCtrl.pop();
            });
          });
          alert.present();
        }
        loading.dismiss();
      },
      (error: any) => {
        loading.dismiss();
        console.log("error: ", error);
      }
    );
  }

  downloadImage() {
    if (!this.business.paid) {
      this.alertCtrl
        .create({
          title: "WARNING",
          message: "This feature is only available to premium users only.",
          buttons: ["OK"]
        })
        .present();
      return;
    }

    let ctrl = this;
    let perc: any;
    const loading = ctrl.loadingCtrl.create({
      content: "Downloading ...",
      dismissOnPageChange: true
    });
    loading.present();
    const alert = ctrl.alertCtrl.create({
      title: "Success",
      subTitle: "Brochure successfully downloaded",
      buttons: ["OK"]
    });

    const download = (dataurl, filename) => {
      const a = document.createElement("a");
      a.href = dataurl;
      a.setAttribute("download", filename);
      const b = document.createEvent("MouseEvents");
      b.initEvent("click", false, true);
      a.dispatchEvent(b);
      return false;
    };

    const downloadNative = (image, filename) => {
      ctrl.platform.ready().then(() => {
        ctrl.fileTransfer = ctrl.transfer.create();
        const newDir = ctrl.storageDirectory + "/" + filename;
        console.log("newDir: ", newDir);
        console.log("filename: ", filename);

        ctrl.file
          .checkFile(ctrl.storageDirectory + environment.AlbumName, filename)
          .then((isExisted: any) => {
            console.log("isExisted: ", isExisted);
            if (isExisted) {
              loading.dismiss();
              ctrl.fileOpener
                .open(newDir, "application/pdf")
                .then(() => console.log("File is opened"))
                .catch(e => console.log("Error opening file", e));
            } else {
              ctrl.fileTransfer
                .download(image, newDir)
                .then(
                  entry => {
                    if (entry) {
                      const alertSuccess = ctrl.alertCtrl.create({
                        title: `Download Succeeded!`,
                        subTitle: "Download successfully",
                        buttons: ["Ok"]
                      });

                      loading.dismiss();
                      alertSuccess.present();
                    }
                  },
                  error => {
                    ctrl.fileTransfer.abort();

                    const alertFailure = ctrl.alertCtrl.create({
                      title: `Download Failed!`,
                      subTitle: `${image} was not successfully downloaded. Error code: ${
                        error.code
                      }`,
                      buttons: ["Ok"]
                    });
                    loading.dismiss();
                    alertFailure.present();
                  }
                )
                .catch(error => {
                  ctrl.fileTransfer.abort();
                  loading.dismiss();
                });
            }
          })
          .catch((error: any) => {
            console.log("checkFile error: ", error);
            if (error && error.message == "NOT_FOUND_ERR") {
              ctrl.fileTransfer
                .download(image, newDir)
                .then(
                  entry => {
                    console.log("entry: ", entry);
                    console.log("entry.toURL(); ", entry.toURL());
                    if (entry) {
                      const alertSuccess = ctrl.alertCtrl.create({
                        title: `Download Succeeded!`,
                        subTitle: "Download successfully",
                        buttons: ["Ok"]
                      });

                      loading.dismiss();
                      alertSuccess.present();
                    }
                  },
                  error => {
                    ctrl.fileTransfer.abort();
                    const alertFailure = ctrl.alertCtrl.create({
                      title: `Download Failed!`,
                      subTitle: `${image} was not successfully downloaded. Error code: ${
                        error.code
                      }`,
                      buttons: ["Ok"]
                    });
                    loading.dismiss();
                    alertFailure.present();
                  }
                )
                .catch(error => {
                  ctrl.fileTransfer.abort();
                  loading.dismiss();
                });
            } else {
              loading.dismiss();
            }
          });

        ctrl.fileTransfer.onProgress(progressEvent => {
          if (progressEvent.lengthComputable) {
            perc = Math.floor(
              (progressEvent.loaded / progressEvent.total) * 100
            );
            console.log("Progress: ", perc + "% loaded...");

            ctrl.zone.run(() => {
              loading.setContent("Downloading " + perc + "% ...");

              if (ctrl.platform.is("android")) {
                ctrl.localNotifications.schedule({
                  title: "Downloading " + filename,
                  text: "Downloading " + perc + "% ...",
                  sound: null,
                  progressBar: { value: perc }
                });
              }
            });
          }
        });
      });
    };

    const getFilename = url => {
      url = url
        .split("/")
        .pop()
        .replace(/\#(.*?)$/, "")
        .replace(/\?(.*?)$/, "");
      url = url.split("."); // separates filename and extension
      return { filename: url[0] || "", ext: url[1] || "" };
    };

    this.print.brochure_download(this.item.id).then(
      (data: any) => {
        if (data && data.success) {
          console.log("brochure_download: ", data.data);
          let filename =
            getFilename(data.data).filename + "." + getFilename(data.data).ext;
          if (!this.platform.is("cordova")) {
            download(data.data, filename);
            loading.dismiss();
            alert.present();
          } else {
            downloadNative(data.data, filename);
          }
        }
      },
      error => {
        loading.dismiss();
        if (error && !error.success) {
          this.alertCtrl
            .create({
              title: "WARNING",
              message: "Error while downloading file. " + error.message,
              buttons: ["OK"]
            })
            .present();
          return;
        }
      }
    );
  }

  viewChat() {
    const modal = this.modalCtrl.create(ChatPage);
    modal.present();
  }

  shareNow() {
    if (!this.business.paid) {
      this.alertCtrl
        .create({
          title: "WARNING",
          message: "This feature is only available to premium users only.",
          buttons: ["OK"]
        })
        .present();
      return;
    }

    if (!this.platform.is("cordova")) {
      this.alertCtrl
        .create({
          title: "WARNING",
          message:
            "Social Sharing is only available on actual device/simulator",
          buttons: ["OK"]
        })
        .present();
      return;
    }

    const loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    loading.present();
    this.print.share_url_print(this.item.id).then(
      (data: any) => {
        if (data && data.success) {
          console.log("data: ", data);
          this.socialSharing.share(
            "This link allows you to download the original version of this image. " +
              "You can send the link to whom ever you need and this will allow them access to the original file.",
            "Share",
            null,
            data.data
          );
          loading.dismiss();
        } else {
          loading.dismiss();
        }
      },
      error => {
        console.log("error: ", error);
        loading.dismiss();
        if (error) {
          if (!error.success) {
            this.alertCtrl
              .create({
                title: "ERROR",
                subTitle: error.message,
                buttons: ["OK"]
              })
              .present();
          }
        }
      }
    );
  }
}
