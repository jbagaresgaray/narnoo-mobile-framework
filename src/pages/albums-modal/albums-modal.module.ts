import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AlbumsModalPage } from './albums-modal';

@NgModule({
  declarations: [
    AlbumsModalPage,
  ],
  imports: [
    IonicPageModule.forChild(AlbumsModalPage),
  ],
})
export class AlbumsModalPageModule {}
