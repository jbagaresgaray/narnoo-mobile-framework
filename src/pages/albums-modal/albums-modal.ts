import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  AlertController,
  ToastController,
  ViewController
} from "ionic-angular";
import * as async from "async";
import * as _ from "lodash";

import { AlbumsServicesProvider } from "../../providers/services/albums";

@IonicPage()
@Component({
  selector: "page-albums-modal",
  templateUrl: "albums-modal.html"
})
export class AlbumsModalPage {
  image: any = {};
  selected: any = {};
  business: any = {};

  token: string;
  albumsArr: any[] = [];
  fakeArr: any[] = [];
  showContent: boolean = false;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public albums: AlbumsServicesProvider,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public viewCtrl: ViewController
  ) {
    this.image = navParams.get("image");
    this.token = localStorage.getItem("bus.token");
    this.business = JSON.parse(localStorage.getItem("business"));

    for (var i = 0; i < 12; ++i) {
      this.fakeArr.push(i);
    }
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad AlbumsModalPage");
  }

  ionViewWillEnter() {
    console.log("ionViewWillEnter AlbumsModalPage");
    let ctrl = this;
    this.albums.album_list(this.token).then(
      (data: any) => {
        if (data && data.success) {
          this.albumsArr = data.data.albums;
          async.each(
            this.albumsArr,
            (item: any, callback) => {
              item.imageCount = 0;
              item.images = [];
              item.checked = false;
              ctrl.albums.album_images(item.id).then(
                (images: any) => {
                  if (images && images.success) {
                    item.images = !_.isNull(images.data.images)
                      ? images.data.images
                      : [];
                    item.imageCount = !_.isNull(images.data.images)
                      ? images.data.images.length
                      : 0;
                  }
                  callback();
                },
                error => {
                  this.showContent = true;
                  callback();
                }
              );
            },
            () => {
              this.showContent = true;
            }
          );
          console.log("this.albumsArr: ", this.albumsArr);
        }
      },
      error => {
        this.showContent = true;
      }
    );
  }

  checkChanged(album) {
    this.selected = album;
    _.each(this.albumsArr, (row: any) => {
      if (album.id == row.id) {
        row.checked = true;
      } else {
        row.checked = false;
      }
    });
  }

  doRefresh(ev) {
    let ctrl = this;
    this.showContent = false;
    this.albums.album_list(this.token).then(
      (data: any) => {
        if (data && data.success) {
          this.albumsArr = [];
          this.albumsArr = data.data.albums;
          async.each(
            this.albumsArr,
            (item: any, callback) => {
              ctrl.albums.album_images(item.id).then(
                (images: any) => {
                  if (images && images.success) {
                    item.images = images.data.images;
                  }
                  callback();
                },
                error => {
                  this.showContent = true;
                  callback();
                }
              );
            },
            () => {
              ev.complete();
              this.showContent = true;
            }
          );
          console.log("this.albumsArr: ", this.albumsArr);
        }
      },
      error => {
        ev.complete();
        this.showContent = true;
      }
    );
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  addImage() {
    if (this.business.role !== 1) {
      this.toastCtrl
        .create({
          message: "Invalid Access!!!",
          duration: 3000
        })
        .present();
      return;
    }

    const loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    loading.present();
    this.albums
      .album_add_image({
        albumId: this.selected.id,
        image: [
          {
            id: this.image.id
          }
        ]
      })
      .then(
        (data: any) => {
          if (data && data.success) {
            const alert = this.alertCtrl.create({
              title: "Success",
              subTitle: "Image added to album",
              buttons: ["OK"]
            });
            loading.dismiss();
            alert.present();
            alert.onDidDismiss(() => {
              this.viewCtrl.dismiss();
            });
          }
        },
        error => {
          loading.dismiss();
          const toast = this.toastCtrl.create({
            message: error.error,
            duration: 2000
          });
          toast.present();
        }
      );
  }
}
