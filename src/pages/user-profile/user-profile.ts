import { Component, ViewEncapsulation } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  AlertController,
  LoadingController,
  Platform,
  ActionSheetController
} from "ionic-angular";
import * as md5 from "crypto-md5";

import { UserPrivacyPage } from "../user-privacy/user-privacy";
import { UserServicesProvider } from "../../providers/services/users";

import { InAppBrowser } from "@ionic-native/in-app-browser";

@IonicPage()
@Component({
  selector: "page-user-profile",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "user-profile.html"
})
export class UserProfilePage {
  profile: any = {};
  prof: any = {};
  profilePicture: any = "./assets/img/user.png";

  constructor(
    public platform: Platform,
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public actionSheetCtrl: ActionSheetController,
    public users: UserServicesProvider,
    private iab: InAppBrowser
  ) {}

  ionViewDidLoad() {
    console.log("ionViewDidLoad UserProfilePage");
    this.initData();
  }

  private initData() {
    const loading = this.loadingCtrl.create({
      content: "Loading profile..."
    });
    loading.present();
    this.users.user_profile().then(
      (data: any) => {
        if (data && data.success) {
          this.profile = data.data;

          if (this.profile.email) {
            this.profilePicture =
              "https://www.gravatar.com/avatar/" +
              md5(this.profile.email.toLowerCase(), "hex");
          } else {
            this.profilePicture = "./assets/img/logo.png";
          }
        }
        loading.dismiss();
      },
      error => {
        console.log("error user_profile: ", error);
        loading.dismiss();
      }
    );
  }

  saveProfile() {
    const savingProfile = () => {
      const loading = this.loadingCtrl.create();
      loading.present();
      this.users.user_edit_profile(this.profile).then(
        (data: any) => {
          if (data && data.success) {
            this.initData();
          }
          loading.dismiss();
        },
        error => {
          console.log("error user_edit_profile: ", error);
          loading.dismiss();
        }
      );
    };

    const confirm = this.alertCtrl.create({
      title: "Save Profile?",
      message: "Do you want to save profile changes?",
      buttons: [
        {
          text: "No",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Yes",
          handler: () => {
            savingProfile();
          }
        }
      ]
    });
    confirm.present();
  }

  savePassword() {
    const confirm = this.alertCtrl.create({
      title: "Save Password?",
      message: "Do you want to save password changes?",
      buttons: [
        {
          text: "No",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Yes",
          handler: () => {
            const loading = this.loadingCtrl.create();
            loading.present();
            this.users.user_edit_profile(this.prof).then(
              (data: any) => {
                if (data && data.success) {
                  this.initData();
                }
                loading.dismiss();
              },
              error => {
                console.log("error user_edit_profile: ", error);
                loading.dismiss();
              }
            );
          }
        }
      ]
    });
    confirm.present();
  }

  createGravatar() {
    const actionSheet = this.actionSheetCtrl.create({
      title: "Create own Gravatar?",
      buttons: [
        {
          text: "Create",
          role: "desctructive",
          handler: () => {
            const browser = this.iab.create(
              "https://en.gravatar.com/connect/",
              "_blank"
            );
            if (this.platform.is("cordova")) {
              browser.on("exit").subscribe(
                () => {
                  this.initData();
                },
                err => {
                  console.error(err);
                }
              );
            }
          }
        },
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        }
      ]
    });
    actionSheet.present();
  }

  gotoSettings() {
    this.navCtrl.push(UserPrivacyPage);
  }
}
