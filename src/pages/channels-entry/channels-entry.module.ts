import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { ReactiveFormsModule } from "@angular/forms";
import { ChannelsEntryPage } from "./channels-entry";

@NgModule({
  declarations: [ChannelsEntryPage],
  imports: [ReactiveFormsModule, IonicPageModule.forChild(ChannelsEntryPage)]
})
export class ChannelsEntryPageModule {}
