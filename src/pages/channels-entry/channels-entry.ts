import { Component, ViewEncapsulation } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ViewController,
  LoadingController,
  AlertController
} from "ionic-angular";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from "@angular/forms";

import { ChannelsServicesProvider } from "../../providers/services/channels";

@IonicPage()
@Component({
  selector: "page-channels-entry",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "channels-entry.html"
})
export class ChannelsEntryPage {
  channel: any = {};
  params: any = {};
  action: string;

  channelForm: FormGroup;

  isSaving = false;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public channels: ChannelsServicesProvider
  ) {
    this.action = navParams.get("action");
    this.params = navParams.get("params");
    console.log("this.action: ", this.action);

    this.channelForm = this.formBuilder.group({
      title: ["", Validators.required]
    });
  }

  get f(): any {
    return this.channelForm.controls;
  }

  private validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad ChannelsEntryPage");
  }

  close() {
    this.viewCtrl.dismiss();
  }

  save() {
    if (this.action == "create") {
      this.saveEntry();
    } else if (this.action == "update") {
      this.updateEntry();
    }
  }
  private updateEntry() {}

  private saveEntry() {
    this.submitted = true;
    this.isSaving = true;

    if (this.channelForm.invalid) {
      this.isSaving = false;
      this.validateAllFormFields(this.channelForm);
      return;
    }

    const loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    loading.present();
    this.channels.channel_create(this.channelForm.value).then(
      (data: any) => {
        if (data && data.success) {
          let alert = this.alertCtrl.create({
            title: "Success",
            message: data.message,
            buttons: ["OK"]
          });
          alert.onDidDismiss(() => {
            this.viewCtrl.dismiss("save");
          });
          alert.present();
        } else {
          let alert = this.alertCtrl.create({
            title: "Warning",
            message: data.message,
            buttons: ["OK"]
          });
          alert.present();
        }
        loading.dismiss();
        this.submitted = false;
        this.isSaving = false;
      },
      (error: any) => {
        console.log("error: ", error);
        loading.dismiss();
        this.submitted = false;
        this.isSaving = false;

        if (error && !error.success) {
          let alert = this.alertCtrl.create({
            title: "Error",
            message: error.message,
            buttons: ["OK"]
          });
          alert.present();
        }
      }
    );
  }
}
