import { Component, NgZone, ViewEncapsulation } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  AlertController,
  ViewController,
  ToastController
} from "ionic-angular";
import * as _ from "lodash";
import { CacheService, Cache } from "ionic-cache-observable";
import { Observable, Subscription } from "rxjs";
import "rxjs/add/operator/finally";

import { ProductsServicesProvider } from "../../providers/services/products";
import { StorageProvider } from "../../providers/storage/storage";
import { LoadingProvider } from "../../providers/loading/loading";

@IonicPage()
@Component({
  selector: "page-products-modal",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "products-modal.html"
})
export class ProductsModalPage {
  business: any = {};
  productArr: any[] = [];
  fakeArr: any[] = [];

  showContent: boolean = false;
  selected: any = {};
  image: any = {};
  action: string;
  type: string;

  productArr$: Observable<any>;
  private productCache: Cache<any>;
  public refreshSubscription: Subscription;

  constructor(
    public zone: NgZone,
    public navCtrl: NavController,
    public navParams: NavParams,
    public products: ProductsServicesProvider,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public viewCtrl: ViewController,
    public toastCtrl: ToastController,
    public cacheService: CacheService,
    public storageService: StorageProvider,
    public loader: LoadingProvider
  ) {
    this.image = navParams.get("image");
    this.action = navParams.get("action");
    this.type = navParams.get("type");

    for (var i = 0; i < 12; ++i) {
      this.fakeArr.push(i);
    }
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad ProductsModalPage");
    this.initBusiness().then(() => {
      this.initData();
    });
  }

  private async initBusiness() {
    const business = await this.storageService.getStorage(
      this.storageService.storageKey.CURRENT_BUSINESS
    );
    this.business = !_.isEmpty(business) ? JSON.parse(business) || {} : {};
  }

  private initData(refresher?: any) {
    this.showContent = false;
    const dataObservable = Observable.fromPromise(this.products.product_list());
    const cacheKey =
      this.storageService.cacheKey.PRODUCT_LIST + "_" + this.business.id;

    this.cacheService.register(cacheKey, dataObservable).subscribe(cache => {
      this.productCache = cache;
      this.zone.run(() => {
        this.productArr$ = cache.get$;

        this.productCache.refresh().subscribe(
          () => {
            this.loader.start();
          },
          () => {
            this.loader.complete();
          },
          () => {
            this.loader.complete();
          }
        );
      });
    });

    this.productArr$.subscribe(
      (data: any) => {
        if (data && data.success) {
          this.zone.run(() => {
            this.productArr = data.data.products;
            _.each(this.productArr, (row: any) => {
              row.checked = false;
            });
          });
        }
        this.showContent = true;
        if (refresher) {
          refresher.complete();
        }
      },
      error => {
        this.showContent = true;
        if (refresher) {
          refresher.complete();
        }
      }
    );
  }

  doRefresh(refresher: any) {
    if (this.productCache) {
      this.zone.run(() => {
        this.initData(refresher);
      });
    } else {
      console.warn("Cache has not been initialized.");
      if (refresher) {
        refresher.cancel();
      }
    }
  }

  checkChanged(item) {
    this.selected = item;
    _.each(this.productArr, (row: any) => {
      if (item.id == row.id) {
        row.checked = true;
      } else {
        row.checked = false;
      }
    });
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  saveData() {
    if (this.action === "add_image") {
      this.addImage();
    } else if (this.action === "feature") {
      this.featureImage();
    }
  }

  addImage() {
    let loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    loading.present();
    this.products
      .product_add_image({
        productId: this.selected.productId,
        image: [this.image.id]
      })
      .then(
        (data: any) => {
          if (data && data.success) {
            let alert = this.alertCtrl.create({
              title: "Success",
              subTitle: data.data,
              buttons: ["OK"]
            });
            loading.dismiss();
            alert.present();
            alert.onDidDismiss(() => {
              this.viewCtrl.dismiss();
            });
          }
        },
        error => {
          loading.dismiss();
          let toast = this.toastCtrl.create({
            message: error.error,
            duration: 2000
          });
          toast.present();
        }
      );
  }

  featureImage() {
    let loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    loading.present();
    this.products
      .product_set_feature_item({
        productId: this.selected.productId,
        mediaId: this.image.id,
        type: this.type
      })
      .then(
        (data: any) => {
          if (data && data.success) {
            let alert = this.alertCtrl.create({
              title: "Success",
              subTitle: data.data,
              buttons: ["OK"]
            });
            loading.dismiss();
            alert.present();
            alert.onDidDismiss(() => {
              this.viewCtrl.dismiss();
            });
          }
        },
        error => {
          loading.dismiss();
          let toast = this.toastCtrl.create({
            message: error.error,
            duration: 2000
          });
          toast.present();
        }
      );
  }
}
