import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { PipesModule } from "../../pipes/pipes.module";
import { ComponentsModule } from "../../components/components.module";
import { IonicImageLoader } from "ionic-image-loader";

import { ProductsModalPage } from "./products-modal";

@NgModule({
	declarations: [ProductsModalPage],
	imports: [
		IonicPageModule.forChild(ProductsModalPage),
		PipesModule,
    ComponentsModule,
    IonicImageLoader
	]
})
export class ProductsModalPageModule {}
