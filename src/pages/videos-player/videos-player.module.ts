import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VideosPlayerPage } from './videos-player';

import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    VideosPlayerPage,
  ],
  imports: [
    IonicPageModule.forChild(VideosPlayerPage),
    PipesModule
  ],
})
export class VideosTabPageModule {}
