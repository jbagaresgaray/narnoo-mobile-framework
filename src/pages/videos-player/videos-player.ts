import { Component, ViewChild, ViewEncapsulation } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ViewController
} from "ionic-angular";
import * as $ from "jquery";

/**
 * Generated class for the VideosTabPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-videos-player",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "videos-player.html"
})
export class VideosPlayerPage {
  @ViewChild("media") media: any;
  item: any = {};
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController
  ) {
    this.item = navParams.get("video");
    console.log("this.item: ", this.item);
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad VideosTabPage");
  }

  ionViewDidLeave() {
    console.log("ionViewDidLeave");
    $("video").each((videoIndex, video) => {
      video.pause();
    });
  }

  ionViewWillUnload() {
    console.log("ionViewWillUnload");
    $("video").each((videoIndex, video) => {
      video.pause();
    });
  }

  back() {
    this.viewCtrl.dismiss();
  }
}
