import { Component, ViewEncapsulation, NgZone } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ModalController,
  LoadingController,
  ActionSheetController,
  AlertController
} from "ionic-angular";
import * as _ from "lodash";
import { Subscription } from "rxjs";

import { NewsFeedServicesProvider } from "../../providers/services/newsfeed";
import { ConnectServicesProvider } from "../../providers/services/connect";
import { UtilitiesServicesProvider } from "../../providers/services/utilities";
import { StorageProvider } from "../../providers/storage/storage";

@IonicPage()
@Component({
  selector: "page-followers-following",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "followers-following.html"
})
export class FollowersFollowingPage {
  mode: string;
  title: string;
  business: any = {};
  showContent: boolean = false;
  userFollowing: any[] = [];

  usersArr: any[] = [];
  usersArrCopy: any[] = [];

  fakeArr: any[] = [];

  public refreshSubscription: Subscription;

  constructor(
    public zone: NgZone,
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public loadingCtrl: LoadingController,
    public actionSheetCtrl: ActionSheetController,
    public alertCtrl: AlertController,
    public newsfeed: NewsFeedServicesProvider,
    public connect: ConnectServicesProvider,
    public utilities: UtilitiesServicesProvider,
    public storageService: StorageProvider
  ) {
    this.mode = navParams.get("mode");
    this.title = navParams.get("title");
    this.userFollowing = navParams.get("following");

    for (var i = 0; i < 20; ++i) {
      this.fakeArr.push(i);
    }
  }
  private async initBusiness() {
    const business = await this.storageService.getStorage(
      this.storageService.storageKey.CURRENT_BUSINESS
    );
    this.business = !_.isEmpty(business) ? JSON.parse(business) || {} : {};
  }

  async ionViewDidLoad() {
    console.log("ionViewDidLoad FollowersFollowingPage");

    this.initBusiness().then(()=>{
      if (this.mode == "followers") {
        this.getFollowers();
      } else if (this.mode == "following") {
        this.getFollowing();
      }
    });
  }

  doRefresh(refresher: any) {
    if (this.mode == "followers") {
      if (this.connect.connect_followersCache) {
        this.showContent = false;
        this.zone.run(() => {
          this.refreshSubscription = this.connect.connect_followersCache
            .refresh()
            .finally(() => (this.refreshSubscription = null))
            .subscribe(
              () => {
                if (refresher) {
                  refresher.complete();
                  this.showContent = true;
                }
              },
              err => {
                console.error("Something went wrong!", err);
                if (refresher) {
                  refresher.cancel();
                  this.showContent = true;
                }
                throw err;
              }
            );
        });
      } else {
        console.warn("Cache has not been initialized.");
        if (refresher) {
          refresher.cancel();
        }
      }
    } else if (this.mode == "following") {
      if (this.connect.connect_followingCache) {
        this.showContent = false;
        this.zone.run(() => {
          this.refreshSubscription = this.connect.connect_followingCache
            .refresh()
            .finally(() => (this.refreshSubscription = null))
            .subscribe(
              () => {
                if (refresher) {
                  refresher.complete();
                  this.showContent = true;
                }
              },
              err => {
                console.error("Something went wrong!", err);
                if (refresher) {
                  refresher.cancel();
                  this.showContent = true;
                }
                throw err;
              }
            );
        });
      } else {
        console.warn("Cache has not been initialized.");
        if (refresher) {
          refresher.cancel();
        }
      }
    }
  }

  refresher() {
    this.showContent = false;
    this.connect.connect_following().then(
      (data: any) => {
        console.log("connect_following: ", data);
        if (data && data.success) {
          this.userFollowing = data.data;
          if (this.mode == "followers") {
            this.getFollowers();
          }
        }
      },
      error => {
        console.log("error: ", error);
      }
    );
  }

  getFollowers() {
    console.log("getFollowers");
    this.showContent = false;
    this.connect.connect_followers$.subscribe(
      (data: any) => {
        if (data && data.success) {
          this.usersArr = [];
          this.usersArrCopy = [];

          const result = data.data.data;
          console.log("result: ", result);
          _.each(result, (row: any) => {
            if (
              _.find(this.userFollowing, (item: any) => {
                return item.details.id == row.details.id;
              })
            ) {
              row.details.connected = true;
            } else {
              row.details.connected = false;
            }
          });

          this.zone.run(() => {
            this.usersArr = result;
            this.usersArrCopy = _.clone(result);
          });
        }
        this.showContent = true;
      },
      error => {
        console.log("error: ", error);
        this.showContent = true;
      }
    );
  }

  getFollowing() {
    console.log("getFollowing");
    this.showContent = false;
    this.connect.connect_following$.subscribe(
      (data: any) => {
        if (data && data.success) {
          this.usersArr = [];
          this.usersArrCopy = [];

          const following = data.data;
          this.zone.run(() => {
            this.usersArr = following.data;
            this.usersArrCopy = _.clone(following.data);
          });
        }
        this.showContent = true;
      },
      error => {
        console.log("error: ", error);
        this.showContent = true;
      }
    );
  }

  getItems(ev) {
    const val = ev.target.value;
    if (val && val.trim() != "") {
      this.usersArr = this.usersArrCopy.filter(item => {
        return (
          item.details.business.toLowerCase().indexOf(val.toLowerCase()) > -1
        );
      });
    } else {
      this.usersArr = _.clone(this.usersArrCopy);
    }
  }

  followBusiness(item: any, ev: any) {
    ev.preventDefault();
    ev.stopPropagation();

    if (this.business.type == 1 && item.details.type == "distributor") {
      let alert = this.alertCtrl.create({
        title: "WARNING!",
        message: "Operator account can’t follow distributor account",
        buttons: ["OK"]
      });
      alert.present();
      return;
    }

    const followBusiness = () => {
      const loading = this.loadingCtrl.create({
        dismissOnPageChange: true
      });
      loading.present();
      this.connect
        .connect_add({
          type: item.details.type,
          id: item.details.id
        })
        .then(
          (data: any) => {
            if (data && data.success) {
              console.log("data: ", data);
              const alert = this.alertCtrl.create({
                title: "SUCCESS",
                message: data.data,
                buttons: ["OK"]
              });
              alert.onDidDismiss(() => {
                if (this.mode == "followers") {
                  this.refresher();
                } else if (this.mode == "following") {
                  this.getFollowing();
                }
              });
              alert.present();
            } else if (data && !data.success) {
              this.alertCtrl
                .create({
                  title: "WARNING",
                  message: data.data,
                  buttons: ["OK"]
                })
                .present();
            }
            loading.dismiss();
          },
          error => {
            console.log("error: ", error);
            loading.dismiss();
          }
        );
    };

    const actionSheet = this.actionSheetCtrl.create({
      title: "Do you want to connect with " + item.details.business + "?",
      buttons: [
        {
          text: "Yes",
          handler: () => {
            console.log("Archive clicked");
            followBusiness();
          }
        },
        {
          text: "No",
          role: "destructive",
          handler: () => {
            console.log("Cancel clicked");
          }
        },
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        }
      ]
    });
    actionSheet.present();
  }

  unfollowBusiness(item: any, ev: any) {
    ev.preventDefault();
    ev.stopPropagation();

    const loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    loading.present();
    this.connect
      .connect_remove({
        type: item.details.type,
        id: item.details.id
      })
      .then(
        (data: any) => {
          console.log("data: ", data);
          if (data && data.success) {
            let alert = this.alertCtrl.create({
              title: "SUCCESS",
              message: data.data,
              buttons: ["OK"]
            });
            alert.onDidDismiss(() => {
              if (this.mode == "followers") {
                this.refresher();
              } else if (this.mode == "following") {
                this.getFollowing();
              }
            });
            alert.present();
          } else if (data && !data.success) {
            let alert = this.alertCtrl.create({
              title: "WARNING",
              message: data.message,
              buttons: ["OK"]
            });
            alert.present();
          }
          loading.dismiss();
        },
        error => {
          console.log("error: ", error);
          loading.dismiss();
        }
      );
  }

  editConnection(item: any, ev: any) {
    let vm = this;

    ev.preventDefault();
    ev.stopPropagation();

    if (item.details.type != "distributor") {
      let alert = this.alertCtrl.create({
        title: "WARNING!",
        message: "Only distributor are allowed on this feature!",
        buttons: ["OK"]
      });
      alert.present();
      return;
    }

    const connect_edit = data => {
      const loading = this.loadingCtrl.create({
        dismissOnPageChange: true
      });
      loading.present();
      this.connect
        .connect_edit({
          type: data.type,
          id: data.id,
          role: data.role
        })
        .then(
          (data: any) => {
            if (data && data.success) {
              this.alertCtrl
                .create({
                  title: "SUCCESS",
                  message: data.data,
                  buttons: ["OK"]
                })
                .present();
            } else if (data && !data.success) {
              this.alertCtrl
                .create({
                  title: "WARNING",
                  message: data.message,
                  buttons: ["OK"]
                })
                .present();
            }
            loading.dismiss();
          },
          error => {
            loading.dismiss();
          }
        );
    };

    const actionSheet = this.actionSheetCtrl.create({
      title: "Modify business connection",
      buttons: [
        {
          text: "Full Role",
          handler: () => {
            console.log("Full Role clicked");
            connect_edit({
              type: item.details.type,
              id: item.details,
              role: "full"
            });
          }
        },
        {
          text: "Products",
          handler: () => {
            console.log("Products clicked");
            connect_edit({
              type: item.details.type,
              id: item.details,
              role: "products"
            });
          }
        },
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        }
      ]
    });
    actionSheet.present();
  }

  errorAvatarHandler(event) {
    event.target.src = this.utilities.radomizeAvatar();
  }
}
