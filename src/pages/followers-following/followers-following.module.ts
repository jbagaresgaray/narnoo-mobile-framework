import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { FollowersFollowingPage } from "./followers-following";
import { PipesModule } from "../../pipes/pipes.module";
import { ComponentsModule } from "../../components/components.module";
import { IonicImageLoader } from "ionic-image-loader";

@NgModule({
  declarations: [FollowersFollowingPage],
  imports: [
    IonicPageModule.forChild(FollowersFollowingPage),
    PipesModule,
    ComponentsModule,
    IonicImageLoader
  ]
})
export class FollowersFollowingPageModule {}
