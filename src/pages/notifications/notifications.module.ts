import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { PipesModule } from "../../pipes/pipes.module";
import { ComponentsModule } from "../../components/components.module";
import { IonicImageLoader } from "ionic-image-loader";

import { NotificationsPage } from "./notifications";

@NgModule({
  declarations: [NotificationsPage],
  imports: [
    PipesModule,
    ComponentsModule,
    IonicImageLoader,
    IonicPageModule.forChild(NotificationsPage)
  ]
})
export class NotificationsPageModule {}
