import { Component, ViewEncapsulation, NgZone } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  AlertController,
  LoadingController,
  ActionSheetController,
  Content
} from "ionic-angular";
import async from "async";
import _ from "lodash";
import { CacheService, Cache } from "ionic-cache-observable";
import { Observable, Subscription } from "rxjs";
import "rxjs/add/operator/finally";

import { NotificationDetailsPage } from "../notification-details/notification-details";

import { UtilitiesServicesProvider } from "../../providers/services/utilities";
import { NewsFeedServicesProvider } from "../../providers/services/newsfeed";
import { StorageProvider } from "../../providers/storage/storage";
import { LoadingProvider } from "../../providers/loading/loading";

@IonicPage()
@Component({
  selector: "page-notifications",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "notifications.html"
})
export class NotificationsPage {
  business: any = {};
  fakeArr: any[] = [];
  notificationsArr: any[] = [];
  _notificationsArr: any[] = [];
  _notificationsArr2: any[] = [];

  totalNotification: number = 0;
  showContent: boolean = false;
  showContent1: boolean = false;
  showContent2: boolean = false;

  showError1: boolean = false;
  showError2: boolean = false;
  showError3: boolean = false;
  errContent1: string;
  errContent2: string;
  errContent3: string;

  page: number = 1;
  limit: number = 20;

  notifType: string = "received";

  notificationArr$: Observable<any>;
  notificationSentArr$: Observable<any>;
  private notificationCache: Cache<any>;
  private notificationSentCache: Cache<any>;
  public refreshSubscription: Subscription;

  content: Content;
  constructor(
    public zone: NgZone,
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public actionSheetCtrl: ActionSheetController,
    public utilities: UtilitiesServicesProvider,
    public newsfeeds: NewsFeedServicesProvider,
    public storageService: StorageProvider,
    public cacheService: CacheService,
    public loader: LoadingProvider
  ) {
    this.totalNotification = this.newsfeeds.totalNotification;

    for (var i = 0; i < 20; ++i) {
      this.fakeArr.push(i);
    }
  }

  private initReceiveData(refresher?: any) {
    this.showContent1 = false;

    const dataObservable = Observable.fromPromise(
      this.newsfeeds.app_notifications({
        page: this.page,
        limit: this.limit
      })
    );
    const cacheKey = this.storageService.cacheKey.APP_NOTIFICATIONS;
    this.cacheService.register(cacheKey, dataObservable).subscribe(cache => {
      this.notificationCache = cache;
      this.zone.run(() => {
        this.notificationArr$ = cache.get$;

        this.notificationCache.refresh().subscribe(
          () => {
            this.loader.start();
          },
          () => {
            this.loader.complete();
          },
          () => {
            this.loader.complete();
          }
        );
      });
    });

    this.notificationArr$.subscribe(
      (data: any) => {
        if (data && data.success) {
          const _notificationsArr = data.data.notification;
          this.zone.run(() => {
            _.each(_notificationsArr, (row: any) => {
              row.action = "earlier";
            });
            this._notificationsArr = _notificationsArr;
          });
        } else if (data && !data.success) {
          this.showError2 = true;
          this.errContent2 = data.message;
        }
        this.showContent1 = true;
        if (refresher) {
          refresher.complete();
        }
      },
      error => {
        console.log("error: ", error);
        this.showContent1 = true;
        if (refresher) {
          refresher.complete();
        }
        return;
      },
      () => {
        this.content.resize();
      }
    );
  }

  private initSentData(refresher?: any) {
    this.showContent2 = false;

    const dataObservable = Observable.fromPromise(
      this.newsfeeds.app_notifications({
        page: this.page,
        limit: this.limit
      })
    );
    const cacheKey = this.storageService.cacheKey.NOTIFICATION_LIST_SENT;
    this.cacheService.register(cacheKey, dataObservable).subscribe(cache => {
      this.notificationSentCache = cache;
      this.zone.run(() => {
        this.notificationSentArr$ = cache.get$;

        this.notificationSentCache.refresh().subscribe(
          () => {
            this.loader.start();
          },
          () => {
            this.loader.complete();
          },
          () => {
            this.loader.complete();
          }
        );
      });
    });

    this.notificationSentArr$.subscribe(
      (data: any) => {
        if (data && data.success) {
          const item = data.data;
          const _notificationsArr2 = item.notification;
          this.zone.run(() => {
            this._notificationsArr2 = _notificationsArr2;
          });
        } else if (data && !data.success) {
          this.showError3 = true;
          this.errContent3 = data.message;
        }
        this.showContent2 = true;
        if (refresher) {
          refresher.complete();
        }
      },
      error => {
        console.log("error: ", error);
        this.showContent2 = true;
        if (refresher) {
          refresher.complete();
        }
        return;
      }
    );
  }

  private async initBusiness() {
    const business = await this.storageService.getStorage(
      this.storageService.storageKey.CURRENT_BUSINESS
    );
    this.business = !_.isEmpty(business) ? JSON.parse(business) || {} : {};
  }

  ionViewWillEnter() {
    console.log("ionViewDidEnter NotificationsPage");
    this.initBusiness().then(() => {
      this.initReceiveData();
      this.initSentData();
    });
  }

  doRefresh(refresher: any) {
    if (this.notifType === "received") {
      if (this.notificationCache) {
        this.showContent1 = false;
        this.zone.run(() => {
          this.initReceiveData(refresher);
        });
      } else {
        console.warn("Cache has not been initialized.");
        if (refresher) {
          refresher.cancel();
          this.showContent1 = true;
        }
      }
    } else if (this.notifType === "sent") {
      if (this.notificationSentCache) {
        this.showContent2 = false;
        this.zone.run(() => {
          this.initSentData(refresher);
        });
      } else {
        console.warn("Cache has not been initialized.");
        if (refresher) {
          refresher.cancel();
          this.showContent2 = true;
        }
      }
    }
  }

  errorAvatarHandler(event) {
    event.target.src = this.utilities.radomizeAvatar();
  }

  viewNotification(item: any, status) {
    this.navCtrl.push(NotificationDetailsPage, {
      notification: _.cloneDeep(item),
      status: status
    });
  }

  deleteNotification(item: any) {
    const deleteNotification = () => {
      const loading = this.loadingCtrl.create({
        dismissOnPageChange: true
      });
      loading.present();
      this.newsfeeds
        .delete_notification({
          id: item.id
        })
        .then(
          (data: any) => {
            if (data && data.success) {
              const alert = this.alertCtrl.create({
                title: "SUCCESS",
                subTitle: data.message,
                buttons: ["OK"]
              });
              alert.onDidDismiss(() => {
                this.showContent = false;

                this.doRefresh(null);
              });
              alert.present();
            }
            loading.dismiss();
          },
          error => {
            console.log("error: ", error);
            loading.dismiss();
          }
        );
    };

    const alert = this.alertCtrl.create({
      title: "Delete this notification?",
      message: "Are you sure to delete this notification?",
      buttons: [
        {
          text: "Cancel",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Confirm",
          handler: () => {
            console.log("Agree clicked");
            deleteNotification();
          }
        }
      ]
    });
    alert.present();
  }
}
