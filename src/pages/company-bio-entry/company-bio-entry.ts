import { Component, ViewEncapsulation } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ViewController,
  LoadingController,
  AlertController,
  ToastController
} from "ionic-angular";
import { FormGroup, FormBuilder, FormControl } from "@angular/forms";
import * as _ from "lodash";

import { BusinessServicesProvider } from "../../providers/services/business";
import { StorageProvider } from "../../providers/storage/storage";

@IonicPage()
@Component({
  selector: "page-company-bio-entry",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "company-bio-entry.html"
})
export class CompanyBioEntryPage {
  action: string;
  type: string;
  item: any = {};
  business: any = {};
  mode: string;
  textEditorConfig: any = {};

  bioForm: FormGroup;
  isSaving = false;
  submitted = false;

  showChinese: boolean = true;
  showJapanese: boolean = false;
  showKorean: boolean = false;
  showGerman: boolean = false;

  chineseObj: any = {};
  japaneseObj: any = {};
  koreanObj: any = {};
  germanObj: any = {};

  itemCtrl: FormControl;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    private formBuilder: FormBuilder,
    public businesses: BusinessServicesProvider,
    public storageService: StorageProvider
  ) {
    this.action = navParams.get("action");
    this.mode = "entry";
    /* this.bioForm = this.formBuilder.group({
      text: ["", Validators.required],
      size: ["", Validators.required],
      language: ["", Validators.required]
    }); */

    if (this.action === "update") {
      this.item = navParams.get("item");
      this.type = navParams.get("type");
      this.item.type = this.type;
    }

    console.log("this.item: ", this.item);
    console.log("this.type: ", this.type);
    console.log("this.action: ", this.action);

    this.textEditorConfig = {
      buttons: "bold,strikethrough,underline,italic"
    };
  }

  async ionViewDidLoad() {
    console.log("ionViewDidLoad CompanyBioEntryPage");
    const business = await this.storageService.getStorage(
      this.storageService.storageKey.CURRENT_BUSINESS
    );
    this.business = !_.isEmpty(business) ? JSON.parse(business) || {} : {};

    this.itemCtrl = this.formBuilder.control("");
  }

  close() {
    this.viewCtrl.dismiss();
  }

  updateBio() {
    if (this.business.role !== 1) {
      this.toastCtrl
        .create({
          message: "Invalid Access!!!",
          duration: 3000
        })
        .present();
      return;
    }

    const loading = this.loadingCtrl.create({
      dismissOnPageChange: true,
      content: "Updating..."
    });
    loading.present();
    this.businesses
      .business_biography_edit(this.item)
      .then((data: any) => {
        if (data && data.success) {
          console.log("success: ", data);
          const alert = this.alertCtrl.create({
            title: "Success",
            message: data.data,
            buttons: ["OK"]
          });
          alert.onDidDismiss(() => {
            this.viewCtrl.dismiss("save");
          });
          alert.present();
        }
        loading.dismiss();
      })
      .catch(error => {
        console.log("error: ", error);
        loading.dismiss();
        if (error) {
          const alert = this.alertCtrl.create({
            title: "Error!",
            subTitle: error.error,
            buttons: ["OK"]
          });
          alert.present();
        }
      });
  }

  saveEntry() {
    if (!this.business.paid) {
      this.alertCtrl
        .create({
          title: "WARNING",
          message: "This feature is only available to premium users only.",
          buttons: ["OK"]
        })
        .present();
      return;
    }

    const confirm = this.alertCtrl.create({
      title: "Save Company Bio?",
      message: "Do you want to save this entry?",
      buttons: [
        {
          text: "No",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Yes",
          handler: () => {
            console.log("Agree clicked");
            this.updateBio();
          }
        }
      ]
    });
    confirm.present();
  }
}
