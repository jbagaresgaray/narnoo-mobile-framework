import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { JoditAngularModule } from "jodit-angular";
import { ReactiveFormsModule } from "@angular/forms";

import { CompanyBioEntryPage } from "./company-bio-entry";
import { ComponentsModule } from "../../components/components.module";

@NgModule({
  declarations: [CompanyBioEntryPage],
  imports: [
    ComponentsModule,
    JoditAngularModule,
    ReactiveFormsModule,
    IonicPageModule.forChild(CompanyBioEntryPage)
  ]
})
export class CompanyBioEntryPageModule {}
