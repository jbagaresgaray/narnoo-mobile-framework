import { Component, NgZone, ViewEncapsulation } from "@angular/core";
import {
  NavParams,
  ViewController,
  LoadingController,
  Events,
  AlertController
} from "ionic-angular";
import * as _ from "lodash";
import * as async from "async";

import { BYOBServicesProvider } from "../../providers/services/byob";

@Component({
  selector: "page-byobconnected-modal",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "byob-connected-platform.html"
})
export class BYOBConnectedPlatformPage {
  connect_user: any = {};
  userName: string;
  token: string;
  selected: any = {};

  platformsArr: any[] = [];
  assignedArr: any = {};

  contentErr: string;

  showLoading: boolean = true;

  constructor(
    public viewCtrl: ViewController,
    params: NavParams,
    public loadingCtrl: LoadingController,
    public events: Events,
    public zone: NgZone,
    public alertCtrl: AlertController,
    public byobServices: BYOBServicesProvider
  ) {
    this.connect_user = params.get("user");
    console.log("connect_user: ", this.connect_user);
    if (this.connect_user) {
      if (this.connect_user.details) {
        this.userName =
          this.connect_user.details.business || this.connect_user.details.name;
        this.selected = this.connect_user.details;
      } else {
        this.userName = this.connect_user.name;
        this.selected = this.connect_user;
      }
    }
  }

  ionViewDidLoad() {
    this.showLoading = true;
    async.waterfall(
      [
        callback => {
          this.byobServices.platform_options(this.selected.id).then(
            (data: any) => {
              if (data && data.success) {
                this.platformsArr = data.data.data;
                _.each(this.platformsArr, (row: any) => {
                  row.selected = false;
                });
              }
              console.log("platform_options: ", this.platformsArr);
              callback();
            },
            (error: any) => {
              console.log("platform_options error: ", error);
              callback();
            }
          );
        },
        callback => {
          this.byobServices.assigned_platform(this.selected.id).then(
            (data: any) => {
              if (data && data.success) {
                this.assignedArr = data.data;
              }
              console.log("assigned_platform: ", this.assignedArr);
              callback();
            },
            (error: any) => {
              console.log("assigned_platform error: ", error);
              if (error && !error.success) {
                this.contentErr = error.message;
              }
              callback();
            }
          );
        }
      ],
      () => {
        this.showLoading = false;
        if (!_.isEmpty(this.platformsArr)) {
          _.each(this.platformsArr, (row: any) => {
            if (row.nickname == this.assignedArr.nickname) {
              row.selected = true;
            } else {
              row.selected = false;
            }
          });
        } else {
          console.log("this.assignedArr: ", this.assignedArr);
          if (!_.isEmpty(this.assignedArr)) {
            this.assignedArr.selected = true;
            this.platformsArr.push(this.assignedArr);
          }
        }
        console.log("this.platformsArr: ", this.platformsArr);
      }
    );
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  selectItem(item: any) {
    _.each(this.platformsArr, (row: any) => {
      if (row.nickname == item.nickname) {
        row.selected = true;
      } else {
        row.selected = false;
      }
    });
    console.log("this.platformsArr: ", this.platformsArr);
  }

  assignPlatform() {
    const loading = this.loadingCtrl.create({
      dismissOnPageChange: true,
      content: "Assigning..."
    });
    loading.present();
    const _selected = _.find(this.platformsArr, (row: any) => {
      return row.selected === true;
    });

    if (_.isEmpty(_selected)) {
      this.alertCtrl
        .create({
          title: "Warning",
          message: "Please select a platform!",
          buttons: ["OK"]
        })
        .present();
      return;
    }

    this.byobServices
      .platform_edit({
        operator: this.selected.id,
        platform: _selected.nickname
      })
      .then(
        (data: any) => {
          if (data && data.success) {
          }
          loading.dismiss();
        },
        (error: any) => {
          loading.dismiss();
        }
      );
  }
}
