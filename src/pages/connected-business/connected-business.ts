import { Component, ViewChild, NgZone, ViewEncapsulation } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ModalController,
  AlertController,
  LoadingController,
  Platform,
  ActionSheetController,
  ToastController,
  App,
  Content,
  Events
} from "ionic-angular";
import * as _ from "lodash";
import { CacheService, Cache } from "ionic-cache-observable";
import { Observable, Subscription } from "rxjs";
import "rxjs/add/operator/finally";

import { Geolocation } from "@ionic-native/geolocation";

import { NewsFeedServicesProvider } from "../../providers/services/newsfeed";
import { ConnectServicesProvider } from "../../providers/services/connect";
import { UtilitiesServicesProvider } from "../../providers/services/utilities";

import { ConnectMediaPage } from "../images-tab/connect-media";
import { ConnectSettingsPage } from "../connect-settings/connect-settings";
import { BYOBConnectedPlatformPage } from "./byob-connected-platform";
import { OperatorRequestPage } from "./operator-request";
import { BusinessProfileSettingsPage } from "../business-profile-settings/business-profile-settings";
import { NotificationsPage } from "../notifications/notifications";
import { StorageProvider } from "../../providers/storage/storage";
import { LoadingProvider } from "../../providers/loading/loading";

@IonicPage()
@Component({
  selector: "page-connected-business",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "connected-business.html"
})
export class ConnectedBusinessPage {
  mode = "follower";
  action: any = "following";
  business: any = {};
  showContent: boolean = false;
  showFollowingContent: boolean = false;
  showFollowerContent: boolean = false;

  followingArr: any[] = [];
  followingArrCopy: any[] = [];

  followerArr: any[] = [];
  followerArrCopy: any[] = [];

  usersArr: any[] = [];
  usersArrCopy: any[] = [];

  followingErr: any;
  followerErr: any;

  totalNotification: number = 0;

  fakeArr: any[] = [];
  @ViewChild(Content) content: Content;

  followingArr$: Observable<any>;
  private followingCache: Cache<any>;

  followerArr$: Observable<any>;
  private followerCache: Cache<any>;

  public refreshSubscription: Subscription;

  constructor(
    public app: App,
    public zone: NgZone,
    public events: Events,
    public platform: Platform,
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public actionSheetCtrl: ActionSheetController,
    public toastCtrl: ToastController,
    public newsfeed: NewsFeedServicesProvider,
    public connect: ConnectServicesProvider,
    public utilities: UtilitiesServicesProvider,
    public storageService: StorageProvider,
    private geolocation: Geolocation,
    public cacheService: CacheService,
    public loader: LoadingProvider
  ) {
    for (var i = 0; i < 21; ++i) {
      this.fakeArr.push(i);
    }

    this.followingErr = null;
    this.followerErr = null;

    events.unsubscribe("showNotifications");
    events.subscribe("showNotifications", count => {
      this.totalNotification = count;
    });

    try {
      this.newsfeed.notificationData$.subscribe((data: any) => {
        console.log("activity feed notificationData: ", data);
        if (data && data.success) {
          this.zone.run(() => {
            this.newsfeed.totalNotification = data.data.total;
            this.totalNotification = data.data.total;
          });
        } else {
          this.zone.run(() => {
            this.newsfeed.totalNotification = 0;
            this.totalNotification = 0;
          });
        }
      });
    } catch (error) {
      this.zone.run(() => {
        this.newsfeed.totalNotification = 0;
        this.totalNotification = 0;
      });
      this.events.publish("initNotifications");
    }
  }

  private async initBusiness() {
    const business = await this.storageService.getStorage(
      this.storageService.storageKey.CURRENT_BUSINESS
    );
    this.business = !_.isEmpty(business) ? JSON.parse(business) || {} : {};
  }

  private initData(ev?: any) {
    this.showContent = false;
    this.connect.connect_find().then(
      (data: any) => {
        if (data && data.success) {
          this.usersArr = [];
          this.usersArrCopy = [];

          const filtered: any[] = _.filter(data.data, (row: any) => {
            return !row.details.connected;
          });

          this.zone.run(() => {
            this.usersArr = filtered;
            this.usersArrCopy = _.clone(filtered);
          });
        }
        this.zone.run(() => {
          this.showContent = true;
        });

        if (ev) {
          ev.complete();
        }
      },
      error => {
        console.log("connect_find error: ", error);
        this.zone.run(() => {
          this.showContent = true;
        });

        if (ev) {
          ev.complete();
        }
      }
    );
  }

  private getFollowers(refresher?: any) {
    this.showFollowerContent = false;

    const successResponse = (data: any) => {
      if (data && data.success) {
        this.followerArr = [];
        this.followerArrCopy = [];
        const item = data.data;

        this.zone.run(() => {
          this.followerArr = item.data;
          this.followerArrCopy = _.clone(item.data);
        });
      }
      if (refresher) {
        refresher.complete();
      }
      this.zone.run(() => {
        this.showFollowerContent = true;
      });
    };

    const errorResponse = error => {
      console.log("error: ", error);
      this.zone.run(() => {
        this.showFollowerContent = true;
      });
      if (refresher) {
        refresher.complete();
      }
      if (error && !error.success) {
        this.followerErr = error.message;
      }
    };

    const dataObservable = Observable.fromPromise(
      this.connect.connect_followers()
    );
    const cacheKey =
      this.storageService.cacheKey.CONNECTED_FOLLOWERS + "_" + this.business.id;

    this.cacheService.register(cacheKey, dataObservable).subscribe(cache => {
      this.followerCache = cache;
      this.zone.run(() => {
        this.followerArr$ = cache.get$;

        this.followerCache.refresh().subscribe(
          () => {
            this.loader.start();
          },
          () => {
            this.loader.complete();
          },
          () => {
            this.loader.complete();
          }
        );
      });
    });

    this.followerArr$.subscribe(successResponse, errorResponse, () => {
      this.zone.run(() => {
        this.showFollowerContent = true;
      });
    });
  }

  private getFollowing(ev?: any) {
    const successResponse = (data: any) => {
      if (data && data.success) {
        this.followingArr = [];
        this.followingArrCopy = [];

        const item = data.data;

        this.zone.run(() => {
          this.followingArr = item.data;
          this.followingArrCopy = _.clone(item.data);
        });
      }
      if (ev) {
        ev.complete();
      }
      this.zone.run(() => {
        this.showFollowingContent = true;
      });
    };

    const errorResponse = (error: any) => {
      console.log("error: ", error);
      this.zone.run(() => {
        this.showFollowingContent = true;
      });
      if (ev) {
        ev.complete();
      }
      if (error && !error.success) {
        this.followingErr = error.message;
      }
    };

    this.showFollowingContent = false;
    const dataObservable = Observable.fromPromise(
      this.connect.connect_following()
    );
    const cacheKey =
      this.storageService.cacheKey.CONNECTED_FOLLOWING + "_" + this.business.id;

    this.cacheService.register(cacheKey, dataObservable).subscribe(cache => {
      this.followingCache = cache;
      this.zone.run(() => {
        this.followingArr$ = cache.get$;

        this.followingCache.refresh().subscribe(
          () => {
            this.loader.start();
          },
          () => {
            this.loader.complete();
          },
          () => {
            this.loader.complete();
          }
        );
      });
    });

    this.followingArr$.subscribe(successResponse, errorResponse, () => {
      this.zone.run(() => {
        this.showFollowingContent = true;
      });
    });
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad ConnectedBusinessPage");
    this.initBusiness().then(() => {
      if (this.business.type == 1) {
        this.initData();
        this.getFollowers();
        this.getFollowing();
      } else {
        this.initData();
        this.getFollowing();
      }
    });
  }

  ionViewWillEnter() {
    console.log("ionViewWillEnter ConnectedBusinessPage");
    this.showContent = false;
  }

  doRefresh(refresher: any) {
    if (this.action == "all") {
      this.initData(refresher);
    } else if (this.action == "followers") {
      if (this.followerCache) {
        this.showFollowerContent = false;
        this.zone.run(() => {
          this.getFollowers(refresher);
        });
      } else {
        console.warn("Cache has not been initialized.");
        if (refresher) {
          refresher.cancel();
        }
      }
    } else if (this.action == "following") {
      if (this.refreshSubscription) {
        console.warn("Already refreshing.");
        refresher.cancel();
        return;
      }

      if (this.followingCache) {
        this.showFollowingContent = false;
        this.zone.run(() => {
          this.getFollowing(refresher);
        });
      } else {
        console.warn("Cache has not been initialized.");
        if (refresher) {
          refresher.cancel();
        }
      }
    }
    this.content.resize();
  }

  getItems(val: string) {
    const params: any = {};
    params.name = val;

    if (val && val.trim() != "") {
      if (this.action == "all") {
        this.showContent = false;
        this.connect.connect_search(params).then(
          (data: any) => {
            if (data && data.success) {
              let result = data.data;
              console.log("connect_search: ", data);
              if (result && result[0] !== false) {
                this.usersArr = result;
                _.each(this.usersArr, (row: any) => {
                  row.details = {
                    connected: row.connected,
                    contact: row.contact,
                    country: row.country,
                    email: row.email,
                    id: row.id,
                    name: row.name,
                    phone: row.phone,
                    postcode: row.postcode,
                    state: row.state,
                    suburb: row.suburb,
                    type: row.type,
                    url: row.url
                  };
                });
              }
            }
            this.showContent = true;
          },
          error => {
            console.log("connect_find error: ", error);
            this.showContent = true;
          }
        );
      } else if (this.action == "following") {
        this.followingArr = this.followingArrCopy.filter(item => {
          return (
            item.details.business.toLowerCase().indexOf(val.toLowerCase()) > -1
          );
        });
      } else if (this.action == "followers") {
        this.followerArr = this.followerArrCopy.filter(item => {
          return (
            item.details.business.toLowerCase().indexOf(val.toLowerCase()) > -1
          );
        });
      }
    } else {
      this.usersArr = _.clone(this.usersArrCopy);
      this.followerArr = _.clone(this.followerArrCopy);
      this.followingArr = _.clone(this.followingArrCopy);
    }
  }

  onCancel(ev) {
    if (this.action == "all") {
      this.initData();
    } else if (this.action == "followers") {
      this.getFollowers();
    } else if (this.action == "following") {
      this.getFollowing();
    }
  }

  onClear(ev) {
    if (this.action == "all") {
      this.initData();
    } else {
      this.usersArr = _.clone(this.usersArrCopy);
    }
  }

  segmentChanged(ev) {
    console.log("segmentChanged: ", this.action);
    /*if (this.action == 'all') {
			this.initData();
		} else if (this.action == 'followers') {
			this.getFollowers();
		} else if (this.action == 'following') {
			this.getFollowing();
		}*/
  }

  navigate() {
    const loading = this.loadingCtrl.create({
      dismissOnPageChange: true,
      content: "Navigating..."
    });

    const navigateNow = () => {
      this.connect
        .connect_find({
          latitude: this.utilities.latitude,
          longitude: this.utilities.longitude
        })
        .then(
          (data: any) => {
            if (data && data.success) {
              console.log("navigate connect_find: ", data);
              this.usersArr = data.data;
            } else if (data && !data.success) {
              this.alertCtrl
                .create({
                  title: "Warning",
                  message: data.message,
                  buttons: ["OK"]
                })
                .present();
            }
            this.showContent = true;
            loading.dismiss();
          },
          error => {
            console.log("navigate connect_find error: ", error);
            loading.dismiss();
            this.showContent = true;
          }
        );
    };

    const startFindingGeolocation = () => {
      this.platform.ready().then(() => {
        loading.present();
        this.showContent = false;

        if (this.platform.is("cordova")) {
          this.geolocation
            .getCurrentPosition()
            .then(resp => {
              console.log("resp.coords: ", resp.coords);
              if (resp) {
                this.utilities.latitude = resp.coords.latitude;
                this.utilities.longitude = resp.coords.longitude;
                navigateNow();
              }
            })
            .catch(error => {
              console.log("Error getting location", error);
            });
        } else {
          let options = {
            enableHighAccuracy: true,
            timeout: 5000,
            maximumAge: 0
          };
          navigator.geolocation.getCurrentPosition(
            pos => {
              let crd = pos.coords;
              this.utilities.latitude = crd.latitude;
              this.utilities.longitude = crd.longitude;
              console.log("Your current position is:");
              console.log(`Latitude : ${crd.latitude}`);
              console.log(`Longitude: ${crd.longitude}`);
              navigateNow();
            },
            err => {
              console.warn(`ERROR(${err.code}): ${err.message}`);
            },
            options
          );
        }
      });
    };

    const confirm = this.alertCtrl.create({
      title: "Navigate?",
      message: "Navigate to check nearby businesses?",
      buttons: [
        {
          text: "No",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Yes",
          handler: () => {
            console.log("Agree clicked");
            startFindingGeolocation();
          }
        }
      ]
    });
    confirm.present();
  }

  connectUser(user: any, ev?: any) {
    let business: any;
    let selected: any = {};
    let actionSheetButtons: any[] = [];

    if (this.action == "following") {
      this.followingAction(user, ev);
      return;
    } else if (this.action == "followers") {
      this.followerAction(user, ev);
      return;
    }

    if (user.details) {
      business = user.details.business || user.details.name;
      selected = user.details;
    } else {
      business = user.name;
      selected = user;
    }

    const followUser = () => {
      if (this.business.role !== 1) {
        const toast = this.toastCtrl.create({
          message: "Invalid Access!!!",
          duration: 3000
        });
        toast.present();
        return;
      }

      const loading = this.loadingCtrl.create({
        dismissOnPageChange: true,
        content: "Following..."
      });
      loading.present();
      this.connect
        .connect_add({
          type: selected.type,
          id: selected.id
        })
        .then(
          (data: any) => {
            if (data && data.success) {
              console.log("data: ", data);
              const alert = this.alertCtrl.create({
                title: "Success",
                message: data.data,
                buttons: ["OK"]
              });
              alert.present();
            } else if (data && !data.success) {
              const alert = this.alertCtrl.create({
                title: "Warning",
                message: data.message,
                buttons: ["OK"]
              });
              alert.present();
            }
            loading.dismiss();
          },
          error => {
            console.log("error: ", error);
            loading.dismiss();
          }
        );
    };

    const unfollowUser = () => {
      if (this.business.role !== 1) {
        const toast = this.toastCtrl.create({
          message: "Invalid Access!!!",
          duration: 3000
        });
        toast.present();
        return;
      }

      let loading = this.loadingCtrl.create({
        dismissOnPageChange: true,
        content: "Unfollowing..."
      });
      loading.present();
      this.connect
        .connect_remove({
          type: selected.type,
          id: selected.id
        })
        .then(
          (data: any) => {
            if (data && data.success) {
              console.log("data: ", data);
              const alert = this.alertCtrl.create({
                title: "Success",
                message: data.data,
                buttons: ["OK"]
              });
              alert.present();
            } else if (data && !data.success) {
              const alert = this.alertCtrl.create({
                title: "Warning",
                message: data.data,
                buttons: ["OK"]
              });
              alert.present();
            }
            loading.dismiss();
          },
          error => {
            console.log("error: ", error);
            loading.dismiss();
          }
        );
    };

    actionSheetButtons.push({
      text: "Cancel",
      role: "cancel",
      handler: () => {
        console.log("Cancel clicked");
      }
    });

    if (this.business.role == 1) {
      if (selected.connected) {
        actionSheetButtons.push({
          text: "Unfollow",
          role: "destructive",
          handler: () => {
            console.log("Cancel clicked");
            unfollowUser();
          }
        });
      } else {
        actionSheetButtons.unshift({
          text: "Follow",
          handler: () => {
            console.log("Archive clicked");
            followUser();
          }
        });
      }
    }

    const actionSheet = this.actionSheetCtrl.create({
      title: "Do you want to connect with " + business + "?",
      buttons: actionSheetButtons
    });
    actionSheet.present();
  }

  followingAction(user: any, ev?: any) {
    console.log("followingAction: ", user);
    let business: any;
    let selected: any = {};

    if (user.details) {
      business = user.details.business;
      selected = user.details;
    } else {
      business = user.name;
      selected = user;
    }

    let followingButtons: any = [
      {
        text: "View",
        handler: () => {
          this.viewDetails(user);
        }
      }
    ];

    if (this.business.role === 1) {
      followingButtons.push({
        text: "Unfollow",
        role: "destructive",
        handler: () => {
          console.log("Archive clicked");
          this.unfollowBusiness(selected);
        }
      });

      if (this.business.paid) {
        followingButtons.splice(
          2,
          0,
          {
            text: "Assigned Platform",
            handler: () => {
              setTimeout(() => {
                this.viewPlatformSettings(user);
              }, 300);
            }
          },
          {
            text: "Send Notification",
            handler: () => {
              setTimeout(() => {
                this.viewOperatorSettings(user);
              }, 300);
            }
          }
        );
      }
    }

    followingButtons.push({
      text: "Cancel",
      role: "cancel",
      handler: () => {
        console.log("Cancel clicked");
      }
    });

    const actionSheet = this.actionSheetCtrl.create({
      title: "Select what to do with " + business + "?",
      buttons: followingButtons
    });
    actionSheet.present();
  }

  followerAction(user: any, ev?: any) {
    console.log("followerAction: ", user);
    let business: any;
    let selected: any = {};

    if (user.details) {
      business = user.details.business;
      selected = user.details;
    } else {
      business = user.name;
      selected = user;
    }

    let actionButtons: any[] = [
      {
        text: "View",
        handler: () => {
          this.viewDetails(user);
        }
      },
      {
        text: "Cancel",
        role: "cancel",
        handler: () => {
          console.log("Cancel clicked");
        }
      }
    ];

    if (this.business.role === 1) {
      actionButtons.unshift(
        {
          text: "Edit Connection",
          handler: () => {
            console.log("Cancel clicked");
            this.viewConnectionSettings(selected);
          }
        },
        {
          text: "Send Notification",
          handler: () => {
            setTimeout(() => {
              this.viewOperatorSettings(user);
            }, 300);
          }
        }
      );
    }

    const actionSheet = this.actionSheetCtrl.create({
      title: "Select what to do with " + business + "?",
      buttons: actionButtons
    });
    actionSheet.present();
  }

  unfollowBusiness(item: any) {
    if (this.business.role !== 1) {
      const toast = this.toastCtrl.create({
        message: "Invalid Access!!!",
        duration: 3000
      });
      toast.present();
      return;
    }

    const loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    loading.present();
    this.connect
      .connect_remove({
        type: item.type,
        id: item.id
      })
      .then(
        (data: any) => {
          if (data && data.success) {
            const alert = this.alertCtrl.create({
              title: "SUCCESS",
              message: data.data,
              buttons: ["OK"]
            });
            alert.onDidDismiss(() => {
              if (this.mode == "followers") {
                this.getFollowers();
              } else if (this.mode == "following") {
                this.getFollowing();
              }
            });
            alert.present();
          } else if (data && !data.success) {
            const alert = this.alertCtrl.create({
              title: "WARNING",
              message: data.message,
              buttons: ["OK"]
            });
            alert.present();
          }
          loading.dismiss();
        },
        error => {
          console.log("error: ", error);
          loading.dismiss();
        }
      );
  }

  viewConnectionSettings(user: any) {
    console.log("user: ", user);
    if (this.business.role !== 1) {
      const toast = this.toastCtrl.create({
        message: "Invalid Access!!!",
        duration: 3000
      });
      toast.present();
      return;
    }

    this.app.getRootNav().push(ConnectSettingsPage, {
      action: "connect",
      params: user
    });
  }

  viewPlatformSettings(user: any) {
    if (this.business.role !== 1) {
      const toast = this.toastCtrl.create({
        message: "Invalid Access!!!",
        duration: 3000
      });
      toast.present();
      return;
    }

    const modal = this.modalCtrl.create(
      BYOBConnectedPlatformPage,
      {
        user: user
      },
      { cssClass: "inset-modal" }
    );

    modal.onDidDismiss(resp => {
      if (resp) {
      }
    });
    modal.present();
  }

  viewOperatorSettings(user: any) {
    if (this.business.role !== 1) {
      const toast = this.toastCtrl.create({
        message: "Invalid Access!!!",
        duration: 3000
      });
      toast.present();
      return;
    }

    const modal = this.modalCtrl.create(OperatorRequestPage, {
      user: user
    });

    modal.onDidDismiss(resp => {
      if (resp) {
      }
    });
    modal.present();
  }

  viewDetails(user: any) {
    if (this.action == "following") {
      if (
        _.toLower(this.business.businessType) == "operator" &&
        user.details.type == "operator"
      ) {
        this.app.getRootNav().push(ConnectMediaPage, {
          params: user.details,
          user: user,
          action: "connect"
        });
      } else if (
        _.toLower(this.business.businessType) == "operator" &&
        user.details.type == "distributor"
      ) {
      } else if (
        _.toLower(this.business.businessType) == "distributor" &&
        user.details.type == "operator"
      ) {
        // Product List of Operator selected
        // Media Items ( images print logos video albums)
        this.app.getRootNav().push(ConnectMediaPage, {
          params: user.details,
          user: user,
          action: "connect"
        });
      } else if (
        _.toLower(this.business.businessType) == "distributor" &&
        user.details.type == "distributor"
      ) {
      }
    } else if (this.action == "followers") {
      console.log("user: ", user);
      if (
        _.toLower(this.business.businessType) == "operator" &&
        user.details.type == "operator"
      ) {
        // Operator Settings Form
        this.app.getRootNav().push(ConnectMediaPage, {
          params: user.details,
          user: user,
          action: "connect"
        });
      } else if (
        _.toLower(this.business.businessType) == "operator" &&
        user.details.type == "distributor"
      ) {
        // Media Items ( images print logos video albums)
        this.app.getRootNav().push(ConnectMediaPage, {
          params: user.details,
          user: user,
          action: "connect"
        });
      } else if (
        _.toLower(this.business.businessType) == "distributor" &&
        user.details.type == "operator"
      ) {
      } else if (
        _.toLower(this.business.businessType) == "distributor" &&
        user.details.type == "distributor"
      ) {
      }
    }
  }

  errorAvatarHandler() {
    // event.target.src = this.utilities.radomizeAvatar();
    return this.utilities.radomizeAvatar();
  }

  viewProfile() {
    this.app.getRootNav().push(BusinessProfileSettingsPage);
  }

  viewNotifications() {
    this.app.getRootNav().push(NotificationsPage);
  }
}
