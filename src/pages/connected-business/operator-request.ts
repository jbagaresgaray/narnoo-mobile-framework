import { Component, NgZone, ViewEncapsulation } from "@angular/core";
import {
  NavParams,
  ViewController,
  LoadingController,
  Events,
  AlertController,
  ModalController
} from "ionic-angular";
import * as _ from "lodash";

import { OperatorRequestActionPage } from "./operator-request-action";

import { NewsFeedServicesProvider } from "../../providers/services/newsfeed";

@Component({
  selector: "page-operator-request-modal",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "operator-request.html"
})
export class OperatorRequestPage {
  connect_user: any = {};
  userName: string;
  selected: any = {};

  requestArr: any[] = [];
  assignedArr: any = {};

  contentErr: string;

  showLoading: boolean = true;

  constructor(
    public viewCtrl: ViewController,
    public params: NavParams,
    public loadingCtrl: LoadingController,
    public events: Events,
    public zone: NgZone,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public newsfeeds: NewsFeedServicesProvider
  ) {
    this.connect_user = params.get("user");
    console.log("this.connect_user: ", this.connect_user);

    if (this.connect_user) {
      if (this.connect_user.details) {
        this.userName =
          this.connect_user.details.business || this.connect_user.details.name;
        this.selected = this.connect_user.details;
      } else {
        this.userName = this.connect_user.name;
        this.selected = this.connect_user;
      }
    }
  }

  ionViewDidLoad() {
    this.initData();
  }

  private initData(){
    this.showLoading = true;
    this.newsfeeds
      .notification_request(
        this.connect_user.details.type,
        this.connect_user.details.id
      )
      .then(
        (data: any) => {
          if (data && data.success) {
            this.requestArr = data.data.requests;
          }
          _.each(this.requestArr, (row: any) => {
            row.selected = false;
          });
          this.showLoading = false;
        },
        error => {
          console.log("error: ", error);
          this.showLoading = false;
        }
      );
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  selectItem(item: any) {
    _.each(this.requestArr, (row: any) => {
      if (row.type == item.type) {
        row.selected = true;
      } else {
        row.selected = false;
      }
    });

    const modal = this.modalCtrl.create(OperatorRequestActionPage, {
      user: this.connect_user,
      request: item
    });
    modal.present();
  }
}
