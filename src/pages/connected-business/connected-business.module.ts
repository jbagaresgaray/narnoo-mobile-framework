import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { JoditAngularModule } from "jodit-angular";
import { IonicImageLoader } from "ionic-image-loader";
import { CacheModule } from "ionic-cache-observable";

import { ConnectedBusinessPage } from "./connected-business";
import { BYOBConnectedPlatformPage } from "./byob-connected-platform";
import { OperatorRequestPage } from "./operator-request";
import { OperatorRequestActionPage } from "./operator-request-action";

import { PipesModule } from "../../pipes/pipes.module";
import { ComponentsModule } from "../../components/components.module";

@NgModule({
	declarations: [
		ConnectedBusinessPage,
		BYOBConnectedPlatformPage,
		OperatorRequestPage,
		OperatorRequestActionPage
	],
	imports: [
		PipesModule,
		ComponentsModule,
    JoditAngularModule,
    IonicImageLoader,
    CacheModule,
		IonicPageModule.forChild(ConnectedBusinessPage)
	],
	entryComponents: [
		BYOBConnectedPlatformPage,
		OperatorRequestPage,
		OperatorRequestActionPage
	]
})
export class ConnectedBusinessPageModule {}
