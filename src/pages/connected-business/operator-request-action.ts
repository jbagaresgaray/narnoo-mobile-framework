import { Component, NgZone, ViewEncapsulation } from "@angular/core";
import {
  NavParams,
  ViewController,
  LoadingController,
  Events,
  AlertController
} from "ionic-angular";
import * as _ from "lodash";

import { NewsFeedServicesProvider } from "../../providers/services/newsfeed";

@Component({
  selector: "page-operator-request-action-modal",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "operator-request-action.html"
})
export class OperatorRequestActionPage {
  connect_user: any = {};
  request: any = {};
  selected: any = {};
  assignedArr: any = {};
  textEditorConfig: any = {};

  userName: string;

  requestArr: any[] = [];

  contentErr: string;

  showLoading: boolean = true;
  isSending: boolean = false;

  constructor(
    public viewCtrl: ViewController,
    public params: NavParams,
    public loadingCtrl: LoadingController,
    public events: Events,
    public zone: NgZone,
    public alertCtrl: AlertController,
    public newsfeeds: NewsFeedServicesProvider
  ) {
    this.connect_user = params.get("user");
    this.request = params.get("request");
    console.log("connect_user: ", this.connect_user);
    console.log("request: ", this.request);

    if (_.isEmpty(this.request.messageText)) {
      this.request.messageText = "";
    }

    this.textEditorConfig = {
      buttons: "bold,strikethrough,underline,italic"
    };

    if (this.connect_user) {
      if (this.connect_user.details) {
        this.userName =
          this.connect_user.details.business || this.connect_user.details.name;
        this.selected = this.connect_user.details;
      } else {
        this.userName = this.connect_user.name;
        this.selected = this.connect_user;
      }
    }
  }

  ionViewDidLoad() {
    this.initData();
  }

  private initData() {
    this.showLoading = true;
    this.newsfeeds
      .notification_request(
        this.connect_user.details.type,
        this.connect_user.details.id
      )
      .then(
        (data: any) => {
          if (data && data.success) {
            this.requestArr = data.data.requests;
          }
          _.each(this.requestArr, (row: any) => {
            row.selected = false;
          });
          this.showLoading = false;
        },
        error => {
          console.log("error: ", error);
          this.showLoading = false;
        }
      );
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  sendMessage() {
    this.isSending = true;
    const loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    loading.present();
    this.newsfeeds
      .create_notifications({
        businessId: this.connect_user.details.id,
        businessType: this.connect_user.details.type,
        request: this.request.type,
        message: this.request.messageText
      })
      .then(
        (data: any) => {
          this.isSending = false;
          loading.dismiss();
          if (data && data.success) {
            const alert = this.alertCtrl.create({
              title: "SUCCESS",
              subTitle: "Notification has been sent",
              buttons: ["OK"]
            });
            alert.onDidDismiss(() => {
              this.viewCtrl.dismiss();
            });
            alert.present();
          } else if (data && !data.success) {
            this.alertCtrl
              .create({
                title: "WARNING",
                subTitle: data.message
              })
              .present();
          }
        },
        error => {
          console.log("error: ", error);
          this.isSending = false;
          loading.dismiss();
        }
      );
  }
}
