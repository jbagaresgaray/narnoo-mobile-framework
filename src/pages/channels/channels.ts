import { Component, ViewEncapsulation, NgZone } from "@angular/core";
import {
  Platform,
  IonicPage,
  NavController,
  NavParams,
  ModalController,
  AlertController,
  LoadingController,
  ToastController,
  ActionSheetController
} from "ionic-angular";
import * as _ from "lodash";
import { CacheService, Cache } from "ionic-cache-observable";
import { Observable, Subscription } from "rxjs";
import "rxjs/add/operator/finally";

import { ChannelsEntryPage } from "../channels-entry/channels-entry";

import { ChannelsServicesProvider } from "../../providers/services/channels";
import { ImageModalPage } from "../image-modal/image-modal";
import { StorageProvider } from "../../providers/storage/storage";
import { LoadingProvider } from "../../providers/loading/loading";

@IonicPage()
@Component({
  selector: "page-channels",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "channels.html"
})
export class ChannelsPage {
  token: string;
  item: any = {};
  channel: any = {};
  business: any = {};
  imagesLoadingArr: any[] = [];

  action: string;
  connect_params: any = {};

  showLoading: boolean = true;

  mediaArr$: Observable<any>;
  private mediaCache: Cache<any>;
  public refreshSubscription: Subscription;
  constructor(
    public zone: NgZone,
    public platform: Platform,
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public actionSheetCtrl: ActionSheetController,
    public channels: ChannelsServicesProvider,
    public storageService: StorageProvider,
    public cacheService: CacheService,
    public loader: LoadingProvider
  ) {
    this.action = navParams.get("action");
    this.connect_params = navParams.get("params");
    this.item = navParams.get("item");

    for (var i = 0; i < 10; ++i) {
      this.imagesLoadingArr.push(i);
    }

    this.channel.video = [];
  }

  private initData(refresher?: any) {
    console.log("ionViewDidLoad ChannelsPage");
    this.showLoading = true;

    const dataObservable = Observable.fromPromise(
      this.channels.channel_details(this.item.id)
    );

    const cacheKey =
      this.storageService.cacheKey.MEDIA_CHANNEL_DETAIL +
      "_" +
      this.business.id +
      "_" +
      this.item.id;

    this.cacheService.register(cacheKey, dataObservable).subscribe(cache => {
      this.mediaCache = cache;
      this.zone.run(() => {
        this.mediaArr$ = cache.get$;

        this.mediaCache.refresh().subscribe(
          () => {
            this.loader.start();
          },
          () => {
            this.loader.complete();
          },
          () => {
            this.loader.complete();
          }
        );
      });
    });

    this.mediaArr$.subscribe(
      (data: any) => {
        if (data && data.success) {
          this.zone.run(() => {
            this.channel = data.data;
          });
          console.log("this.channel: ", this.channel);
        }
        this.showLoading = false;
        if (refresher) {
          refresher.complete();
        }
      },
      _error => {
        this.showLoading = false;
        if (refresher) {
          refresher.complete();
        }
      }
    );
  }

  private async initBusiness() {
    const business = await this.storageService.getStorage(
      this.storageService.storageKey.CURRENT_BUSINESS
    );
    this.business = !_.isEmpty(business) ? JSON.parse(business) || {} : {};
  }

  ionViewDidLoad() {
    this.initBusiness().then(() => {
      if (this.action == "connect") {
      } else {
        this.initData();
      }
    });
  }

  doRefresh(refresher: any) {
    if (this.action == "connect") {
      refresher.complete();
    } else {
      if (this.mediaCache) {
        this.showLoading = true;
        this.zone.run(() => {
          this.initData(refresher);
        });
      } else {
        console.warn("Cache has not been initialized.");
        if (refresher) {
          refresher.cancel();
        }
      }
    }
  }

  deleteChannels() {
    let vm = this;

    if (this.business.role !== 1) {
      this.toastCtrl
        .create({
          message: "Invalid Access!!!",
          duration: 3000
        })
        .present();
      return;
    }

    const deleteChannel = id => {
      const loading = this.loadingCtrl.create({
        dismissOnPageChange: true
      });
      loading.present();
      this.channels
        .channel_delete({
          id: id
        })
        .then(
          (data: any) => {
            if (data && data.success) {
              console.log("result: ", data);
              const alert = this.alertCtrl.create({
                title: "Error",
                message: data.message,
                buttons: ["OK"]
              });
              alert.onDidDismiss(() => {
                this.navCtrl.pop();
              });
              alert.present();
            }
            loading.dismiss();
          },
          error => {
            loading.dismiss();
            console.log("error: ", error);

            const alert = this.alertCtrl.create({
              title: "Error",
              message: error.message,
              buttons: ["OK"]
            });
            alert.present();
          }
        );
    };

    const confirm = this.alertCtrl.create({
      title: "Delete this channel?",
      message: "Are you sure to delete this channel?",
      buttons: [
        {
          text: "No",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Yes",
          handler: () => {
            console.log("Agree clicked");
            deleteChannel(this.channel.id);
          }
        }
      ]
    });
    confirm.present();
  }

  updateChannnel() {
    if (this.business.role !== 1) {
      this.toastCtrl
        .create({
          message: "Invalid Access!!!",
          duration: 3000
        })
        .present();
      return;
    }

    const modal = this.modalCtrl.create(ChannelsEntryPage);
    modal.onDidDismiss(resp => {
      if (resp == "save") {
        this.initData();
      }
    });
    modal.present();
  }

  private addMediaFile() {
    if (this.business.role !== 1) {
      this.toastCtrl
        .create({
          message: "Invalid Access!!!",
          duration: 3000
        })
        .present();
      return;
    }

    const modal = this.modalCtrl.create(ImageModalPage, {
      params: this.channel,
      media: "video",
      mode: "channel"
    });
    modal.onDidDismiss(resp => {
      if (resp == "save") {
        this.initData();
      }
    });
    modal.present();
  }

  presentActionSheet() {
    const actionSheet = this.actionSheetCtrl.create({
      title: "Select action for collection",
      buttons: [
        {
          text: "Edit",
          handler: () => {
            setTimeout(() => {
              this.updateChannnel();
            }, 300);
          }
        },
        {
          text: "Delete",
          role: "destructive",
          handler: () => {
            setTimeout(() => {
              this.deleteChannels();
            }, 300);
          }
        },
        {
          text: "Add Media File",
          handler: () => {
            setTimeout(() => {
              this.addMediaFile();
            }, 300);
          }
        },
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        }
      ]
    });
    actionSheet.present();
  }
}
