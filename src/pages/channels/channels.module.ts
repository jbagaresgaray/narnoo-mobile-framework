import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChannelsPage } from './channels';

import { PipesModule } from '../../pipes/pipes.module';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
	declarations: [
		ChannelsPage,
	],
	imports: [
		IonicPageModule.forChild(ChannelsPage),
    PipesModule,
    ComponentsModule,
	],
})
export class ChannelsPageModule { }
