import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StaffDirectoryPage } from './staff-directory';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
	declarations: [
		StaffDirectoryPage,
	],
	imports: [
		ComponentsModule,
		IonicPageModule.forChild(StaffDirectoryPage),
	],
})
export class StaffDirectoryPageModule { }
