import { Component, ViewEncapsulation, NgZone } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ModalController,
  AlertController,
  LoadingController,
  ToastController
} from "ionic-angular";
import { CacheService, Cache } from "ionic-cache-observable";
import { Observable, Subscription } from "rxjs";
import "rxjs/add/operator/finally";
import * as md5 from "crypto-md5";
import * as _ from "lodash";

import { StaffDirectoryEntryPage } from "../../pages/staff-directory-entry/staff-directory-entry";
import { BusinessUserDetailPage } from "../../pages/business-user-detail/business-user-detail";

import { StaffServicesProvider } from "../../providers/services/staff";
import { StorageProvider } from "../../providers/storage/storage";
import { LoadingProvider } from "./../../providers/loading/loading";

@IonicPage()
@Component({
  selector: "page-staff-directory",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "staff-directory.html"
})
export class StaffDirectoryPage {
  contactsArr: any[] = [];
  contactsArrCopy: any[] = [];
  fakeArr: any[] = [];

  business: any = {};
  contentErr: any = {};

  contactsArr$: Observable<any>;
  private contactsCache: Cache<any>;
  public refreshSubscription: Subscription;

  showContent: boolean = false;
  constructor(
    public zone: NgZone,
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public staffs: StaffServicesProvider,
    public storageService: StorageProvider,
    public cacheService: CacheService,
    public loader: LoadingProvider
  ) {
    for (var i = 0; i < 12; ++i) {
      this.fakeArr.push(i);
    }
  }

  private initializeData(refresher?: any) {
    this.showContent = false;

    const dataObservable = Observable.fromPromise(this.staffs.staff_list());
    const cacheKey =
      this.storageService.cacheKey.STAFFS + "_" + this.business.id;

    this.cacheService.register(cacheKey, dataObservable).subscribe(cache => {
      this.contactsCache = cache;
      this.zone.run(() => {
        this.contactsArr$ = cache.get$;

        this.contactsCache.refresh().subscribe(
          () => {
            this.loader.start();
          },
          () => {
            this.loader.complete();
          },
          () => {
            this.loader.complete();
          }
        );
      });
    });

    const successResponse = (data: any) => {
      if (data && data.success) {
        _.each(data.data, (row: any) => {
          row.gravatar =
            "https://www.gravatar.com/avatar/" +
            md5(row.email.toLowerCase(), "hex");
        });

        this.zone.run(() => {
          this.contactsArr = data.data;
          this.contactsArrCopy = data.data;
          this.showContent = true;
        });
      }

      if (refresher) {
        refresher.complete();
      }
    };

    const errorResponse = error => {
      this.showContent = true;
      if (refresher) {
        refresher.complete();
      }
      if (error) {
        this.contentErr = error;
      }
    };

    this.contactsArr$.subscribe(successResponse, errorResponse);
  }

  private async initBusiness() {
    const business = await this.storageService.getStorage(
      this.storageService.storageKey.CURRENT_BUSINESS
    );
    this.business = !_.isEmpty(business) ? JSON.parse(business) || {} : {};
  }

  ionViewWillEnter() {
    console.log("ionViewWillEnter StaffDirectoryPage");
    this.initBusiness().then(() => {
      this.initializeData();
    });
  }

  createStaff() {
    if (this.business.role !== 1) {
      this.toastCtrl
        .create({
          message: "Invalid Access!!!",
          duration: 3000
        })
        .present();
      return;
    }

    const profileModal = this.modalCtrl.create(StaffDirectoryEntryPage, {
      action: "add",
      type: "staff"
    });
    profileModal.present();
    profileModal.onDidDismiss(status => {
      console.log("status: ", status);
      if (status === "save") {
        this.initializeData();
      }
    });
  }

  deleteStaff(item, slidingItem) {
    if (this.business.role !== 1) {
      this.toastCtrl
        .create({
          message: "Invalid Access!!!",
          duration: 3000
        })
        .present();
      return;
    }

    slidingItem.close();
    console.log("item: ", item);
    const confirm = this.alertCtrl.create({
      title: "Delete Staff?",
      message: "Are you sure to delete this Staff?",
      buttons: [
        {
          text: "No",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Yes",
          handler: () => {
            console.log("Agree clicked");
            let loading = this.loadingCtrl.create();
            loading.present();
            this.staffs.staff_remove(item).then(
              (data: any) => {
                if (data && data.success) {
                  this.initializeData();
                  loading.dismiss();
                }
              },
              error => {
                loading.dismiss();
                if (error && !error.success) {
                  let alert = this.alertCtrl.create({
                    title: "WARNING",
                    subTitle: error.message,
                    buttons: ["OK"]
                  });
                  alert.present();
                }
              }
            );
          }
        }
      ]
    });
    confirm.present();
  }

  updateStaff(item, slidingItem) {
    if (this.business.role !== 1) {
      this.toastCtrl
        .create({
          message: "Invalid Access!!!",
          duration: 3000
        })
        .present();
      return;
    }

    slidingItem.close();

    const profileModal = this.modalCtrl.create(StaffDirectoryEntryPage, {
      action: "update",
      type: "staff",
      item: item
    });
    profileModal.present();
    profileModal.onDidDismiss(status => {
      console.log("status: ", status);
      if (status) {
        this.initializeData();
      }
    });
  }

  gotoDetail(item) {
    this.navCtrl.push(BusinessUserDetailPage, { item: item, action: "staff" });
  }

  doRefresh(refresher: any) {
    if (this.contactsCache) {
      this.showContent = false;
      this.zone.run(() => {
        this.initializeData(refresher);
      });
    } else {
      console.warn("Cache has not been initialized.");
      if (refresher) {
        refresher.complete();
      }
    }
  }

  onCancel(ev) {
    this.contactsArr = this.contactsArrCopy;
  }

  getItems(ev: any) {
    this.showContent = false;
    let val = ev.target.value;
    if (val && val.trim() != "") {
      this.contactsArr = this.contactsArrCopy.filter((item: any) => {
        return item.name.toLowerCase().indexOf(val.toLowerCase()) > -1;
      });
      this.showContent = true;
    } else {
      this.contactsArr = this.contactsArrCopy;
    }
  }
}
