import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { AlbumsPage } from "./albums";
import { IonicImageViewerModule } from "ionic-img-viewer";
import { CacheModule } from "ionic-cache-observable";

import { PipesModule } from "../../pipes/pipes.module";
import { ComponentsModule } from "../../components/components.module";

@NgModule({
  declarations: [AlbumsPage],
  imports: [
    IonicPageModule.forChild(AlbumsPage),
    IonicImageViewerModule,
    CacheModule,
    PipesModule,
    ComponentsModule
  ]
})
export class AlbumsPageModule {}
