import { Component, ViewEncapsulation, NgZone } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  ToastController
} from "ionic-angular";
import * as async from "async";
import * as _ from "lodash";
import { CacheService, Cache } from "ionic-cache-observable";
import { Observable, Subscription } from "rxjs";
import "rxjs/add/operator/finally";

import { AlbumsServicesProvider } from "../../providers/services/albums";

import { AlbumGalleryPage } from "../../pages/album-gallery/album-gallery";
import { ImageDetailPage } from "../../pages/image-detail/image-detail";
import { StorageProvider } from "../../providers/storage/storage";

@IonicPage()
@Component({
  selector: "page-albums",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "albums.html"
})
export class AlbumsPage {
  albumsArr: any[] = [];
  imagesArr: any[] = [];
  fakeArr: any[] = [];
  showContent: boolean = false;
  business: any = {};

  action: string;
  title: string;
  medias: any = {};

  mediaArr$: Observable<any>;
  private mediaCache: Cache<any>;
  public refreshSubscription: Subscription;

  constructor(
    public zone: NgZone,
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public albums: AlbumsServicesProvider,
    public storageService: StorageProvider,
    public cacheService: CacheService
  ) {
    this.action = navParams.get("action");
    this.medias = navParams.get("medias");
    console.log("action: ", this.action);
    console.log("medias: ", this.medias);

    if (this.action != "more") {
      this.title = "Albums";
    } else if (this.action == "more") {
      this.title = "Images";
    }

    for (var i = 0; i < 12; ++i) {
      this.fakeArr.push(i);
    }
  }

  private initData(ev?: any) {
    this.showContent = false;

    this.albums.album_list().then(
      (data: any) => {
        if (data && data.success) {
          this.albumsArr = data.data.albums;
          async.each(
            this.albumsArr,
            (item: any, callback) => {
              item.imageCount = 0;
              item.images = [];
              this.albums.album_images(item.id).then(
                (images: any) => {
                  if (images && images.success) {
                    item.images = !_.isNull(images.data.images)
                      ? images.data.images
                      : [];
                    item.imageCount = !_.isNull(images.data.images)
                      ? images.data.images.length
                      : 0;
                  }
                  callback();
                },
                error => {
                  this.showContent = true;
                }
              );
            },
            () => {
              this.showContent = true;
            }
          );
          console.log("this.albumsArr: ", this.albumsArr);
        }
        if (ev) {
          ev.complete();
        }
      },
      error => {
        this.showContent = true;
        if (ev) {
          ev.complete();
        }
      }
    );
  }

  async ionViewDidLoad() {
    console.log("ionViewDidLoad AlbumsPage");
    const business = await this.storageService.getStorage(
      this.storageService.storageKey.CURRENT_BUSINESS
    );
    this.business = !_.isEmpty(business) ? JSON.parse(business) || {} : {};

    if (this.action != "more") {
      this.initData();
    } else {
      this.imagesArr = this.medias.media;

      setTimeout(() => {
        this.showContent = true;
      }, 1000);
    }
  }

  doRefresh(ev) {
    if (this.action != "more") {
      this.initData(ev);
    }
  }

  viewAlbumImages(item: any) {
    this.navCtrl.push(AlbumGalleryPage, {
      id: item.id,
      album: item.title
    });
  }

  viewImageDetail(item) {
    console.log("item: ", item);

    if (
      this.business.role !== 3 &&
      this.business.role !== 4 &&
      this.business.role !== 1
    ) {
      let toast = this.toastCtrl.create({
        message: "Invalid Access!!!",
        duration: 3000
      });
      toast.present();
      return;
    }

    this.navCtrl.push(ImageDetailPage, {
      image: item
    });
  }
}
