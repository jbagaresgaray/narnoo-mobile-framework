import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ActivityDetailPage } from './activity-detail';
import { IonicImageLoader } from 'ionic-image-loader';
import { IonicImageViewerModule } from 'ionic-img-viewer';

import { PipesModule } from '../../pipes/pipes.module';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
	declarations: [
		ActivityDetailPage,
	],
	imports: [
		IonicPageModule.forChild(ActivityDetailPage),
		IonicImageLoader,
		PipesModule,
		IonicImageViewerModule,
		PipesModule,ComponentsModule
	],
})
export class ActivityDetailPageModule { }
