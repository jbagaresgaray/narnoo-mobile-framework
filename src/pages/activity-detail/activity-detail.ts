import { Component, NgZone } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  AlertController,
  Platform,
  ActionSheetController
} from "ionic-angular";
import * as _ from "lodash";
import * as $ from "jquery";

import { FileTransfer, FileTransferObject } from "@ionic-native/file-transfer";
import { LocalNotifications } from "@ionic-native/local-notifications";

import { PrintServicesProvider } from "../../providers/services/print";
import { LogoServicesProvider } from "../../providers/services/logos";
import { VideosServicesProvider } from "../../providers/services/videos";
import { ImagesServicesProvider } from "../../providers/services/images";
import { AlbumsServicesProvider } from "../../providers/services/albums";
import { StorageProvider } from "../../providers/storage/storage";

import { environment } from "../../environments/environment";

@IonicPage()
@Component({
  selector: "page-activity-detail",
  templateUrl: "activity-detail.html"
})
export class ActivityDetailPage {
  token: string;
  type: string;
  item: any = {};
  business: any = {};
  curbusiness: any = {};
  details: any = {};
  showDownload: boolean;
  showLoading: boolean = true;

  storageDirectory: string = "";

  constructor(
    public platform: Platform,
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public actionSheetCtrl: ActionSheetController,
    public prints: PrintServicesProvider,
    public images: ImagesServicesProvider,
    public logos: LogoServicesProvider,
    public videos: VideosServicesProvider,
    public albums: AlbumsServicesProvider,
    public storageService: StorageProvider,
    public zone: NgZone,
    private transfer: FileTransfer,
    private localNotifications: LocalNotifications
  ) {
    this.initData();

    this.business = navParams.get("business");
    this.item = navParams.get("item");
    this.type = navParams.get("type");

    console.log("item: ", this.item);
    console.log("business: ", this.business);
    console.log("type: ", this.type);

    if (platform.is("cordova")) {
      if (this.platform.is("ios")) {
        this.storageDirectory =
          window["cordova"].file.documentsDirectory + environment.AlbumName;
      } else if (this.platform.is("android")) {
        this.storageDirectory =
          window["cordova"].file.externalRootDirectory + environment.AlbumName;
      }
      console.log("this.storageDirectory: ", this.storageDirectory);
    }

    if (_.isEmpty(this.item.caption)) {
      this.item.caption = "No caption available";
    }
    /*this.item.imageSize = parseFloat(this.item.imageSize);
     */

    if (this.type === "image") {
      const successResponse = (data: any) => {
        console.log("images: ", data);
        if (data && data.success && data.data[0] != false) {
          this.details = data.data;
          setTimeout(() => {
            this.progressImage();
          }, 2000);
        }
        this.showLoading = false;
      };

      const errorResponse = error => {
        this.showLoading = false;
      };

      if (this.business.businessType === "Operator") {
        this.showLoading = true;
        this.images
          .image_operator_image(this.business.id, this.item.objectId)
          .then(successResponse, errorResponse);
      } else if (this.business.businessType === "Distributor") {
        this.showLoading = true;
        this.images
          .image_distributor_image(this.business.id, this.item.objectId)
          .then(successResponse, errorResponse);
      }
    } else if (this.type === "brochure") {
      this.showLoading = true;
      this.prints.brochure_detail(this.item.objectId).then(
        (data: any) => {
          console.log("brochure: ", data);
          if (data && data.success && data.data[0] != false) {
            this.details = data.data;

            setTimeout(() => {
              this.progressImage();
            }, 2000);
          }
          this.showLoading = false;
        },
        error => {
          this.showLoading = false;
        }
      );
    } else if (this.type === "logo") {
      this.showLoading = true;
      this.logos.logos_detail(this.item.objectId).then(
        (data: any) => {
          console.log("logo: ", data);
          if (data && data.success && data.data[0] != false) {
            this.details = data.data;

            setTimeout(() => {
              this.progressImage();
            }, 2000);
          }
          this.showLoading = false;
        },
        error => {
          this.showLoading = false;
        }
      );
    } else if (this.type === "video") {
      this.showLoading = true;
      this.videos.video_detail(this.item.objectId).then(
        (data: any) => {
          console.log("video: ", data);
          if (data && data.success && data.data[0] != false) {
            this.details = data.data;
          }
          this.showLoading = false;
        },
        error => {
          this.showLoading = false;
        }
      );
    } else if (this.type === "album") {
      const successResponse = (data: any) => {
        console.log("album: ", data);
        if (data && data.success && data.data[0] != false) {
          this.details = data.data;
        }
        this.showLoading = false;
      };

      const errorResponse = error => {
        this.showLoading = false;
      };

      if (this.business.businessType === "Operator") {
        this.showLoading = true;
        this.images
          .image_detail(this.item.objectId)
          .then(successResponse, errorResponse);
      } else if (this.business.businessType === "Distributor") {
        this.showLoading = true;
        this.images
          .image_detail(this.item.objectId)
          .then(successResponse, errorResponse);
      }
    }
  }

  ionViewWillEnter() {
    this.progressImage();
  }

  private progressImage(){
    $(".progressive-image").each(function() {
      const image = new Image();
      const previewImage = $(this).find(".loadingImage");
      const newImage = $(this).find(".overlay");

      image.onload = function() {
        newImage.css("background-image", "url(" + image.src + ")");
        newImage.css("opacity", "1");
      };
      image.src = previewImage.data("image");
    });
  }

  private async initData() {
    const business = await this.storageService.getStorage(
      this.storageService.storageKey.CURRENT_BUSINESS
    );
    this.curbusiness = !_.isEmpty(business) ? JSON.parse(business) || {} : {};
  }

  presentActionSheet() {
    const actionSheet = this.actionSheetCtrl.create({
      title: "Modify your album",
      buttons: [
        {
          text: "Edit Details",
          handler: () => {
            console.log("Destructive clicked");
          }
        },
        {
          text: "Add to Album",
          handler: () => {
            console.log("Archive clicked");
          }
        },
        {
          text: "Add to Product Gallery",
          handler: () => {
            console.log("Archive clicked");
          }
        },
        {
          text: "Set feature",
          handler: () => {
            console.log("Archive clicked");
          }
        },
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        }
      ]
    });
    actionSheet.present();
  }

  downloadFile() {
    let ctrl = this;
    let perc: any;

    if (!this.curbusiness.paid) {
      this.alertCtrl
        .create({
          title: "WARNING",
          message: "This feature is only available to premium users only.",
          buttons: ["OK"]
        })
        .present();
      return;
    }

    const loading = this.loadingCtrl.create({
      content: "Downloading ...",
      dismissOnPageChange: true
    });
    loading.present();
    const alert = this.alertCtrl.create({
      title: "Success",
      subTitle: "Image successfully downloaded",
      buttons: ["OK"]
    });

    const download = (dataurl, filename) => {
      var a = document.createElement("a");
      a.href = dataurl;
      a.setAttribute("download", filename);
      var b = document.createEvent("MouseEvents");
      b.initEvent("click", false, true);
      a.dispatchEvent(b);
      return false;
    };

    const downloadNative = (image, filename) => {
      ctrl.platform.ready().then(() => {
        const fileTransfer: FileTransferObject = ctrl.transfer.create();
        const newDir = ctrl.storageDirectory + "/" + filename;
        console.log("newDir: ", newDir);

        fileTransfer.download(image, newDir).then(
          (entry: any) => {
            console.log("entry: ", entry);
            console.log("entry.toURL(); ", entry.toURL());
            if (entry) {
              const alertSuccess = ctrl.alertCtrl.create({
                title: `Download Succeeded!`,
                subTitle: "Download successfully",
                buttons: ["Ok"]
              });

              loading.dismiss();
              alertSuccess.present();
            }
          },
          error => {
            console.log("fileTransfer error: ", error);
            const alertFailure = ctrl.alertCtrl.create({
              title: `Download Failed!`,
              subTitle: `${image} was not successfully downloaded. Error code: ${
                error.code
              }`,
              buttons: ["Ok"]
            });
            loading.dismiss();
            alertFailure.present();
          }
        );

        fileTransfer.onProgress(progressEvent => {
          if (progressEvent.lengthComputable) {
            perc = Math.floor(
              (progressEvent.loaded / progressEvent.total) * 100
            );
            console.log("Progress: ", perc + "% loaded...");

            ctrl.zone.run(() => {
              loading.setContent("Downloading " + perc + "% ...");

              if (ctrl.platform.is("android")) {
                ctrl.localNotifications.schedule({
                  title: "Downloading " + filename,
                  text: "Downloading " + perc + "% ...",
                  sound: null,
                  progressBar: { value: perc }
                });
              }
            });
          }
        });
      });
    };

    const getFilename = url => {
      url = url
        .split("/")
        .pop()
        .replace(/\#(.*?)$/, "")
        .replace(/\?(.*?)$/, "");
      url = url.split("."); // separates filename and extension
      return { filename: url[0] || "", ext: url[1] || "" };
    };

    const successResponse = (data: any) => {
      if (data && data.success) {
        console.log("successResponse: ", data.data);
        const filename =
          getFilename(data.data).filename + "." + getFilename(data.data).ext;

        if (!ctrl.platform.is("cordova")) {
          download(data.data, filename);
          loading.dismiss();
          alert.present();
        } else {
          downloadNative(data.data, filename);
        }
      }
    };

    if (this.item.itemType == "image") {
      if (this.business.businessType === "Operator") {
        this.images
          .image_operator_download(this.details.id, this.business.id)
          .then(successResponse, error => {
            loading.dismiss();
            if (error && !error.success) {
              this.alertCtrl
                .create({
                  title: "WARNING",
                  message: error.message,
                  buttons: ["OK"]
                })
                .present();
              return;
            }
          });
      } else if (this.business.businessType === "Distributor") {
        this.images
          .image_distributor_download(this.details.id, this.business.id)
          .then(successResponse, error => {
            loading.dismiss();
            if (error && !error.success) {
              this.alertCtrl
                .create({
                  title: "WARNING",
                  message: error.message,
                  buttons: ["OK"]
                })
                .present();
              return;
            }
          });
      }
    } else if (this.item.itemType == "brochure") {
      this.prints
        .brochure_download(this.details.id)
        .then(successResponse, error => {
          loading.dismiss();
          if (error && !error.success) {
            this.alertCtrl
              .create({
                title: "WARNING",
                message: error.message,
                buttons: ["OK"]
              })
              .present();
            return;
          }
        });
    } else if (this.item.itemType == "logo") {
      this.logos
        .logos_download(this.details.id)
        .then(successResponse, error => {
          loading.dismiss();
          if (error && !error.success) {
            this.alertCtrl
              .create({
                title: "WARNING",
                message: error.message,
                buttons: ["OK"]
              })
              .present();
            return;
          }
        });
    }
  }
}
