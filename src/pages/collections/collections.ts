import { Component, ViewChild, ViewEncapsulation, NgZone } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  Slides,
  ModalController,
  AlertController,
  Content,
  LoadingController,
  ToastController,
  ActionSheetController,
  Platform
} from "ionic-angular";
import * as _ from "lodash";
import * as async from "async";
import { CacheService, Cache } from "ionic-cache-observable";
import { Observable, Subscription } from "rxjs";
import "rxjs/add/operator/finally";

import { CollectionsServicesProvider } from "../../providers/services/collections";

import { ImageDetailPage } from "../../pages/image-detail/image-detail";
import { VideoDetailPage } from "../../pages/video-detail/video-detail";
import { PrintDetailPage } from "../../pages/print-detail/print-detail";
import { ProductDetailPage } from "../../pages/product-detail/product-detail";
import { ImageModalPage } from "../../pages/image-modal/image-modal";
import { CollectionsEntryPage } from "../../pages/collections-entry/collections-entry";
import { StorageProvider } from "../../providers/storage/storage";
import { LoadingProvider } from "../../providers/loading/loading";

declare let safari: any;

@IonicPage()
@Component({
  selector: "page-collections",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "collections.html"
})
export class CollectionsPage {
  item: any = {};
  business: any = {};
  collection: any = {};
  slides: any;
  selectedSegment: string = "info";
  imagesLoadingArr: any[] = [];

  action: string;
  connect_params: any = {};

  showImageContent: boolean = false;
  showCheckbox: boolean = false;
  isSafari: boolean = false;

  @ViewChild("mySlider") slider: Slides;
  @ViewChild(Content) content: Content;

  mediaArr$: Observable<any>;
  private mediaCache: Cache<any>;
  public refreshSubscription: Subscription;

  constructor(
    public zone: NgZone,
    public platform: Platform,
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public actionSheetCtrl: ActionSheetController,
    public collections: CollectionsServicesProvider,
    public storageService: StorageProvider,
    public cacheService: CacheService,
    public loader: LoadingProvider
  ) {
    this.action = navParams.get("action");
    this.connect_params = navParams.get("params");
    this.item = navParams.get("item");

    this.collection.images = [];
    this.collection.prints = [];
    this.collection.videos = [];
    this.collection.products = [];

    for (var i = 0; i < 10; ++i) {
      this.imagesLoadingArr.push(i);
    }

    this.slides = [
      {
        id: "info"
      },
      {
        id: "image"
      },
      {
        id: "print"
      },
      {
        id: "video"
      },
      {
        id: "product"
      }
    ];

    this.isSafari =
      /constructor/i.test(window["HTMLElement"]) ||
      (function(p) {
        return p.toString() === "[object SafariRemoteNotification]";
      })(!window["safari"] || safari.pushNotification);
  }

  private initData(refresher?: any) {
    this.showImageContent = false;

    const successResponse = (data: any) => {
      if (data && data.success) {
        this.collection = data.data;
        this.collection.totalImages = this.collection.totalImages || 0;
        this.collection.totalPrints = this.collection.totalPrints || 0;
        this.collection.totalVideos = this.collection.totalVideos || 0;

        _.each(this.collection.images, (row: any) => {
          row.selected = false;
        });

        _.each(this.collection.prints, (row: any) => {
          row.selected = false;
        });

        _.each(this.collection.videos, (row: any) => {
          row.selected = false;
        });

        this.showImageContent = true;
      } else {
        this.showImageContent = true;
      }

      if (refresher) {
        refresher.complete();
      }
    };

    const errorResponse = error => {
      console.log("error: ", error);
      this.showImageContent = true;
      if (refresher) {
        refresher.complete();
      }
    };

    const dataObservable = Observable.fromPromise(
      this.collections.collecton_details(this.item.id)
    );
    const cacheKey =
      this.storageService.cacheKey.MEDIA_COLLECTION_DETAIL +
      "_" +
      this.business.id +
      "_" +
      this.item.id;

    this.cacheService.register(cacheKey, dataObservable).subscribe(cache => {
      this.mediaCache = cache;
      this.zone.run(() => {
        this.mediaArr$ = cache.get$;

        this.mediaCache.refresh().subscribe(
          () => {
            this.loader.start();
          },
          () => {
            this.loader.complete();
          },
          () => {
            this.loader.complete();
          }
        );
      });
    });

    this.mediaArr$.subscribe(successResponse, errorResponse);
  }

  private async initBusiness() {
    const business = await this.storageService.getStorage(
      this.storageService.storageKey.CURRENT_BUSINESS
    );
    this.business = !_.isEmpty(business) ? JSON.parse(business) || {} : {};
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad CollectionsPage");
    this.initBusiness().then(() => {
      if (this.action == "connect") {
      } else {
        setTimeout(() => {
          this.initData();
        }, 600);
      }
    });
  }

  doRefresh(refresher: any) {
    if (this.action == "connect") {
      refresher.complete();
    } else {
      if (this.mediaCache) {
        this.showImageContent = false;

        this.zone.run(() => {
          this.initData(refresher);
        });
      } else {
        console.warn("Cache has not been initialized.");
        if (refresher) {
          refresher.cancel();
        }
      }
    }
  }

  onSegmentChanged(segmentButton) {
    const selectedIndex = this.slides.findIndex(slide => {
      return slide.id === segmentButton.value;
    });
    this.slider.slideTo(selectedIndex);
  }

  onSlideChanged(slider) {
    console.log(slider.getActiveIndex());
    const currentSlide = this.slides[slider.getActiveIndex()];
    if (currentSlide) {
      this.selectedSegment = currentSlide.id;
      console.log("this.selectedSegment: ", this.selectedSegment);
    }
  }

  viewImageDetail(item) {
    console.log("viewImageDetail: ", item);
    if (this.showCheckbox) {
      _.each(this.collection.images, (row: any) => {
        if (item.id == row.id) {
          row.selected = !row.selected;
        }
      });
      return;
    }

    this.navCtrl.push(ImageDetailPage, { image: item });
  }

  viewVideoDetail(item) {
    console.log("viewVideoDetail: ", item);
    if (this.showCheckbox) {
      _.each(this.collection.videos, (row: any) => {
        if (item.id == row.id) {
          row.selected = !row.selected;
        }
      });
      return;
    }

    this.navCtrl.push(VideoDetailPage, { video: item });
  }

  viewPrintDetail(item) {
    console.log("viewPrintDetail: ", item);
    if (this.showCheckbox) {
      _.each(this.collection.prints, (row: any) => {
        if (item.id == row.id) {
          row.selected = !row.selected;
        }
      });
      return;
    }
    this.navCtrl.push(PrintDetailPage, { print: item });
  }

  viewProductDetail(item) {
    this.navCtrl.push(ProductDetailPage, { product: item });
  }

  selectClick() {
    this.showCheckbox = true;
  }

  cancelClick() {
    this.showCheckbox = false;
  }

  presentActionSheet() {
    let actionSheetButtons: any[] = [];

    if (this.business.role !== 1) {
      const toast = this.toastCtrl.create({
        message: "Invalid Access!!!",
        duration: 3000
      });
      toast.present();
      return;
    }

    if (this.selectedSegment == "info") {
      actionSheetButtons = [
        {
          text: "Edit",
          handler: () => {
            setTimeout(() => {
              this.editCollection();
            }, 300);
          }
        },
        {
          text: "Delete",
          role: "destructive",
          handler: () => {
            setTimeout(() => {
              this.deleteCollection();
            }, 300);
          }
        }
      ];
    } else {
      actionSheetButtons.push({
        text: "Add Media File",
        handler: () => {
          setTimeout(() => {
            this.addMediaFile();
          }, 300);
        }
      });
    }

    actionSheetButtons.push({
      text: "Cancel",
      role: "cancel",
      handler: () => {
        console.log("Cancel clicked");
      }
    });

    const actionSheet = this.actionSheetCtrl.create({
      title: "Select action for collection",
      buttons: actionSheetButtons
    });
    actionSheet.present();
  }

  private editCollection() {
    if (this.business.role !== 1) {
      const toast = this.toastCtrl.create({
        message: "Invalid Access!!!",
        duration: 3000
      });
      toast.present();
      return;
    }

    let modal = this.modalCtrl.create(CollectionsEntryPage, {
      action: "update",
      collection: this.collection
    });
    modal.onDidDismiss(resp => {
      if (resp == "save") {
        this.initData();
      }
    });
    modal.present();
  }

  private addMediaFile() {
    if (this.business.role !== 1) {
      const toast = this.toastCtrl.create({
        message: "Invalid Access!!!",
        duration: 3000
      });
      toast.present();
      return;
    }

    const modal = this.modalCtrl.create(ImageModalPage, {
      params: this.collection,
      media: this.selectedSegment,
      mode: "collection"
    });
    modal.onDidDismiss(resp => {
      if (resp == "save") {
        this.initData();
      }
    });
    modal.present();
  }

  private deleteCollection() {
    const deleteCollection = id => {
      const loading = this.loadingCtrl.create({
        dismissOnPageChange: true
      });
      loading.present();

      const formData = new FormData();
      formData.append("id", id);

      this.collections.collection_delete(formData).then(
        (data: any) => {
          if (data && data.success) {
            console.log("result: ", data);
            let alert = this.alertCtrl.create({
              title: "Error",
              message: data.data,
              buttons: ["OK"]
            });
            alert.onDidDismiss(() => {
              this.navCtrl.pop();
            });
            alert.present();
          }
          loading.dismiss();
        },
        error => {
          loading.dismiss();
          console.log("error: ", error);
          if (error) {
            let alert = this.alertCtrl.create({
              title: "Error",
              message: error.message || error.error,
              buttons: ["OK"]
            });
            alert.present();
          }
        }
      );
    };

    const confirm = this.alertCtrl.create({
      title: "Delete Collection?",
      message: "Are you sure to delete collection?",
      buttons: [
        {
          text: "Cancel",
          handler: () => {}
        },
        {
          text: "Confirm",
          handler: () => {
            deleteCollection(this.collection.id);
          }
        }
      ]
    });
    confirm.present();
  }

  removeMediaFile() {
    if (this.business.role !== 1) {
      const toast = this.toastCtrl.create({
        message: "Invalid Access!!!",
        duration: 3000
      });
      toast.present();
      return;
    }

    let selectedDatas: any[] = [];
    if (this.selectedSegment == "image") {
      selectedDatas = _.filter(this.collection.images, (row: any) => {
        return row.selected === true;
      });
    } else if (this.selectedSegment == "print") {
      selectedDatas = _.filter(this.collection.prints, (row: any) => {
        return row.selected === true;
      });
    } else if (this.selectedSegment == "video") {
      selectedDatas = _.filter(this.collection.videos, (row: any) => {
        return row.selected === true;
      });
    }

    if (_.isEmpty(selectedDatas)) {
      const alert = this.alertCtrl.create({
        title: "Warning!",
        subTitle: "Please select atleast 1 media to remove!",
        buttons: ["OK"]
      });
      alert.present();
      return;
    }

    const confirm = this.alertCtrl.create({
      title: "Remove Media File?",
      message: "Are you sure to remove the selected media file?",
      buttons: [
        {
          text: "No",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Yes",
          handler: () => {
            let loading = this.loadingCtrl.create({
              dismissOnPageChange: true
            });
            loading.present();
            async.eachSeries(
              selectedDatas,
              (item: any, callback) => {
                this.collections
                  .collection_remove_item({
                    id: this.collection.id,
                    media: this.selectedSegment,
                    mediaId: item.id
                  })
                  .then(
                    (data: any) => {
                      if (data && data.success) {
                        console.log("data: ", data);
                        const toast = this.toastCtrl.create({
                          message: data.data,
                          duration: 1000,
                          showCloseButton: true,
                          dismissOnPageChange: true
                        });
                        toast.present();
                      }
                      callback();
                    },
                    error => {
                      console.log("error: ", error);
                      if (error && !error.success) {
                        const toast = this.toastCtrl.create({
                          message: error.message
                        });
                        toast.present();
                      }
                      callback();
                    }
                  );
              },
              () => {
                loading.dismiss();
                this.initData();
              }
            );
          }
        }
      ]
    });
    confirm.present();
  }
}
