import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { IonicImageLoader } from "ionic-image-loader";
import { CacheModule } from "ionic-cache-observable";

import { PipesModule } from "../../pipes/pipes.module";
import { ComponentsModule } from "../../components/components.module";

import { CollectionsPage } from "./collections";

@NgModule({
  declarations: [CollectionsPage],
  imports: [
    PipesModule,
    ComponentsModule,
    IonicImageLoader,
    CacheModule,
    IonicPageModule.forChild(CollectionsPage)
  ]
})
export class CollectionsPageModule {}
