import { Component, ViewEncapsulation, NgZone } from "@angular/core";
import {
  Platform,
  IonicPage,
  NavController,
  NavParams,
  Events,
  ToastController,
  LoadingController,
  App,
  AlertController
} from "ionic-angular";
import {
  AuthService,
  FacebookLoginProvider,
  GoogleLoginProvider
} from "angularx-social-login";
import * as _ from "lodash";
import { JwtHelper } from "angular2-jwt";

import { HomePage } from "../../pages/home/home";
import { ForgotPasswordPage } from "../../pages/forgot_password/forgot_password";
import { RegisterPage } from "../../pages/register/register";
import { ImageDetailPage } from "../image-detail/image-detail";
import { PrintDetailPage } from "../print-detail/print-detail";
import { LogoDetailPage } from "../logo-detail/logo-detail";
import { VideoDetailPage } from "../video-detail/video-detail";
import { ProductDetailPage } from "../product-detail/product-detail";

import { LoginServicesProvider } from "../../providers/services/login";
import { UserServicesProvider } from "../../providers/services/users";

import { StorageProvider } from "../../providers/storage/storage";

import { GooglePlus } from "@ionic-native/google-plus";
import { Facebook, FacebookLoginResponse } from "@ionic-native/facebook";
import { OneSignal } from "@ionic-native/onesignal";
import { Keyboard } from "@ionic-native/keyboard";

declare const $: any;

@IonicPage()
@Component({
  selector: "page-login",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "login.html"
})
export class LoginPage {
  users: any = {};
  notif_notification: any = {};

  profilePicture: any = "./assets/img/logo.png";
  isRound: boolean = false;
  showFooter: boolean = true;

  jwtHelper: JwtHelper = new JwtHelper();

  constructor(
    public app: App,
    public zone: NgZone,
    public platform: Platform,
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    public navParams: NavParams,
    public events: Events,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    public services: LoginServicesProvider,
    public userServices: UserServicesProvider,
    public storageService: StorageProvider,
    public googlePlus: GooglePlus,
    public facebook: Facebook,
    private socialAuthService: AuthService,
    private oneSignal: OneSignal,
    private keyboard: Keyboard
  ) {
    // this.users.username = 'jimkenwells@hotmail.com';
    // this.users.password = 'Voltron123';

    // this.users.username = 'wellzie.james@gmail.com';
    // this.users.password = '12345678';

    if (this.platform.is("cordova")) {
      this.oneSignal.getIds().then((data: any) => {
        if (data) {
          console.log("playerIds: ", data);
          this.userServices.user_edit_settings({
            mobilePushId: data.userId
          });
        }
      });

      this.keyboard.onKeyboardShow().subscribe(() => {
        if (this.platform.is("android")) {
          this.showFooter = false;
        }
      });

      this.keyboard.onKeyboardHide().subscribe(() => {
        if (this.platform.is("android")) {
          this.showFooter = true;
        }
      });
    }
  }

  private async isExpired() {
    const token = await this.storageService.getStorage(
      this.storageService.storageKey.USER_TOKENS
    );
    let isExpired = false;
    if (!_.isEmpty(token)) {
      isExpired = this.jwtHelper.isTokenExpired(token);
    } else {
      isExpired = true;
    }
    return isExpired;
  }

  async ionViewDidLoad() {
    console.log("ionViewDidLoad");
    $(".load-bar").remove();
    const notif_notification = await this.storageService.getStorage(
      this.storageService.storageKey.BUSINESS_NOTIFICATION
    );
    this.notif_notification = !_.isEmpty(notif_notification)
      ? JSON.parse(notif_notification || {})
      : {};
  }

  async ionViewWillEnter() {
    this.storageService.removeStorage(
      this.storageService.storageKey.BUSINESS_NOTIFICATION
    );
    this.storageService.removeStorage(
      this.storageService.storageKey.BUSINESS_TOKENS
    );
    this.storageService.removeStorage(
      this.storageService.storageKey.CURRENT_BUSINESS
    );

    const isExpired = await this.isExpired();
    if (!isExpired) {
      this.app.getRootNav().setRoot(HomePage);
    }
  }

  private navigateWithNotification(loading?: any) {
    const notification: any = this.notif_notification.notification;
    console.log("notification: ", notification);
    const notifData: any = notification.payload.additionalData;
    console.log("notifData: ", notifData);
    if (notifData) {
      this.userServices
        .businesses()
        .then((data: any) => {
          if (data && data.success) {
            const businessArr = data.accounts;
            console.log("businessArr: ", businessArr);
            const result: any = _.find(businessArr, (row: any) => {
              return row.id == parseInt(notifData.businessId);
            });
            console.log("result: ", result);

            if (result) {
              this.storageService.removeStorage(
                this.storageService.storageKey.BUSINESS_NOTIFICATION
              );
              this.storageService.setStorage(
                this.storageService.storageKey.BUSINESS_TOKENS,
                result.token
              );
              this.storageService.setStorage(
                this.storageService.storageKey.CURRENT_BUSINESS,
                JSON.stringify(result)
              );

              if (notifData.mediaType == "image") {
                this.zone.run(() => {
                  this.app.getRootNav().push(ImageDetailPage, {
                    action: "notif",
                    business: {
                      businessType: notifData.businessType,
                      businessId: notifData.businessId
                    },
                    image: {
                      id: notifData.mediaId
                    }
                  });
                });
              } else if (notifData.mediaType == "print") {
                this.zone.run(() => {
                  this.app.getRootNav().push(PrintDetailPage, {
                    action: "notif",
                    business: {
                      businessType: notifData.businessType,
                      businessId: notifData.businessId
                    },
                    print: {
                      id: notifData.mediaId
                    }
                  });
                });
              } else if (notifData.mediaType == "logo") {
                this.zone.run(() => {
                  this.app.getRootNav().push(LogoDetailPage, {
                    action: "notif",
                    business: {
                      businessType: notifData.businessType,
                      businessId: notifData.businessId
                    },
                    logo: {
                      id: notifData.mediaId
                    }
                  });
                });
              } else if (notifData.mediaType == "video") {
                this.zone.run(() => {
                  this.app.getRootNav().push(VideoDetailPage, {
                    action: "notif",
                    business: {
                      businessType: notifData.businessType,
                      businessId: notifData.businessId
                    },
                    video: {
                      id: notifData.mediaId
                    }
                  });
                });
              } else if (notifData.mediaType == "product") {
                this.zone.run(() => {
                  this.app.getRootNav().push(ProductDetailPage, {
                    action: "notif",
                    business: {
                      businessType: notifData.businessType,
                      businessId: notifData.businessId
                    },
                    product: {
                      productId: notifData.mediaId
                    }
                  });
                });
              }
            } else {
              this.alertCtrl
                .create({
                  title: "WARNING",
                  message: "No business found on this account!",
                  buttons: ["OK"]
                })
                .present();
              return;
            }
          }
        })
        .then(() => {
          if (loading) {
            loading.dismiss();
          }
        });
    } else {
      if (loading) {
        loading.dismiss();
      }
    }
  }

  loginApp() {
    const loading = this.loadingCtrl.create({
      content: "Signing in..."
    });
    loading.present();
    this.services.authenticate(this.users).then(
      (data: any) => {
        if (data && data.success) {
          this.storageService.setStorage(
            this.storageService.storageKey.CURRENT_USER,
            JSON.stringify(data.userData)
          );
          this.storageService.setStorage(
            this.storageService.storageKey.USER_TOKENS,
            data.token
          );

          this.events.publish("applogin");

          if (_.isEmpty(this.notif_notification)) {
            loading.dismiss();
            this.zone.run(() => {
              this.navCtrl.setRoot(HomePage, null, {
                animate: true,
                direction: "forward"
              });
            });
          } else {
            this.navigateWithNotification(loading);
          }
        } else {
          const toast = this.toastCtrl.create({
            message: data.message,
            duration: 2000,
            position: "top"
          });
          toast.present();
        }
      },
      error => {
        loading.dismiss();
        const toast = this.toastCtrl.create({
          message: error.message,
          duration: 2000,
          position: "top"
        });
        toast.present();
      }
    );
  }

  forgotPassword() {
    this.navCtrl.push(ForgotPasswordPage);
  }

  registerUser() {
    this.navCtrl.push(RegisterPage);
  }

  authenticateSocial(loading, userData) {
    this.services.authenticate_social(userData).then((data: any) => {
      if (data && data.success) {
        this.storageService.setStorage(
          this.storageService.storageKey.CURRENT_USER,
          JSON.stringify(data.userData)
        );
        this.storageService.setStorage(
          this.storageService.storageKey.USER_TOKENS,
          data.token
        );

        this.events.publish("applogin");

        const toast = this.toastCtrl.create({
          message: "Login successfully..",
          duration: 1000
        });
        toast.present();

        if (_.isEmpty(this.notif_notification)) {
          loading.dismiss();
          this.zone.run(() => {
            this.navCtrl.setRoot(HomePage, null, {
              animate: true,
              direction: "forward"
            });
          });
        } else {
          this.navigateWithNotification(loading);
        }
      }
    });
  }

  loginWithFacebook() {
    const loading = this.loadingCtrl.create({
      content: "Please wait..."
    });

    this.platform.ready().then(() => {
      if (this.platform.is("cordova")) {
        loading.present();
        this.facebook.login(["public_profile", "user_friends", "email"]).then(
          (res: FacebookLoginResponse) => {
            console.log("FacebookLoginResponse: ", res);
            this.facebook
              .api(
                "me?fields=id,name,email,first_name,last_name,token,picture.width(720).height(720).as(picture_large)",
                []
              )
              .then((profile: any) => {
                console.log("profile: ", profile);
                const userData: any = {
                  platform: "facebook",
                  email: profile["email"],
                  id: profile["id"],
                  firstName: profile["first_name"],
                  lastName: profile["last_name"],
                  picture: profile["picture_large"]["data"]["url"],
                  name: profile["name"],
                  token: res["accessToken"]
                };
                this.authenticateSocial(loading, userData);
              });
          },
          e => {
            loading.dismiss();
            console.log("Error logging into Facebook", e);
          }
        );
      } else {
        loading.present();
        const socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
        this.socialAuthService.signIn(socialPlatformProvider).then(
          (profile: any) => {
            const userData: any = {
              platform: "facebook",
              email: profile["email"],
              id: profile["id"],
              firstName: profile["firstName"],
              lastName: profile["lastName"],
              picture: profile["photoUrl"],
              name: profile["name"],
              token: profile["authToken"]
            };
            this.authenticateSocial(loading, userData);
          },
          error => {
            console.log("error: ", error);
            loading.dismiss();
          }
        );
      }
    });
  }

  loginWithGoogle() {
    const loading = this.loadingCtrl.create({
      content: "Please wait..."
    });
    this.platform.ready().then(() => {
      if (this.platform.is("cordova")) {
        loading.present();
        this.googlePlus.login({}).then(
          (profile: any) => {
            console.log("user: ", profile);
            const userData: any = {
              platform: "google",
              email: profile["email"],
              id: profile["userId"],
              firstName: profile["givenName"],
              lastName: profile["familyName"],
              picture: profile["imageUrl"],
              name: profile["displayName"]
            };
            console.log("userData: ", userData);
            this.authenticateSocial(loading, userData);
          },
          error => {
            console.log("error: ", error);
            loading.dismiss();
          }
        );
      } else {
        loading.present();
        const socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
        this.socialAuthService.signIn(socialPlatformProvider).then(
          (profile: any) => {
            console.log("profile: ", profile);
            let userData: any = {
              platform: "google",
              email: profile["email"],
              id: profile["id"],
              firstName: profile["firstName"] || profile["name"],
              lastName: profile["lastName"] || profile["name"],
              picture: profile["photoUrl"],
              name: profile["name"],
              token: profile["authToken"]
            };
            this.authenticateSocial(loading, userData);
          },
          error => {
            console.log("loginWithGoogle error: ", error);
          }
        );
      }
    });
  }
}
