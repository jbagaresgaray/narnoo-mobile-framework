import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

import { ChatDetailPage } from '../../pages/chat-detail/chat-detail';

@IonicPage()
@Component({
	selector: 'page-chat',
	templateUrl: 'chat.html',
})
export class ChatPage {
	pet: any;
	contactsArr: any[] = [];

	constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
		this.pet = 'all';
		this.initializeData();
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad ChatPage');
	}

	initializeData() {
		this.contactsArr = [
			{
				"Id": "210000198410281948",
				"name": "Cassio Zen",
				"email": "cassiozen@gmail.com"
			},
			{
				"Id": "210000198410281948",
				"name": "Dan Abramov",
				"email": "gaearon@somewhere.com"
			},
			{
				"Id": "210000198410281948",
				"name": "Pete Hunt",
				"email": "floydophone@somewhere.com"
			},
			{
				"Id": "210000198410281948",
				"name": "Paul O’Shannessy",
				"email": "zpao@somewhere.com"
			},
			{
				"Id": "210000198410281948",
				"name": "Ryan Florence",
				"email": "rpflorence@somewhere.com"
			},
			{
				"Id": "210000198410281948",
				"name": "Sebastian Markbage",
				"email": "sebmarkbage@here.com"
			}
		];
	}

	close() {
		this.viewCtrl.dismiss();
	}

	getItems(ev: any) {
		this.initializeData();

		let val = ev.target.value;
		if (val && val.trim() != '') {
			this.contactsArr = this.contactsArr.filter((item: any) => {
				return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
			})
		}
	}

	doRefresh(ev) {
		setTimeout(() => {
			ev.complete();
		}, 2000);
	}

	gotoDetail(user) {
		this.navCtrl.push(ChatDetailPage, {
			user: user
		});
	}
}
