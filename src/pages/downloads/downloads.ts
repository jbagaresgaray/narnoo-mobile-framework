import { Component, ViewEncapsulation, NgZone } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  Platform,
  AlertController,
  normalizeURL
} from "ionic-angular";
import * as _ from "lodash";

import { Storage } from "@ionic/storage";
import { File, FileEntry, Entry } from "@ionic-native/file";
import { PhotoViewer } from "@ionic-native/photo-viewer";
import { FileOpener } from "@ionic-native/file-opener";

import { environment } from "../../environments/environment";

@IonicPage()
@Component({
  selector: "page-downloads",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "downloads.html"
})
export class DownloadsPage {
  downloadsArr: any[] = [];
  fakeArr: any[] = [];
  selectedData: any[] = [];

  showContent: boolean = false;
  showContentErr: boolean = false;
  showCheckbox: boolean = false;

  contentErr: any = {};
  storageDirectory: string = "";

  isList: boolean = true;

  constructor(
    public zone: NgZone,
    public platform: Platform,
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    private fileCtrl: File,
    private fileOpener: FileOpener,
    private photoViewer: PhotoViewer,
    public storage: Storage
  ) {
    for (var i = 0; i < 21; ++i) {
      this.fakeArr.push(i);
    }

    if (platform.is("cordova")) {
      if (this.platform.is("ios")) {
        this.storageDirectory = window["cordova"].file.documentsDirectory;
      } else if (this.platform.is("android")) {
        this.storageDirectory = window["cordova"].file.externalRootDirectory;
      }
    }
    console.log("this.storageDirectory: ", this.storageDirectory);
  }

  getFile(path: string): Promise<FileEntry> {
    return this.fileCtrl.resolveDirectoryUrl(path).then(dir => {
      return this.fileCtrl.getFile(dir, path, { create: false });
    });
  }

  private listDir(path: string): Promise<Entry[]> {
    return this.fileCtrl.listDir(this.storageDirectory, path);
  }

  private deleteFile(fileName: string): Promise<any> {
    return this.fileCtrl.removeFile(
      this.storageDirectory + environment.AlbumName,
      fileName
    );
  }

  private loadFiles(ev?: any) {
    this.downloadsArr = [];
    this.showContent = false;
    this.listDir(environment.AlbumName).then(
      (list: any) => {
        console.log("list: ", list);
        this.zone.run(() => {
          _.each(list, (row: any) => {
            if (this.platform.is("ios")) {
              row.nativeURL = normalizeURL(row.nativeURL);
              console.log("Fixed: " + row.nativeURL);
            }
            list.selected = false;
          });
          this.downloadsArr = list;
        });
        this.showContent = true;

        if (ev) {
          ev.complete();
        }
      },
      (error: any) => {
        this.showContent = true;
        if (ev) {
          ev.complete();
        }
      }
    );
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad NewsfeedPage");
    this.storage.get("isList").then(val => {
      console.log("Your age is", val);
      this.isList = val;
    });

    if (this.platform.is("cordova")) {
      this.loadFiles();
    } else {
      setTimeout(() => {
        this.showContent = true;
        this.showContentErr = true;
        this.contentErr = {
          message: "Must be run on a device."
        };
      }, 2000);
    }
  }

  open(item: any) {
    console.log("open: ", item);

    if (!item.isFile) {
      this.alertCtrl
        .create({
          title: "WARNING",
          message: "Invalid file format!",
          buttons: ["OK"]
        })
        .present();
      return;
    }

    if (this.showCheckbox) {
      item.selected = !item.selected;
      this.selectedData = _.filter(this.downloadsArr, (row: any) => {
        return row.selected;
      });
      return;
    }

    if (item.name.indexOf(".pdf") > 0) {
      this.fileOpener
        .open(item.nativeURL, "application/pdf")
        .then(() => console.log("File is opened"))
        .catch(e => console.log("Error opening file", e));
    } else if (item.name.indexOf(".png") > 0) {
      this.photoViewer.show(item.nativeURL);
    } else if (item.name.indexOf(".jpg") > 0) {
      this.photoViewer.show(item.nativeURL);
    } else if (item.name.indexOf(".gif") > 0) {
      this.photoViewer.show(item.nativeURL);
    } else if (item.name.indexOf(".jpeg") > 0) {
      this.photoViewer.show(item.nativeURL);
    } else if (item.name.indexOf(".bmp") > 0) {
      this.photoViewer.show(item.nativeURL);
    }
  }

  deleteSelectedFile() {
    console.log("this.selectedData: ", this.selectedData);

    if (!this.platform.is("cordova")) {
      this.alertCtrl
        .create({
          title: "Warning",
          message: "Deleting file is allowed on actual device only!"
        })
        .present();
      return;
    }

    if (_.isEmpty(this.selectedData)) {
      this.alertCtrl
        .create({
          title: "Warning",
          message: "Please select file to delete!"
        })
        .present();
      return;
    }

    let alert = this.alertCtrl.create({
      title: "Delete file?",
      message: "Delete this selected file?",
      buttons: [
        {
          text: "Cancel",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Confirm",
          handler: () => {
            console.log("Agree clicked");
            Promise.all(
              this.selectedData.map((file: any) => {
                return this.deleteFile(file.name);
              })
            ).then(
              (resp: any) => {
                console.log("deleteFile Resp: ", resp);
                this.loadFiles();
                this.showCheckbox = false;
              },
              (error: any) => {
                console.log("deleteFile Error: ", error);
              }
            );
          }
        }
      ]
    });
    alert.present();
  }

  doRefresh(ev) {
    this.loadFiles(ev);
  }

  display() {
    this.isList = !this.isList;
    this.storage.set("isList", this.isList);
  }

  selectClick() {
    this.showCheckbox = true;
  }

  cancelClick() {
    this.showCheckbox = false;
    _.each(this.downloadsArr, (row: any) => {
      row.selected = this.showCheckbox;
    });
  }
}
