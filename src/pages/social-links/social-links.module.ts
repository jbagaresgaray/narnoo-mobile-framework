import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SocialLinksPage } from './social-links';

import { ComponentsModule } from '../../components/components.module';

@NgModule({
	declarations: [
		SocialLinksPage,
	],
	imports: [
		ComponentsModule,
		IonicPageModule.forChild(SocialLinksPage),
	],
})
export class SocialLinksPageModule { }
