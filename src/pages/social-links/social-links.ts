import { Component, ViewEncapsulation, NgZone } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  AlertController,
  LoadingController,
  ToastController
} from "ionic-angular";
import * as _ from "lodash";
import { CacheService, Cache } from "ionic-cache-observable";
import { Observable, Subscription } from "rxjs";
import "rxjs/add/operator/finally";

import { BusinessServicesProvider } from "../../providers/services/business";
import { StorageProvider } from "../../providers/storage/storage";

@IonicPage()
@Component({
  selector: "page-social-links",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "social-links.html"
})
export class SocialLinksPage {
  link: any = {};
  fakeArr: any[] = [];

  linkArr$: Observable<any>;
  private linkCache: Cache<any>;
  public refreshSubscription: Subscription;

  showLoading: boolean = true;
  showEdit: boolean = false;
  showContentErr: boolean = false;
  contentErr: any = {};
  business: any = {};

  constructor(
    public zone: NgZone,
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public businessServices: BusinessServicesProvider,
    public storage: StorageProvider,
    public cacheService: CacheService
  ) {
    for (var i = 0; i < 12; ++i) {
      this.fakeArr.push(i);
    }
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad SocialLinksPage");
    this.initBusiness().then(() => {
      this.initData();
    });
  }

  private async initBusiness() {
    const business = await this.storage.getStorage(
      this.storage.storageKey.CURRENT_BUSINESS
    );
    this.business = !_.isEmpty(business) ? JSON.parse(business) || {} : {};
  }

  private initData() {
    this.showLoading = true;
    const dataObservable = Observable.fromPromise(
      this.businessServices.business_social_links()
    );
    const cacheKey =
      this.storage.cacheKey.SOCIAL_LINKS + "_" + this.business.id;
    this.cacheService.register(cacheKey, dataObservable).subscribe(cache => {
      this.linkCache = cache;
      this.zone.run(() => {
        this.linkArr$ = cache.get$;
      });
    });

    this.linkArr$.subscribe(
      (data: any) => {
        if (data && data.success) {
          this.zone.run(() => {
            this.link = data.data;
          });
        }
        this.showLoading = false;
      },
      error => {
        this.showLoading = false;
        if (error) {
          this.showContentErr = true;
          this.contentErr = error;
          console.log("this.contentErr: ", this.contentErr);
        }
      }
    );
  }

  editSocialLinks() {
    if (this.business.role !== 1) {
      this.toastCtrl
        .create({
          message: "Invalid Access!!!",
          duration: 3000
        })
        .present();
      return;
    }

    this.showEdit = !this.showEdit;
    if (this.showEdit) {
      this.showContentErr = false;
    }
  }

  updateSocialLinks() {
    if (this.business.role !== 1) {
      this.toastCtrl
        .create({
          message: "Invalid Access!!!",
          duration: 3000
        })
        .present();
      return;
    }

    const updateSocial = () => {
      const loading = this.loadingCtrl.create();
      loading.present();
      this.link.type = "links";
      this.businessServices.business_social_links_edit(this.link).then(
        (data: any) => {
          if (data && data.success) {
            loading.dismiss();
            this.initData();
          } else if (data && !data.success) {
            loading.dismiss();
            this.alertCtrl
              .create({
                title: "WARNING",
                subTitle: data.message,
                buttons: ["OK"]
              })
              .present();
          }
        },
        error => {
          loading.dismiss();
          if (error && !error.success) {
            this.alertCtrl
              .create({
                title: "WARNING",
                subTitle: error.message,
                buttons: ["OK"]
              })
              .present();
          }
        }
      );
    };

    const confirm = this.alertCtrl.create({
      message: "Update Social links?",
      buttons: [
        {
          text: "No",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Yes",
          handler: () => {
            updateSocial();
          }
        }
      ]
    });
    confirm.present();
  }
}
