import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnInit,
  ViewEncapsulation
} from "@angular/core";
import { NavParams } from "ionic-angular";

@Component({
  selector: "albums-component",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "albums-component.html"
})
export class AlbumsTabComponent implements OnInit {
  @Input() showCheckbox: boolean;
  @Input() showContentAlbums: boolean;
  @Input() showContentAlbumsErr: boolean;
  @Input() albumsContentErr: any = {};
  @Input() albumsArr: any[] = [];
  @Input() imagesLoadingArr: any[] = [];

  @Output() clickViewAlbumImages = new EventEmitter<any>();
  @Output() clickCreateAlbums = new EventEmitter<any>();

  business: any = {};
  action: any;

  constructor(public navParams: NavParams) {}

  viewAlbumImages(item) {
    this.clickViewAlbumImages.emit(item);
  }

  createAlbums() {
    this.clickCreateAlbums.emit();
  }

  ngOnInit() {
    this.action = this.navParams.get("action");
    this.business = JSON.parse(localStorage.getItem("business")) || {};
  }
}
