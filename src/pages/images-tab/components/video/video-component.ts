import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnInit,
  ViewEncapsulation
} from "@angular/core";
import { NavParams } from "ionic-angular";

@Component({
  selector: "video-component",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "video-component.html"
})
export class VideoTabComponent implements OnInit {
  @Input() showContentVideos: boolean;
  @Input() showContentVideosErr: boolean;
  @Input() showCheckbox: boolean;
  @Input() videosContentErr: any = {};
  @Input() videosArr: any[] = [];
  @Input() imagesLoadingArr: any[] = [];

  business: any = {};

  @Output() clickViewVideoDetail = new EventEmitter<any>();
  @Output() clickUploadFile = new EventEmitter<any>();

  action: any;
  constructor(public navParams: NavParams) {}

  viewVideoDetail(item) {
    this.clickViewVideoDetail.emit(item);
  }

  uploadFile() {
    this.clickUploadFile.emit();
  }

  ngOnInit() {
    this.action = this.navParams.get("action");
    this.business = JSON.parse(localStorage.getItem("business")) || {};
  }
}
