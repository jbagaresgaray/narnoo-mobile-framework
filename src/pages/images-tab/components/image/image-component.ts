import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnInit,
  ViewEncapsulation
} from "@angular/core";
import { NavParams } from "ionic-angular";

@Component({
  selector: "image-component",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "image-component.html"
})
export class ImageTabComponent implements OnInit {
  @Input() showContentImage: boolean;
  @Input() showContentImageErr: boolean;
  @Input() showCheckbox: boolean;
  @Input() imageContentErr: any = {};
  @Input() imagesArr: any[] = [];
  @Input() imagesLoadingArr: any[] = [];
  @Input() imagesUploadArr: any[] = [];

  business: any = {};

  @Output() clickViewImageDetail = new EventEmitter<any>();
  @Output() clickUploadFile = new EventEmitter<any>();

  action: any;
  constructor(public navParams: NavParams) {}

  viewImageDetail(item) {
    this.clickViewImageDetail.emit(item);
  }

  uploadFile() {
    this.clickUploadFile.emit();
  }

  ngOnInit() {
    this.action = this.navParams.get("action");
    this.business = JSON.parse(localStorage.getItem("business")) || {};
  }
}
