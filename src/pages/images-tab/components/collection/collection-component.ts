import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnInit,
  ViewEncapsulation
} from "@angular/core";
import { NavParams } from "ionic-angular";

@Component({
  selector: "collection-component",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "collection-component.html"
})
export class CollectionTabComponent implements OnInit {
  @Input() showContentCollections: boolean;
  @Input() showContentCollectionsErr: boolean;
  @Input() collectionsContentErr: any = {};
  @Input() collectionArr: any[] = [];
  @Input() imagesLoadingArr: any[] = [];

  business: any = {};

  @Output() clickViewCollections = new EventEmitter<any>();
  @Output() _deleteCollection = new EventEmitter<any>();
  @Output() _createCollections = new EventEmitter<any>();

  action: any;

  constructor(public navParams: NavParams) {}

  viewCollections(item) {
    this.clickViewCollections.emit(item);
  }

  createCollections() {
    this._createCollections.emit();
  }

  deleteCollection(item, ev: Event) {
    this._deleteCollection.emit({
      item: item,
      ev: ev
    });
  }

  ngOnInit() {
    this.action = this.navParams.get("action");
    this.business = JSON.parse(localStorage.getItem("business")) || {};
    console.log("collectionsContentErr: ", this.collectionsContentErr);
  }
}
