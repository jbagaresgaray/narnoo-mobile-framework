import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
	selector: 'products-component',
	templateUrl: 'products-component.html',
})
export class ProductsTabComponent {
	@Input() showContentProducts: boolean;
	@Input() showContentProductsErr: boolean;
	@Input() productsContentErr: any = {};
	@Input() productArr: any[] = [];
	@Input() fakeArr: any[] = [];

	@Output() clickViewProductDetails = new EventEmitter<any>();

	viewProductDetails(item){
		this.clickViewProductDetails.emit(item);
	}
}