import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnInit,
  ViewEncapsulation
} from "@angular/core";
import { NavParams } from "ionic-angular";

@Component({
  selector: "channels-component",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "channels-component.html"
})
export class ChannelsTabComponent implements OnInit {
  @Input() showContentChannels: boolean;
  @Input() showContentChannelsErr: boolean;
  @Input() channelsContentErr: any = {};
  @Input() channelArr: any[] = [];
  @Input() imagesLoadingArr: any[] = [];

  business: any = {};

  @Output() clickViewChannels = new EventEmitter<any>();
  @Output() clickUpdateChannel = new EventEmitter<any>();
  @Output() clickDeleteChannel = new EventEmitter<any>();
  @Output() _createChannels = new EventEmitter<any>();

  action: any;

  constructor(public navParams: NavParams) {}

  viewChannels(item) {
    this.clickViewChannels.emit(item);
  }

  updateChannnel(item, fab: any) {
    this.clickUpdateChannel.emit({
      item: item,
      fab: fab
    });
  }

  deleteChannel(item, fab: any) {
    this.clickDeleteChannel.emit({
      item: item,
      fab: fab
    });
  }

  createChannels() {
    this._createChannels.emit();
  }

  ngOnInit() {
    this.action = this.navParams.get("action");
    this.business = JSON.parse(localStorage.getItem("business")) || {};
  }
}
