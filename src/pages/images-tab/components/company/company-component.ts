import {
  Component,
  Input,
  Output,
  EventEmitter,
  ViewEncapsulation,
  OnInit
} from "@angular/core";
import {
  NavParams
} from "ionic-angular";

@Component({
  selector: "company-component",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "company-component.html"
})
export class CompanyTabComponent implements OnInit {
  @Input() showContent: boolean;
  @Input() showContentErr: boolean;

  @Input() contentErr: any;
  @Input() business: any;

  @Input() summaryArr: any[] = [];
  @Input() descriptionArr: any[] = [];
  @Input() imagesLoadingArr: any[] = [];

  action: any;

  @Output() clickSelectSummary = new EventEmitter<any>();
  @Output() clickUpdateBio = new EventEmitter<any>();
  @Output() clickSelectDescription = new EventEmitter<any>();


  constructor(public navParams: NavParams){}

  ngOnInit(){
    this.action = this.navParams.get('action');
  }

  selectDescription(item) {
    this.clickSelectDescription.emit(item);
  }

  updateBio(item, type) {
    this.clickUpdateBio.emit({
      item: item,
      type: type
    });
  }

  selectSummary(item) {
    this.clickSelectSummary.emit(item);
  }
}
