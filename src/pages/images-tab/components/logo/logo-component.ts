import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnInit,
  ViewEncapsulation
} from "@angular/core";
import { NavParams } from "ionic-angular";

@Component({
  selector: "logo-component",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "logo-component.html"
})
export class LogoTabComponent implements OnInit {
  @Input() showContentLogo: boolean;
  @Input() showContentLogoErr: boolean;
  @Input() showCheckbox: boolean;
  @Input() logoContentErr: any = {};
  @Input() logosArr: any[] = [];
  @Input() imagesLoadingArr: any[] = [];

  business: any = {};

  @Output() clickViewLogoDetail = new EventEmitter<any>();
  @Output() clickUploadFile = new EventEmitter<any>();

  action: any;
  constructor(public navParams: NavParams) {}

  viewLogoDetail(item) {
    this.clickViewLogoDetail.emit(item);
  }

  uploadFile() {
    this.clickUploadFile.emit();
  }

  ngOnInit() {
    this.action = this.navParams.get("action");
    this.business = JSON.parse(localStorage.getItem("business")) || {};
  }
}
