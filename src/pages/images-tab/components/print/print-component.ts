import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnInit,
  ViewEncapsulation
} from "@angular/core";
import { NavParams } from "ionic-angular";

@Component({
  selector: "print-component",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "print-component.html"
})
export class PrintTabComponent implements OnInit {
  @Input() showContentPrint: boolean;
  @Input() showContentPrintErr: boolean;
  @Input() showCheckbox: boolean;
  @Input() printContentErr: any = {};
  @Input() printArr: any[] = [];
  @Input() imagesLoadingArr: any[] = [];

  business: any = {};

  @Output() clickViewPrintDetail = new EventEmitter<any>();
  @Output() clickUploadFile = new EventEmitter<any>();

  action: any;
  constructor(public navParams: NavParams) {}

  viewPrintDetail(item) {
    this.clickViewPrintDetail.emit(item);
  }

  uploadFile() {
    this.clickUploadFile.emit();
  }

  ngOnInit() {
    this.action = this.navParams.get("action");
    console.log("this.action: ", this.action);
    this.business = JSON.parse(localStorage.getItem("business")) || {};
  }
}
