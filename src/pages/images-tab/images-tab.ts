import {
  Component,
  ViewChild,
  NgZone,
  ViewEncapsulation,
  OnInit
} from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  AlertController,
  ActionSheetController,
  Platform,
  ToastController,
  ModalController,
  PopoverController,
  Events,
  ViewController,
  App,
  Content,
  MenuController,
  FabContainer,
  LoadingController
} from "ionic-angular";
import * as _ from "lodash";
import * as async from "async";

import { CacheService, Cache } from "ionic-cache-observable";
import { Observable, Subscription } from "rxjs";
import "rxjs/add/operator/finally";

// import { Network } from "@ionic-native/network";

import { ImagesServicesProvider } from "../../providers/services/images";
import { AlbumsServicesProvider } from "../../providers/services/albums";
import { PrintServicesProvider } from "../../providers/services/print";
import { VideosServicesProvider } from "../../providers/services/videos";
import { LogoServicesProvider } from "../../providers/services/logos";
import { CollectionsServicesProvider } from "../../providers/services/collections";
import { ChannelsServicesProvider } from "../../providers/services/channels";
import { BusinessServicesProvider } from "../../providers/services/business";
import { NewsFeedServicesProvider } from "../../providers/services/newsfeed";

import { ImageDetailPage } from "../image-detail/image-detail";
import { VideoDetailPage } from "../video-detail/video-detail";
import { PrintDetailPage } from "../print-detail/print-detail";
import { LogoDetailPage } from "../logo-detail/logo-detail";
import { FileUploadPage } from "../file-upload/file-upload";
import { AlbumsPage } from "../albums/albums";
import { ChatPage } from "../chat/chat";
import { AlbumGalleryPage } from "../album-gallery/album-gallery";
import { BusinessProfileSettingsPage } from "../business-profile-settings/business-profile-settings";
import { CollectionsPage } from "../collections/collections";
import { CollectionsEntryPage } from "../collections-entry/collections-entry";
import { ChannelsPage } from "../channels/channels";
import { ChannelsEntryPage } from "../channels-entry/channels-entry";
import { CompanyBioEntryPage } from "../company-bio-entry/company-bio-entry";
import { AlbumsEntryPage } from "../albums-entry/albums-entry";
import { NotificationsPage } from "../notifications/notifications";
import { StorageProvider } from "../../providers/storage/storage";

@Component({
  selector: "app-image-popover",
  templateUrl: "image-popover.html"
})
export class ImageTabsPopoverPage {
  business: any = {};
  imageAction: string;
  showContentImage: boolean;
  showUpload: boolean;

  constructor(
    public app: App,
    public events: Events,
    public params: NavParams,
    public viewCtrl: ViewController,
    public toastCtrl: ToastController,
    public storageService: StorageProvider
  ) {
    this.initData();

    this.imageAction = params.get("imageAction");
    this.showContentImage = params.get("showContentImage");
    this.showUpload = params.get("showUpload");
    console.log("this.showUpload: ", this.showUpload);
  }

  async initData() {
    const business = await this.storageService.getStorage(
      this.storageService.storageKey.CURRENT_BUSINESS
    );
    this.business = !_.isEmpty(business) ? JSON.parse(business) || {} : {};
  }

  close() {
    this.viewCtrl.dismiss();
  }

  viewChat() {
    this.events.publish("ImagesTabPage:viewChat");
    this.viewCtrl.dismiss();
  }

  uploadFile() {
    this.events.publish("ImagesTabPage:uploadFile");
    this.viewCtrl.dismiss();
  }

  viewFilter() {
    this.events.publish("ImagesTabPage:viewFilter");
    this.viewCtrl.dismiss();
  }

  createChannels() {
    this.events.publish("ImagesTabPage:createChannels");
    this.viewCtrl.dismiss();
  }

  createCollections() {
    this.events.publish("ImagesTabPage:createCollections");
    this.viewCtrl.dismiss();
  }

  createCompanyBio() {
    this.events.publish("ImagesTabPage:createCompanyBio");
    this.viewCtrl.dismiss();
  }

  viewAlbums() {
    this.events.publish("ImagesTabPage:viewAlbums");
    this.viewCtrl.dismiss();
  }

  createAlbums() {
    this.events.publish("ImagesTabPage:createAlbums");
    this.viewCtrl.dismiss();
  }
}
@IonicPage()
@Component({
  selector: "page-images-tab",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "images-tab.html"
})
export class ImagesTabPage implements OnInit {
  token: string;
  business: any = {};
  action: string;

  imagesArr: any[] = [];
  imagesLoadingArr: any[] = [];

  biog: any[] = [];
  logosArr: any[] = [];
  printArr: any[] = [];
  printCopyArr: any[] = [];
  videosArr: any[] = [];
  albumsArr: any[] = [];
  collectionArr: any[] = [];
  channelArr: any[] = [];
  descriptionArr: any[] = [];
  summaryArr: any[] = [];
  myInput: string = "";

  filters: any = {};
  showPreview: boolean = false;
  showUpload: boolean = false;
  showFinishingUpload: boolean = false;

  showContentImage: boolean = false;
  showContentImageErr: boolean = false;
  imageContentErr: any = {};
  pageImage: number = 0;
  totalPageImage: number = 0;

  showContentLogo: boolean = false;
  showContentLogoErr: boolean = false;
  logoContentErr: any = {};
  pageLogo: number = 0;
  totalPageLogo: number = 0;

  showContentPrint: boolean = false;
  showContentPrintErr: boolean = false;
  printContentErr: any = {};
  pagePrint: number = 0;
  totalPagePrint: number = 0;

  showContentVideos: boolean = false;
  showContentVideosErr: boolean = false;
  videosContentErr: any = {};
  pageVideo: number = 0;
  totalPageVideo: number = 0;

  showContentAlbums: boolean = false;
  showContentAlbumsErr: boolean = false;
  albumsContentErr: any = {};
  pageAlbum: number = 0;
  totalPageAlbum: number = 0;

  showContentCollections: boolean = false;
  showContentCollectionsErr: boolean = false;
  collectionsContentErr: any = {};
  pageCollection: number = 0;
  totalPageCollection: number = 0;

  showContentChannels: boolean = false;
  showContentChannelsErr: boolean = false;
  channelsContentErr: any = {};
  pageChannel: number = 0;
  totalPageChannel: number = 0;

  showContent: boolean = false;
  showContentErr: boolean = false;
  showCheckbox: boolean = false;
  contentErr: any = {};

  imageAction: string = "media";
  printstatus: string = "all";

  totalNotification: number = 0;
  perPage: number = 20;
  imageUploadDuration: number = 15000;
  brochureUploadDuration: number = 20000;
  videoUploadDuration: number = 20000;

  @ViewChild(Content) content: Content;

  imagesArr$: Observable<any>;
  private imageCache: Cache<any>;

  logosArr$: Observable<any>;
  private logoCache: Cache<any>;

  printArr$: Observable<any>;
  private printCache: Cache<any>;

  videosArr$: Observable<any>;
  private videosCache: Cache<any>;

  albumsArr$: Observable<any>;
  private albumsCache: Cache<any>;

  collectionArr$: Observable<any>;
  private collectionCache: Cache<any>;

  channelArr$: Observable<any>;
  private channelCache: Cache<any>;

  businessBio$: Observable<any>;
  private businessBioCache: Cache<any>;

  public refreshSubscription: Subscription;

  constructor(
    public app: App,
    public events: Events,
    public platform: Platform,
    public zone: NgZone,
    public navCtrl: NavController,
    public navParams: NavParams,
    public images: ImagesServicesProvider,
    public albums: AlbumsServicesProvider,
    public prints: PrintServicesProvider,
    public videos: VideosServicesProvider,
    public logos: LogoServicesProvider,
    public alertCtrl: AlertController,
    public actionSheetCtrl: ActionSheetController,
    public toastCtrl: ToastController,
    public modalCtrl: ModalController,
    public popoverCtrl: PopoverController,
    public collections: CollectionsServicesProvider,
    public channels: ChannelsServicesProvider,
    public menuCtrl: MenuController,
    public businesses: BusinessServicesProvider,
    public loadingCtrl: LoadingController,
    public cacheService: CacheService,
    public newsfeeds: NewsFeedServicesProvider,
    public storageService: StorageProvider
  ) {
    this.action = navParams.get("action");

    for (var i = 0; i < 20; ++i) {
      this.imagesLoadingArr.push(i);
    }

    // ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //
    // ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //
    // ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //

    events.unsubscribe("ImagesTabPage:uploadFile");
    events.unsubscribe("ImagesTabPage:viewChat");
    events.unsubscribe("ImagesTabPage:viewFilter");
    events.unsubscribe("applyFilter");
    events.unsubscribe("applyFilterPrint");
    events.unsubscribe("ImagesTabPage:createChannels");
    events.unsubscribe("ImagesTabPage:createCollections");
    events.unsubscribe("ImagesTabPage:viewAlbums");
    events.unsubscribe("ImagesTabPage:createAlbums");
    events.unsubscribe("ImagesTabPage:createCompanyBio");

    events.subscribe("ImagesTabPage:uploadFile", () => {
      this.uploadFile();
    });

    events.subscribe("ImagesTabPage:viewChat", () => {
      this.viewChat();
    });

    events.subscribe("ImagesTabPage:viewFilter", () => {
      this.viewFilter();
    });

    events.subscribe("ImagesTabPage:createChannels", () => {
      this.createChannels();
    });

    events.subscribe("ImagesTabPage:createCollections", () => {
      this.createCollections();
    });

    events.subscribe("ImagesTabPage:createCompanyBio", () => {
      this.createCompanyBio();
    });

    events.subscribe("ImagesTabPage:viewAlbums", () => {
      this.viewAlbums();
    });

    events.subscribe("ImagesTabPage:createAlbums", () => {
      this.createAlbums();
    });

    events.subscribe("applyFilter", (data: any) => {
      console.log("applyFilter: ", data);
      this.filters = data;
      this.filterImage();
    });

    events.subscribe("applyFilterPrint", (data: any) => {
      console.log("applyFilterPrint: ", data);
      this.filters = data;
      this.filterPrints();
    });

    events.unsubscribe("showNotifications");
    events.subscribe("showNotifications", count => {
      this.totalNotification = count;
    });

    try {
      this.newsfeeds.notificationData$.subscribe((data: any) => {
        console.log("activity feed notificationData: ", data);
        if (data && data.success) {
          this.zone.run(() => {
            this.newsfeeds.totalNotification = data.data.total;
            this.totalNotification = data.data.total;
          });
        } else {
          this.zone.run(() => {
            this.newsfeeds.totalNotification = 0;
            this.totalNotification = 0;
          });
        }
      });
    } catch (error) {
      this.zone.run(() => {
        this.newsfeeds.totalNotification = 0;
        this.totalNotification = 0;
      });
      this.events.publish("initNotifications");
    }

    this.initBusiness();
  }

  ngOnInit(): void {
    this.initBusiness();
  }

  ionViewDidLoad() {
    this.events.publish("filterWhat", "media");

    this.pageImage = 1;
    this.pagePrint = 1;
    this.pageVideo = 1;
    this.pageLogo = 1;
    this.pageAlbum = 1;
    this.pageCollection = 1;
    this.pageChannel = 1;

    this.imagesArr = [];
    this.logosArr = [];
    this.printArr = [];
    this.videosArr = [];
    this.albumsArr = [];
    this.collectionArr = [];

    this.showContentImage = false;
    this.showContentImageErr = false;

    this.showContentLogo = false;
    this.showContentLogoErr = false;

    this.showContentPrint = false;
    this.showContentPrintErr = false;

    this.showContentVideos = false;
    this.showContentVideosErr = false;

    this.showContentAlbums = false;
    this.showContentAlbumsErr = false;

    this.showContentCollections = false;
    this.showContentCollectionsErr = false;

    this.initBusiness().then(() => {
      this.loadImage(this.pageImage, this.perPage);
      this.loadLogos(this.pageLogo, this.perPage);
      this.loadPrints(this.pagePrint, this.perPage);
      this.loadVideos(this.pageVideo, this.perPage);
      this.loadAlbums(this.pageAlbum, this.perPage);
      this.loadCollections(this.pageCollection, this.perPage);

      this.initOtherMedias();
      this.initMediaRules();
    });
  }

  private async initBusiness() {
    const business = await this.storageService.getStorage(
      this.storageService.storageKey.CURRENT_BUSINESS
    );
    this.zone.run(() => {
      this.business = !_.isEmpty(business) ? JSON.parse(business) || {} : {};

      this.initOtherMedias();
      this.initMediaRules();
      console.log("business: ", this.business);
    });
  }

  private initOtherMedias() {
    if (this.business.type === 2) {
      this.channelArr = [];

      this.showContentChannels = false;
      this.showContentChannelsErr = false;

      this.loadChannels(this.pageChannel, this.perPage);
    }

    if (this.business.type === 1) {
      this.descriptionArr = [];
      this.summaryArr = [];

      this.showContent = false;
      this.showContentErr = false;

      this.loadCompanyBio();
    }
  }

  private initMediaRules() {
    if (this.imageAction == "print") {
      if (this.platform.is("android")) {
        if (this.business.role == 2 || this.business.role == 1) {
          this.showUpload = true;
        } else {
          this.showUpload = false;
        }
      } else {
        this.showUpload = false;
      }
    } else if (this.imageAction == "media") {
      if (this.business.role == 2 || this.business.role == 1) {
        this.showUpload = true;
      } else {
        this.showUpload = false;
      }
    } else if (this.imageAction == "logos") {
      if (this.business.role == 2 || this.business.role == 1) {
        this.showUpload = true;
      } else {
        this.showUpload = false;
      }
    } else if (this.imageAction == "videos") {
      if (this.business.role == 2 || this.business.role == 1) {
        this.showUpload = true;
      } else {
        this.showUpload = false;
      }
    } else {
      this.showUpload = false;
    }
  }

  private loadImage(page, total, refresher?: any) {
    const dataObservable: any = Observable.fromPromise(
      this.images.image_list({
        page: page,
        total: total
      })
    );

    const cacheKey =
      this.storageService.cacheKey.MEDIA_IMAGES +
      "_" +
      this.business.id +
      "_" +
      page;
    this.cacheService.register(cacheKey, dataObservable).subscribe(cache => {
      this.imageCache = cache;
      this.zone.run(() => {
        this.imagesArr$ = cache.get$;
      });
    });

    const successResponse = (data: any) => {
      if (data && data.success && data.data[0] != false) {
        const image = data.data;
        this.pageImage = parseInt(image.currentPage);
        this.totalPageImage = parseInt(image.totalPages);

        this.zone.run(() => {
          for (let i = 0; i < _.size(image.images); i++) {
            image.images[i].selected = false;

            if (_.isEmpty(image.images[i].caption)) {
              image.images[i].caption = "No Caption";
            }
            if (_.isEmpty(image.images[i].location)) {
              image.images[i].location = "No Location";
            }

            this.imagesArr.push(image.images[i]);
          }
        });
      } else if (data && data.success && data.data[0] == false) {
        this.showContentImageErr = true;
        this.imageContentErr = {
          message: "No results found"
        };
      }
      this.showContentImage = true;
      if (refresher) {
        refresher.complete();
      }
      this.content.resize();
    };

    const errorResponse = error => {
      this.showContentImage = true;
      this.showContentImageErr = true;
      if (refresher) {
        refresher.complete();
      }
      if (error && !error.success) {
        this.imageContentErr = error;
      }
    };

    this.imagesArr$.subscribe(successResponse, errorResponse, () => {
      this.content.resize();
    });
  }

  private loadLogos(page, total, refresher?: any) {
    const dataObservable: any = Observable.fromPromise(
      this.logos.logos_list({
        page: page,
        total: total
      })
    );

    const cacheKey =
      this.storageService.cacheKey.MEDIA_LOGOS +
      "_" +
      this.business.id +
      "_" +
      page;
    this.cacheService.register(cacheKey, dataObservable).subscribe(cache => {
      this.logoCache = cache;
      this.zone.run(() => {
        this.logosArr$ = cache.get$;
      });
    });

    const successResponse = (data: any) => {
      if (data && data.success && data.data[0] != false) {
        this.pageLogo = parseInt(data.data.currentPage);
        this.totalPageLogo = parseInt(data.data.totalPages);
        this.zone.run(() => {
          for (let i = 0; i < _.size(data.data.logos); i++) {
            data.data.logos[i].selected = false;
            this.logosArr.push(data.data.logos[i]);
          }
        });
      } else if (data && data.success && data.data[0] == false) {
        this.showContentLogoErr = true;
        this.logoContentErr = {
          message: "No results found"
        };
      } else if (data && data.success && data.data.error) {
        console.log("showError: ", data.data);
        this.showContentLogoErr = true;
        this.logoContentErr = {
          message: data.data.msg
        };
      }
      this.showContentLogo = true;
      if (refresher) {
        refresher.complete();
      }
      this.content.resize();
    };

    const errorResponse = error => {
      this.showContentLogo = true;
      this.showContentLogoErr = true;
      if (refresher) {
        refresher.complete();
      }
      if (error && !error.success) {
        this.logoContentErr = error;
      }
    };

    this.logosArr$.subscribe(successResponse, errorResponse, () => {
      this.content.resize();
    });
  }

  private loadPrints(page, total, refresher?: any) {
    const dataObservable: any = Observable.fromPromise(
      this.prints.brochure_list({
        page: page,
        total: total
      })
    );

    const cacheKey =
      this.storageService.cacheKey.MEDIA_PRINTS +
      "_" +
      this.business.id +
      "_" +
      page;
    this.cacheService.register(cacheKey, dataObservable).subscribe(cache => {
      this.printCache = cache;
      this.zone.run(() => {
        this.printArr$ = cache.get$;
      });
    });

    const successResponse = (data: any) => {
      if (data && data.success && data.data[0] != false) {
        this.pagePrint = parseInt(data.data.currentPage);
        this.totalPagePrint = parseInt(data.data.totalPages);
        this.zone.run(() => {
          for (let i = 0; i < _.size(data.data.prints); i++) {
            data.data.prints[i].selected = false;
            if (_.isEmpty(data.data.prints[i].brochure_caption)) {
              data.data.prints[i].brochure_caption = "No caption";
            }
            this.printArr.push(data.data.prints[i]);
          }
        });
        this.printCopyArr = _.cloneDeep(this.printArr);
        this.printSegmentChanged();
      } else if (data && data.success && data.data[0] == false) {
        this.showContentPrintErr = true;
        this.printContentErr = {
          message: "No results found"
        };
      }
      this.showContentPrint = true;
      if (refresher) {
        refresher.complete();
      }
      this.content.resize();
    };

    const errorResponse = error => {
      this.showContentPrint = true;
      this.showContentPrintErr = true;
      if (refresher) {
        refresher.complete();
      }
      if (error && !error.success) {
        this.printContentErr = error;
      }
    };

    this.printArr$.subscribe(successResponse, errorResponse, () => {
      this.content.resize();
    });
  }

  private loadVideos(page, total, refresher?: any) {
    const dataObservable: any = Observable.fromPromise(
      this.videos.video_list({
        page: page,
        total: total
      })
    );

    const cacheKey =
      this.storageService.cacheKey.MEDIA_VIDEOS +
      "_" +
      this.business.id +
      "_" +
      page;
    this.cacheService.register(cacheKey, dataObservable).subscribe(cache => {
      this.videosCache = cache;
      this.zone.run(() => {
        this.videosArr$ = cache.get$;
      });
    });

    const successResponse = (data: any) => {
      if (data && data.success && data.data[0] != false) {
        this.pageVideo = parseInt(data.data.currentPage);
        this.totalPageVideo = parseInt(data.data.totalPages);

        this.zone.run(() => {
          for (let i = 0; i < _.size(data.data.videos); i++) {
            data.data.videos[i].selected = false;
            if (_.isEmpty(data.data.videos[i].caption)) {
              data.data.videos[i].caption = "No caption";
            }
            this.videosArr.push(data.data.videos[i]);
          }
        });
      } else if (data && data.success && data.data[0] == false) {
        this.showContentVideosErr = true;
        this.videosContentErr = {
          message: "No results found"
        };
      }
      this.showContentVideos = true;
      if (refresher) {
        refresher.complete();
      }
      this.content.resize();
    };

    const errorResponse = error => {
      this.showContentVideos = true;
      this.showContentVideosErr = true;
      if (refresher) {
        refresher.complete();
      }
      if (error && !error.success) {
        this.videosContentErr = error;
      }
    };

    this.videosArr$.subscribe(successResponse, errorResponse, () => {
      this.content.resize();
    });
  }

  private loadAlbums(page, total, refresher?: any) {
    const dataObservable: any = Observable.fromPromise(
      this.albums.album_list({
        page: page,
        total: total
      })
    );

    const cacheKey =
      this.storageService.cacheKey.MEDIA_ALBUMS +
      "_" +
      this.business.id +
      "_" +
      page;
    this.cacheService.register(cacheKey, dataObservable).subscribe(cache => {
      this.albumsCache = cache;
      this.zone.run(() => {
        this.albumsArr$ = cache.get$;
      });
    });

    const successResponse = (data: any) => {
      if (data && data.success && data.data[0] != false) {
        this.pageAlbum = parseInt(data.data.currentPage);
        this.totalPageAlbum = parseInt(data.data.totalPages);

        this.zone.run(() => {
          for (let i = 0; i < _.size(data.data.albums); i++) {
            data.data.albums[i].selected = false;
            if (_.isEmpty(data.data.albums[i].title)) {
              data.data.albums[i].title = "No caption";
            }
            this.albumsArr.push(data.data.albums[i]);
          }
        });
      } else if (data && data.success && data.data[0] == false) {
        this.showContentAlbumsErr = true;
        this.albumsContentErr = {
          message: "No results found"
        };
      }
      this.showContentAlbums = true;
      if (refresher) {
        refresher.complete();
      }
      this.content.resize();
    };

    const errorResponse = error => {
      this.showContentAlbums = true;
      this.showContentAlbumsErr = true;
      if (refresher) {
        refresher.complete();
      }
      if (error && !error.success) {
        this.albumsContentErr = error;
      }
    };

    this.albumsArr$.subscribe(successResponse, errorResponse, () => {
      this.content.resize();
    });
  }

  private loadCollections(page, total, refresher?: any) {
    const dataObservable: any = Observable.fromPromise(
      this.collections.collecton_list({
        page: page,
        total: total
      })
    );

    const cacheKey =
      this.storageService.cacheKey.MEDIA_COLLECTIONS +
      "_" +
      this.business.id +
      "_" +
      page;
    this.cacheService.register(cacheKey, dataObservable).subscribe(cache => {
      this.collectionCache = cache;
      this.zone.run(() => {
        this.collectionArr$ = cache.get$;
      });
    });

    const successResponse = (data: any) => {
      if (data && data.success && data.data[0] != false) {
        this.pageCollection = parseInt(data.data.page);
        this.totalPageCollection = parseInt(data.data.totalPages);

        this.zone.run(() => {
          for (let i = 0; i < _.size(data.data.data); i++) {
            data.data.data[i].selected = false;
            this.collectionArr.push(data.data.data[i]);
          }
        });
      } else if (data && data.success && data.data[0] == false) {
        this.showContentCollectionsErr = true;
        this.collectionsContentErr = {
          message: "No results found"
        };
      }
      this.showContentCollections = true;
      if (refresher) {
        refresher.complete();
      }
      this.content.resize();
    };

    const errorResponse = error => {
      this.showContentCollections = true;
      this.showContentCollectionsErr = true;
      if (refresher) {
        refresher.complete();
      }
      if (error && !error.success) {
        this.collectionsContentErr = error;
      }
    };

    this.collectionArr$.subscribe(successResponse, errorResponse, () => {
      this.content.resize();
    });
  }

  private loadChannels(page, total, refresher?: any) {
    console.log("loadChannels");
    const dataObservable: any = Observable.fromPromise(
      this.channels.channel_list({
        page: page,
        total: total
      })
    );

    const cacheKey =
      this.storageService.cacheKey.MEDIA_CHANNELS +
      "_" +
      this.business.id +
      "_" +
      page;
    this.cacheService.register(cacheKey, dataObservable).subscribe(cache => {
      this.channelCache = cache;
      this.zone.run(() => {
        this.channelArr$ = cache.get$;
      });
    });

    const successResponse = (data: any) => {
      if (data && data.success && data.data[0] != false) {
        this.pageChannel = parseInt(data.data.page);
        this.totalPageChannel = parseInt(data.data.totalPages);

        this.zone.run(() => {
          for (let i = 0; i < _.size(data.data.data); i++) {
            data.data.data[i].selected = false;
            this.channelArr.push(data.data.data[i]);
          }
        });
      } else if (data && data.success && data.data[0] == false) {
        this.showContentChannelsErr = true;
        this.channelsContentErr = {
          message: "No results found"
        };
      }
      this.showContentChannels = true;
      if (refresher) {
        refresher.complete();
      }
      this.content.resize();
    };

    const errorResponse = error => {
      this.showContentChannels = true;
      this.showContentChannelsErr = true;
      if (refresher) {
        refresher.complete();
      }
      if (error && !error.success) {
        this.channelsContentErr = error;
      }
    };

    this.channelArr$.subscribe(successResponse, errorResponse, () => {
      this.content.resize();
    });
  }

  private loadCompanyBio() {
    console.log("loadCompanyBio");
    const dataObservable: any = Observable.fromPromise(
      this.businesses.business_biography()
    );

    const cacheKey =
      this.storageService.cacheKey.BUSINESS_BIOGRAPHY + "_" + this.business.id;
    this.cacheService.register(cacheKey, dataObservable).subscribe(cache => {
      this.businessBioCache = cache;
      this.zone.run(() => {
        this.businessBio$ = cache.get$;
      });
    });

    const successResponse = (data: any) => {
      if (data && data.success) {
        const biog = data.data;

        const result = _.filter(biog, { size: "description" });
        _.each(result, (row: any) => {
          row.selected = false;
          if (row.language !== "english" && this.business.paid) {
            row.paid = true;
          } else if (row.language == "english" && this.business.paid) {
            row.paid = true;
          } else if (row.language == "english" && !this.business.paid) {
            row.paid = true;
          } else {
            row.paid = false;
          }
        });

        const result2 = _.filter(biog, { size: "summary" });
        _.each(result2, (row: any) => {
          row.selected = false;
          if (row.language !== "english" && this.business.paid) {
            row.paid = true;
          } else if (row.language == "english" && this.business.paid) {
            row.paid = true;
          } else if (row.language == "english" && !this.business.paid) {
            row.paid = true;
          } else {
            row.paid = false;
          }
        });

        this.zone.run(() => {
          this.descriptionArr = result;
          this.summaryArr = result2;
        });
      }
      this.showContent = true;
    };

    const errorResponse = error => {
      this.showContent = true;
      console.log("error: ", error);
      if (error) {
        this.showContentErr = true;
        this.contentErr = error;
      }
    };

    this.businessBio$.subscribe(successResponse, errorResponse);
  }

  doRefresh(refresher: any) {
    if (this.imageAction === "media") {
      this.pageImage = 1;
      this.showContentImage = true;
      this.showContentImageErr = false;
      this.imagesArr = [];
      this.zone.run(() => {
        this.loadImage(this.pageImage, this.perPage, refresher);
      });
    } else if (this.imageAction === "logos") {
      this.pageLogo = 1;
      this.showContentLogo = true;
      this.showContentLogoErr = false;
      this.logosArr = [];
      this.zone.run(() => {
        this.loadLogos(this.pageLogo, this.perPage, refresher);
      });
    } else if (this.imageAction === "print") {
      this.pagePrint = 1;
      this.showContentPrint = true;
      this.showContentPrintErr = false;
      this.printArr = [];
      this.printCopyArr = [];
      this.zone.run(() => {
        this.loadPrints(this.pagePrint, this.perPage, refresher);
      });
    } else if (this.imageAction === "videos") {
      this.pageVideo = 1;
      this.showContentVideos = true;
      this.showContentVideosErr = false;
      this.videosArr = [];
      this.zone.run(() => {
        this.loadVideos(this.pageVideo, this.perPage, refresher);
      });
    } else if (this.imageAction === "albums") {
      this.pageAlbum = 1;
      this.showContentAlbums = true;
      this.showContentAlbumsErr = false;
      this.albumsArr = [];
      this.zone.run(() => {
        this.loadAlbums(this.pageAlbum, this.perPage, refresher);
      });
    } else if (this.imageAction === "collection") {
      this.pageCollection = 1;
      this.showContentCollections = true;
      this.showContentCollectionsErr = false;
      this.collectionArr = [];
      this.zone.run(() => {
        this.loadCollections(this.pageCollection, this.perPage, refresher);
      });
    } else if (this.imageAction === "channels" && this.business.type === 2) {
      this.pageChannel = 1;
      this.showContentChannels = true;
      this.showContentChannelsErr = false;
      this.channelArr = [];
      this.zone.run(() => {
        this.loadChannels(this.pageChannel, this.perPage, refresher);
      });
    } else if (this.imageAction === "company" && this.business.type === 1) {
      this.showContent = true;
      this.showContentErr = false;
      this.descriptionArr = [];
      this.summaryArr = [];

      this.zone.run(() => {
        this.refreshSubscription = this.businessBioCache
          .refresh()
          .finally(() => (this.refreshSubscription = null))
          .subscribe(
            () => {
              if (refresher) {
                refresher.complete();
              }
            },
            err => {
              console.error("Something went wrong!", err);
              if (refresher) {
                refresher.complete();
              }
              throw err;
            }
          );
      });
    } else {
      console.warn("Cache has not been initialized.");
      if (refresher) {
        refresher.cancel();
      }
    }
    this.content.resize();
  }

  doInfinite(refresher: any) {
    if (this.action === "connect") {
      refresher.complete();
    } else {
      if (this.imageAction === "media") {
        this.pageImage = this.pageImage + 1;
        if (this.pageImage <= this.totalPageImage) {
          this.loadImage(this.pageImage, this.perPage, refresher);
        } else {
          refresher.complete();
        }
      } else if (this.imageAction === "logos") {
        this.pageLogo = this.pageLogo + 1;
        if (this.pageLogo <= this.totalPageLogo) {
          this.loadLogos(this.pageLogo, this.perPage, refresher);
        } else {
          refresher.complete();
        }
      } else if (this.imageAction === "print") {
        this.pagePrint = this.pagePrint + 1;
        if (this.pagePrint <= this.totalPagePrint) {
          this.loadPrints(this.pagePrint, this.perPage, refresher);
        } else {
          refresher.complete();
        }
      } else if (this.imageAction === "videos") {
        this.pageVideo = this.pageVideo + 1;
        if (this.pageVideo <= this.totalPageVideo) {
          this.loadVideos(this.pageVideo, this.perPage, refresher);
        } else {
          refresher.complete();
        }
      } else if (this.imageAction === "albums") {
        this.pageAlbum = this.pageAlbum + 1;
        if (this.pageAlbum <= this.totalPageAlbum) {
          this.loadAlbums(this.pageAlbum, this.perPage, refresher);
        } else {
          refresher.complete();
        }
      } else if (this.imageAction === "collection") {
        this.pageCollection = this.pageCollection + 1;
        if (this.pageCollection <= this.totalPageCollection) {
          this.loadCollections(this.pageCollection, this.perPage, refresher);
        } else {
          refresher.complete();
        }
      } else if (this.imageAction === "channels") {
        this.pageChannel = this.pageChannel + 1;
        if (this.pageChannel <= this.totalPageChannel) {
          this.loadChannels(this.pageChannel, this.perPage, refresher);
        } else {
          refresher.complete();
        }
      } else {
        refresher.complete();
      }
    }
  }

  searchMedia(val, type) {
    const onSuccess = (data: any) => {
      if (data && data.success) {
        if (type === "media") {
          this.imagesArr = [];
          this.pageImage = parseInt(data.data.currentPage);
          this.totalPageImage = parseInt(data.data.totalPages);

          this.zone.run(() => {
            for (let i = 0; i < _.size(data.data.results); i++) {
              data.data.results[i].selected = false;
              if (_.isEmpty(data.data.results[i].caption)) {
                data.data.results[i].caption = "No Caption";
              }
              if (_.isEmpty(data.data.results[i].location)) {
                data.data.results[i].location = "No Location";
              }
              this.imagesArr.push(data.data.results[i]);
            }
          });
        } else if (type === "logos") {
          this.logosArr = [];
          this.pageLogo = parseInt(data.data.currentPage);
          this.totalPageLogo = parseInt(data.data.totalPages);

          this.zone.run(() => {
            for (let i = 0; i < _.size(data.data.results); i++) {
              data.data.results[i].selected = false;
              this.logosArr.push(data.data.results[i]);
            }
          });
        } else if (type === "print") {
          this.printArr = [];
          this.pagePrint = parseInt(data.data.currentPage);
          this.totalPagePrint = parseInt(data.data.totalPages);

          this.zone.run(() => {
            for (let i = 0; i < _.size(data.data.results); i++) {
              data.data.results[i].selected = false;
              if (_.isEmpty(data.data.results[i].brochure_caption)) {
                data.data.results[i].brochure_caption = "No caption";
              }
              this.printArr.push(data.data.results[i]);
            }
          });
          this.printCopyArr = _.cloneDeep(this.printArr);
        } else if (type === "videos") {
          this.videosArr = [];
          this.pageVideo = parseInt(data.data.currentPage);
          this.totalPageVideo = parseInt(data.data.totalPages);

          this.zone.run(() => {
            for (let i = 0; i < _.size(data.data.results); i++) {
              data.data.results[i].selected = false;
              if (_.isEmpty(data.data.results[i].caption)) {
                data.data.results[i].caption = "No caption";
              }
              this.videosArr.push(data.data.results[i]);
            }
          });
        } else if (type === "albums") {
          this.albumsArr = [];
          this.pageAlbum = parseInt(data.data.currentPage);
          this.totalPageAlbum = parseInt(data.data.totalPages);

          this.zone.run(() => {
            for (let i = 0; i < _.size(data.data.results); i++) {
              data.data.results[i].selected = false;
              if (_.isEmpty(data.data.results[i].title)) {
                data.data.results[i].title = "No caption";
              }
              this.albumsArr.push(data.data.results[i]);
            }
          });
        } else if (type === "collection") {
          this.collectionArr = [];
          this.pageCollection = parseInt(data.data.page);
          this.totalPageCollection = parseInt(data.data.totalPages);

          this.zone.run(() => {
            for (let i = 0; i < _.size(data.data.results); i++) {
              data.data.results[i].selected = false;
              this.collectionArr.push(data.data.results[i]);
            }
          });
        } else if (type === "channels") {
          this.channelArr = [];
          this.pageChannel = parseInt(data.data.page);
          this.totalPageChannel = parseInt(data.data.totalPages);

          this.zone.run(() => {
            for (let i = 0; i < _.size(data.data.results); i++) {
              data.data.results[i].selected = false;
              this.channelArr.push(data.data.results[i]);
            }
          });
        }
        this.showContent = true;
      }
    };

    const onError = () => {
      if (type === "media") {
        this.showContentImage = true;
      } else if (type === "logos") {
        this.showContentLogo = true;
      } else if (type === "print") {
        this.showContentPrint = true;
      } else if (type === "videos") {
        this.showContentVideos = true;
      } else if (type === "albums") {
        this.showContentAlbums = true;
      } else if (type === "collection") {
        this.showContentCollections = true;
      } else if (type === "channels") {
        this.showContentChannels = true;
      }
    };

    const onComplete = () => {
      if (type === "media") {
        this.showContentImage = true;
      } else if (type === "logos") {
        this.showContentLogo = true;
      } else if (type === "print") {
        this.showContentPrint = true;
      } else if (type === "videos") {
        this.showContentVideos = true;
      } else if (type === "albums") {
        this.showContentAlbums = true;
      } else if (type === "collection") {
        this.showContentCollections = true;
      } else if (type === "channels") {
        this.showContentChannels = true;
      }
    };

    if (type === "media") {
      this.showContentImage = false;
      this.images
        .search_media({
          media: "image",
          keyword: val
        })
        .then(onSuccess, onError)
        .then(onComplete);
    } else if (type === "logos") {
      this.showContentLogo = false;
      this.images
        .search_media({
          media: "logos",
          keyword: val
        })
        .then(onSuccess, onError)
        .then(onComplete);
    } else if (type === "print") {
      this.showContentPrint = false;
      this.images
        .search_media({
          media: "print",
          keyword: val
        })
        .then(onSuccess, onError)
        .then(onComplete);
    } else if (type === "videos") {
      this.showContentVideos = false;
      this.images
        .search_media({
          media: "videos",
          keyword: val
        })
        .then(onSuccess, onError)
        .then(onComplete);
    } else if (type === "albums") {
      this.showContentAlbums = false;
      this.images
        .search_media({
          media: "albums",
          keyword: val
        })
        .then(onSuccess, onError)
        .then(onComplete);
    } else if (type === "collection") {
      this.showContentCollections = false;
      this.images
        .search_media({
          media: "collections",
          keyword: val
        })
        .then(onSuccess, onError)
        .then(onComplete);
    } else if (type === "channels") {
      this.showContentChannels = false;
      this.images
        .search_media({
          media: "channels",
          keyword: val
        })
        .then(onSuccess, onError)
        .then(onComplete);
    }
  }

  presentPopover(ev) {
    const popover = this.popoverCtrl.create(ImageTabsPopoverPage, {
      imageAction: this.imageAction,
      showContentImage: this.showContentImage,
      showUpload: this.showUpload
    });
    popover.present({ ev: ev });
  }

  getItems(ev) {
    let val = ev.target.value;

    if (val && val.trim() == "") {
      return;
    }

    this.searchMedia(val, this.imageAction);
  }

  selectClick(fab?: any) {
    if (fab) {
      fab.close();
    }

    if (this.imageAction === "media") {
      if (!this.showContentImage) {
        return;
      }
    } else if (this.imageAction === "logos") {
      if (!this.showContentLogo) {
        return;
      }
    } else if (this.imageAction === "print") {
      if (!this.showContentPrint) {
        return;
      }
    } else if (this.imageAction === "videos") {
      if (!this.showContentVideos) {
        return;
      }
    } else if (this.imageAction === "albums") {
      if (!this.showContentAlbums) {
        return;
      }
    } else if (this.imageAction === "collection") {
      if (!this.pageCollection) {
        return;
      }
    } else if (this.imageAction === "channels" && this.business.type === 2) {
      if (!this.showContentChannels) {
        return;
      }
    }

    this.showCheckbox = true;
    this.content.resize();
  }

  cancelClick(fab?: any) {
    this.showCheckbox = false;
    if (fab) {
      fab.close();
    }

    if (this.imageAction == "media") {
      _.each(this.imagesArr, (row: any) => {
        row.selected = this.showCheckbox;
      });
    } else if (this.imageAction == "logos") {
      _.each(this.logosArr, (row: any) => {
        row.selected = this.showCheckbox;
      });
    } else if (this.imageAction == "print") {
      _.each(this.logosArr, (row: any) => {
        row.selected = this.showCheckbox;
      });
    } else if (this.imageAction == "videos") {
      _.each(this.videosArr, (row: any) => {
        row.selected = this.showCheckbox;
      });
    } else if (this.imageAction == "albums") {
      _.each(this.albumsArr, (row: any) => {
        row.selected = this.showCheckbox;
      });
    }
    this.content.resize();
  }

  segmentChanged() {
    this.myInput = "";

    this.content.scrollToTop();
    this.events.publish("filterWhat", this.imageAction);

    if (this.imageAction != "media") {
      this.showCheckbox = false;
      _.each(this.imagesArr, (row: any) => {
        row.selected = this.showCheckbox;
      });
    }

    this.initMediaRules();
    this.content.resize();
    this.cancelClick(null);
  }

  printSegmentChanged() {
    let date1 = new Date();
    date1.setHours(0, 0, 0, 0);
    if (this.printstatus == "all") {
      this.zone.run(() => {
        this.printArr = _.cloneDeep(this.printCopyArr);
      });
    } else if (this.printstatus == "expired") {
      this.zone.run(() => {
        this.printArr = _.filter(this.printCopyArr, (row: any) => {
          let date2 = new Date(row.validityDate);
          date2.setHours(0, 0, 0, 0);
          return date2 <= date1;
        });
      });
    }
  }

  filterPrints() {
    const params: any = {};
    params.media = "print";

    if (this.filters.type) {
      params.type = this.filters.type;
    }
    console.log("params: ", params);
    this.showContentPrint = false;

    this.zone.run(() => {
      this.printArr = _.filter(this.printCopyArr, { type: params.type });
    });

    setTimeout(() => {
      this.showContentPrint = true;
    }, 2000);
  }

  filterImage() {
    const params: any = {};
    params.media = "image";

    if (!_.isEmpty(this.filters.category)) {
      params.category = this.filters.category.title;
    }
    if (this.filters.location) {
      params.location = this.filters.location.name;
    }
    if (this.filters.creator) {
      params.contributor = this.filters.creator.name;
    }
    console.log("params: ", params);
    this.showContentImage = false;
    this.images.search_media(params).then(
      (data: any) => {
        if (data && data.success && data.data[0] != false) {
          let imagesArr: any[] = [];
          imagesArr = data.data.results;
          _.each(imagesArr, (row: any) => {
            if (_.isEmpty(row.caption)) {
              row.caption = "No Caption";
            }
            if (_.isEmpty(row.location)) {
              row.location = "No Location";
            }
          });
          console.log("this.imagesArr: ", imagesArr);

          this.zone.run(() => {
            this.imagesArr = imagesArr;
          });
        }
        this.showContentImage = true;
      },
      error => {
        this.showContentImage = true;
      }
    );
  }

  private getCallbackResponse = resp => {
    return new Promise((resolve, reject) => {
      console.log("resp: ", resp);
      if (resp == "refresh") {
        this.zone.run(() => {
          if (this.imageAction === "media") {
            this.pageImage = 1;
            this.imagesArr = [];
            this.showContentImage = false;
            this.showContentImageErr = false;
            this.loadImage(this.pageImage, this.perPage);
          } else if (this.imageAction === "logos") {
            this.pageLogo = 1;
            this.logosArr = [];
            this.showContentLogo = false;
            this.showContentLogoErr = false;
            this.loadLogos(this.pageLogo, this.perPage);
          } else if (this.imageAction === "print") {
            this.pagePrint = 1;
            this.printArr = [];
            this.showContentPrint = false;
            this.showContentPrintErr = false;
            this.loadPrints(this.pagePrint, this.perPage);
          } else if (this.imageAction === "videos") {
            this.pageVideo = 1;
            this.videosArr = [];
            this.showContentVideos = false;
            this.showContentVideosErr = false;
            this.loadVideos(this.pageVideo, this.perPage);
          } else if (this.imageAction === "albums") {
            this.pageAlbum = 1;
            this.albumsArr = [];
            this.showContentAlbums = false;
            this.showContentAlbumsErr = false;
            this.loadAlbums(this.pageAlbum, this.perPage);
          } else if (this.imageAction === "collection") {
            this.pageCollection = 1;
            this.collectionArr = [];
            this.showContentCollections = false;
            this.showContentCollectionsErr = false;
            this.loadCollections(this.pageCollection, this.perPage);
          } else if (
            this.imageAction === "channels" &&
            this.business.type === 2
          ) {
            this.pageChannel = 1;
            this.channelArr = [];
            this.showContentChannels = false;
            this.showContentChannelsErr = false;
            this.loadChannels(this.pageChannel, this.perPage);
          }
        });
        resolve();
      }
    });
  };

  viewImageDetail(item) {
    if (this.showCheckbox) {
      _.each(this.imagesArr, (row: any) => {
        if (item.id == row.id) {
          row.selected = !row.selected;
        }
      });
      return;
    }

    this.app.getRootNav().push(ImageDetailPage, {
      image: item,
      callback: this.getCallbackResponse
    });
  }

  viewVideoDetail(item) {
    if (this.showCheckbox) {
      _.each(this.videosArr, (row: any) => {
        if (item.id == row.id) {
          row.selected = !row.selected;
        }
      });
      return;
    }

    this.app.getRootNav().push(VideoDetailPage, {
      video: item,
      callback: this.getCallbackResponse
    });
  }

  viewPrintDetail(item) {
    if (this.showCheckbox) {
      _.each(this.printArr, (row: any) => {
        if (item.id == row.id) {
          row.selected = !row.selected;
        }
      });
      return;
    }

    this.app.getRootNav().push(PrintDetailPage, {
      print: item,
      callback: this.getCallbackResponse
    });
  }

  viewLogoDetail(item) {
    if (this.showCheckbox) {
      _.each(this.logosArr, (row: any) => {
        if (item.id == row.id) {
          row.selected = !row.selected;
        }
      });
      return;
    }

    this.app.getRootNav().push(LogoDetailPage, {
      logo: item,
      callback: this.getCallbackResponse
    });
  }

  viewAlbums() {
    this.app.getRootNav().push(AlbumsPage);
  }

  viewAlbumImages(item: any) {
    if (this.showCheckbox) {
      _.each(this.albumsArr, (row: any) => {
        if (item.id == row.id) {
          row.selected = !row.selected;
        }
      });
      return;
    }

    this.app.getRootNav().push(AlbumGalleryPage, {
      id: item.id,
      album: item.title
    });
  }

  viewCollections(item: any) {
    this.app.getRootNav().push(CollectionsPage, {
      item: item
    });
  }

  viewChannels(item: any) {
    this.app.getRootNav().push(ChannelsPage, {
      item: item
    });
  }

  viewProfile() {
    // this.navCtrl.push(BusinessProfileSettingsPage);
    this.app.getRootNav().push(BusinessProfileSettingsPage);
  }

  viewNotifications() {
    if (this.business.role !== 1) {
      this.toastCtrl
        .create({
          message: "Invalid Access!!!",
          duration: 3000
        })
        .present();
      return;
    }

    this.app.getRootNav().push(NotificationsPage);
  }

  uploadFile(fab?: any) {
    if (fab) {
      fab.close();
    }

    if (this.business.role !== 1) {
      this.toastCtrl
        .create({
          message: "Invalid Access!!!",
          duration: 3000
        })
        .present();
      return;
    }

    let title: string;
    if (this.imageAction === "media") {
      title = "Upload Image";
    } else if (this.imageAction === "logos") {
      title = "Upload Logos";
    } else if (this.imageAction === "print") {
      title = "Upload Brochure";

      if (this.platform.is("ios")) {
        this.alertCtrl
          .create({
            title: "Upload not allowed",
            message: "Print file upload not allowed at this platform!",
            buttons: ["OK"]
          })
          .present();
        return;
      }
    } else if (this.imageAction === "videos") {
      title = "Upload Videos";
    }
    let modal = this.modalCtrl.create(FileUploadPage, {
      action: this.imageAction,
      title: title
    });
    modal.onDidDismiss((resp: any) => {
      if (this.imageAction === "media") {
        if (resp && resp.status == "save") {
          this.showFinishingUpload = true;
          this.content.resize();
          setTimeout(() => {
            this.showFinishingUpload = false;
            this.content.resize();
            this.pageImage = 1;
            this.imagesArr = [];
            this.showContentImage = false;
            this.loadImage(this.pageImage, this.perPage);
          }, this.imageUploadDuration);
        }
      } else if (this.imageAction === "print") {
        if (resp && resp.status == "save") {
          this.showFinishingUpload = true;
          this.content.resize();
          setTimeout(() => {
            this.showFinishingUpload = false;
            this.content.resize();
            this.pagePrint = 1;
            this.printArr = [];
            this.showContentPrint = false;
            this.loadPrints(this.pagePrint, this.perPage);
          }, this.brochureUploadDuration);
        }
      } else if (this.imageAction === "logos") {
        if (resp && resp.status == "save") {
          this.showFinishingUpload = true;
          this.content.resize();
          setTimeout(() => {
            this.showFinishingUpload = false;
            this.content.resize();
            this.pageLogo = 1;
            this.logosArr = [];
            this.showContentLogo = false;
            this.loadLogos(this.pageLogo, this.perPage);
          }, this.imageUploadDuration);
        }
      } else if (this.imageAction === "videos") {
        if (resp && resp.status == "save") {
          this.showFinishingUpload = true;
          this.content.resize();
          setTimeout(() => {
            this.showFinishingUpload = false;
            this.content.resize();
            this.pageVideo = 1;
            this.videosArr = [];
            this.showContentVideos = false;
            this.loadVideos(this.pageVideo, this.perPage);
          }, this.videoUploadDuration);
        }
      }
    });
    modal.present();
  }

  viewChat() {
    let modal = this.modalCtrl.create(ChatPage);
    modal.present();
  }

  viewFilter() {
    this.menuCtrl.toggle("right");
  }

  selectDescription(item: any) {
    _.each(this.descriptionArr, (row: any) => {
      if (item.id == row.id) {
        row.selected = !item.selected;
      } else {
        row.selected = false;
      }
    });
  }

  selectSummary(item: any) {
    _.each(this.summaryArr, (row: any) => {
      if (item.id == row.id) {
        row.selected = !item.selected;
      } else {
        row.selected = false;
      }
    });
  }

  createChannels() {
    if (this.business.role !== 1) {
      this.toastCtrl
        .create({
          message: "Invalid Access!!!",
          duration: 3000
        })
        .present();
      return;
    }

    let modal = this.modalCtrl.create(ChannelsEntryPage, {
      action: "create"
    });
    modal.onDidDismiss(resp => {
      console.log("resp: ", resp);
      if (resp == "save") {
        this.pageChannel = 1;
        this.imagesArr = [];
        this.showContentImage = false;
        this.loadChannels(this.pageChannel, this.perPage);
      }
    });
    modal.present();
  }

  deleteChannel(item: any, fab?: any) {
    console.log("deleteChannel: ", item);
    if (fab) {
      fab.close();
    }

    if (this.business.role !== 1) {
      this.toastCtrl
        .create({
          message: "Invalid Access!!!",
          duration: 3000
        })
        .present();
      return;
    }

    const deleteChannel = id => {
      const loading = this.loadingCtrl.create({
        dismissOnPageChange: true
      });
      loading.present();
      this.channels
        .channel_delete({
          id: id
        })
        .then(
          (data: any) => {
            if (data && data.success) {
              console.log("result: ", data);
              const alert = this.alertCtrl.create({
                title: "Error",
                message: data.message,
                buttons: ["OK"]
              });
              alert.onDidDismiss(() => {
                this.navCtrl.pop();
              });
              alert.present();
            }
            loading.dismiss();
          },
          error => {
            loading.dismiss();
            console.log("error: ", error);

            const alert = this.alertCtrl.create({
              title: "Error",
              message: error.message,
              buttons: ["OK"]
            });
            alert.present();
          }
        );
    };

    const confirm = this.alertCtrl.create({
      title: "Delete this channel?",
      message: "Are you sure to delete this channel?",
      buttons: [
        {
          text: "Cancel",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Confirm",
          handler: () => {
            console.log("Agree clicked");
            deleteChannel(item.id);
          }
        }
      ]
    });
    confirm.present();
  }

  updateChannnel(item: any, fab: FabContainer) {
    if (this.business.role !== 1) {
      this.toastCtrl
        .create({
          message: "Invalid Access!!!",
          duration: 3000
        })
        .present();
      return;
    }

    const modal = this.modalCtrl.create(ChannelsEntryPage, {
      action: "update"
    });
    modal.onDidDismiss(resp => {
      if (resp == "save") {
        this.pageChannel = 1;
        this.channelArr = [];
        this.showContentChannels = false;
        this.loadChannels(this.pageChannel, this.perPage);
      }
    });
    modal.present();
  }

  createCollections() {
    if (this.business.role !== 1) {
      this.toastCtrl
        .create({
          message: "Invalid Access!!!",
          duration: 3000
        })
        .present();
      return;
    }

    const modal = this.modalCtrl.create(CollectionsEntryPage, {
      action: "create"
    });
    modal.onDidDismiss(resp => {
      if (resp == "save") {
        this.pageCollection = 1;
        this.collectionArr = [];
        this.showContentCollections = false;
        this.loadCollections(this.pageCollection, this.perPage);
      }
    });
    modal.present();
  }

  deleteCollection(collection, event) {
    event.preventDefault();
    event.stopPropagation();

    if (this.business.role !== 1) {
      this.toastCtrl
        .create({
          message: "Invalid Access!!!",
          duration: 3000
        })
        .present();
      return;
    }

    const deleteCollection = id => {
      const loading = this.loadingCtrl.create({
        dismissOnPageChange: true
      });
      loading.present();

      const formData = new FormData();
      formData.append("id", id);
      this.collections.collection_delete(formData).then(
        (data: any) => {
          if (data && data.success) {
            console.log("result: ", data);
            const alert = this.alertCtrl.create({
              title: "SUCCESS",
              message: data.data,
              buttons: ["OK"]
            });
            alert.onDidDismiss(() => {
              this.pageCollection = 1;
              this.collectionArr = [];
              this.showContentCollections = false;
              this.loadCollections(this.pageCollection, this.perPage);
            });
            alert.present();
          }
          loading.dismiss();
        },
        error => {
          loading.dismiss();
          console.log("error: ", error);
          if (error) {
            const alert = this.alertCtrl.create({
              title: "Error",
              message: error.message || error.error,
              buttons: ["OK"]
            });
            alert.present();
          }
        }
      );
    };

    const confirm = this.alertCtrl.create({
      title: "Delete this collection?",
      message: "Are you sure to delete this collection?",
      buttons: [
        {
          text: "Cancel",
          handler: () => {}
        },
        {
          text: "Confirm",
          handler: () => {
            console.log("Agree clicked");
            deleteCollection(collection.id);
          }
        }
      ]
    });
    confirm.present();
  }

  createAlbums() {
    if (this.business.role !== 1) {
      this.toastCtrl
        .create({
          message: "Invalid Access!!!",
          duration: 3000
        })
        .present();
      return;
    }

    const modal = this.modalCtrl.create(AlbumsEntryPage, {
      action: "create"
    });
    modal.onDidDismiss(resp => {
      if (resp == "save") {
        this.pageAlbum = 1;
        this.albumsArr = [];
        this.showContentAlbums = false;
        this.loadAlbums(this.pageAlbum, this.perPage);
      }
    });
    modal.present();
  }

  updateBio(item, type) {
    if (this.business.role !== 1) {
      this.toastCtrl
        .create({
          message: "Invalid Access!!!",
          duration: 3000
        })
        .present();
      return;
    }

    const modal = this.modalCtrl.create(CompanyBioEntryPage, {
      item: item,
      type: type,
      action: "update"
    });
    modal.onDidDismiss(resp => {
      if (resp == "save") {
        this.descriptionArr = [];
        this.summaryArr = [];

        this.showContent = false;
        this.showContentErr = false;
        this.loadCompanyBio();
      }
    });
    modal.present();
  }

  createCompanyBio() {
    if (this.business.role !== 1) {
      this.toastCtrl
        .create({
          message: "Invalid Access!!!",
          duration: 3000
        })
        .present();
      return;
    }

    const modal = this.modalCtrl.create(CompanyBioEntryPage, {
      action: "create"
    });
    modal.onDidDismiss(resp => {
      if (resp == "save") {
        this.descriptionArr = [];
        this.summaryArr = [];

        this.showContent = false;
        this.showContentErr = false;
        this.loadCompanyBio();
      }
    });
    modal.present();
  }

  deleteSelected(fab?: any) {
    if (fab) {
      fab.close();
    }

    let title: string;
    let message: string;
    let selectedDatas: any[] = [];

    if (this.business.role !== 1) {
      this.toastCtrl
        .create({
          message: "Invalid Access!!!",
          duration: 3000
        })
        .present();
      return;
    }

    if (this.imageAction === "media") {
      title = "Delete Selected Image";
      message = "Are you sure to delete selected images?";
      selectedDatas = _.map(
        _.filter(this.imagesArr, (row: any) => {
          return row.selected;
        }),
        (row: any) => {
          return row.id;
        }
      );
    } else if (this.imageAction === "logos") {
      title = "Delete Selected Logo";
      message = "Are you sure to delete selected logos?";
      selectedDatas = _.map(
        _.filter(this.logosArr, (row: any) => {
          return row.selected;
        }),
        (row: any) => {
          return row.id;
        }
      );
    } else if (this.imageAction === "print") {
      title = "Delete Selected Brochure";
      message = "Are you sure to delete selected brochures?";
      selectedDatas = _.map(
        _.filter(this.printArr, (row: any) => {
          return row.selected;
        }),
        (row: any) => {
          return row.id;
        }
      );
    } else if (this.imageAction === "videos") {
      title = "Delete Selected Video";
      message = "Are you sure to delete selected videos?";
      selectedDatas = _.map(
        _.filter(this.videosArr, (row: any) => {
          return row.selected;
        }),
        (row: any) => {
          return row.id;
        }
      );
    } else if (this.imageAction == "albums") {
      title = "Delete Selected Albums";
      message = "Are you sure to delete selected albums?";
      selectedDatas = _.map(
        _.filter(this.albumsArr, (row: any) => {
          return row.selected;
        }),
        (row: any) => {
          return row.id;
        }
      );
    }

    if (_.isEmpty(selectedDatas)) {
      this.alertCtrl
        .create({
          title: "WARNING",
          message: "No selected data to delete!",
          buttons: ["OK"]
        })
        .present();
      return;
    }

    console.log("selectedDatas: ", selectedDatas);

    const deleteImages = images => {
      const loading = this.loadingCtrl.create({
        dismissOnPageChange: true,
        content: "Deleting images..."
      });
      loading.present();
      this.images
        .image_delete(images)
        .then(
          (data: any) => {
            if (data && data.success) {
              console.log("data: ", data);
              setTimeout(() => {
                this.showCheckbox = false;
                this.pageImage = 1;
                this.imagesArr = [];
                this.showContentImage = false;
                this.showContentImageErr = false;

                this.loadImage(this.pageImage, this.perPage);
              }, 600);
            }
            loading.dismiss();
          },
          (error: any) => {
            console.log("error: ", error);
            loading.dismiss();
          }
        )
        .catch((error: any) => {
          console.log("error: ", error);
          loading.dismiss();
        });
    };

    const deleteLogos = logos => {
      const loading = this.loadingCtrl.create({
        dismissOnPageChange: true,
        content: "Deleting logos..."
      });
      loading.present();
      this.logos.logos_delete(logos).then(
        (data: any) => {
          if (data && data.success) {
            console.log("data: ", data);
            setTimeout(() => {
              this.showCheckbox = false;
              this.pageLogo = 1;
              this.logosArr = [];
              this.showContentLogo = false;
              this.showContentLogoErr = false;
              this.loadLogos(this.pageLogo, this.perPage);
            }, 600);
          }
          loading.dismiss();
        },
        (error: any) => {
          console.log("error: ", error);
          loading.dismiss();
        }
      );
    };

    const deletePrints = prints => {
      const loading = this.loadingCtrl.create({
        dismissOnPageChange: true,
        content: "Deleting brochures..."
      });
      loading.present();
      this.prints.print_delete(prints).then(
        (data: any) => {
          if (data && data.success) {
            console.log("data: ", data);
            setTimeout(() => {
              this.showCheckbox = false;
              this.pagePrint = 1;
              this.printArr = [];
              this.showContentPrint = false;
              this.showContentPrintErr = false;
              this.loadPrints(this.pagePrint, this.perPage);
            }, 600);
          }
          loading.dismiss();
        },
        (error: any) => {
          console.log("error: ", error);
          loading.dismiss();
        }
      );
    };

    const deleteVideos = videos => {
      const loading = this.loadingCtrl.create({
        dismissOnPageChange: true,
        content: "Deleting videos..."
      });
      loading.present();
      this.videos.videos_delete(videos).then(
        (data: any) => {
          if (data && data.success) {
            console.log("data: ", data);
            setTimeout(() => {
              this.showCheckbox = false;
              this.pageVideo = 1;
              this.videosArr = [];
              this.showContentVideos = false;
              this.showContentVideosErr = false;
              this.loadVideos(this.pageVideo, this.perPage);
            }, 600);
          }
          loading.dismiss();
        },
        (error: any) => {
          console.log("error: ", error);
          loading.dismiss();
        }
      );
    };

    const deleteAlbums = albums => {
      const loading = this.loadingCtrl.create({
        dismissOnPageChange: true,
        content: "Deleting albums..."
      });
      loading.present();

      async.eachSeries(
        albums,
        (album, cb) => {
          this.albums
            .album_delete({
              id: album
            })
            .then(
              (data: any) => {
                if (data && data.success) {
                  console.log("data: ", data);
                  cb();
                }
                loading.dismiss();
              },
              (error: any) => {
                console.log("error: ", error);
                loading.dismiss();
                if (error && error.error) {
                  this.toastCtrl
                    .create({
                      message: error.message,
                      duration: 1500
                    })
                    .present();
                }
                cb();
              }
            );
        },
        () => {
          setTimeout(() => {
            this.showCheckbox = false;
            this.pageAlbum = 1;
            this.albumsArr = [];
            this.showContentAlbums = false;
            this.showContentAlbumsErr = false;
            this.loadAlbums(this.pageAlbum, this.perPage);
          }, 600);
        }
      );
    };

    const actionSheet = this.actionSheetCtrl.create({
      title: message,
      buttons: [
        {
          text: title,
          role: "destructive",
          handler: () => {
            console.log("Destructive clicked");

            if (this.imageAction === "media") {
              deleteImages(selectedDatas);
            } else if (this.imageAction === "logos") {
              deleteLogos(selectedDatas);
            } else if (this.imageAction === "print") {
              deletePrints(selectedDatas);
            } else if (this.imageAction === "videos") {
              deleteVideos(selectedDatas);
            } else if (this.imageAction == "albums") {
              deleteAlbums(selectedDatas);
            }
          }
        },
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        }
      ]
    });
    actionSheet.present();
  }
}
