import { Component, ViewChild, NgZone, ViewEncapsulation } from "@angular/core";
import {
  NavController,
  NavParams,
  AlertController,
  ActionSheetController,
  Platform,
  ToastController,
  ModalController,
  PopoverController,
  Events,
  App,
  Content,
  MenuController,
  LoadingController
} from "ionic-angular";
import * as _ from "lodash";
import { CacheService, Cache } from "ionic-cache-observable";
import { Observable, Subscription } from "rxjs";
import "rxjs/add/operator/finally";

import { ImagesServicesProvider } from "../../providers/services/images";
import { AlbumsServicesProvider } from "../../providers/services/albums";
import { PrintServicesProvider } from "../../providers/services/print";
import { VideosServicesProvider } from "../../providers/services/videos";
import { LogoServicesProvider } from "../../providers/services/logos";
import { CollectionsServicesProvider } from "../../providers/services/collections";
import { ChannelsServicesProvider } from "../../providers/services/channels";
import { BusinessServicesProvider } from "../../providers/services/business";
import { ProductsServicesProvider } from "../../providers/services/products";

import { ImageDetailPage } from "../image-detail/image-detail";
import { VideoDetailPage } from "../video-detail/video-detail";
import { PrintDetailPage } from "../print-detail/print-detail";
import { LogoDetailPage } from "../logo-detail/logo-detail";
import { AlbumsPage } from "../albums/albums";
import { AlbumGalleryPage } from "../album-gallery/album-gallery";
import { CollectionsPage } from "../collections/collections";
import { ChannelsPage } from "../channels/channels";
import { ProductDetailPage } from "../product-detail/product-detail";
import { OperatorRequestPage } from "../connected-business/operator-request";
import { BYOBConnectedPlatformPage } from "../connected-business/byob-connected-platform";
import { StorageProvider } from "../../providers/storage/storage";

@Component({
  selector: "connect-media",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "connect-media.html"
})
export class ConnectMediaPage {
  business: any = {};
  connect_params: any = {};
  connect_user: any = {};

  imagesArr: any[] = [];
  imagesArrCopy: any[] = [];
  imagesLoadingArr: any[] = [];

  biog: any[] = [];

  logosArr: any[] = [];
  logosArrCopy: any[] = [];

  printArr: any[] = [];
  printCopyArr: any[] = [];

  videosArr: any[] = [];
  videosArrCopy: any[] = [];

  albumsArr: any[] = [];
  albumsArrCopy: any[] = [];

  collectionArr: any[] = [];
  collectionArrCopy: any[] = [];

  channelArr: any[] = [];
  channelArrCopy: any[] = [];

  descriptionArr: any[] = [];
  summaryArr: any[] = [];

  productArr: any[] = [];
  productArrCopy: any[] = [];

  fakeArr: any[] = [];

  myInput: string = "";

  filters: any = {};
  showPreview: boolean = false;

  showContentImage: boolean = false;
  showContentImageErr: boolean = false;
  imageContentErr: any = {};
  pageImage: number = 0;
  totalPageImage: number = 0;

  showContentLogo: boolean = false;
  showContentLogoErr: boolean = false;
  logoContentErr: any = {};
  pageLogo: number = 0;
  totalPageLogo: number = 0;

  showContentPrint: boolean = false;
  showContentPrintErr: boolean = false;
  printContentErr: any = {};
  pagePrint: number = 0;
  totalPagePrint: number = 0;

  showContentVideos: boolean = false;
  showContentVideosErr: boolean = false;
  videosContentErr: any = {};
  pageVideo: number = 0;
  totalPageVideo: number = 0;

  showContentAlbums: boolean = false;
  showContentAlbumsErr: boolean = false;
  albumsContentErr: any = {};
  pageAlbum: number = 0;
  totalPageAlbum: number = 0;

  showContentCollections: boolean = false;
  showContentCollectionsErr: boolean = false;
  collectionsContentErr: any = {};
  pageCollection: number = 0;
  totalPageCollection: number = 0;

  showContentChannels: boolean = false;
  showContentChannelsErr: boolean = false;
  channelsContentErr: any = {};
  pageChannel: number = 0;
  totalPageChannel: number = 0;

  showContentProducts: boolean = false;
  showContentProductsErr: boolean = false;
  productsContentErr: any = {};
  pageProduct: number = 0;
  totalPageProduct: number = 0;

  showContent: boolean = false;
  showContentErr: boolean = false;
  contentErr: any = {};

  showCheckbox: boolean = false;
  showSearchBar: boolean = true;

  imageAction: string = "media";
  printstatus: string = "all";

  perPage = 20;

  @ViewChild(Content) content: Content;

  imagesArr$: Observable<any>;
  private imageCache: Cache<any>;

  logosArr$: Observable<any>;
  private logoCache: Cache<any>;

  printArr$: Observable<any>;
  private printCache: Cache<any>;

  videosArr$: Observable<any>;
  private videosCache: Cache<any>;

  albumsArr$: Observable<any>;
  private albumsCache: Cache<any>;

  collectionArr$: Observable<any>;
  private collectionCache: Cache<any>;

  channelArr$: Observable<any>;
  private channelCache: Cache<any>;

  businessBio$: Observable<any>;
  private businessBioCache: Cache<any>;

  productArr$: Observable<any>;
  private productCache: Cache<any>;

  public refreshSubscription: Subscription;

  constructor(
    public app: App,
    public zone: NgZone,
    public platform: Platform,
    public navCtrl: NavController,
    public navParams: NavParams,
    public images: ImagesServicesProvider,
    public albums: AlbumsServicesProvider,
    public prints: PrintServicesProvider,
    public videos: VideosServicesProvider,
    public logos: LogoServicesProvider,
    public alertCtrl: AlertController,
    public actionSheetCtrl: ActionSheetController,
    public toastCtrl: ToastController,
    public modalCtrl: ModalController,
    public popoverCtrl: PopoverController,
    public events: Events,
    public collections: CollectionsServicesProvider,
    public channels: ChannelsServicesProvider,
    public menuCtrl: MenuController,
    public businesses: BusinessServicesProvider,
    public products: ProductsServicesProvider,
    public loadingCtrl: LoadingController,
    public storageService: StorageProvider,
    public cacheService: CacheService
  ) {
    this.connect_params = navParams.get("params");
    this.connect_user = navParams.get("user");
    console.log("this.connect_params: ", this.connect_params);
    console.log("this.connect_user: ", this.connect_user);

    for (var i = 0; i < 12; ++i) {
      this.imagesLoadingArr.push(i);
      this.fakeArr.push(i);
    }

    if (this.business.type === 2 && this.connect_params.type == "operator") {
      this.showSearchBar = false;
    } else if (
      this.business.type === 1 &&
      (this.imageAction !== "company" && this.imageAction == "products")
    ) {
      this.showSearchBar = false;
    }

    events.subscribe("applyFilter", (data: any) => {
      console.log("applyFilter: ", data);
      this.filters = data;
      this.filterImage();
    });
  }

  ionViewDidLoad() {
    this.events.publish("filterWhat", "connected-media");

    this.pageImage = 1;
    this.pagePrint = 1;
    this.pageVideo = 1;
    this.pageLogo = 1;
    this.pageAlbum = 1;
    this.pageCollection = 1;
    this.pageChannel = 1;
    this.pageProduct = 1;

    this.imagesArr = [];
    this.imagesArrCopy = [];

    this.logosArr = [];
    this.logosArrCopy = [];

    this.printArr = [];
    this.printCopyArr = [];

    this.videosArr = [];
    this.videosArrCopy = [];

    this.albumsArr = [];
    this.albumsArrCopy = [];

    this.showContentImage = false;
    this.showContentImageErr = false;

    this.showContentLogo = false;
    this.showContentLogoErr = false;

    this.showContentPrint = false;
    this.showContentPrintErr = false;

    this.showContentVideos = false;
    this.showContentVideosErr = false;

    this.showContentAlbums = false;
    this.showContentAlbumsErr = false;

    this.initBusiness().then(() => {
      this.loadImage(this.pageImage, this.perPage);
      this.loadLogos(this.pageLogo, this.perPage);
      this.loadPrints(this.pagePrint, this.perPage);
      this.loadVideos(this.pageVideo, this.perPage);
      this.loadAlbums(this.pageAlbum, this.perPage);

      if (this.connect_params.type == "operator") {
        this.productArr = [];
        this.showContentProducts = false;
        this.showContentProductsErr = false;
        this.loadProducts(this.connect_params.id);
      }

      if (this.business.type === 1) {
        this.descriptionArr = [];
        this.summaryArr = [];

        this.showContent = false;
        this.showContentErr = false;

        this.loadCompanyBio();

        if (this.connect_params.type == "distributor") {
          this.events.publish("generateConnectedFilters", this.connect_params);
        }
      }

      if (this.connect_params.type == "distributor") {
        this.collectionArr = [];
        this.collectionArrCopy = [];

        this.channelArr = [];
        this.channelArrCopy = [];

        this.showContentCollections = false;
        this.showContentCollectionsErr = false;

        this.showContentChannels = false;
        this.showContentChannelsErr = false;

        this.loadCollections(this.pageCollection, this.perPage);
        this.loadChannels(this.pageChannel, this.perPage);
      }
    });
  }

  private async initBusiness() {
    const business = await this.storageService.getStorage(
      this.storageService.storageKey.CURRENT_BUSINESS
    );

    this.zone.run(() => {
      this.business = !_.isEmpty(business) ? JSON.parse(business) || {} : {};
    });
  }

  private loadImage(page, total, refresher?: any) {
    const successResponse = (data: any) => {
      if (data && data.success && data.data[0] != false) {
        this.pageImage = parseInt(data.data.currentPage);
        this.totalPageImage = parseInt(data.data.totalPages);

        this.zone.run(() => {
          for (let i = 0; i < _.size(data.data.images); i++) {
            data.data.images[i].selected = false;
            if (_.isEmpty(data.data.images[i].caption)) {
              data.data.images[i].caption = "No Caption";
            }
            if (_.isEmpty(data.data.images[i].location)) {
              data.data.images[i].location = "No Location";
            }
            this.imagesArr.push(data.data.images[i]);
          }
        });
        this.imagesArrCopy = _.cloneDeep(this.imagesArr);
      } else if (data && data.success && data.data[0] == false) {
        this.showContentImageErr = true;
        this.imageContentErr = {
          message: "No results found"
        };
      }
      this.showContentImage = true;
      if (refresher) {
        refresher.complete();
      }
    };

    const errorResponse = (error: any) => {
      this.showContentImage = true;
      this.showContentImageErr = true;

      if (refresher) {
        refresher.complete();
      }
      if (error && !error.success) {
        this.imageContentErr = error;
      }
    };

    let dataObservable: any;
    let cacheKey: any;
    this.imagesArr = [];
    this.imagesArrCopy = [];

    if (this.connect_params.type == "operator") {
      dataObservable = Observable.fromPromise(
        this.images.image_operator_list(this.connect_params.id, {
          page: page,
          total: total
        })
      );
      cacheKey =
        this.storageService.cacheKey.MEDIA_IMAGES +
        "_" +
        this.business.id +
        "_operator_" +
        this.connect_params.id +
        "_" +
        page;
    } else if (this.connect_params.type == "distributor") {
      dataObservable = Observable.fromPromise(
        this.images.image_distributor_list(this.connect_params.id, {
          page: page,
          total: total
        })
      );
      cacheKey =
        this.storageService.cacheKey.MEDIA_IMAGES +
        "_" +
        this.business.id +
        "_distributor_" +
        this.connect_params.id +
        "_" +
        page;
    }

    this.cacheService.register(cacheKey, dataObservable).subscribe(cache => {
      this.imageCache = cache;
      this.zone.run(() => {
        this.imagesArr$ = cache.get$;
      });
    });

    this.imagesArr$.subscribe(successResponse, errorResponse, () => {
      this.content.resize();
    });
  }

  private loadLogos(page, total, refresher?: any) {
    const successResponse = (data: any) => {
      if (data && data.success && !data.data.error) {
        this.pageLogo = parseInt(data.data.currentPage);
        this.totalPageLogo = parseInt(data.data.totalPages);

        this.zone.run(() => {
          for (let i = 0; i < _.size(data.data.logos); i++) {
            data.data.logos[i].selected = false;
            this.logosArr.push(data.data.logos[i]);
          }
        });
        this.logosArrCopy = _.cloneDeep(this.logosArr);
      } else if (data && data.success && data.data.error) {
        this.showContentLogoErr = true;
        this.logoContentErr = {
          message: data.data.msg
        };
      }
      this.showContentLogo = true;
      if (refresher) {
        refresher.complete();
      }
    };

    const errorResponse = (error: any) => {
      this.showContentLogo = true;
      this.showContentLogoErr = true;
      if (refresher) {
        refresher.complete();
      }
      if (error && !error.success) {
        this.logoContentErr = error;
      }
    };

    let dataObservable: any;
    let cacheKey: any;
    this.logosArr = [];
    this.logosArrCopy = [];

    if (this.connect_params.type == "operator") {
      dataObservable = Observable.fromPromise(
        this.logos.logos_list_operator(this.connect_params.id, {
          page: page,
          total: total
        })
      );
      cacheKey =
        this.storageService.cacheKey.MEDIA_LOGOS +
        "_" +
        this.business.id +
        "_operator_" +
        this.connect_params.id +
        "_" +
        page;
    } else if (this.connect_params.type == "distributor") {
      dataObservable = Observable.fromPromise(
        this.logos.logos_list_distributor(this.connect_params.id, {
          page: page,
          total: total
        })
      );
      cacheKey =
        this.storageService.cacheKey.MEDIA_LOGOS +
        "_" +
        this.business.id +
        "_distributor_" +
        this.connect_params.id +
        "_" +
        page;
    }

    this.cacheService.register(cacheKey, dataObservable).subscribe(cache => {
      this.logoCache = cache;
      this.zone.run(() => {
        this.logosArr$ = cache.get$;
      });
    });

    this.logosArr$.subscribe(successResponse, errorResponse, () => {
      this.content.resize();
    });
  }

  private loadPrints(page, total, refresher?: any) {
    const successResponse = (data: any) => {
      if (data && data.success && data.data[0] != false) {
        this.pagePrint = parseInt(data.data.currentPage);
        this.totalPagePrint = parseInt(data.data.totalPages);

        this.zone.run(() => {
          for (let i = 0; i < _.size(data.data.prints); i++) {
            data.data.prints[i].selected = false;
            if (_.isEmpty(data.data.prints[i].brochure_caption)) {
              data.data.prints[i].brochure_caption = "No caption";
            }
            this.printArr.push(data.data.prints[i]);
          }
        });
        this.printCopyArr = _.cloneDeep(this.printArr);
        this.printSegmentChanged();
      } else if (data && data.success && data.data[0] == false) {
        this.showContentPrintErr = true;
        this.printContentErr = {
          message: "No results found"
        };
      }
      this.showContentPrint = true;
      if (refresher) {
        refresher.complete();
      }
    };

    const errorResponse = error => {
      this.showContentPrint = true;
      this.showContentPrintErr = true;
      if (refresher) {
        refresher.complete();
      }
      if (error && !error.success) {
        this.printContentErr = error;
      }
    };

    let dataObservable: any;
    let cacheKey: any;
    this.printArr = [];
    this.printCopyArr = [];

    if (this.connect_params.type == "operator") {
      dataObservable = Observable.fromPromise(
        this.prints.brochure_operator_list(this.connect_params.id, {
          page: page,
          total: total
        })
      );
      cacheKey =
        this.storageService.cacheKey.MEDIA_PRINTS +
        "_" +
        this.business.id +
        "_operator_" +
        this.connect_params.id +
        "_" +
        page;
    } else if (this.connect_params.type == "distributor") {
      dataObservable = Observable.fromPromise(
        this.prints.brochure_distributor_list(this.connect_params.id, {
          page: page,
          total: total
        })
      );
      cacheKey =
        this.storageService.cacheKey.MEDIA_PRINTS +
        "_" +
        this.business.id +
        "_distributor_" +
        this.connect_params.id +
        "_" +
        page;
    }

    this.cacheService.register(cacheKey, dataObservable).subscribe(cache => {
      this.printCache = cache;
      this.zone.run(() => {
        this.printArr$ = cache.get$;
      });
    });
    this.printArr$.subscribe(successResponse, errorResponse, () => {
      this.content.resize();
    });
  }

  private loadVideos(page, total, refresher?: any) {
    const successResponse = (data: any) => {
      if (data && data.success && data.data[0] != false) {
        this.pageVideo = parseInt(data.data.currentPage);
        this.totalPageVideo = parseInt(data.data.totalPages);

        this.zone.run(() => {
          for (let i = 0; i < _.size(data.data.videos); i++) {
            data.data.videos[i].selected = false;
            if (_.isEmpty(data.data.videos[i].caption)) {
              data.data.videos[i].caption = "No caption";
            }
            this.videosArr.push(data.data.videos[i]);
          }
        });
        this.videosArrCopy = _.cloneDeep(this.videosArr);
      } else if (data && data.success && data.data[0] == false) {
        this.showContentVideosErr = true;
        this.videosContentErr = {
          message: "No results found"
        };
      }
      this.showContentVideos = true;
      if (refresher) {
        refresher.complete();
      }
    };

    const errorResponse = error => {
      this.showContentVideos = true;
      this.showContentVideosErr = true;
      if (refresher) {
        refresher.complete();
      }
      if (error && !error.success) {
        this.videosContentErr = error;
      }
    };

    let dataObservable: any;
    let cacheKey: any;
    this.videosArr = [];
    this.videosArrCopy = [];

    if (this.connect_params.type == "operator") {
      dataObservable = Observable.fromPromise(
        this.videos.video_operator_list(this.connect_params.id, {
          page: page,
          total: total
        })
      );
      cacheKey =
        this.storageService.cacheKey.MEDIA_VIDEOS +
        "_" +
        this.business.id +
        "_operator_" +
        this.connect_params.id +
        "_" +
        page;
    } else if (this.connect_params.type == "distributor") {
      dataObservable = Observable.fromPromise(
        this.videos.video_distributor_list(this.connect_params.id, {
          page: page,
          total: total
        })
      );
      cacheKey =
        this.storageService.cacheKey.MEDIA_VIDEOS +
        "_" +
        this.business.id +
        "_distributor_" +
        this.connect_params.id +
        "_" +
        page;
    }

    this.cacheService.register(cacheKey, dataObservable).subscribe(cache => {
      this.videosCache = cache;
      this.zone.run(() => {
        this.videosArr$ = cache.get$;
      });
    });

    this.videosArr$.subscribe(successResponse, errorResponse, () => {
      this.content.resize();
    });
  }

  private loadAlbums(page, total, refresher?: any) {
    const successResponse = (data: any) => {
      if (data && data.success && data.data[0] != false) {
        this.pageAlbum = parseInt(data.data.currentPage);
        this.totalPageAlbum = parseInt(data.data.totalPages);

        this.zone.run(() => {
          for (let i = 0; i < _.size(data.data.albums); i++) {
            data.data.albums[i].selected = false;
            if (_.isEmpty(data.data.albums[i].title)) {
              data.data.albums[i].title = "No caption";
            }
            this.albumsArr.push(data.data.albums[i]);
          }
        });
        this.albumsArrCopy = _.cloneDeep(this.albumsArr);
      } else if (data && data.success && data.data[0] == false) {
        this.showContentAlbumsErr = true;
        this.albumsContentErr = {
          message: "No results found"
        };
      }
      this.showContentAlbums = true;
      if (refresher) {
        refresher.complete();
      }
    };

    const errorResponse = error => {
      this.showContentAlbums = true;
      this.showContentAlbumsErr = true;
      if (refresher) {
        refresher.complete();
      }
      if (error && !error.success) {
        this.albumsContentErr = error;
        console.log("this.albumsContentErr: ", this.albumsContentErr);
      }
    };

    let dataObservable: any;
    let cacheKey: any;
    this.albumsArr = [];
    this.albumsArrCopy = [];

    if (this.connect_params.type == "operator") {
      dataObservable = Observable.fromPromise(
        this.albums.album_operator_list(this.connect_params.id, {
          page: page,
          total: total
        })
      );
      cacheKey =
        this.storageService.cacheKey.MEDIA_ALBUMS +
        "_" +
        this.business.id +
        "_operator_" +
        this.connect_params.id +
        "_" +
        page;
    } else if (this.connect_params.type == "distributor") {
      dataObservable = Observable.fromPromise(
        this.albums.album_distributor_list(this.connect_params.id, {
          page: page,
          total: total
        })
      );
      cacheKey =
        this.storageService.cacheKey.MEDIA_ALBUMS +
        "_" +
        this.business.id +
        "_distributor_" +
        this.connect_params.id +
        "_" +
        page;
    }

    this.cacheService.register(cacheKey, dataObservable).subscribe(cache => {
      this.albumsCache = cache;
      this.zone.run(() => {
        this.albumsArr$ = cache.get$;
      });
    });

    this.albumsArr$.subscribe(successResponse, errorResponse, () => {
      this.content.resize();
    });
  }

  private loadCollections(page, total, refresher?: any) {
    const successResponse = (data: any) => {
      if (data && data.success && data.data[0] != false) {
        this.pageCollection = parseInt(data.data.page);
        this.totalPageCollection = parseInt(data.data.totalPages);

        this.zone.run(() => {
          for (let i = 0; i < _.size(data.data.data); i++) {
            data.data.data[i].selected = false;
            this.collectionArr.push(data.data.data[i]);
          }
        });
        this.collectionArrCopy = _.cloneDeep(this.collectionArr);
      } else if (data && data.success && data.data[0] == false) {
        this.showContentCollectionsErr = true;
        this.collectionsContentErr = {
          message: "No results found"
        };
      }
      this.showContentCollections = true;
      if (refresher) {
        refresher.complete();
      }
    };

    const errorResponse = error => {
      console.log("error: ", error);
      this.zone.run(() => {
        this.showContentCollections = true;
        this.showContentCollectionsErr = true;
        if (refresher) {
          refresher.complete();
        }
        if (error && !error.success) {
          this.collectionsContentErr = error;
          console.log("collectionsContentErr: ", this.collectionsContentErr);
        }
      });
    };

    let dataObservable: any;
    let cacheKey: any;
    this.collectionArr = [];
    this.collectionArrCopy = [];

    if (this.connect_params.type == "operator") {
      dataObservable = Observable.fromPromise(
        this.collections.collecton_operator_list(this.connect_params.id, {
          page: page,
          total: total
        })
      );
      cacheKey =
        this.storageService.cacheKey.MEDIA_COLLECTIONS +
        "_" +
        this.business.id +
        "_operator_" +
        this.connect_params.id +
        "_" +
        page;
    } else if (this.connect_params.type == "distributor") {
      dataObservable = Observable.fromPromise(
        this.collections.collecton_distributor_list(this.connect_params.id, {
          page: page,
          total: total
        })
      );
      cacheKey =
        this.storageService.cacheKey.MEDIA_COLLECTIONS +
        "_" +
        this.business.id +
        "_distributor_" +
        this.connect_params.id +
        "_" +
        page;
    }

    this.cacheService.register(cacheKey, dataObservable).subscribe(cache => {
      this.collectionCache = cache;
      this.zone.run(() => {
        this.collectionArr$ = cache.get$;
      });
    });
    this.collectionArr$.subscribe(successResponse, errorResponse, () => {
      this.content.resize();
    });
  }

  private loadChannels(page, total, refresher?: any) {
    const successResponse = (data: any) => {
      console.log("data: ", data);
      if (data && data.success && data.data[0] != false) {
        this.pageChannel = parseInt(data.data.page);
        this.totalPageChannel = parseInt(data.data.totalPages);

        this.zone.run(() => {
          for (let i = 0; i < _.size(data.data.data); i++) {
            data.data.data[i].selected = false;
            this.channelArr.push(data.data.data[i]);
          }
        });
        this.channelArrCopy = _.cloneDeep(this.channelArr);
      } else if (data && data.success && data.data[0] == false) {
        this.showContentChannelsErr = true;
        this.channelsContentErr = {
          message: "No results found"
        };
      }
      this.showContentChannels = true;
      if (refresher) {
        refresher.complete();
      }
    };

    const errorResponse = error => {
      this.showContentChannels = true;
      this.showContentChannelsErr = true;
      if (refresher) {
        refresher.complete();
      }
      if (error && !error.success) {
        this.channelsContentErr = error;
        console.log("channelsContentErr: ", this.channelsContentErr);
      }
    };

    let dataObservable: any;
    let cacheKey: any;
    this.channelArr = [];
    this.channelArrCopy = [];

    if (this.connect_params.type == "operator") {
      dataObservable = Observable.fromPromise(
        this.channels.channel_operator_list(this.connect_params.id, {
          page: page,
          total: total
        })
      );
      cacheKey =
        this.storageService.cacheKey.MEDIA_CHANNELS +
        "_" +
        this.business.id +
        "_operator_" +
        this.connect_params.id +
        "_" +
        page;
    } else if (this.connect_params.type == "distributor") {
      dataObservable = Observable.fromPromise(
        this.channels.channel_distributor_list(this.connect_params.id, {
          page: page,
          total: total
        })
      );
      cacheKey =
        this.storageService.cacheKey.MEDIA_CHANNELS +
        "_" +
        this.business.id +
        "_distributor_" +
        this.connect_params.id +
        "_" +
        page;
    }

    this.cacheService.register(cacheKey, dataObservable).subscribe(cache => {
      this.channelCache = cache;
      this.zone.run(() => {
        this.channelArr$ = cache.get$;
      });
    });

    this.channelArr$.subscribe(successResponse, errorResponse, () => {
      this.content.resize();
    });
  }

  private loadCompanyBio() {
    const successResponse = (data: any) => {
      if (data && data.success) {
        const biog = data.data;
        const result = _.filter(biog, { size: "description" });
        _.each(result, (row: any) => {
          row.selected = false;
          row.paid = true;
        });

        const result2 = _.filter(biog, { size: "summary" });
        _.each(result2, (row: any) => {
          row.selected = false;
          row.paid = true;
        });

        this.zone.run(() => {
          this.descriptionArr = result;
          this.summaryArr = result2;
          console.log("this.descriptionArr: ", this.descriptionArr);
          console.log("this.summaryArr: ", this.summaryArr);
        });
      }
      this.showContent = true;
    };

    const errorResponse = error => {
      this.showContent = true;
      this.showContentErr = true;
      console.log("error: ", error);
      if (error && !error.success) {
        this.contentErr = error;
      }
    };

    let dataObservable: any;
    let cacheKey: any;
    this.descriptionArr = [];
    this.summaryArr = [];
    if (this.connect_params.type == "operator") {
      dataObservable = Observable.fromPromise(
        this.businesses.business_biography_operator(this.connect_params.id)
      );
      cacheKey =
        this.storageService.cacheKey.BUSINESS_BIOGRAPHY +
        "_" +
        this.business.id +
        "_operator_" +
        this.connect_params.id;
    } else if (this.connect_params.type == "distributor") {
    }

    this.cacheService.register(cacheKey, dataObservable).subscribe(cache => {
      this.businessBioCache = cache;
      this.zone.run(() => {
        this.businessBio$ = cache.get$;
      });
    });
    this.businessBio$.subscribe(successResponse, errorResponse);
  }

  private loadProducts(optId, refresher?: any) {
    const successResponse = (data: any) => {
      if (data && data.success && data.data[0] != false) {
        this.pageProduct = parseInt(data.data.currentPage);
        this.totalPageProduct = parseInt(data.data.totalPages);
        let products = data.data.data;

        this.zone.run(() => {
          for (let i = 0; i < _.size(products); i++) {
            products[i].selected = false;
            this.productArr.push(products[i]);
          }
        });
        this.productArrCopy = _.cloneDeep(this.productArr);
      } else if (data && data.success && data.data[0] == false) {
        this.showContentProducts = true;
        this.productsContentErr = {
          message: "No results found"
        };
      }
      this.showContentProducts = true;
      if (refresher) {
        refresher.complete();
      }
    };

    const errorResponse = (error: any) => {
      this.showContentProducts = true;
      this.showContentProductsErr = true;
      if (refresher) {
        refresher.complete();
      }
      if (error && !error.success) {
        this.productsContentErr = error;
      }
    };

    this.showContentProducts = false;
    this.productArr = [];
    this.productArrCopy = [];

    const dataObservable: any = Observable.fromPromise(
      this.products.operator_products(optId, {
        page: this.pageProduct,
        total: this.perPage
      })
    );

    const cacheKey =
      this.storageService.cacheKey.PRODUCT_LIST +
      "_" +
      this.business.id +
      "_operator_" +
      this.connect_params.id +
      "_" +
      this.pageProduct;

    this.cacheService.register(cacheKey, dataObservable).subscribe(cache => {
      this.productCache = cache;
      this.zone.run(() => {
        this.productArr$ = cache.get$;
      });
    });

    this.productArr$.subscribe(successResponse, errorResponse);
  }

  segmentChanged() {
    console.log("segmentChanged: ", this.imageAction);
    // this.showContent = false;
    this.myInput = "";

    this.content.scrollToTop();
    this.events.publish("filterWhat", "connected-" + this.imageAction);

    if (this.imageAction != "media") {
      this.showCheckbox = false;
      _.each(this.imagesArr, (row: any) => {
        row.selected = this.showCheckbox;
      });
    }
    this.content.resize();
  }

  printSegmentChanged() {
    let date1 = new Date();
    date1.setHours(0, 0, 0, 0);
    if (this.printstatus == "all") {
      this.zone.run(() => {
        this.printArr = _.cloneDeep(this.printCopyArr);
      });
    } else if (this.printstatus == "expired") {
      this.zone.run(() => {
        this.printArr = _.filter(this.printCopyArr, (row: any) => {
          let date2 = new Date(row.validityDate);
          date2.setHours(0, 0, 0, 0);
          return date2 <= date1;
        });
      });
    }
  }

  doRefresh(refresher: any) {
    if (this.imageAction === "media") {
      this.pageImage = 1;
      this.showContentImage = true;
      this.showContentImageErr = false;
      this.imagesArr = [];
      this.zone.run(() => {
        this.loadImage(this.pageImage, this.perPage, refresher);
      });
    } else if (this.imageAction === "logos") {
      this.pageLogo = 1;
      this.showContentLogo = true;
      this.showContentLogoErr = false;
      this.logosArr = [];
      this.zone.run(() => {
        this.loadLogos(this.pageLogo, this.perPage, refresher);
      });
    } else if (this.imageAction === "print") {
      this.pagePrint = 1;
      this.showContentPrint = true;
      this.showContentPrintErr = false;
      this.printArr = [];
      this.printCopyArr = [];
      this.zone.run(() => {
        this.loadPrints(this.pagePrint, this.perPage, refresher);
      });
    } else if (this.imageAction === "videos") {
      this.pageVideo = 1;
      this.showContentVideos = true;
      this.showContentVideosErr = false;
      this.videosArr = [];
      this.zone.run(() => {
        this.loadVideos(this.pageVideo, this.perPage, refresher);
      });
    } else if (this.imageAction === "albums") {
      this.pageAlbum = 1;
      this.showContentAlbums = true;
      this.showContentAlbumsErr = false;
      this.albumsArr = [];
      this.zone.run(() => {
        this.loadAlbums(this.pageAlbum, this.perPage, refresher);
      });
    } else if (this.imageAction === "collection" && this.business.type === 2) {
      this.pageCollection = 1;
      this.showContentCollections = true;
      this.showContentCollectionsErr = false;
      this.collectionArr = [];
      this.zone.run(() => {
        this.loadCollections(this.pageCollection, this.perPage, refresher);
      });
    } else if (this.imageAction === "channels" && this.business.type === 2) {
      this.pageChannel = 1;
      this.showContentChannels = true;
      this.showContentChannelsErr = false;
      this.channelArr = [];
      this.zone.run(() => {
        this.loadChannels(this.pageChannel, this.perPage, refresher);
      });
    } else if (this.imageAction === "company" && this.business.type === 1) {
      this.showContent = true;
      this.showContentErr = false;
      this.descriptionArr = [];
      this.summaryArr = [];

      this.zone.run(() => {
        this.refreshSubscription = this.businessBioCache
          .refresh()
          .finally(() => (this.refreshSubscription = null))
          .subscribe(
            () => {
              if (refresher) {
                refresher.complete();
              }
            },
            err => {
              console.error("Something went wrong!", err);
              if (refresher) {
                refresher.complete();
              }
              throw err;
            }
          );
      });
    } else if (this.imageAction == "products" && this.business.type == 2) {
      this.pageProduct = 1;
      this.showContentProducts = true;
      this.showContentProductsErr = false;
      this.productArr = [];
      this.productArrCopy = [];

      this.zone.run(() => {
        this.loadProducts(this.connect_params.id, refresher);
      });
    }
    this.content.resize();
  }

  doInfinite(refresher: any) {
    if (this.imageAction === "media") {
      this.pageImage = this.pageImage + 1;
      if (this.pageImage <= this.totalPageImage) {
        this.loadImage(this.pageImage, this.perPage, refresher);
      } else {
        refresher.complete();
      }
    } else if (this.imageAction === "logos") {
      this.pageLogo = this.pageLogo + 1;
      if (this.pageLogo <= this.totalPageLogo) {
        this.loadLogos(this.pageLogo, this.perPage, refresher);
      } else {
        refresher.complete();
      }
    } else if (this.imageAction === "print") {
      this.pagePrint = this.pagePrint + 1;
      if (this.pagePrint <= this.totalPagePrint) {
        this.loadPrints(this.pagePrint, this.perPage, refresher);
      } else {
        refresher.complete();
      }
    } else if (this.imageAction === "videos") {
      this.pageVideo = this.pageVideo + 1;
      if (this.pageVideo <= this.totalPageVideo) {
        this.loadVideos(this.pageVideo, this.perPage, refresher);
      } else {
        refresher.complete();
      }
    } else if (this.imageAction === "albums") {
      this.pageAlbum = this.pageAlbum + 1;
      if (this.pageAlbum <= this.totalPageAlbum) {
        this.loadAlbums(this.pageAlbum, this.perPage, refresher);
      } else {
        refresher.complete();
      }
    } else if (this.imageAction === "collection") {
      this.pageCollection = this.pageCollection + 1;
      if (this.pageCollection <= this.totalPageCollection) {
        this.loadCollections(this.pageCollection, this.perPage, refresher);
      } else {
        refresher.complete();
      }
    } else if (this.imageAction === "channels") {
      this.pageChannel = this.pageChannel + 1;
      if (this.pageChannel <= this.totalPageChannel) {
        this.loadChannels(this.pageChannel, this.perPage, refresher);
      } else {
        refresher.complete();
      }
    } else if (this.imageAction === "products") {
      this.pageProduct = this.pageProduct + 1;
      if (this.pageProduct <= this.totalPageProduct) {
        this.loadProducts(this.connect_params.id, refresher);
      } else {
        refresher.complete();
      }
    } else {
      refresher.complete();
    }
  }

  searchMedia(val, type) {
    const onSuccess = (data: any) => {
      if (data && data.success) {
        if (type === "media") {
          this.imagesArr = [];
          this.pageImage = parseInt(data.data.currentPage);
          this.totalPageImage = parseInt(data.data.totalPages);

          this.zone.run(() => {
            for (let i = 0; i < _.size(data.data.results); i++) {
              data.data.results[i].selected = false;
              if (_.isEmpty(data.data.results[i].caption)) {
                data.data.results[i].caption = "No Caption";
              }
              if (_.isEmpty(data.data.results[i].location)) {
                data.data.results[i].location = "No Location";
              }
              this.imagesArr.push(data.data.results[i]);
            }
          });
        } else if (type === "logos") {
          this.logosArr = [];
          this.pageLogo = parseInt(data.data.currentPage);
          this.totalPageLogo = parseInt(data.data.totalPages);

          this.zone.run(() => {
            for (let i = 0; i < _.size(data.data.results); i++) {
              data.data.results[i].selected = false;
              this.logosArr.push(data.data.results[i]);
            }
          });
        } else if (type === "print") {
          this.printArr = [];
          this.pagePrint = parseInt(data.data.currentPage);
          this.totalPagePrint = parseInt(data.data.totalPages);

          this.zone.run(() => {
            for (let i = 0; i < _.size(data.data.results); i++) {
              data.data.results[i].selected = false;
              if (_.isEmpty(data.data.results[i].brochure_caption)) {
                data.data.results[i].brochure_caption = "No caption";
              }
              this.printArr.push(data.data.results[i]);
            }
          });
          this.printCopyArr = _.cloneDeep(this.printArr);
        } else if (type === "videos") {
          this.videosArr = [];
          this.pageVideo = parseInt(data.data.currentPage);
          this.totalPageVideo = parseInt(data.data.totalPages);

          this.zone.run(() => {
            for (let i = 0; i < _.size(data.data.results); i++) {
              data.data.results[i].selected = false;
              if (_.isEmpty(data.data.results[i].caption)) {
                data.data.results[i].caption = "No caption";
              }
              this.videosArr.push(data.data.results[i]);
            }
          });
        } else if (type === "albums") {
          this.albumsArr = [];
          this.pageAlbum = parseInt(data.data.currentPage);
          this.totalPageAlbum = parseInt(data.data.totalPages);

          this.zone.run(() => {
            for (let i = 0; i < _.size(data.data.results); i++) {
              data.data.results[i].selected = false;
              if (_.isEmpty(data.data.results[i].title)) {
                data.data.results[i].title = "No caption";
              }
              this.albumsArr.push(data.data.results[i]);
            }
          });
        } else if (type === "collection") {
          this.collectionArr = [];
          this.pageCollection = parseInt(data.data.page);
          this.totalPageCollection = parseInt(data.data.totalPages);

          this.zone.run(() => {
            for (let i = 0; i < _.size(data.data.results); i++) {
              data.data.results[i].selected = false;
              this.collectionArr.push(data.data.results[i]);
            }
          });
        } else if (type === "channels") {
          this.channelArr = [];
          this.pageChannel = parseInt(data.data.page);
          this.totalPageChannel = parseInt(data.data.totalPages);

          this.zone.run(() => {
            for (let i = 0; i < _.size(data.data.results); i++) {
              data.data.results[i].selected = false;
              this.channelArr.push(data.data.results[i]);
            }
          });
        }
        this.showContent = true;
      }
    };

    const onError = () => {
      if (type === "media") {
        this.showContentImage = true;
      } else if (type === "logos") {
        this.showContentLogo = true;
      } else if (type === "print") {
        this.showContentPrint = true;
      } else if (type === "videos") {
        this.showContentVideos = true;
      } else if (type === "albums") {
        this.showContentAlbums = true;
      } else if (type === "collection") {
        this.showContentCollections = true;
      } else if (type === "channels") {
        this.showContentChannels = true;
      }
    };

    const onComplete = () => {
      if (type === "media") {
        this.showContentImage = true;
      } else if (type === "logos") {
        this.showContentLogo = true;
      } else if (type === "print") {
        this.showContentPrint = true;
      } else if (type === "videos") {
        this.showContentVideos = true;
      } else if (type === "albums") {
        this.showContentAlbums = true;
      } else if (type === "collection") {
        this.showContentCollections = true;
      } else if (type === "channels") {
        this.showContentChannels = true;
      }
    };

    if (type === "media") {
      this.showContentImage = false;
      if (this.connect_params.type == "distributor") {
        let params: any = {};
        params.media = "image";
        if (!_.isEmpty(this.filters.category)) {
          params.category = this.filters.category.title;
        }
        if (this.filters.location) {
          params.location = this.filters.location.name;
        }
        if (this.filters.creator) {
          params.contributor = this.filters.creator.name;
        }
        params.keyword = val;

        console.log("params: ", params);
        this.images
          .search_connected_media(this.connect_params.id, params)
          .then(onSuccess, onError)
          .then(onComplete);
      } else {
        this.images
          .search_media({
            media: "image",
            keyword: val
          })
          .then(onSuccess, onError)
          .then(onComplete);
      }
    } else if (type === "logos") {
      this.showContentLogo = false;
      this.images
        .search_media({
          media: "logos",
          keyword: val
        })
        .then(onSuccess, onError)
        .then(onComplete);
    } else if (type === "print") {
      this.showContentPrint = false;
      this.images
        .search_media({
          media: "print",
          keyword: val
        })
        .then(onSuccess, onError)
        .then(onComplete);
    } else if (type === "videos") {
      this.showContentVideos = false;
      this.images
        .search_media({
          media: "videos",
          keyword: val
        })
        .then(onSuccess, onError)
        .then(onComplete);
    } else if (type === "albums") {
      this.showContentAlbums = false;
      this.images
        .search_media({
          media: "albums",
          keyword: val
        })
        .then(onSuccess, onError)
        .then(onComplete);
    } else if (type === "collection") {
      this.showContentCollections = false;
      this.images
        .search_media({
          media: "collections",
          keyword: val
        })
        .then(onSuccess, onError)
        .then(onComplete);
    } else if (type === "channels") {
      this.showContentChannels = false;
      this.images
        .search_media({
          media: "channels",
          keyword: val
        })
        .then(onSuccess, onError)
        .then(onComplete);
    }
  }

  viewImageDetail(item) {
    this.navCtrl.push(ImageDetailPage, {
      image: item,
      action: "connect",
      params: this.connect_params
    });
  }

  viewVideoDetail(item) {
    this.navCtrl.push(VideoDetailPage, {
      video: item,
      action: "connect",
      params: this.connect_params
    });
  }

  viewPrintDetail(item) {
    this.navCtrl.push(PrintDetailPage, {
      print: item,
      action: "connect",
      params: this.connect_params
    });
  }

  viewLogoDetail(item) {
    this.navCtrl.push(LogoDetailPage, {
      logo: item,
      action: "connect",
      params: this.connect_params
    });
  }

  viewAlbums() {
    this.navCtrl.push(AlbumsPage);
  }

  viewAlbumImages(item: any) {
    this.navCtrl.push(AlbumGalleryPage, {
      id: item.id,
      album: item.title,
      action: "connect",
      params: this.connect_params
    });
  }

  viewCollections(item: any) {
    this.navCtrl.push(CollectionsPage, {
      item: item,
      action: "connect",
      params: this.connect_params
    });
  }

  viewChannels(item: any) {
    this.navCtrl.push(ChannelsPage, {
      item: item,
      action: "connect",
      params: this.connect_params
    });
  }

  viewProductDetails(item) {
    this.navCtrl.push(ProductDetailPage, {
      product: item,
      action: "connect",
      params: this.connect_params
    });
  }

  selectDescription(item: any) {
    _.each(this.descriptionArr, (row: any) => {
      if (item.id == row.id) {
        row.selected = !item.selected;
      } else {
        row.selected = false;
      }
    });
  }

  selectSummary(item: any) {
    _.each(this.summaryArr, (row: any) => {
      if (item.id == row.id) {
        row.selected = !item.selected;
      } else {
        row.selected = false;
      }
    });
  }

  viewFilter() {
    this.menuCtrl.toggle("right");
  }

  filterImage() {
    const params: any = {};
    params.media = "image";

    if (!_.isEmpty(this.filters.category)) {
      params.category = this.filters.category.title;
    }
    if (this.filters.location) {
      params.location = this.filters.location.name;
    }
    if (this.filters.creator) {
      params.contributor = this.filters.creator.name;
    }
    console.log("params: ", params);

    this.showContentImage = false;
    this.images.search_connected_media(this.connect_params.id, params).then(
      (data: any) => {
        if (data && data.success && data.data[0] != false) {
          let imagesArr: any[] = [];
          imagesArr = data.data.results;
          _.each(imagesArr, (row: any) => {
            if (_.isEmpty(row.caption)) {
              row.caption = "No Caption";
            }
            if (_.isEmpty(row.location)) {
              row.location = "No Location";
            }
          });
          console.log("this.imagesArr: ", imagesArr);
          this.imagesArr = imagesArr;
          this.imagesArrCopy = imagesArr;
        } else if (data && data.success && data.data.error) {
          this.alertCtrl
            .create({
              title: "Error",
              message: data.data.msg,
              buttons: ["OK"]
            })
            .present();
          return;
        }
        this.showContentImage = true;
      },
      error => {
        this.showContentImage = true;
      }
    );
  }

  viewSettings() {
    if (this.business.role !== 1) {
      const toast = this.toastCtrl.create({
        message: "Invalid Access!!!",
        duration: 3000
      });
      toast.present();
      return;
    }

    let buttonsArr: any[] = [
      {
        text: "Send Notification",
        handler: () => {
          this.viewOperatorSettings(this.connect_user);
        }
      },
      {
        text: "Cancel",
        role: "cancel",
        handler: () => {
          console.log("Cancel clicked");
        }
      }
    ];

    if (this.connect_params.type == "operator") {
      buttonsArr.unshift({
        text: "Assigned Platform",
        handler: () => {
          console.log("Cancel clicked");
          this.viewPlatformSettings(this.connect_user);
        }
      });
    }

    const actionSheet = this.actionSheetCtrl.create({
      title: "Select what to do with " + this.connect_params.business + "?",
      buttons: buttonsArr
    });
    actionSheet.present();
  }

  viewPlatformSettings(user: any) {
    if (this.business.role !== 1) {
      const toast = this.toastCtrl.create({
        message: "Invalid Access!!!",
        duration: 3000
      });
      toast.present();
      return;
    }

    let modal = this.modalCtrl.create(
      BYOBConnectedPlatformPage,
      {
        user: user
      },
      { cssClass: "inset-modal" }
    );

    modal.onDidDismiss(resp => {
      if (resp) {
      }
    });
    modal.present();
  }

  viewOperatorSettings(user: any) {
    if (this.business.role !== 1) {
      const toast = this.toastCtrl.create({
        message: "Invalid Access!!!",
        duration: 3000
      });
      toast.present();
      return;
    }

    let modal = this.modalCtrl.create(
      OperatorRequestPage,
      {
        user: user
      },
      { cssClass: "inset-modal" }
    );

    modal.onDidDismiss(resp => {
      if (resp) {
      }
    });
    modal.present();
  }
}
