import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { ImagesTabPage, ImageTabsPopoverPage } from "./images-tab";
import { ConnectMediaPage } from "./connect-media";
import { IonicImageViewerModule } from "ionic-img-viewer";
import { CacheModule } from "ionic-cache-observable";
import { IonicImageLoader } from "ionic-image-loader";

import { ComponentsModule } from "../../components/components.module";
import { PipesModule } from "../../pipes/pipes.module";

import { ImageTabComponent } from "./components/image/image-component";
import { LogoTabComponent } from "./components/logo/logo-component";
import { PrintTabComponent } from "./components/print/print-component";
import { VideoTabComponent } from "./components/video/video-component";
import { AlbumsTabComponent } from "./components/albums/albums-component";
import { CollectionTabComponent } from "./components/collection/collection-component";
import { ChannelsTabComponent } from "./components/channels/channels-component";
import { CompanyTabComponent } from "./components/company/company-component";
import { ProductsTabComponent } from "./components/products/products-component";

@NgModule({
  declarations: [
    ImagesTabPage,
    ImageTabsPopoverPage,
    ConnectMediaPage,
    ImageTabComponent,
    LogoTabComponent,
    PrintTabComponent,
    VideoTabComponent,
    AlbumsTabComponent,
    CollectionTabComponent,
    ChannelsTabComponent,
    CompanyTabComponent,
    ProductsTabComponent
  ],
  imports: [
    IonicPageModule.forChild(ImagesTabPage),
    IonicImageViewerModule,
    PipesModule,
    ComponentsModule,
    CacheModule,
    IonicImageLoader
  ],
  entryComponents: [
    ImageTabsPopoverPage,
    ConnectMediaPage,
    ImageTabComponent,
    LogoTabComponent,
    PrintTabComponent,
    VideoTabComponent,
    AlbumsTabComponent,
    CollectionTabComponent,
    ChannelsTabComponent,
    CompanyTabComponent,
    ProductsTabComponent
  ]
})
export class ImagesTabPageModule {}
