import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { ReactiveFormsModule } from "@angular/forms";
import { IonTagsInputModule } from "ionic-tags-input";
import { IonicImageViewerModule } from "ionic-img-viewer";
import { JoditAngularModule } from "jodit-angular";

import { PipesModule } from "../../pipes/pipes.module";

import { ProductsEntryPage } from "./products-entry";
import {
  ProductEntrySummaryComponent,
  ProductEntryDetailsComponent,
  ProductEntryGalleryComponent,
  ProductEntrySettingsComponent
} from "./components/product-entry.components";

@NgModule({
  declarations: [
    ProductsEntryPage,
    ProductEntrySummaryComponent,
    ProductEntryDetailsComponent,
    ProductEntryGalleryComponent,
    ProductEntrySettingsComponent
  ],
  imports: [
    IonTagsInputModule,
    JoditAngularModule,
    ReactiveFormsModule,
    IonicPageModule.forChild(ProductsEntryPage),
    IonicImageViewerModule,
    PipesModule
  ],
  entryComponents: [
    ProductEntrySummaryComponent,
    ProductEntryDetailsComponent,
    ProductEntryGalleryComponent,
    ProductEntrySettingsComponent
  ]
})
export class ProductsEntryPageModule {}
