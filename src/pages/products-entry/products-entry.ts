import { Component, ViewEncapsulation } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  AlertController,
  ToastController,
  ModalController,
  ViewController
} from "ionic-angular";
import * as _ from "lodash";
import * as async from "async";
import * as $ from "jquery";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from "@angular/forms";

import { FileUploadPage } from "../../pages/file-upload/file-upload";

import { ProductsServicesProvider } from "../../providers/services/products";
import { StorageProvider } from "../../providers/storage/storage";

@IonicPage()
@Component({
  selector: "page-products-entry",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "products-entry.html"
})
export class ProductsEntryPage {
  product: any = {};
  item: any = {};
  business: any = {};
  features: any = {};
  action: string = "info";
  mode: string;
  title: string;

  newDescriptionArr: any[] = [];
  newSummaryArr: any[] = [];

  callback: any;
  textEditorConfig: any = {};

  productForm: FormGroup;

  isSaving = false;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public modalCtrl: ModalController,
    public viewCtrl: ViewController,
    public products: ProductsServicesProvider,
    public storageService: StorageProvider
  ) {
    this.item = navParams.get("product");
    this.mode = navParams.get("action");
    this.callback = navParams.get("callback");

    this.product.keywordsArr = [];
    this.product.settingsLibrary = false;
    this.product.settingsBookable = false;

    if (this.mode == "create") {
      this.title = "Create Product";
    } else if (this.mode == "update") {
      this.title = "Update Product";
    }

    this.textEditorConfig = {
      buttons: "bold,strikethrough,underline,italic"
    };

    this.productForm = this.formBuilder.group({
      title: ["", Validators.required],
      summaryEnglish: [""],
      summaryChinese: [""]
    });
  }

  get f(): any {
    return this.productForm.controls;
  }

  private validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  private initializeData(event?: any) {
    let loading = this.loadingCtrl.create();
    if (_.isEmpty(event)) {
      loading.present();
    }

    this.products.product_details(this.item.productId).then(
      (data: any) => {
        if (data && data.success) {
          this.product = data.data;
          console.log("product: ", this.product);

          if (this.product.description) {
            let descriptionArr = this.product.description.description;
            let summaryArr = this.product.description.summary;

            if (!_.isEmpty(descriptionArr)) {
              _.each(descriptionArr, (row: any) => {
                let arr: any = Object.keys(row).map(key => ({
                  key,
                  value: row[key],
                  expanded: false,
                  action: "desc",
                  mode: "desc_" + key
                }));
                this.newDescriptionArr.push(arr[0]);
              });
              console.log("this.newDescriptionArr: ", this.newDescriptionArr);
            }

            if (!_.isEmpty(summaryArr)) {
              _.each(summaryArr, (row: any) => {
                let arr: any = Object.keys(row).map(key => ({
                  key,
                  value: row[key],
                  expanded: false,
                  action: "sum",
                  mode: "sum_" + key
                }));
                this.newSummaryArr.push(arr[0]);
              });
              console.log("this.newSummaryArr: ", this.newSummaryArr);
            }
          }

          if (this.product.additionalInformation) {
            this.product.startTime = this.product.additionalInformation.startTime;
            this.product.endTime = this.product.additionalInformation.endTime;
            this.product.duration = this.product.additionalInformation.operatingHours;
            this.product.transportText = this.product.additionalInformation.transfer;
            this.product.purchasesText = this.product.additionalInformation.purchases;
            this.product.packingText = this.product.additionalInformation.packing;
            this.product.healthText = this.product.additionalInformation.fitness;
            this.product.childrenText = this.product.additionalInformation.child;
            this.product.additionalText = this.product.additionalInformation.additional;
            this.product.termsText = this.product.additionalInformation.terms;
          }

          this.product.price = this.product.avgPrice;
          this.product.bookingLink = this.product.directBooking;
          this.product.settingsBookable = this.product.bookable;
          this.product.settingsLibrary = this.product.access;
          if (this.product.keywords) {
            this.product.keywordsArr = this.product.keywords.split(",");
          } else {
            this.product.keywordsArr = [];
          }

          // ======================================================
          // FEATURES
          // ======================================================
          this.features = this.product.features;
          if (this.features) {
            // ==================================================================
            // ==================================================================
            // ==================================================================
            this.features.disabledAccess =
              this.features.disabledAccess == 1 ? true : false;
            this.features.giftShop = this.features.giftShop == 1 ? true : false;
            this.features.storageLockers =
              this.features.storageLockers == 1 ? true : false;
            this.features.visitorInformation =
              this.features.visitorInformation == 1 ? true : false;
            this.features.dinnerIncluded =
              this.features.dinnerIncluded == 1 ? true : false;
            this.features.internetAccess =
              this.features.internetAccess == 1 ? true : false;
            this.features.media = this.features.media == 1 ? true : false;
            this.features.functionHire =
              this.features.functionHire == 1 ? true : false;
            this.features.hotelPickupService =
              this.features.hotelPickupService == 1 ? true : false;
            this.features.mealProvided =
              this.features.mealProvided == 1 ? true : false;
            this.features.guidedTours =
              this.features.guidedTours == 1 ? true : false;
            this.features.weddingServices =
              this.features.weddingServices == 1 ? true : false;
            this.features.breakfestIncluded =
              this.features.breakfestIncluded == 1 ? true : false;
            this.features.cafe = this.features.cafe == 1 ? true : false;
            this.features.languageTranslation =
              this.features.languageTranslation == 1 ? true : false;
            this.features.conferenceServices =
              this.features.conferenceServices == 1 ? true : false;
            this.features.lunchIncluded =
              this.features.lunchIncluded == 1 ? true : false;
            // ==================================================================
            // ==================================================================
            // ==================================================================
            this.features["24HourDesk"] =
              this.features["24HourDesk"] == 1 ? true : false;
            this.features.bar = this.features.bar == 1 ? true : false;
            this.features.conciergeService =
              this.features.conciergeService == 1 ? true : false;
            this.features.conferenceServices =
              this.features.conferenceServices == 1 ? true : false;
            this.features.expressCheckIn =
              this.features.expressCheckIn == 1 ? true : false;
            this.features.expressCheckOut =
              this.features.expressCheckOut == 1 ? true : false;
            this.features.fitnessCentre =
              this.features.fitnessCentre == 1 ? true : false;
            this.features.kitchenFacilities =
              this.features.kitchenFacilities == 1 ? true : false;
            this.features.laundryFacilities =
              this.features.laundryFacilities == 1 ? true : false;
            this.features.meetingFacilities =
              this.features.meetingFacilities == 1 ? true : false;
            this.features.nonSmoking =
              this.features.nonSmoking == 1 ? true : false;
            this.features.parkingAccess =
              this.features.parkingAccess == 1 ? true : false;
            this.features.restaurant =
              this.features.restaurant == 1 ? true : false;
            this.features.roomService =
              this.features.roomService == 1 ? true : false;
            this.features.safeDeposit =
              this.features.safeDeposit == 1 ? true : false;
            this.features.swimmingPool =
              this.features.swimmingPool == 1 ? true : false;
            this.features.tourBookingService =
              this.features.tourBookingService == 1 ? true : false;
            this.features.wifiAccess =
              this.features.wifiAccess == 1 ? true : false;
          } else {
            this.features = {};

            this.features.disabledAccess = false;
            this.features.giftShop = false;
            this.features.storageLockers = false;
            this.features.visitorInformation = false;
            this.features.dinnerIncluded = false;
            this.features.internetAccess = false;
            this.features.media = false;
            this.features.functionHire = false;
            this.features.hotelPickupService = false;
            this.features.mealProvided = false;
            this.features.guidedTours = false;
            this.features.weddingServices = false;
            this.features.breakfestIncluded = false;
            this.features.cafe = false;
            this.features.languageTranslation = false;
            this.features.conferenceServices = false;
            this.features.lunchIncluded = false;
            // ==================================================================
            // ==================================================================
            // ==================================================================
            this.features["24HourDesk"] = false;
            this.features.bar = false;
            this.features.conciergeService = false;
            this.features.conferenceServices = false;
            this.features.expressCheckIn = false;
            this.features.expressCheckOut = false;
            this.features.fitnessCentre = false;
            this.features.kitchenFacilities = false;
            this.features.laundryFacilities = false;
            this.features.meetingFacilities = false;
            this.features.nonSmoking = false;
            this.features.parkingAccess = false;
            this.features.restaurant = false;
            this.features.roomService = false;
            this.features.safeDeposit = false;
            this.features.swimmingPool = false;
            this.features.tourBookingService = false;
            this.features.wifiAccess = false;
          }
          // ======================================================
          // FEATURES
          // ======================================================

          setTimeout(() => {
            $(".progressive-image").each(function() {
              console.log("progressive-image: ");
              var image = new Image();
              var previewImage = $(this).find(".loadingImage");
              var newImage = $(this).find(".overlay");

              image.onload = function() {
                newImage.css("background-image", "url(" + image.src + ")");
                newImage.css("opacity", "1");
                console.log("complete");
              };
              image.onerror = function() {
                newImage.css("background-image", "url(./assets/img/thumb.jpg)");
                newImage.css("opacity", "1");
                console.log("complete");
              };
              image.src = previewImage.data("image");
            });
          }, 1000);
        }
        loading.dismiss();
        if (event) {
          event.complete();
        }
      },
      error => {
        loading.dismiss();
        if (event) {
          event.complete();
        }
      }
    );
  }

  async ionViewDidLoad() {
    console.log("ionViewDidLoad ProductsEntryPage");
    const business = await this.storageService.getStorage(
      this.storageService.storageKey.CURRENT_BUSINESS
    );
    this.business = !_.isEmpty(business) ? JSON.parse(business) || {} : {};

    if (this.mode == "update") {
      this.initializeData();
    }
  }

  setFeature(type) {
    let title: string;
    let action: string;
    if (this.business.type == 1 && this.business.role !== 1) {
      let toast = this.toastCtrl.create({
        message: "Invalid Access!!!",
        duration: 3000
      });
      toast.present();
      return;
    }

    if (type == "image") {
      title = "Set Feature Image";
      action = "feature_image";
    } else if (type == "video") {
      title = "Set Feature Video";
      action = "feature_video";
    } else if (type == "print") {
      title = "Set Feature Print Item";
      action = "feature_print";
    } else if (type == "logo") {
      title = "Set Feature Logo";
      action = "feature_logo";
    }

    let modal = this.modalCtrl.create(FileUploadPage, {
      action: action,
      title: title,
      params: this.product
    });
    modal.onDidDismiss(resp => {
      console.log("resp: ", resp);
      if (resp == "save") {
        this.initializeData();
      }
    });
    modal.present();
  }

  uploadFile() {
    let title: string;

    if (this.business.type == 1 && this.business.role !== 1) {
      let toast = this.toastCtrl.create({
        message: "Invalid Access!!!",
        duration: 3000
      });
      toast.present();
      return;
    }

    if (this.action === "gallery") {
      title = "Upload Image";
    }

    let modal = this.modalCtrl.create(FileUploadPage, {
      action: this.action,
      title: title,
      params: this.product
    });
    modal.onDidDismiss(resp => {
      if (this.action === "gallery" && resp == "save") {
        this.initializeData();
      }
    });
    modal.present();
  }

  private updateProduct() {
    let loading = this.loadingCtrl.create({
      dismissOnPageChange: true,
      content: "Updating product..."
    });
    loading.present();

    if (this.product.keywordsArr) {
      this.product.keywords = this.product.keywordsArr.join(",");
    }

    async.waterfall(
      [
        callback => {
          this.products
            .product_edit_title({
              productId: this.product.productId,
              title: this.product.title
            })
            .then(
              (data: any) => {
                if (data && data.success) {
                  let toast = this.toastCtrl.create({
                    message: data.data,
                    duration: 2000,
                    position: "top"
                  });
                  toast.onDidDismiss(() => {
                    callback();
                  });
                  toast.present();
                } else {
                  callback();
                }
              },
              (error: any) => {
                loading.dismiss();
                console.log("error: ", error);
                callback();
              }
            );
        },
        callback => {
          this.products.product_edit(this.product).then(
            (data: any) => {
              if (data && data.success) {
                let toast = this.toastCtrl.create({
                  message: data.data,
                  duration: 2000,
                  position: "top"
                });
                toast.onDidDismiss(() => {
                  callback();
                });
                toast.present();
              } else {
                callback();
              }
            },
            error => {
              loading.dismiss();
              console.log("error: ", error);
              callback();
            }
          );
        },
        callback => {
          loading.setContent("Updating feature...");
          let featuresObj: any = {};

          if (this.product.category) {
            if (this.product.category == "service") {
              featuresObj = {
                productId: this.product.productId,
                disabilityAccess: this.features.disabledAccess,
                internet: this.features.internetAccess,
                foodIncluded: this.features.mealProvided,
                cafe: this.features.cafe,
                giftShop: this.features.giftShop,
                mediaService: this.features.media,
                guidedTours: this.features.guidedTours,
                translation: this.features.languageTranslation,
                lockers: this.features.storageLockers,
                functions: this.features.functionHire,
                weddings: this.features.weddingServices,
                conference: this.features.conferenceServices,
                vistiorInformation: this.features.visitorInformation,
                pickUp: this.features.hotelPickupService,
                breakfast: this.features.breakfestIncluded,
                lunch: this.features.lunchIncluded,
                dinner: this.features.dinnerIncluded
              };
            } else if (this.product.category == "accommodation") {
              featuresObj = {
                productId: this.product.productId,
                expressCheckIn: this.features.expressCheckIn,
                expressCheckOut: this.features.expressCheckOut,
                desk24hr: this.features["24HourDesk"],
                concierge: this.features.conciergeService,
                tourDesk: this.features.tourBookingService,
                fitness: this.features.fitnessCentre,
                parking: this.features.parkingAccess,
                meetingFacilities: this.features.meetingFacilities,
                laundry: this.features.laundryFacilities,
                restaurant: this.features.restaurant,
                roomService: this.features.roomService,
                kitchen: this.features.kitchenFacilities,
                internet: this.features.internetAccess,
                wifi: this.features.wifiAccess,
                nonsmoking: this.features.nonSmoking,
                disabilityAccess: this.features.disabledAccess,
                safe: this.features.safeDeposit,
                weddings: this.features.weddingServices,
                conference: this.features.conferenceServices,
                bar: this.features.bar,
                swimming: this.features.swimmingPool
              };
            }
          } else {
            featuresObj = {
              productId: this.product.productId,
              expressCheckIn: this.features.expressCheckIn,
              expressCheckOut: this.features.expressCheckOut,
              desk24hr: this.features["24HourDesk"],
              concierge: this.features.conciergeService,
              tourDesk: this.features.tourBookingService,
              fitness: this.features.fitnessCentre,
              parking: this.features.parkingAccess,
              meetingFacilities: this.features.meetingFacilities,
              laundry: this.features.laundryFacilities,
              restaurant: this.features.restaurant,
              roomService: this.features.roomService,
              kitchen: this.features.kitchenFacilities,
              internet: this.features.internetAccess,
              wifi: this.features.wifiAccess,
              nonsmoking: this.features.nonSmoking,
              disabilityAccess: this.features.disabledAccess,
              safe: this.features.safeDeposit,
              weddings: this.features.weddingServices,
              conference: this.features.conferenceServices,
              bar: this.features.bar,
              swimming: this.features.swimmingPool,
              foodIncluded: this.features.mealProvided,
              cafe: this.features.cafe,
              giftShop: this.features.giftShop,
              mediaService: this.features.media,
              guidedTours: this.features.guidedTours,
              translation: this.features.languageTranslation,
              lockers: this.features.storageLockers,
              functions: this.features.functionHire,
              vistiorInformation: this.features.visitorInformation,
              pickUp: this.features.hotelPickupService,
              breakfast: this.features.breakfestIncluded,
              lunch: this.features.lunchIncluded,
              dinner: this.features.dinnerIncluded
            };
          }

          this.products.product_edit_features(featuresObj).then(
            (data: any) => {
              if (data && data.success) {
                let toast = this.toastCtrl.create({
                  message: data.data,
                  duration: 2000,
                  position: "top"
                });
                toast.onDidDismiss(() => {
                  callback();
                });
                toast.present();
              } else {
                callback();
              }
            },
            error => {
              loading.dismiss();
              console.log("error: ", error);
            }
          );
        }
      ],
      () => {
        loading.dismiss();
        // this.callback('save').then(() => { this.navCtrl.pop() });
        this.viewCtrl.dismiss("save");
      }
    );
  }

  private saveProduct() {
    let loading = this.loadingCtrl.create({
      dismissOnPageChange: true,
      content: "Saving product..."
    });
    loading.present();
    async.waterfall(
      [
        callback => {
          this.products
            .product_create({
              title: this.product.title
            })
            .then(
              (data: any) => {
                if (data && data.success) {
                  this.product.productId = data.data.productId;
                  this.product.id = data.data.id;
                  callback();
                } else if (data && !data.success) {
                  let toast = this.alertCtrl.create({
                    title: "WARNING",
                    message: data.message,
                    buttons: ["OK"]
                  });
                  toast.onDidDismiss(() => {
                    loading.dismiss();
                  });
                  toast.present();
                } else {
                  callback();
                }
              },
              error => {
                loading.dismiss();
                console.log("error: ", error);
                callback();
              }
            );
        },
        callback => {
          if (this.product.keywordsArr) {
            this.product.keywords = this.product.keywordsArr.join(",");
          }
          this.products.product_edit(this.product).then(
            (data: any) => {
              if (data && data.success) {
                let toast = this.toastCtrl.create({
                  message: data.data,
                  duration: 2000,
                  position: "top"
                });
                toast.onDidDismiss(() => {
                  callback();
                });
                toast.present();
              } else if (data && !data.success) {
                let toast = this.alertCtrl.create({
                  title: "WARNING",
                  message: data.message,
                  buttons: ["OK"]
                });
                toast.onDidDismiss(() => {
                  loading.dismiss();
                });
                toast.present();
              } else {
                callback();
              }
            },
            error => {
              loading.dismiss();
              console.log("error: ", error);
              callback();
            }
          );
        }
      ],
      () => {
        loading.dismiss();
        // this.callback('save').then(() => { this.navCtrl.pop() });
        this.viewCtrl.dismiss("save");
      }
    );
  }

  saveEntry() {
    _.each(this.newDescriptionArr, (row: any) => {
      this.product["description" + _.startCase(row.key)] = row.value.text;
    });

    _.each(this.newSummaryArr, (row: any) => {
      this.product["summary" + _.startCase(row.key)] = row.value.text;
    });
    console.log("products: ", this.product);

    const confirm = this.alertCtrl.create({
      title: "Save Entry?",
      message: "Do you want to save changes?",
      buttons: [
        {
          text: "No",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Yes",
          handler: () => {
            if (this.mode == "update") {
              this.updateProduct();
            } else if (this.mode == "create") {
              this.saveProduct();
            }
          }
        }
      ]
    });
    confirm.present();
  }
  dismiss() {
    this.viewCtrl.dismiss();
  }
}
