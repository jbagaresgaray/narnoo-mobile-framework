import { Component, Input, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "product-entry-summary-component",
  templateUrl: "product-entry-summary-component.html"
})
export class ProductEntrySummaryComponent {
  @Input() features: any = {};
  @Input() product: any = {};
  @Input() mode: string;
  @Input() textEditorConfig: any = {};
  @Input() newSummaryArr: any[] = [];
  @Input() newDescriptionArr: any[] = [];
}

@Component({
  selector: "product-entry-details-component",
  templateUrl: "product-entry-details-component.html"
})
export class ProductEntryDetailsComponent {
  @Input() product: any = {};
  @Input() textEditorConfig: any = {};
  @Output() _setFeature = new EventEmitter<any>();

  setFeature(type) {
    this._setFeature.emit(type);
  }
}

@Component({
  selector: "product-entry-gallery-component",
  templateUrl: "product-entry-gallery-component.html"
})
export class ProductEntryGalleryComponent {
  @Input() product: any = {};
}

@Component({
  selector: "product-entry-settings-component",
  templateUrl: "product-entry-settings-component.html"
})
export class ProductEntrySettingsComponent {
  @Input() product: any = {};
}
