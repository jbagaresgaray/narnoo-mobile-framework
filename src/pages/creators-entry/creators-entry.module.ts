import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreatorsEntryPage } from './creators-entry';

@NgModule({
  declarations: [
    CreatorsEntryPage,
  ],
  imports: [
    IonicPageModule.forChild(CreatorsEntryPage),
  ],
})
export class CreatorsEntryPageModule {}
