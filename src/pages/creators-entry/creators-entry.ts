import { Component, ViewEncapsulation } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  AlertController,
  LoadingController,
  ToastController
} from "ionic-angular";
import * as _ from "lodash";

import { CreatorsServicesProvider } from "../../providers/services/creators";
import { StorageProvider } from "../../providers/storage/storage";

@IonicPage()
@Component({
  selector: "page-creators-entry",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "creators-entry.html"
})
export class CreatorsEntryPage {
  creator: any = {};
  business: any = {};
  action: string;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public creatorServices: CreatorsServicesProvider,
    public storageService: StorageProvider
  ) {
    this.action = navParams.get("action");
    if (this.action == "update") {
      this.creator = navParams.get("item");
    }
  }

  async ionViewDidLoad() {
    console.log("ionViewDidLoad CreatorsEntryPage");
    const business = await this.storageService.getStorage(
      this.storageService.storageKey.CURRENT_BUSINESS
    );
    this.business = !_.isEmpty(business) ? JSON.parse(business) || {} : {};
  }

  private createCreator() {
    const loading = this.loadingCtrl.create({
      dismissOnPageChange: true,
      content: "Saving..."
    });
    loading.present();
    this.creatorServices.creator_create_business(this.creator).then(
      (data: any) => {
        if (data && data.success) {
          let alert = this.alertCtrl.create({
            title: "SUCCESS",
            message: data.result,
            buttons: ["OK"]
          });
          alert.onDidDismiss(() => {
            this.navCtrl.pop();
          });
          alert.present();
        }
        loading.dismiss();
      },
      (error: any) => {
        loading.dismiss();
        console.log("error: ", error);
        if (error && !error.success) {
          this.alertCtrl
            .create({
              title: "ERROR",
              message: error.error,
              buttons: ["OK"]
            })
            .present();
          return;
        }
      }
    );
  }

  saveEntry() {
    if (this.business.role !== 1) {
      this.toastCtrl
        .create({
          message: "Invalid Access!!!",
          duration: 3000
        })
        .present();
      return;
    }

    if (_.isEmpty(this.creator.type)) {
      this.alertCtrl
        .create({
          title: "WARNING",
          message: "Creator type is required!"
        })
        .present();
      return;
    }

    if (_.isEmpty(this.creator.firstName)) {
      this.alertCtrl
        .create({
          title: "WARNING",
          message: "First Name is required!"
        })
        .present();
      return;
    }

    if (_.isEmpty(this.creator.lastName)) {
      this.alertCtrl
        .create({
          title: "WARNING",
          message: "Last Name is required!"
        })
        .present();
      return;
    }

    if (_.isEmpty(this.creator.email)) {
      this.alertCtrl
        .create({
          title: "WARNING",
          message: "Email Address is required!"
        })
        .present();
      return;
    }

    if (this.action == "create") {
      this.createCreator();
    } else if (this.action == "update") {
    }
  }

  unlinkCreator() {
    if (this.business.role !== 1) {
      this.toastCtrl
        .create({
          message: "Invalid Access!!!",
          duration: 3000
        })
        .present();
      return;
    }

    const deleteCreator = () => {
      let loading = this.loadingCtrl.create({
        dismissOnPageChange: true,
        content: "Unlinking..."
      });
      loading.present();
      this.creatorServices
        .creator_unlink_business({
          creatorId: this.creator.id
        })
        .then(
          (data: any) => {
            if (data && data.success) {
              console.log("data: ", data);
              this.alertCtrl
                .create({
                  title: "SUCCESS",
                  message: data.message,
                  buttons: ["OK"]
                })
                .present();
              return;
            }
            loading.dismiss();
          },
          (error: any) => {
            loading.dismiss();
            console.log("error: ", error);
            if (error && !error.success) {
              this.alertCtrl
                .create({
                  title: "ERROR",
                  message: error.error,
                  buttons: ["OK"]
                })
                .present();
              return;
            }
          }
        );
    };

    const confirm = this.alertCtrl.create({
      title: "Unlink this creator?",
      message: "Do you want to unlink this creator?",
      buttons: [
        {
          text: "No",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Yes",
          handler: () => {
            console.log("Agree clicked");
            deleteCreator();
          }
        }
      ]
    });
    confirm.present();
  }
}
