import { Injectable } from "@angular/core";
import { Storage } from "@ionic/storage";

/*
  Generated class for the StorageProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class StorageProvider {
  public storageKey = {
    USER_TOKENS: "app.usertoken",
    CURRENT_USER: "app.userData",
    BUSINESS_TOKENS: "bus.token",
    CURRENT_BUSINESS: "business",
    BUSINESS_NOTIFICATION: "bus.notification",
    PAYMENT_METHOD: "payment_method",
    ACCOUNT_TYPE: "account_type",
    BOOKING_RESERVATION: "booking_reservations",
    BOOKING_PAYMENT_RESULT: "booking_payment",
    BOOKING_PRODUCT_NAMES: "booking_product_names",
    LANGUAGE: "default_language"
  };

  public cacheKey = {
    USERS_BUSINESSES: "user_businesses",
    FEED_ACTIVITY: "feed_activity",
    NEXT_FEED_ACTIVITY: "next_feed_activity",
    PRODUCT_LIST: "product_list",
    PRODUCT_DETAIL: "product_detail",
    CONNECTED_PRODUCT_LIST: "connected_product_list",
    CONNECTED_FOLLOWERS: "connected_followers",
    CONNECTED_FOLLOWING: "connected_following",
    SEARCH_OPTIONS_IMAGE: "search_options_image",
    NOTIFICATION_LATEST: "notification_latest",
    APP_NOTIFICATIONS: "app_notifications",
    NOTIFICATION_LIST_SENT: "notification_list_sent",
    MEDIA_IMAGES: "media_images",
    MEDIA_IMAGE_DETAIL: "media_image_detail",
    MEDIA_IMAGES_RELATED: "media_images_related",
    MEDIA_LOGOS: "media_logos",
    MEDIA_LOGO_DETAIL: "media_logo_detail",
    MEDIA_PRINTS: "media_prints",
    MEDIA_PRINT_DETAIL: "media_print_detail",
    MEDIA_VIDEOS: "media_videos",
    MEDIA_VIDEO_DETAIL: "media_video_detail",
    MEDIA_ALBUMS: "media_albums",
    MEDIA_COLLECTIONS: "media_collection",
    MEDIA_COLLECTION_DETAIL: "media_collection_detail",
    MEDIA_CHANNELS: "media_channels",
    MEDIA_CHANNEL_DETAIL: "media_channel_detail",
    ALBUMS_GALLERY: "albums_gallery",
    BUSINESS_BIOGRAPHY: "business_biography",
    STAFFS: "staffs",
    SOCIAL_LINKS: "social_links",
    CREATORS: "creators",
    LOCATION: "location",
    USERS_LIST: "user_list",
    BUSINESS_PROFILE: "business_profile",
    BUSINESS_PROFILE_FOLLOWERS: "business_profile_followers",
    BUSINESS_PROFILE_FOLLOWING: "business_profile_following"
  };

  constructor(private storage: Storage) {}

  async setStorage(key, value) {
    await this.storage.set(key, value);
  }

  async getStorage(key) {
    return await this.storage.get(key).then(val => {
      return val;
    });
  }

  async removeStorage(key) {
    await this.storage.remove(key);
  }

  async cleanStorage() {
    this.storage.clear();
  }
}
