import { Injectable, Injector } from "@angular/core";
import {
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpEvent,
  HttpResponse
} from "@angular/common/http";
import { Observable } from "rxjs/Rx";
import "rxjs/add/observable/throw";
import "rxjs/add/operator/catch";

import { LoadingProvider } from "./loading/loading";

@Injectable()
export class MyHttpInterceptor implements HttpInterceptor {
  constructor(public loadingBar: LoadingProvider) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return Observable.fromPromise(this.handleAccess(request, next));
  }

  private async handleAccess(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Promise<HttpEvent<any>> {
    this.loadingBar.start();
    return next
      .handle(request)
      .retry(3)
      .map(resp => {
        if (resp instanceof HttpResponse) {
          this.loadingBar.complete();
        }
        return resp;
      })
      .catch((err: any) => {
        if (err instanceof HttpResponse) {
          this.loadingBar.complete();
        } else {
          this.loadingBar.complete();
        }
        return Observable.throw(err);
      })
      .toPromise();
  }
}
