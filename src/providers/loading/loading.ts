import { Injectable, NgZone } from "@angular/core";
import { Platform } from "ionic-angular";

declare const $: any;

@Injectable()
export class LoadingProvider {
  public preloader: any;
  constructor(public zone: NgZone, public platform: Platform) {}

  start() {
    this.zone.run(() => {
      if (this.preloader) {
        $(".load-bar").remove();
        this.preloader.off();
      }

      let options = {};
      if (this.platform.is("android")) {
        options = {
          col_1: "#159756",
          col_2: "#da4733",
          col_3: "#3b78e7",
          col_4: "#fdba2c",
          position: "top",
          height: "5px",
          fadeIn: 200,
          fadeOut: 200
        };
      } else if (this.platform.is("ios")) {
        options = {
          col_1: "#4cd964",
          col_2: "#ff2d55",
          col_3: "#007aff",
          col_4: "#5856d6",
          position: "top",
          height: "5px",
          fadeIn: 200,
          fadeOut: 200
        };
      }

      this.preloader = new $.materialPreloader(options);
      this.preloader.on();
    });
  }

  complete() {
    this.zone.run(() => {
      $(".load-bar").remove();
      this.preloader.off();
    });
  }
}
