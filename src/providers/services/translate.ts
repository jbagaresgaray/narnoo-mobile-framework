import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";

@Injectable()
export class TranslateServiceProvider {
  apiKey = "AIzaSyBrMXNHb83MEXi0mfLL4snwi11mC4VYbEg";
  url = "https://translation.googleapis.com/language/translate";
  result: any;
  q: any;

  constructor(public http: HttpClient) {}

  translate(q, language) {
    let params = new HttpParams();
    params = params.append("q", q);
    params = params.append("target", language);
    params = params.append("key ", this.apiKey);

    return new Promise((resolve, reject) => {
      let callbackResponse = (resp: any) => {
        resolve(resp);
      };

      let errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.http
        .get(this.url, { params: params })
        .subscribe(callbackResponse, errorResponse);
    });
  }
}
