import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { environment } from '../../environments/environment';


@Injectable()
export class RegisterServicesProvider {
	base_url: string = environment.api_url + environment.api_version;

	constructor(public http: HttpClient) {
	}

	user(data: any) {
		const headers = new HttpHeaders({
			'API-KEY': environment.api_key,
			'API-SECRET-KEY': environment.api_secret_key,
			'Content-Type': 'application/json'
		});
		return new Promise((resolve, reject) => {
			const callbackResponse = (resp: any) => {
				resolve(resp);
			};

			const errorResponse = (error) => {
				console.log('error: ', error);
				reject(error.error);
			};

			this.http.post(this.base_url + 'register/user',{
				firstName: data.firstName,
				lastName: data.lastName,
				email: data.email,
				password: data.password,
				confirmation: data.confirmation
			}, { headers: headers }).subscribe(callbackResponse, errorResponse);
		});
	}
}
