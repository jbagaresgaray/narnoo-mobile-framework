import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { StorageProvider } from "../../providers/storage/storage";

import { environment } from "../../environments/environment";

@Injectable()
export class LocationsServicesProvider {
  base_url: string = environment.api_url + environment.api_version;
  constructor(public http: HttpClient, public storage: StorageProvider) {}

  get_locations() {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };
          const headers = new HttpHeaders(header);
          this.http
            .get(this.base_url + "location/list", { headers: headers })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  delete_locations(Id) {
    const formData = new FormData();
    formData.append("location", Id);

    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };
          const headers = new HttpHeaders(header);
          this.http
            .post(this.base_url + "location/delete", formData, {
              headers: headers
            })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  add_location(data) {
    const formData = new FormData();
    formData.append("location", data.name);

    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };
          const headers = new HttpHeaders(header);
          this.http
            .post(this.base_url + "location/add", formData, {
              headers: headers
            })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  edit_location(data) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };
          const headers = new HttpHeaders(header);
          this.http
            .post(
              this.base_url + "location/edit",
              {
                id: data.id,
                name: data.name
              },
              { headers: headers }
            )
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }
}
