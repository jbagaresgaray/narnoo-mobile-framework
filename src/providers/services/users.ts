import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { StorageProvider } from "../../providers/storage/storage";

import { environment } from "../../environments/environment";

@Injectable()
export class UserServicesProvider {
  base_url: string = environment.api_url + environment.api_version;
  constructor(public http: HttpClient, public storage: StorageProvider) {}

  businesses() {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.USER_TOKENS)
        .then((token: any) => {
          const header: any = {
            "API-KEY": environment.api_key,
            "API-SECRET-KEY": environment.api_secret_key,
            Authorization: "Bearer " + token
          };
          const headers = new HttpHeaders(header);

          this.http
            .get(this.base_url + "user/businesses", { headers: headers })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  request_access(data) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.USER_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };
          const headers = new HttpHeaders(header);
          this.http
            .post(this.base_url + "user/request", data, { headers: headers })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  revoke_access(data) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };
          const headers = new HttpHeaders(header);
          this.http
            .post(
              this.base_url + "user/revoke",
              {
                userId: data.id
              },
              { headers: headers }
            )
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  user_unlink(data) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.USER_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };
          const headers = new HttpHeaders(header);
          this.http
            .post(this.base_url + "user/unlink", data, { headers: headers })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  user_list() {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };
          const headers = new HttpHeaders(header);
          this.http
            .get(this.base_url + "user/list", { headers: headers })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  user_edit_profile(data) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.USER_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };
          const headers = new HttpHeaders(header);
          this.http
            .post(this.base_url + "user/edit", data, { headers: headers })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  add_user(data) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };
          const headers = new HttpHeaders(header);
          this.http
            .post(this.base_url + "user/add", data, { headers: headers })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  edit_access(data) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };
          const headers = new HttpHeaders(header);
          this.http
            .post(this.base_url + "user/edit_access", data, {
              headers: headers
            })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  user_profile() {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.USER_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };
          const headers = new HttpHeaders(header);
          this.http
            .get(this.base_url + "user/profile", { headers: headers })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  user_edit_settings(data) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.USER_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };
          const headers = new HttpHeaders(header);
          this.http
            .post(this.base_url + "user/edit_settings", data, {
              headers: headers
            })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }
}
