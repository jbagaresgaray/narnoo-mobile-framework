import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { StorageProvider } from "../../providers/storage/storage";

import { environment } from "../../environments/environment";

@Injectable()
export class UtilitiesServicesProvider {
  base_url: string = environment.api_url + environment.api_version;

  public latitude: any;
  public longitude: any;

  constructor(public http: HttpClient, public storage: StorageProvider) {}

  radomizeAvatar() {
    const avatarArr = [
      "./assets/avatar/coconut/SVG/coconut-placeholder.svg",
      "./assets/avatar/feet/SVG/feet-placeholder.svg",
      "./assets/avatar/flippers/SVG/flipper-placeholder.svg",
      "./assets/avatar/mask/SVG/mask-placeholder.svg",
      "./assets/avatar/tree/SVG/tree-placeholder.svg"
    ];

    const randomNumber = Math.floor(Math.random() * avatarArr.length);
    return avatarArr[randomNumber];
  }

  category() {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.USER_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };
          const headers = new HttpHeaders(header);
          this.http
            .get(this.base_url + "utilities/category", { headers: headers })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  subcategory(id) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.USER_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };
          const headers = new HttpHeaders(header);
          this.http
            .get(this.base_url + "utilities/subcategory/" + id, {
              headers: headers
            })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  booking_platforms() {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.USER_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };
          const headers = new HttpHeaders(header);
          this.http
            .get(this.base_url + "utilities/booking_platforms", {
              headers: headers
            })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  distributor_category() {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.USER_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };
          const headers = new HttpHeaders(header);
          this.http
            .get(this.base_url + "utilities/distributor_category", {
              headers: headers
            })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  getApppVersion() {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.http
        .get("../../../package.json")
        .subscribe(callbackResponse, errorResponse);
    });
  }

  getCountries() {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.USER_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };
          const headers = new HttpHeaders(header);
          this.http
            .get(this.base_url + "utilities/country_names", {
              headers: headers
            })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }
}
