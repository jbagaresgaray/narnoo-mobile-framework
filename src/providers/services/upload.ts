import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { StorageProvider } from "../../providers/storage/storage";

import { environment } from "../../environments/environment";

@Injectable()
export class UploadServicesProvider {
  base_url: string = environment.api_url + environment.api_version;
  constructor(public http: HttpClient, public storage: StorageProvider) {}

  upload_image_policy() {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };
          const headers = new HttpHeaders(header);
          this.http
            .get(this.base_url + "upload/image_policy", { headers: headers })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  upload_print_policy() {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };
          const headers = new HttpHeaders(header);
          this.http
            .get(this.base_url + "upload/print_policy", { headers: headers })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  upload_video_policy() {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };
          const headers = new HttpHeaders(header);
          this.http
            .get(this.base_url + "upload/video_policy", { headers: headers })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  upload_logo_policy() {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };
          const headers = new HttpHeaders(header);
          this.http
            .get(this.base_url + "upload/logo_policy", { headers: headers })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  update_print_policy(Id) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };
          const headers = new HttpHeaders(header);
          this.http
            .get(this.base_url + "upload/update_print_policy/" + Id, {
              headers: headers
            })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  update_video_policy(Id) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };
          const headers = new HttpHeaders(header);
          this.http
            .get(this.base_url + "upload/update_video_policy/" + Id, {
              headers: headers
            })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  update_video_thumbnail_policy(Id) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };
          const headers = new HttpHeaders(header);
          this.http
            .get(this.base_url + "upload/video_thumbnail_policy/" + Id, {
              headers: headers
            })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }
}
