import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { StorageProvider } from "../../providers/storage/storage";

import { environment } from "../../environments/environment";

@Injectable()
export class CollectionsServicesProvider {
  base_url: string = environment.api_url + environment.api_version;

  constructor(public http: HttpClient, public storage: StorageProvider) {}

  collecton_list(params?: any) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            "Content-Type": "application/json; charset=utf-8",
            Authorization: "Bearer " + token
          };

          const headers = new HttpHeaders(header);
          this.http
            .get(this.base_url + "collection/list", {
              headers: headers,
              params: params
            })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  collecton_details(Id) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            "Content-Type": "application/json; charset=utf-8",
            Authorization: "Bearer " + token
          };

          const headers = new HttpHeaders(header);
          this.http
            .get(this.base_url + "collection/details/" + Id, {
              headers: headers
            })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  collection_create(data: any) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };
          const headers = new HttpHeaders(header);
          this.http
            .post(this.base_url + "collection/create", data, {
              headers: headers
            })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  collection_edit(data: any) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token,
            "Content-Type": "application/json; charset=utf-8"
          };
          const headers = new HttpHeaders(header);
          this.http
            .post(this.base_url + "collection/edit", data, { headers: headers })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  collection_delete(data: any) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };
          const headers = new HttpHeaders(header);
          this.http
            .post(this.base_url + "collection/delete", data, {
              headers: headers
            })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  collection_add_media(data: any) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            "Content-Type": "application/json",
            Authorization: "Bearer " + token
          };
          const headers = new HttpHeaders(header);
          this.http
            .post(this.base_url + "collection/add", data, { headers: headers })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  collection_remove_item(data: any) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            "Content-Type": "application/json",
            Authorization: "Bearer " + token
          };
          const headers = new HttpHeaders(header);
          this.http
            .post(
              this.base_url + "collection/remove_item",
              JSON.stringify(data),
              {
                headers: headers
              }
            )
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  collecton_operator_list(optId, params?: any) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            "Content-Type": "application/json; charset=utf-8",
            Authorization: "Bearer " + token
          };

          const headers = new HttpHeaders(header);
          this.http
            .get(this.base_url + "collection/list/operator/" + optId, {
              headers: headers,
              params: params
            })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  collecton_operator_details(Id, optId) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            "Content-Type": "application/json; charset=utf-8",
            Authorization: "Bearer " + token
          };

          const headers = new HttpHeaders(header);
          this.http
            .get(
              this.base_url + "collection/details/" + Id + "/operator/" + optId,
              { headers: headers }
            )
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  collecton_distributor_list(distId, params?: any) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            "Content-Type": "application/json; charset=utf-8",
            Authorization: "Bearer " + token
          };

          const headers = new HttpHeaders(header);
          this.http
            .get(this.base_url + "collection/list/distributor/" + distId, {
              headers: headers,
              params: params
            })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  collecton_distributor_details(Id, distId) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            "Content-Type": "application/json; charset=utf-8",
            Authorization: "Bearer " + token
          };

          const headers = new HttpHeaders(header);
          this.http
            .get(
              this.base_url +
                "collection/details/" +
                Id +
                "/distributor/" +
                distId,
              { headers: headers }
            )
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }
}
