import { HttpClient, HttpRequest, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Storage } from "@ionic/storage";
import { Observable, BehaviorSubject } from "rxjs";
import { StorageProvider } from "../../providers/storage/storage";
import { Cache } from "ionic-cache-observable";

import { environment } from "../../environments/environment";

@Injectable()
export class NewsFeedServicesProvider {
  base_url: string = environment.api_url + environment.api_version;
  public totalNotification: number = 0;
  public notificationData$: Observable<any>;
  public notificationCache$: Cache<any>;

  constructor(public http: HttpClient, public storage: StorageProvider) {}

  feed_activity(params?: any) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };
          const headers = new HttpHeaders(header);
          this.http
            .get(this.base_url + "feed/activity", {
              headers: headers,
              params: params
            })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  feed_next_page(data: any) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };
          const headers = new HttpHeaders(header);
          this.http
            .post(this.base_url + "feed/next_page", data, { headers: headers })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  feed_following() {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };
          const headers = new HttpHeaders(header);
          this.http
            .get(this.base_url + "feed/following", { headers: headers })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  feed_followers() {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };
          const headers = new HttpHeaders(header);
          this.http
            .get(this.base_url + "feed/followers", { headers: headers })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  postFeed(token, data) {
    const header: any = {
      Authorization: "Bearer " + token
    };
    const headers = new HttpHeaders(header);
    return this.http.request(
      new HttpRequest("POST", this.base_url + "shout/create", data, {
        headers: headers,
        reportProgress: true
      })
    );
  }

  remotePostedFeed(id) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      const formData = new FormData();
      formData.append("id", id);

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };
          const headers = new HttpHeaders(header);
          this.http
            .post(this.base_url + "shout/remove", formData, {
              headers: headers
            })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  flagPostedFeed(shoutId) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      const formData = new FormData();
      formData.append("shoutId", shoutId);

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };
          const headers = new HttpHeaders(header);
          this.http
            .post(this.base_url + "shout/remove", formData, {
              headers: headers
            })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  notification_request(type, businessId) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };
          const headers = new HttpHeaders(header);
          this.http
            .get(
              this.base_url +
                "notification/requests/" +
                type +
                "/" +
                businessId,
              {
                headers: headers
              }
            )
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  app_notifications_latest() {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          if (token) {
            const header: any = {
              Authorization: "Bearer " + token
            };
            const headers = new HttpHeaders(header);
            this.http
              .get(this.base_url + "notification/latest", {
                headers: headers
              })
              .subscribe(callbackResponse, errorResponse);
          }
        });
    });
  }

  app_notifications(params?: any) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };
          const headers = new HttpHeaders(header);
          this.http
            .get(this.base_url + "notification/list", {
              headers: headers,
              params: params
            })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  app_notifications_sent(params?: any) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };
          const headers = new HttpHeaders(header);
          this.http
            .get(this.base_url + "notification/list_sent", {
              headers: headers,
              params: params
            })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  app_notification_acknowledge() {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };
          const headers = new HttpHeaders(header);
          this.http
            .get(this.base_url + "notification/acknowledge", {
              headers: headers
            })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  create_notifications(data) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };
          const headers = new HttpHeaders(header);
          this.http
            .post(this.base_url + "notification/create", data, {
              headers: headers
            })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  create_notification_message(data) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };
          const headers = new HttpHeaders(header);
          this.http
            .post(this.base_url + "notification/message", data, {
              headers: headers
            })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  create_app_notification_acknowledge(data) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };
          const headers = new HttpHeaders(header);
          this.http
            .post(this.base_url + "notification/acknowledge", data, {
              headers: headers
            })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  update_app_notification_acknowledge(data) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };
          const headers = new HttpHeaders(header);
          this.http
            .post(this.base_url + "notification/acknowledge_update", data, {
              headers: headers
            })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  delete_notification(data) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };
          const headers = new HttpHeaders(header);
          this.http
            .post(this.base_url + "notification/trash", data, {
              headers: headers
            })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }
}
