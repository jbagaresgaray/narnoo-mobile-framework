import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { StorageProvider } from "../../providers/storage/storage";
import { Observable, } from "rxjs";
import { Cache } from "ionic-cache-observable";

import { environment } from "../../environments/environment";

@Injectable()
export class ConnectServicesProvider {
  base_url: string = environment.api_url + environment.api_version;

  public connect_followers$: Observable<any>;
  public connect_following$: Observable<any>;

  public connect_followersCache: Cache<any>;
  public connect_followingCache: Cache<any>;

  constructor(public http: HttpClient, public storage: StorageProvider) {}

  search_businesses(data) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.USER_TOKENS)
        .then(token => {
          const header: any = {
            "Content-Type": "application/json; charset=utf-8",
            Authorization: "Bearer " + token
          };

          const headers = new HttpHeaders(header);
          this.http
            .post(this.base_url + "search/business", data, { headers: headers })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  connect_list() {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            "Content-Type": "application/json; charset=utf-8",
            Authorization: "Bearer " + token
          };

          const headers = new HttpHeaders(header);
          this.http
            .get(this.base_url + "connect/list", { headers: headers })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  connect_find(data?: any) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };

          const headers = new HttpHeaders(header);
          this.http
            .post(this.base_url + "connect/find", data, { headers: headers })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  connect_search(data: any) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };

          const headers = new HttpHeaders(header);
          this.http
            .post(this.base_url + "connect/search", data, { headers: headers })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  connect_add(data: any) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };

          const headers = new HttpHeaders(header);
          this.http
            .post(this.base_url + "connect/add", data, { headers: headers })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  connect_edit(data: any) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };

          const headers = new HttpHeaders(header);
          this.http
            .post(this.base_url + "connect/edit", data, { headers: headers })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  connect_revoked() {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            "Content-Type": "application/json; charset=utf-8",
            Authorization: "Bearer " + token
          };

          const headers = new HttpHeaders(header);
          this.http
            .get(this.base_url + "connect/revoked", { headers: headers })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  connect_followers() {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            "Content-Type": "application/json; charset=utf-8",
            Authorization: "Bearer " + token
          };

          const headers = new HttpHeaders(header);
          this.http
            .get(this.base_url + "connect/followers", { headers: headers })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  connect_following() {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            "Content-Type": "application/json; charset=utf-8",
            Authorization: "Bearer " + token
          };

          const headers = new HttpHeaders(header);
          this.http
            .get(this.base_url + "connect/following", { headers: headers })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  connect_remove(data: any) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };

          const headers = new HttpHeaders(header);
          this.http
            .post(this.base_url + "connect/remove", data, { headers: headers })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }
}
