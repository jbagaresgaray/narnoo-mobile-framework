import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { StorageProvider } from "../storage/storage";

import { environment } from "../../environments/environment";

@Injectable()
export class LoginServicesProvider {
  base_url: string = environment.api_url + environment.api_version;

  constructor(
    public http: HttpClient,
    public storageService: StorageProvider
  ) {}

  authenticate(data: any) {
    const headers = new HttpHeaders({
      "API-KEY": environment.api_key,
      "API-SECRET-KEY": environment.api_secret_key
    });
    const formData = new FormData();
    formData.append("username", data.username);
    formData.append("password", data.password);

    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.http
        .post(this.base_url + "login/authenticate", formData, {
          headers: headers
        })
        .subscribe(callbackResponse, errorResponse);
    });
  }

  authenticate_social(data: any) {
    const headers = new HttpHeaders({
      "API-KEY": environment.api_key,
      "API-SECRET-KEY": environment.api_secret_key,
      "Content-type": "application/json"
    });
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.http
        .post(
          this.base_url + "login/social",
          {
            platform: data.platform,
            id: data.id,
            firstName: data.firstName,
            lastName: data.lastName,
            email: data.email,
            token: data.token || ""
          },
          { headers: headers }
        )
        .subscribe(callbackResponse, errorResponse);
    });
  }

  forgot_password(email: string) {
    const headers = new HttpHeaders({
      "API-KEY": environment.api_key,
      "API-SECRET-KEY": environment.api_secret_key
    });
    const formData = new FormData();
    formData.append("email", email);

    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.http
        .post(this.base_url + "login/forgot_password", formData, {
          headers: headers
        })
        .subscribe(callbackResponse, errorResponse);
    });
  }

  logout() {
    this.storageService.removeStorage(
      this.storageService.storageKey.CURRENT_USER
    );
    this.storageService.removeStorage(
      this.storageService.storageKey.USER_TOKENS
    );
    this.storageService.removeStorage(
      this.storageService.storageKey.BUSINESS_TOKENS
    );
    this.storageService.removeStorage(
      this.storageService.storageKey.CURRENT_BUSINESS
    );
    this.storageService.removeStorage(
      this.storageService.storageKey.BUSINESS_NOTIFICATION
    );
  }

  token_validate(token: string) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + token
      };
      const headers = new HttpHeaders(header);
      this.http
        .get(this.base_url + "token/validate", { headers: headers })
        .subscribe(callbackResponse, errorResponse);
    });
  }
}
