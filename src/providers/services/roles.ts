import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { StorageProvider } from "../../providers/storage/storage";

import { environment } from "../../environments/environment";

@Injectable()
export class RolesServicesProvider {
  base_url: string = environment.api_url + environment.api_version;
  constructor(public http: HttpClient, public storage: StorageProvider) {}

  privacy_settings() {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.USER_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };
          const headers = new HttpHeaders(header);
          this.http
            .get(this.base_url + "privacy/settings", { headers: headers })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  privacy_edit(data) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.USER_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token,
            "Content-Type": "application/json"
          };
          const headers = new HttpHeaders(header);
          this.http
            .post(this.base_url + "privacy/edit", data, { headers: headers })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }
}
