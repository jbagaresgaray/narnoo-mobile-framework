import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { StorageProvider } from "../../providers/storage/storage";

import { environment } from "../../environments/environment";

@Injectable()
export class VideosServicesProvider {
  base_url: string = environment.api_url + environment.api_version;
  constructor(public http: HttpClient, public storage: StorageProvider) {}

  video_list(params?: any) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };
          const headers = new HttpHeaders(header);
          this.http
            .get(this.base_url + "video/list", {
              headers: headers,
              params: params
            })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  video_detail(id) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };
          const headers = new HttpHeaders(header);
          this.http
            .get(this.base_url + "video/details/" + id, { headers: headers })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  video_edit(data: any) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };
          const headers = new HttpHeaders(header);
          this.http
            .post(this.base_url + "video/edit", data, { headers: headers })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  video_operator_list(optId, params?: any) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };
          const headers = new HttpHeaders(header);
          this.http
            .get(this.base_url + "video/list/operator/" + optId, {
              headers: headers,
              params: params
            })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  video_operator_detail(imageId, optId) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };
          const headers = new HttpHeaders(header);
          this.http
            .get(
              this.base_url + "video/details/" + imageId + "/operator/" + optId,
              { headers: headers }
            )
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  video_distributor_list(distId, params?: any) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };
          const headers = new HttpHeaders(header);
          this.http
            .get(this.base_url + "video/list/distributor/" + distId, {
              headers: headers,
              params: params
            })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  video_distributor_detail(imageId, distId) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };
          const headers = new HttpHeaders(header);
          this.http
            .get(
              this.base_url +
                "video/details/" +
                imageId +
                "/distributor/" +
                distId,
              { headers: headers }
            )
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  videos_delete(data) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            "Content-Type": "application/json",
            Authorization: "Bearer " + token
          };
          const headers = new HttpHeaders(header);
          this.http
            .post(this.base_url + "video/delete", data, { headers: headers })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  share_url_video(Id) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };
          const headers = new HttpHeaders(header);
          this.http
            .get(this.base_url + "share/url/video/" + Id, { headers: headers })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  video_download(videoId) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };
          const headers = new HttpHeaders(header);
          this.http
            .get(this.base_url + "video/download/" + videoId, {
              headers: headers
            })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }
}
