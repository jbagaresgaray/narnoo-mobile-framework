import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { StorageProvider } from "../../providers/storage/storage";

import { environment } from "../../environments/environment";

@Injectable()
export class ChannelsServicesProvider {
  base_url: string = environment.api_url + environment.api_version;

  constructor(public http: HttpClient, public storage: StorageProvider) {}

  channel_create(data: any) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      const formData = new FormData();
      formData.append("title", data.title);

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };
          const headers = new HttpHeaders(header);
          this.http
            .post(this.base_url + "channel/create", formData, {
              headers: headers
            })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  channel_list(params?: any) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };

          const headers = new HttpHeaders(header);
          this.http
            .get(this.base_url + "channel/list", {
              headers: headers,
              params: params
            })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  channel_details(Id) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };

          const headers = new HttpHeaders(header);
          this.http
            .get(this.base_url + "channel/details/" + Id, { headers: headers })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  channel_add_video(data: any) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };

          const headers = new HttpHeaders(header);
          this.http
            .post(this.base_url + "channel/add", data, { headers: headers })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  channel_delete(data: any) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };

          const formData = new FormData();
          formData.append("id", data.id);

          const headers = new HttpHeaders(header);
          this.http
            .post(this.base_url + "channel/delete", formData, {
              headers: headers
            })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  channel_remove_video(data: any) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };

          const headers = new HttpHeaders(header);
          this.http
            .post(this.base_url + "channel/remove", data, { headers: headers })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  channel_operator_list(optId, params?: any) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };

          const headers = new HttpHeaders(header);
          this.http
            .get(this.base_url + "channel/list/operator/" + optId, {
              headers: headers,
              params: params
            })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  channel_operator_details(Id, optId) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };

          const headers = new HttpHeaders(header);
          this.http
            .get(
              this.base_url + "channel/details/" + Id + "/operator/" + optId,
              {
                headers: headers
              }
            )
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  channel_distributor_list(distId, params?: any) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };

          const headers = new HttpHeaders(header);
          this.http
            .get(this.base_url + "channel/list/distributor/" + distId, {
              headers: headers,
              params: params
            })
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }

  channel_distributor_details(Id, distId) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error.error);
      };

      this.storage
        .getStorage(this.storage.storageKey.BUSINESS_TOKENS)
        .then(token => {
          const header: any = {
            Authorization: "Bearer " + token
          };

          const headers = new HttpHeaders(header);
          this.http
            .get(
              this.base_url +
                "channel/details/" +
                Id +
                "/distributor/" +
                distId,
              { headers: headers }
            )
            .subscribe(callbackResponse, errorResponse);
        });
    });
  }
}
