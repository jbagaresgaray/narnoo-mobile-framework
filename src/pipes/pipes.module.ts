import { NgModule } from "@angular/core";
import { TimesagoPipe } from "./timesago/timesago";
import { FileSizePipe } from "./file-size/file-size";
import { LinkHttpPipe } from "./link-http/link-http";
import { RelativeTimePipe } from "./relative-time/relative-time";
import { CountdownPipe } from "./countdown/countdown";
@NgModule({
	declarations: [
		TimesagoPipe,
		FileSizePipe,
		LinkHttpPipe,
		RelativeTimePipe,
		CountdownPipe
	],
	imports: [],
	exports: [
		TimesagoPipe,
		FileSizePipe,
		LinkHttpPipe,
		RelativeTimePipe,
		CountdownPipe
	]
})
export class PipesModule {
	static forRoot() {
		return {
			ngModule: PipesModule,
			providers: []
		};
	}
}
