import { Pipe, PipeTransform } from "@angular/core";

/**
 * Generated class for the CountdownPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
	name: "countdown",
	pure: false
})
export class CountdownPipe implements PipeTransform {
	/**
	 * Takes a value and makes it lowercase.
	 */
	/*transform(text: string, args: number) {
		let maxLength = args || 140;
		let length = text.length;
		return maxLength - length;
	}*/
	transform(text: string, args: number) {
		let maxLength = args || 140;
		let words = text.match(/\b[-?(\w+)?]+\b/gi);
		if(words){
			return maxLength - words.length;
		}else{
			return maxLength;
		}
	}
}
