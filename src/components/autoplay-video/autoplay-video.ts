import { AutoplayVideoDirective } from "../../directives/autoplay-video/autoplay-video";
import {
  Component,
  ContentChildren,
  ElementRef,
  NgZone,
  OnDestroy,
  OnInit,
  QueryList
} from "@angular/core";

declare const $: any;
declare const IntersectionObserver: any;

@Component({
  selector: "autoplay-content",
  template: `
    <ng-content></ng-content>
  `
})
export class AutoplayContentComponent implements OnInit, OnDestroy {
  @ContentChildren(AutoplayVideoDirective, {
    read: ElementRef,
    descendants: true
  })
  autoPlayVideoRefs: QueryList<AutoplayVideoDirective>;

  private intersectionObserver: IntersectionObserver;
  private mutationObserver: MutationObserver;

  intersectionRatio: number = 0.5;

  constructor(private element: ElementRef, public ngZone: NgZone) {}

  public ngOnInit() {
    // we can run this outside the ngZone, no need
    // to trigger change detection
    this.ngZone.runOutsideAngular(() => {
      this.intersectionObserver = this.getIntersectionObserver();
      this.mutationObserver = this.getMutationObserver(
        this.element.nativeElement
      );
    });
  }

  // clean things ups
  public ngOnDestroy() {
    if (this.intersectionObserver) {
      this.intersectionObserver.disconnect();
    }

    if (this.mutationObserver) {
      this.mutationObserver.disconnect();
    }
  }

  // construct the InterSectionObserver and return it
  private getIntersectionObserver() {
    return new IntersectionObserver(
      // trigger the 'onIntersection' callback on ...
      entries => this.onIntersection(entries),
      {
        // ... both 0% and 50% percent of the intersection of the video
        threshold: [0, this.intersectionRatio]
      }
    );
  }

  // construct the MutationObserver and return it
  private getMutationObserver(containerElement: HTMLElement) {
    let mutationObserver = new MutationObserver(
      // execute the onDomChange
      () => this.onDomChange()
    );

    // at the very least, childList, attributes, or characterData
    // must be set to true
    const config = {
      attributes: true,
      characterData: true,
      childList: true
    };

    // attach the mutation observer to the container element
    // and start observing it
    mutationObserver.observe(containerElement, config);
    return mutationObserver;
  }

  private onDomChange() {
    // when the DOM changes, loop over each element
    // we want to observe for its interection,
    // and do observe it
    this.autoPlayVideoRefs.forEach((video: ElementRef) => {
      this.intersectionObserver.observe(video.nativeElement);
    });
  }

  private onIntersection(entries: IntersectionObserverEntry[]) {
    entries.forEach((entry: any) => {
      // get the video element
      let video: any = entry.target;
      // are we intersecting?
      if (!entry.isIntersecting) {
        this.ngZone.run(() => {
          $("video").each((videoIndex, _video) => {
            _video.pause();
          });
        });
        return;
      }

      // play the video if we passed the threshold
      // of 0.5 and store the promise so we can safely
      // pause it again
      if (entry.intersectionRatio >= this.intersectionRatio) {
        // for demo purposes we use a single video
        // this code can easely be
        // extended to support multiple videos
        this.ngZone.run(() => {
          $("video").each((videoIndex, _video) => {
            if (_video.id === video.id) {
              _video.play();
            } else {
              _video.pause();
            }
          });
        });
      } else if (entry.intersectionRatio < this.intersectionRatio) {
        // no need to pause something if it didn't start
        // playing yet.
        this.ngZone.run(() => {
          $("video").each((videoIndex, _video) => {
            if (_video.id === video.id) {
              _video.pause();
            }
          });
        });
      }
    });
  }
}
