import {
  Component,
  OnInit,
  ViewEncapsulation,
  Input,
  ElementRef,
  ViewChild,
  OnChanges
} from "@angular/core";
import { Platform } from "ionic-angular";

declare const google: any;
declare const document: any;

@Component({
  selector: 'app-map',
  encapsulation: ViewEncapsulation.None,
  templateUrl: 'app-map.html'
})
export class AppMapComponent implements OnInit, OnChanges {

  @Input() latitude: number;
  @Input() longitude: number;
  @Input() title: string;
  @Input() imageSrc: any;

  map: any;

  @ViewChild("map_canvas") mapElement: ElementRef;

  constructor(public platform: Platform) { }

  ngOnInit() {
    this.platform.ready().then(() => {
      // if (this.platform.is("cordova")) {
      //   this.loadNativeMap(this.latitude, this.longitude);
      // } else {
      //   this.loadMap(this.latitude, this.longitude);
      // }

      this.loadMap(this.latitude, this.longitude, this.title, this.imageSrc);
    });
  }

  ngOnChanges() {
    this.platform.ready().then(() => {
      // if (this.platform.is("cordova")) {
      //   this.loadNativeMap(this.latitude, this.longitude);
      // } else {
      //   this.loadMap(this.latitude, this.longitude);
      // }

      this.loadMap(this.latitude, this.longitude, this.title, this.imageSrc);
    });
  }

  private loadMap(latitude, longitude, title?, imageSrc?) {
    console.log("loadMap: ", imageSrc);

    function CustomMarker(latlng, map, _imageSrc) {
      this.latlng_ = latlng;
      this.imageSrc = _imageSrc;
      this.setMap(map);
    }

    CustomMarker.prototype = new google.maps.OverlayView();

    CustomMarker.prototype.draw = function() {
      // Check if the div has been created.
      let div = this.div_;
      if (!div) {
        // Create a overlay text DIV
        div = this.div_ = document.createElement("div");
        // Create the DIV representing our CustomMarker
        div.className = "customMarker";

        const img = document.createElement("img");
        img.src = this.imageSrc;
        div.appendChild(img);
        const me = this;
        google.maps.event.addDomListener(div, "click", function(event) {
          google.maps.event.trigger(me, "click");
        });

        // Then add the overlay to the DOM
        const panes = this.getPanes();
        panes.overlayImage.appendChild(div);
      }

      // Position the overlay
      const point = this.getProjection().fromLatLngToDivPixel(this.latlng_);
      if (point) {
        div.style.left = point.x + "px";
        div.style.top = point.y + "px";
      }
    };

    CustomMarker.prototype.remove = function() {
      // Check if the overlay was on the map and needs to be removed.
      if (this.div_) {
        this.div_.parentNode.removeChild(this.div_);
        this.div_ = null;
      }
    };

    CustomMarker.prototype.getPosition = function() {
      return this.latlng_;
    };

    const latLng = new google.maps.LatLng(latitude, longitude);
    const mapOptions = {
      center: latLng,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      disableDefaultUI: true,
      draggable: false
    };
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    const markerOptions: any = {
      position: latLng,
      title: title,
      map: this.map,
      animation: google.maps.Animation.DROP
    };
    let marker: any;
    if (imageSrc) {
      marker = new CustomMarker(latLng, this.map, imageSrc);
    } else {
      marker = new google.maps.Marker(markerOptions);
    }
    this.addInfoWindow(marker, title);
  }

  private addInfoWindow(marker, content) {
    const infoWindow = new google.maps.InfoWindow({
      content
    });
    google.maps.event.addListener(marker, "click", () => {
      infoWindow.open(this.map, marker);
    });
  }
}
