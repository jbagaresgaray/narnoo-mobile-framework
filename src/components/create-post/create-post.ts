import {
  Component,
  Input,
  ViewEncapsulation,
  Output,
  EventEmitter
} from "@angular/core";

/**
 * Generated class for the CreatePostComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: "create-post",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "create-post.html"
})
export class CreatePostComponent {
  @Input() showContent: boolean;
  @Input() showPostContent: boolean;

  @Input() business: any = {};

  @Output() _checkFocus = new EventEmitter<any>();

  checkFocus(isLink) {
    this._checkFocus.emit(isLink);
  }
}
