import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { IonicModule } from "ionic-angular";
import { EmojiPickerComponent } from "./emoji-picker/emoji-picker";
import { RichTextComponent } from "./rich-text/rich-text";
import { IonicImageLoader } from "ionic-image-loader";
import { CacheModule } from "ionic-cache-observable";
import { IonicImageViewerModule } from "ionic-img-viewer";
import { InViewportModule } from 'ng-in-viewport';

import { PipesModule } from "./../pipes/pipes.module";
import { DirectivesModule } from "./../directives/directives.module";

import { IonSimpleWizard } from "../pages/ion-simple-wizard/ion-simple-wizard.component";
import { IonSimpleWizardStep } from "../pages/ion-simple-wizard/ion-simple-wizard.step.component";
import { ParallaxHeaderDirective } from "./parallax-header/parallax-header";
import { KeyboardAttachDirective } from "./keyboard-attach/keyboard-attach";
import { SkeletonItemComponent } from "./skeleton-item/skeleton-item";
import { AutoplayContentComponent } from "./autoplay-video/autoplay-video";
import { AppMapComponent } from "./app-map/app-map";
import { CardShoutComponent } from "./feed-cards/card-shout/card-shout";
import { CardImagesComponent } from "./feed-cards/card-images/card-images";
import { CardImageComponent } from "./feed-cards/card-image/card-image";
import { CardVideoComponent } from "./feed-cards/card-video/card-video";
import { CardFollowingJoinedComponent } from "./feed-cards/card-following-joined/card-following-joined";
import { CardProductComponent } from "./feed-cards/card-product/card-product";
import { CardBrochureComponent } from "./feed-cards/card-brochure/card-brochure";
import { CardAlbumsComponent } from "./feed-cards/card-albums/card-albums";
import { CardCreatePostComponent } from "./feed-cards/card-create-post/card-create-post";
import { CreatePostComponent } from "./create-post/create-post";

@NgModule({
  declarations: [
    EmojiPickerComponent,
    RichTextComponent,
    IonSimpleWizard,
    IonSimpleWizardStep,
    ParallaxHeaderDirective,
    KeyboardAttachDirective,
    SkeletonItemComponent,
    AutoplayContentComponent,
    AppMapComponent,
    CardShoutComponent,
    CardImagesComponent,
    CardImageComponent,
    CardVideoComponent,
    CardFollowingJoinedComponent,
    CardProductComponent,
    CardBrochureComponent,
    CardAlbumsComponent,
    CreatePostComponent,
    CardCreatePostComponent,
  ],
  imports: [
    CommonModule,
    IonicModule,
    DirectivesModule,
    IonicImageLoader,
    IonicImageViewerModule,
    CacheModule,
    PipesModule,
    InViewportModule
  ],
  exports: [
    EmojiPickerComponent,
    RichTextComponent,
    IonSimpleWizard,
    IonSimpleWizardStep,
    ParallaxHeaderDirective,
    KeyboardAttachDirective,
    SkeletonItemComponent,
    AutoplayContentComponent,
    AppMapComponent,
    CardShoutComponent,
    CardImagesComponent,
    CardImageComponent,
    CardVideoComponent,
    CardFollowingJoinedComponent,
    CardProductComponent,
    CardBrochureComponent,
    CardAlbumsComponent,
    CreatePostComponent,
    CardCreatePostComponent
  ]
})
export class ComponentsModule {}
