import {
  Component,
  Input,
  ViewEncapsulation,
  Output,
  EventEmitter
} from "@angular/core";

/**
 * Generated class for the CardShoutComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: "card-shout",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "card-shout.html"
})
export class CardShoutComponent {
  @Input() item: any = {};
  @Input() avatar: any = {};
  @Input() external: any = {};
  @Input() media: any = {};

  @Output() _viewExternalLink = new EventEmitter<any>();
  @Output() _moreAction = new EventEmitter<any>();
  @Output() _downloadImage = new EventEmitter<any>();
  @Output() _shareActivity = new EventEmitter<any>();

  constructor() {}

  viewExternalLink(item: any) {
    this._viewExternalLink.emit(item);
  }

  moreAction(type, item: any) {
    this._moreAction.emit({
      type: type,
      item: item
    });
  }

  downloadImage(item: any) {
    this._downloadImage.emit(item);
  }

  shareActivity(item: any) {
    this._shareActivity.emit(item);
  }
}
