import {
  Component,
  Input,
  ViewEncapsulation,
  Output,
  EventEmitter
} from "@angular/core";

/**
 * Generated class for the CardImageComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: "card-image",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "card-image.html"
})
export class CardImageComponent {
  @Input() item: any = {};
  @Input() avatar: any = {};
  @Input() media: any = {};

  @Output() _viewImageDetail = new EventEmitter<any>();
  @Output() _viewFeedDetails = new EventEmitter<any>();
  @Output() _downloadImage = new EventEmitter<any>();
  @Output() _shareActivity = new EventEmitter<any>();

  constructor() {}

  viewImageDetail(item: any) {
    this._viewImageDetail.emit(item);
  }

  viewFeedDetails(type, item: any) {
    this._viewFeedDetails.emit({
      type: type,
      item: item
    });
  }

  downloadImage(item: any) {
    this._downloadImage.emit(item);
  }

  shareActivity(item: any) {
    this._shareActivity.emit(item);
  }
}
