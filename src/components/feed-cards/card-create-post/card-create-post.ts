import {
  Component,
  Input,
  ViewEncapsulation,
  Output,
  EventEmitter
} from "@angular/core";
import { ActionSheetController } from "ionic-angular";

/**
 * Generated class for the CardCreatePostComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: "card-create-post",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "card-create-post.html"
})
export class CardCreatePostComponent {
  @Input() business: any = {};
  @Input() _shout: any = {};
  @Input() percentDone: number = 0;
  @Input() retryMsg: string;

  @Input() isRetry: boolean;
  @Input() showRetry: boolean;
  @Input() isLimit: boolean;

  @Output() _deleteCacheShout = new EventEmitter<any>();
  @Output() _createPost = new EventEmitter<any>();

  constructor(public actionSheetCtrl: ActionSheetController) {}

  shoutAction(shout: any, link: any) {
    const actionSheet = this.actionSheetCtrl.create({
      buttons: [
        {
          text: "Retry",
          handler: () => {
            console.log("Resend clicked");
            this._createPost.emit({
              shout: shout,
              link: link
            });
          }
        },
        {
          text: "Discard",
          role: "destructive",
          handler: () => {
            console.log("Discard clicked");
            this._deleteCacheShout.emit();
          }
        },
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        }
      ]
    });
    actionSheet.present();
  }

  deleteCacheShout() {
    this._deleteCacheShout.emit();
  }

  createPost(shout, link) {
    this._createPost.emit({});
  }
}
