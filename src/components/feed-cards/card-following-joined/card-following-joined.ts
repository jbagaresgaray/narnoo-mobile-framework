import {
  Component,
  Input,
  ViewEncapsulation,
  Output,
  EventEmitter
} from "@angular/core";

/**
 * Generated class for the CardFollowingJoinedComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: "card-following-joined",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "card-following-joined.html"
})
export class CardFollowingJoinedComponent {
  @Input() item: any = {};
  @Input() avatar: any = {};
  @Input() media: any = {};

  @Output() _moreAction = new EventEmitter<any>();
  @Output() _downloadImage = new EventEmitter<any>();
  @Output() _shareActivity = new EventEmitter<any>();

  constructor() {}

  moreAction(type, item: any) {
    this._moreAction.emit({
      type: type,
      item: item
    });
  }

  downloadImage(item: any) {
    this._downloadImage.emit(item);
  }

  shareActivity(item: any) {
    this._shareActivity.emit(item);
  }
}
