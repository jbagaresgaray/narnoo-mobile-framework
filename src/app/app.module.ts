import { BrowserModule } from "@angular/platform-browser";
import { CommonModule } from "@angular/common";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { ErrorHandler, NgModule } from "@angular/core";
import { IonicApp, IonicErrorHandler, IonicModule } from "ionic-angular";
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { IonicStorageModule } from "@ionic/storage";
import {
  SocialLoginModule,
  AuthServiceConfig,
  GoogleLoginProvider,
  FacebookLoginProvider
} from "angularx-social-login";
import { IonicImageViewerModule } from "ionic-img-viewer";
import { IonicImageLoader } from "ionic-image-loader";
import { IonTagsInputModule } from "ionic-tags-input";
import { NgProgressModule } from "@ngx-progressbar/core";
import { AuthHttp, AuthConfig } from "angular2-jwt";
import { Http, RequestOptions } from "@angular/http";
import { JoditAngularModule } from "jodit-angular";
import { InViewportModule } from "ng-in-viewport";
import { Storage } from "@ionic/storage";
import { CacheModule } from "ionic-cache-observable";


import { MyApp } from "./app.component";
import { HomePage } from "../pages/home/home";
import { RegisterPage } from "../pages/register/register";

import { AboutPageModule } from "../pages/about/about.module";
import { ForgotPasswordModule } from "../pages/forgot_password/forgot_password.module";
import { ResetPasswordPageModule } from "../pages/reset_password/reset_password.module";
import { LoginPageModule } from "../pages/login/login.module";
import { CreatebusinessPageModule } from "../pages/createbusiness/createbusiness.module";
import { SearchPageModule } from "../pages/search/search.module";
import { BusinessprofilePageModule } from "../pages/businessprofile/businessprofile.module";
import { DownloadsPageModule } from "../pages/downloads/downloads.module";
import { BusinesstabsPageModule } from "../pages/businesstabs/businesstabs.module";
import { FileUploadPageModule } from "../pages/file-upload/file-upload.module";
import { ImagesTabPageModule } from "../pages/images-tab/images-tab.module";
import { ImageDetailPageModule } from "../pages/image-detail/image-detail.module";
import { ImageModalPageModule } from "../pages/image-modal/image-modal.module";
import { ImageDetailInfoPageModule } from "../pages/image-detail-info/image-detail-info.module";
import { LogoDetailPageModule } from "../pages/logo-detail/logo-detail.module";
import { PrintDetailPageModule } from "../pages/print-detail/print-detail.module";
import { PrintdetailinfoPageModule } from "../pages/printdetailinfo/printdetailinfo.module";
import { ProductTabPageModule } from "../pages/product-tab/product-tab.module";
import { ProductDetailPageModule } from "../pages/product-detail/product-detail.module";
import { ProductsModalPageModule } from "../pages/products-modal/products-modal.module";
import { ProductsEntryPageModule } from "../pages/products-entry/products-entry.module";
import { VideosTabPageModule } from "../pages/videos-player/videos-player.module";
import { VideoDetailPageModule } from "../pages/video-detail/video-detail.module";
import { VideoDetailInfoPageModule } from "../pages/video-detail-info/video-detail-info.module";
import { StaffDirectoryPageModule } from "../pages/staff-directory/staff-directory.module";
import { StaffDirectoryEntryPageModule } from "../pages/staff-directory-entry/staff-directory-entry.module";
import { BusinessUsersPageModule } from "../pages/business-users/business-users.module";
import { AlbumsPageModule } from "../pages/albums/albums.module";
import { AlbumGalleryPageModule } from "../pages/album-gallery/album-gallery.module";
import { AlbumsModalPageModule } from "../pages/albums-modal/albums-modal.module";
import { AlbumsEntryPageModule } from "../pages/albums-entry/albums-entry.module";
import { BusinessProfileSettingsPageModule } from "../pages/business-profile-settings/business-profile-settings.module";
import { ActivityDetailPageModule } from "../pages/activity-detail/activity-detail.module";
import { BusinessUserDetailPageModule } from "../pages/business-user-detail/business-user-detail.module";
import { BusinessUserEntryPageModule } from "../pages/business-user-entry/business-user-entry.module";
import { ChatPageModule } from "../pages/chat/chat.module";
import { ChatDetailPageModule } from "../pages/chat-detail/chat-detail.module";
import { FollowersFollowingPageModule } from "../pages/followers-following/followers-following.module";
import { LocationPageModule } from "../pages/location/location.module";
import { CreatorsPageModule } from "../pages/creators/creators.module";
import { CreatorsEntryPageModule } from "../pages/creators-entry/creators-entry.module";
import { UserPrivacyPageModule } from "../pages/user-privacy/user-privacy.module";
import { ConnectedBusinessPageModule } from "../pages/connected-business/connected-business.module";
import { SocialLinksPageModule } from "../pages/social-links/social-links.module";
import { CompanyBioPageModule } from "../pages/company-bio/company-bio.module";
import { CompanyBioEntryPageModule } from "../pages/company-bio-entry/company-bio-entry.module";
import { CollectionsPageModule } from "../pages/collections/collections.module";
import { CollectionsModalPageModule } from "../pages/collections-modal/collections-modal.module";
import { CollectionsEntryPageModule } from "../pages/collections-entry/collections-entry.module";
import { ChannelsPageModule } from "../pages/channels/channels.module";
import { ChannelsEntryPageModule } from "../pages/channels-entry/channels-entry.module";
import { UserProfilePageModule } from "../pages/user-profile/user-profile.module";
import { BusinessProfileEntryPageModule } from "../pages/business-profile-entry/business-profile-entry.module";
import { SettingsPageModule } from "../pages/settings/settings.module";
import { ConnectSettingsPageModule } from "../pages/connect-settings/connect-settings.module";
import { CreateFeedPageModule } from "../pages/create-feed/create-feed.module";
import { NotificationsPageModule } from "../pages/notifications/notifications.module";
import { NotificationDetailsPageModule } from "../pages/notification-details/notification-details.module";

import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";
import { GooglePlus } from "@ionic-native/google-plus";
import { Facebook } from "@ionic-native/facebook";
import { Camera } from "@ionic-native/camera";
import { FileTransfer } from "@ionic-native/file-transfer";
import { File } from "@ionic-native/file";
import { FilePath } from "@ionic-native/file-path";
import { InAppBrowser } from "@ionic-native/in-app-browser";
import { LocalNotifications } from "@ionic-native/local-notifications";
import { ScreenOrientation } from "@ionic-native/screen-orientation";
import { Keyboard } from "@ionic-native/keyboard";
import { OneSignal } from "@ionic-native/onesignal";
import { PhotoViewer } from "@ionic-native/photo-viewer";
import { DocumentViewer } from "@ionic-native/document-viewer";
import { FileChooser } from "@ionic-native/file-chooser";
import { Diagnostic } from "@ionic-native/diagnostic";
import { LocationAccuracy } from "@ionic-native/location-accuracy";
import { Geolocation } from "@ionic-native/geolocation";
import { FileOpener } from "@ionic-native/file-opener";
import { Network } from "@ionic-native/network";
import { IOSFilePicker } from "@ionic-native/file-picker";
import { SocialSharing } from "@ionic-native/social-sharing";
import { AppVersion } from "@ionic-native/app-version";

import { LoginServicesProvider } from "../providers/services/login";
import { RegisterServicesProvider } from "../providers/services/register";
import { UserServicesProvider } from "../providers/services/users";
import { UtilitiesServicesProvider } from "../providers/services/utilities";
import { ConnectServicesProvider } from "../providers/services/connect";
import { NewsFeedServicesProvider } from "../providers/services/newsfeed";
import { ImagesServicesProvider } from "../providers/services/images";
import { AlbumsServicesProvider } from "../providers/services/albums";
import { ProductsServicesProvider } from "../providers/services/products";
import { PrintServicesProvider } from "../providers/services/print";
import { VideosServicesProvider } from "../providers/services/videos";
import { LogoServicesProvider } from "../providers/services/logos";
import { UploadServicesProvider } from "../providers/services/upload";
import { StaffServicesProvider } from "../providers/services/staff";
import { CreatorsServicesProvider } from "../providers/services/creators";
import { BusinessServicesProvider } from "../providers/services/business";
import { ChatService } from "../providers/services/chat";
import { EmojiProvider } from "../providers/services/emojis";
import { LocationsServicesProvider } from "../providers/services/locations";
import { ChannelsServicesProvider } from "../providers/services/channels";
import { CollectionsServicesProvider } from "../providers/services/collections";
import { BYOBServicesProvider } from "../providers/services/byob";
import { RolesServicesProvider } from "../providers/services/roles";
import { TranslateServiceProvider } from "../providers/services/translate";
import { StorageProvider } from '../providers/storage/storage';
import { LoadingProvider } from '../providers/loading/loading';

import { MyHttpInterceptor } from '../providers/httpconfig.interceptor';

import { PipesModule } from "../pipes/pipes.module";
import { ComponentsModule } from "../components/components.module";
import { DirectivesModule } from "../directives/directives.module";
import { ProgressBarComponent } from "../components/progress-bar/progress-bar";

import { environment } from "../environments/environment";


const fbLoginOptions: any = {
  scope:
    "pages_messaging,pages_messaging_subscriptions,email,pages_show_list,manage_pages",
  return_scopes: true,
  enable_profile_selector: true
};

/*const googleLoginOptions: any = {
  scope: 'profile email'
};*/

export function getAuthServiceConfigs() {
  let config = new AuthServiceConfig([
    {
      id: FacebookLoginProvider.PROVIDER_ID,
      provider: new FacebookLoginProvider(
        environment.facebookAppID,
        fbLoginOptions
      )
    },
    {
      id: GoogleLoginProvider.PROVIDER_ID,
      provider: new GoogleLoginProvider(environment.googleWebClient)
    }
  ]);
  return config;
}

let storage = new Storage({});
export function authHttpServiceFactory(http: Http, options: RequestOptions) {
  return new AuthHttp(
    new AuthConfig({
      tokenGetter: () => storage.get("app.token").then((token: string) => token)
    }),
    http,
    options
  );
}

const entryDeclarationsComponents: any[] = [
  MyApp,
  HomePage,
  RegisterPage,
  ProgressBarComponent
];

@NgModule({
  declarations: entryDeclarationsComponents,
  imports: [
    LoginPageModule,
    ResetPasswordPageModule,
    ForgotPasswordModule,
    AboutPageModule,
    CreatebusinessPageModule,
    SearchPageModule,
    DownloadsPageModule,
    FileUploadPageModule,
    LogoDetailPageModule,
    ProductDetailPageModule,
    ProductsModalPageModule,
    ProductTabPageModule,
    ProductsEntryPageModule,
    ImagesTabPageModule,
    ImageDetailPageModule,
    ImageDetailInfoPageModule,
    ImageModalPageModule,
    PrintDetailPageModule,
    PrintdetailinfoPageModule,
    VideoDetailPageModule,
    VideoDetailInfoPageModule,
    VideosTabPageModule,
    StaffDirectoryPageModule,
    StaffDirectoryEntryPageModule,
    BusinessprofilePageModule,
    BusinesstabsPageModule,
    BusinessUsersPageModule,
    BusinessProfileEntryPageModule,
    BusinessUserDetailPageModule,
    BusinessProfileSettingsPageModule,
    BusinessUserEntryPageModule,
    AlbumsPageModule,
    AlbumGalleryPageModule,
    AlbumsEntryPageModule,
    ActivityDetailPageModule,
    NotificationsPageModule,
    NotificationDetailsPageModule,
    AlbumsModalPageModule,
    ChatPageModule,
    ChatDetailPageModule,
    FollowersFollowingPageModule,
    LocationPageModule,
    CreatorsPageModule,
    CreatorsEntryPageModule,
    UserPrivacyPageModule,
    ConnectedBusinessPageModule,
    ConnectSettingsPageModule,
    SocialLinksPageModule,
    CompanyBioPageModule,
    CompanyBioEntryPageModule,
    CollectionsPageModule,
    CollectionsEntryPageModule,
    CollectionsModalPageModule,
    ChannelsPageModule,
    ChannelsEntryPageModule,
    UserProfilePageModule,
    SettingsPageModule,
    CreateFeedPageModule,
    SocialLoginModule,
    IonicModule.forRoot(MyApp, {
      backButtonText: "",
      // mode: "ios",
      swipeBackEnabled: false,
      scrollAssist: false,
      autoFocusAssist: false
    }),
    BrowserAnimationsModule,
    BrowserModule,
    IonicImageViewerModule,
    HttpClientModule,
    CommonModule,
    JoditAngularModule,
    CacheModule,
    InViewportModule.forRoot(),
    IonicStorageModule.forRoot({
      name: "narnoo_mobile_db",
      driverOrder: ["indexeddb", "sqlite", "websql"]
    }),
    PipesModule.forRoot(),
    IonicImageLoader.forRoot(),
    IonTagsInputModule,
    NgProgressModule.forRoot(),
    ComponentsModule,
    DirectivesModule
  ],
  bootstrap: [IonicApp],
  exports: [],
  entryComponents: entryDeclarationsComponents,
  providers: [
    StatusBar,
    SplashScreen,
    SocialSharing,
    AppVersion,
    GooglePlus,
    Facebook,
    IOSFilePicker,
    Camera,
    FileTransfer,
    File,
    FilePath,
    FileChooser,
    InAppBrowser,
    LocalNotifications,
    ScreenOrientation,
    Keyboard,
    OneSignal,
    PhotoViewer,
    DocumentViewer,
    FileOpener,
    Network,
    Geolocation,
    LocationAccuracy,
    Diagnostic,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    { provide: AuthServiceConfig, useFactory: getAuthServiceConfigs },
    {
      provide: AuthHttp,
      useFactory: authHttpServiceFactory,
      deps: [Http, RequestOptions]
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: MyHttpInterceptor,
      multi: true
    },
    LoginServicesProvider,
    RegisterServicesProvider,
    UserServicesProvider,
    UtilitiesServicesProvider,
    ConnectServicesProvider,
    NewsFeedServicesProvider,
    ImagesServicesProvider,
    AlbumsServicesProvider,
    ProductsServicesProvider,
    PrintServicesProvider,
    VideosServicesProvider,
    LogoServicesProvider,
    UploadServicesProvider,
    StaffServicesProvider,
    CreatorsServicesProvider,
    BusinessServicesProvider,
    EmojiProvider,
    ChatService,
    LocationsServicesProvider,
    ChannelsServicesProvider,
    CollectionsServicesProvider,
    BYOBServicesProvider,
    RolesServicesProvider,
    TranslateServiceProvider,
    StorageProvider,
    LoadingProvider
  ]
})
export class AppModule { }
