import { Component, ViewChild, NgZone } from "@angular/core";
import {
  Nav,
  Platform,
  Events,
  App,
  LoadingController,
  MenuController,
  AlertController,
  ToastController
} from "ionic-angular";
import { Storage } from "@ionic/storage";
import * as md5 from "crypto-md5";
import { JwtHelper } from "angular2-jwt";
import { CacheService, Cache } from "ionic-cache-observable";
import { Observable } from "rxjs";
import "rxjs/add/operator/finally";
// import * as SendBird from "SendBird";
import * as _ from "lodash";
import { ImageLoaderConfig, ImageLoader } from "ionic-image-loader";

import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";
import { ScreenOrientation } from "@ionic-native/screen-orientation";
// import { Keyboard } from "@ionic-native/keyboard";
import { OneSignal } from "@ionic-native/onesignal";
import { Diagnostic } from "@ionic-native/diagnostic";
import { LocationAccuracy } from "@ionic-native/location-accuracy";
import { Geolocation } from "@ionic-native/geolocation";
import { Network } from "@ionic-native/network";
import { AppVersion } from "@ionic-native/app-version";

import { HomePage } from "../pages/home/home";
import { UserProfilePage } from "../pages/user-profile/user-profile";
import { LoginPage } from "../pages/login/login";
import { DownloadsPage } from "../pages/downloads/downloads";

import { ImageDetailPage } from "../pages/image-detail/image-detail";
import { LogoDetailPage } from "../pages/logo-detail/logo-detail";
import { PrintDetailPage } from "../pages/print-detail/print-detail";
import { VideoDetailPage } from "../pages/video-detail/video-detail";
import { ProductDetailPage } from "../pages/product-detail/product-detail";

import { LoginServicesProvider } from "../providers/services/login";
import { ImagesServicesProvider } from "../providers/services/images";
import { UtilitiesServicesProvider } from "../providers/services/utilities";
import { UserServicesProvider } from "../providers/services/users";
import { NewsFeedServicesProvider } from "../providers/services/newsfeed";
import { StorageProvider } from "../providers/storage/storage";

import { environment } from "../environments/environment";

@Component({
  templateUrl: "app.html"
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any;

  pages: Array<{ title: string; component: any; icon: string }>;
  users: any = {};
  business: any = {};
  profilePicture: any = "./assets/img/user.png";
  jwtHelper: JwtHelper = new JwtHelper();

  categoriesArr: any[] = [];
  locationArr: any[] = [];
  creatorArr: any[] = [];

  filter: any = {};
  filterWhat: any;
  globalToast: any;

  _appVersion: any = {};

  searchOptionsImage$: Observable<any>;
  private cache: Cache<any>;

  searchOptionsImageUser$: Observable<any>;
  private cacheUser: Cache<any>;

  constructor(
    public app: App,
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public storage: Storage,
    private diagnostic: Diagnostic,
    private network: Network,
    // private keyboard: Keyboard,
    private oneSignal: OneSignal,
    private screenOrientation: ScreenOrientation,
    private geolocation: Geolocation,
    private locationAccuracy: LocationAccuracy,
    private appVersion: AppVersion,
    public loadingCtrl: LoadingController,
    public menutCtrl: MenuController,
    public alertCtrl: AlertController,
    public events: Events,
    public zone: NgZone,
    public toastCtrl: ToastController,
    public cacheService: CacheService,
    public storageService: StorageProvider,
    public logins: LoginServicesProvider,
    public images: ImagesServicesProvider,
    public utilities: UtilitiesServicesProvider,
    public userServices: UserServicesProvider,
    public newsfeeds: NewsFeedServicesProvider,
    private imageLoaderConfig: ImageLoaderConfig,
    public imageLoader: ImageLoader
  ) {
    this.initializeApp();

    this.pages = [
      { title: "Accounts", component: HomePage, icon: "ios-contacts-outline" },
      {
        title: "Profile",
        component: UserProfilePage,
        icon: "ios-contact-outline"
      },
      {
        title: "Downloads",
        component: DownloadsPage,
        icon: "ios-cloud-download-outline"
      }
    ];

    // ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //
    // ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //
    // ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //
    events.unsubscribe("applogin");
    events.unsubscribe("generateFilters");
    events.unsubscribe("generateConnectedFilters");
    events.unsubscribe("filterWhat");
    events.unsubscribe("showNotifications");
    events.unsubscribe("initNotifications");

    events.subscribe("applogin", () => {
      this.getUser();
    });

    events.subscribe("generateFilters", () => {
      this.generateFilters();
    });

    events.subscribe("generateConnectedFilters", (data: any) => {
      console.log("generateConnectedFilters: ", data);
      this.initBusiness().then(() => {
        this.generateConnectedFilters(data.id);
      });
    });

    events.subscribe("filterWhat", what => {
      this.filterWhat = what;
    });

    this.initImageLoaderConfig();
  }

  private async initBusiness() {
    const business = await this.storageService.getStorage(
      this.storageService.storageKey.CURRENT_BUSINESS
    );
    this.business = !_.isEmpty(business) ? JSON.parse(business) || {} : {};
  }

  private initImageLoaderConfig() {
    // this.imageLoaderConfig.enableDebugMode();
    this.imageLoaderConfig.setMaximumCacheAge(7 * 24 * 60 * 60 * 1000);
    this.imageLoaderConfig.setImageReturnType("base64");
    this.imageLoaderConfig.enableFallbackAsPlaceholder(true);
  }

  private async getUser() {
    const users = await this.storageService.getStorage(
      this.storageService.storageKey.CURRENT_USER
    );
    this.zone.run(() => {
      this.users = JSON.parse(users) || {};
      if (this.users.email) {
        this.profilePicture =
          "https://www.gravatar.com/avatar/" +
          md5(this.users.email.toLowerCase(), "hex");
      } else {
        this.profilePicture = "./assets/img/logo.png";
      }
    });
  }

  private async isExpired() {
    const token = await this.storageService.getStorage(
      this.storageService.storageKey.USER_TOKENS
    );
    let isExpired = false;
    if (token) {
      isExpired = this.jwtHelper.isTokenExpired(token);
    } else {
      isExpired = true;
    }
    return isExpired;
  }

  private async isBusinessExpired() {
    const token = await this.storageService.getStorage(
      this.storageService.storageKey.BUSINESS_TOKENS
    );
    let isExpired = false;
    if (token) {
      isExpired = this.jwtHelper.isTokenExpired(token);
    } else {
      isExpired = true;
    }
    return isExpired;
  }

  async initializeApp() {
    const isExpired = await this.isExpired();
    const isBusinessExpired = await this.isBusinessExpired();

    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      /*setTimeout(() => {
        this.viewNotification({
          notification: {
            payload: {
              additionalData: {
                businessType: "operator",
                businessId: 39,
                mediaType: "print",
                mediaId: 15248372
              }
            }
          }
        });
      }, 2000);*/

      this.network.onDisconnect().subscribe(() => {
        console.log("network was disconnected :-(");
        this.events.publish("app:offline");

        /*if (this.globalToast) {
          this.globalToast.dismiss();
        }*/

        this.globalToast = this.toastCtrl.create({
          message: "No Internet Connection!",
          position: "top",
          duration: 5000,
          cssClass: "offline-toast-notif-danger"
        });
        this.globalToast.present();
      });

      this.network.onConnect().subscribe(() => {
        console.log("network connected!");
        this.events.publish("app:online");

        /*if (this.globalToast) {
          this.globalToast.dismiss();
        }*/

        this.globalToast = this.toastCtrl.create({
          message: "Online!",
          position: "top",
          duration: 5000,
          cssClass: "offline-toast-notif-success"
        });
        this.globalToast.present();
      });

      if (this.platform.is("cordova")) {
        // set to portrait
        this.screenOrientation.lock(
          this.screenOrientation.ORIENTATIONS.PORTRAIT
        );
        /*if (this.platform.is("ios")) {
          this.keyboard.disableScroll(true);
        }*/

        /* this.platform.pause.subscribe(() => {
          sb.setBackgroundState();
        });

        //Subscribe on resume
        this.platform.resume.subscribe(() => {
          sb.setForegroundState();
        }); */

        this.oneSignal.startInit(
          environment.OneSignal_AppID,
          environment.OneSignal_GoogleID
        );
        this.oneSignal.inFocusDisplaying(
          this.oneSignal.OSInFocusDisplayOption.Notification
        );
        this.oneSignal
          .handleNotificationReceived()
          .subscribe((notification: any) => {
            console.log(
              "do something when notification is received: ",
              notification
            );
          });
        this.oneSignal
          .handleNotificationOpened()
          .subscribe((notification: any) => {
            console.log(
              "do something when a notification is opened: ",
              notification
            );
            this.viewNotification(notification);
          });

        if (this.platform.is("android")) {
          this.oneSignal.enableVibrate(true);
          this.oneSignal.enableSound(true);
        }

        this.oneSignal.endInit();

        this.locationAccuracy.canRequest().then((canRequest: boolean) => {
          if (canRequest) {
            this.locationAccuracy
              .request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY)
              .then(
                success => {
                  this.geolocation
                    .getCurrentPosition()
                    .then((resp: any) => {
                      if (resp) {
                        this.utilities.latitude = resp.coords.latitude;
                        this.utilities.longitude = resp.coords.longitude;
                      }
                    })
                    .catch(error => {
                      console.log("Error getting location", error);
                    });
                },
                (error: any) => {
                  if (
                    error.code !== this.locationAccuracy.ERROR_USER_DISAGREED
                  ) {
                    if (
                      window.confirm(
                        "Failed to automatically set Location Mode to 'High Accuracy'. Would you like to switch to the Location Settings page and do this manually?"
                      )
                    ) {
                      this.diagnostic.switchToLocationSettings();
                    }
                  }
                }
              );
          }
        });

        this.geolocation.watchPosition().subscribe(data => {
          if (data) {
            this.utilities.latitude = data.coords.latitude;
            this.utilities.longitude = data.coords.longitude;
          }
        });

        if (!isExpired) {
          this.oneSignal.getIds().then((data: any) => {
            if (data) {
              this.userServices.user_edit_settings({
                mobilePushId: data.userId
              });
            }
          });
        }

        this.appVersion.getAppName().then((appName: string) => {
          console.log("appName: ", appName);
          this._appVersion.appName = appName;
        });
        this.appVersion.getPackageName().then((packageName: string) => {
          console.log("packageName: ", packageName);
          this._appVersion.packageName = packageName;
        });
        this.appVersion.getVersionCode().then((versionCode: string) => {
          console.log("versionCode: ", versionCode);
          this._appVersion.versionCode = versionCode;
        });
        this.appVersion.getVersionNumber().then((versionNumber: string) => {
          console.log("versionNumber: ", versionNumber);
          this._appVersion.versionNumber = versionNumber;
        });
      } else {
        const options = {
          enableHighAccuracy: true,
          timeout: 5000,
          maximumAge: 0
        };
        navigator.geolocation.getCurrentPosition(
          pos => {
            let crd = pos.coords;
            this.utilities.latitude = crd.latitude;
            this.utilities.longitude = crd.longitude;
          },
          err => {
            console.warn(`ERROR(${err.code}): ${err.message}`);
          },
          options
        );

        this._appVersion.appName = "Narnoo";
        this._appVersion.versionNumber = "web version";

        if (navigator.onLine) {
          // true|false
          this.events.publish("app:online");
        } else {
          this.events.publish("app:offline");
        }

        window.addEventListener("online", () => {
          this.events.publish("app:online");

          if (this.globalToast) {
            this.globalToast.dismiss();
          }

          this.globalToast = this.toastCtrl.create({
            message: "Online!",
            position: "top",
            duration: 5000,
            cssClass: "offline-toast-notif-success"
          });
          this.globalToast.present();
        });
        window.addEventListener("offline", () => {
          this.events.publish("app:offline");

          if (this.globalToast) {
            this.globalToast.dismiss();
          }

          this.globalToast = this.toastCtrl.create({
            message: "No Internet Connection!",
            position: "top",
            duration: 5000,
            cssClass: "offline-toast-notif-danger"
          });
          this.globalToast.present();
        });
      }
    });

    console.log("this.isExpired(): ", isExpired);
    if (isExpired) {
      console.log("this.rootPage = LoginPage;");
      this.storageService.removeStorage(
        this.storageService.storageKey.CURRENT_USER
      );
      this.storageService.removeStorage(
        this.storageService.storageKey.USER_TOKENS
      );
      this.storageService.removeStorage(
        this.storageService.storageKey.BUSINESS_TOKENS
      );
      this.storageService.removeStorage(
        this.storageService.storageKey.CURRENT_BUSINESS
      );

      this.rootPage = LoginPage;
    } else {
      this.rootPage = HomePage;
      this.getUser();

      this.initBusiness().then(() => {
        if (!isBusinessExpired) {
          this.generateFilters();
        }
      });
    }
  }

  async viewNotification(notif: any) {
    console.log("viewNotification: ", notif);
    this.storageService.setStorage(
      this.storageService.storageKey.BUSINESS_NOTIFICATION,
      JSON.stringify(notif)
    );
    const isExpired = await this.isExpired();
    console.log("this.isExpired(): ", isExpired);
    if (isExpired) {
      this.storageService.removeStorage(
        this.storageService.storageKey.CURRENT_USER
      );
      this.storageService.removeStorage(
        this.storageService.storageKey.USER_TOKENS
      );
      this.storageService.removeStorage(
        this.storageService.storageKey.BUSINESS_TOKENS
      );
      this.storageService.removeStorage(
        this.storageService.storageKey.CURRENT_BUSINESS
      );
      this.rootPage = LoginPage;
    } else {
      if (notif.notification) {
        const notification: any = notif.notification;
        const notifData: any = notification.payload.additionalData;
        console.log("notifData: ", notifData);
        if (notifData) {
          localStorage.removeItem("bus.notification");
          const loading = this.loadingCtrl.create({
            dismissOnPageChange: true
          });
          loading.present();
          this.userServices.businesses().then((data: any) => {
            if (data && data.success) {
              const businessArr = data.accounts;
              console.log("businessArr: ", businessArr);
              const result: any = _.find(businessArr, (row: any) => {
                return row.id == parseInt(notifData.businessId);
              });
              console.log("result: ", result);
              if (result) {
                this.storageService.removeStorage(
                  this.storageService.storageKey.BUSINESS_NOTIFICATION
                );
                this.storageService.setStorage(
                  this.storageService.storageKey.BUSINESS_TOKENS,
                  result.token
                );
                this.storageService.setStorage(
                  this.storageService.storageKey.CURRENT_BUSINESS,
                  JSON.stringify(result)
                );

                loading.dismiss();

                if (notifData.mediaType == "image") {
                  this.zone.run(() => {
                    this.app.getRootNav().push(ImageDetailPage, {
                      action: "notif",
                      business: {
                        businessType: notifData.businessType,
                        businessId: notifData.businessId
                      },
                      image: {
                        id: notifData.mediaId
                      }
                    });
                  });
                } else if (notifData.mediaType == "print") {
                  this.zone.run(() => {
                    this.app.getRootNav().push(PrintDetailPage, {
                      action: "notif",
                      business: {
                        businessType: notifData.businessType,
                        businessId: notifData.businessId
                      },
                      print: {
                        id: notifData.mediaId
                      }
                    });
                  });
                } else if (notifData.mediaType == "logo") {
                  this.zone.run(() => {
                    this.app.getRootNav().push(LogoDetailPage, {
                      action: "notif",
                      business: {
                        businessType: notifData.businessType,
                        businessId: notifData.businessId
                      },
                      logo: {
                        id: notifData.mediaId
                      }
                    });
                  });
                } else if (notifData.mediaType == "video") {
                  this.zone.run(() => {
                    this.app.getRootNav().push(VideoDetailPage, {
                      action: "notif",
                      business: {
                        businessType: notifData.businessType,
                        businessId: notifData.businessId
                      },
                      video: {
                        id: notifData.mediaId
                      }
                    });
                  });
                } else if (notifData.mediaType == "product") {
                  this.zone.run(() => {
                    this.app.getRootNav().push(ProductDetailPage, {
                      action: "notif",
                      business: {
                        businessType: notifData.businessType,
                        businessId: notifData.businessId
                      },
                      product: {
                        productId: notifData.mediaId
                      }
                    });
                  });
                }
              } else {
                loading.dismiss();
                this.alertCtrl
                  .create({
                    title: "WARNING",
                    message: "No business found on this account!",
                    buttons: ["OK"]
                  })
                  .present();
                return;
              }
            }
          });
        }
      }
    }
  }

  async generateFilters() {
    this.categoriesArr = [];
    this.creatorArr = [];
    this.locationArr = [];

    const dataObservable = Observable.fromPromise(
      this.images.search_options_image()
    );
    const cacheKey = this.storageService.cacheKey.SEARCH_OPTIONS_IMAGE;

    this.cacheService.register(cacheKey, dataObservable).subscribe(cache => {
      this.cache = cache;
      this.zone.run(() => {
        this.searchOptionsImage$ = cache.get$;
      });
    });

    this.searchOptionsImage$.subscribe(
      options => {
        this.zone.run(() => {
          if (_.isArray(options.category)) {
            this.categoriesArr = options.category;
          }

          if (_.isArray(options.creator)) {
            this.creatorArr = options.creator;
          }

          if (_.isArray(options.locations)) {
            this.locationArr = options.locations;
          }
        });
      },
      error => {
        console.log("error search_options_image: ", error);
      }
    );
  }

  async generateConnectedFilters(connectedId) {
    this.categoriesArr = [];
    this.creatorArr = [];
    this.locationArr = [];

    const dataObservable = Observable.fromPromise(
      this.images.search_options_connected_image(connectedId)
    );
    const cacheKey =
      this.storageService.cacheKey.SEARCH_OPTIONS_IMAGE + "_" + connectedId;

    const loading = this.loadingCtrl.create({
      dismissOnPageChange: true,
      content: "Generating filters..."
    });
    loading.present();

    this.cacheService.register(cacheKey, dataObservable).subscribe(cache => {
      this.cacheUser = cache;
      this.zone.run(() => {
        this.searchOptionsImageUser$ = cache.get$;
      });
    });

    this.searchOptionsImageUser$.subscribe(
      options => {
        this.zone.run(() => {
          if (_.isArray(options.category)) {
            this.categoriesArr = options.category;
          }

          if (_.isArray(options.creator)) {
            this.creatorArr = options.creator;
          }

          if (_.isArray(options.locations)) {
            this.locationArr = options.locations;
          }
          loading.dismiss();
        });
      },
      error => {
        console.log("error search_options_image: ", error);
        loading.dismiss();
      }
    );
  }

  openPage(page) {
    this.nav.setRoot(page.component);
  }

  logOutApp() {
    this.storageService.removeStorage(
      this.storageService.storageKey.CURRENT_USER
    );
    this.storageService.removeStorage(
      this.storageService.storageKey.USER_TOKENS
    );
    this.storageService.removeStorage(
      this.storageService.storageKey.BUSINESS_TOKENS
    );
    this.storageService.removeStorage(
      this.storageService.storageKey.CURRENT_BUSINESS
    );
    this.storageService.removeStorage(
      this.storageService.storageKey.BUSINESS_NOTIFICATION
    );

    setTimeout(() => {
      this.zone.run(() => {
        this.nav.setRoot(LoginPage, {}, { animate: true, direction: "back" });
      });
    }, 600);
  }

  applyFilter() {
    console.log("filters: ", this.filter);
    this.menutCtrl.close();
    if (this.filterWhat === "print") {
      this.events.publish("applyFilterPrint", this.filter);
    } else if (this.filterWhat === "media") {
      this.events.publish("applyFilter", this.filter);
    } else if (this.filterWhat === "connected-media") {
      this.events.publish("applyFilter", this.filter);
    }
  }

  clearFilter() {
    this.filter = {
      category: {},
      location: {},
      creator: {},
      type: {}
    };
    console.log("clearFilter: ", this.filter);
  }

  clearCache() {
    const alert = this.alertCtrl.create({
      title: "Confirm Clearing...",
      message: "Confirm to clear app cache? This will logout the app afterwards...",
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        },
        {
          text: "Confirm",
          cssClass: "danger",
          handler: () => {
            console.log("Buy clicked");

            this.menutCtrl.close();

            const loading = this.loadingCtrl.create({
              dismissOnPageChange: true
            });
            loading.present();
            this.imageLoader.clearCache();
            this.storage.clear();

            setTimeout(() => {
              loading.dismiss();
              this.zone.run(() => {
                this.nav.setRoot(
                  LoginPage,
                  {},
                  { animate: true, direction: "back" }
                );
              });
            }, 2000);
          }
        }
      ]
    });
    alert.present();
  }
}
