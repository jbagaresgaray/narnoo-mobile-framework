import { enableProdMode } from "@angular/core";
import { platformBrowserDynamic } from "@angular/platform-browser-dynamic";

import { environment } from "../environments/environment";
import { AppModule } from "./app.module";

import * as $ from "jquery";
window["$"] = $;
window["jQuery"] = $;

if (environment.production) {
  enableProdMode();
  if (window) {
    window.console.log = function() {};
  }
}

platformBrowserDynamic()
  .bootstrapModule(AppModule)
  .catch(err => console.error(err));
