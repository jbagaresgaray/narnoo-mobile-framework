Narnoo Mobile Framework


#JavaScript heap out of memory !!!!!!
https://github.com/ionic-team/ionic-app-scripts/issues/1467
https://github.com/ionic-team/ionic-app-scripts/issues/1036

Open node_modules/@ionic/app-scripts/bin/ionic-app-scripts.js

Change the first line from:
#!/usr/bin/env node
into
#!/usr/bin/env node --max-old-space-size=8192

I tried values 1024 and 2048, but for my relatively large app I needed 4096/8192.

Now I am able to run ionic cordova build android --prod --release with out the FATAL ERROR: Ineffective mark-compacts near heap limit Allocation failed - JavaScript heap out of memory error.



// "ionic:build": "node --max-old-space-size=8192 ./node_modules/@ionic/app-scripts/bin/ionic-app-scripts.js build"
