module.exports = {
  includePaths: [
    "node_modules/ionic-angular/themes",
    "node_modules/ionicons/dist/scss",
    "node_modules/ionic-angular/fonts",
    "node_modules/videojs-font/scss",
    "node_modules/video.js/src/css",
    "src/vendor/material-preloader"
  ]
};
