#!/bin/bash
set -o errexit #exit with error code if something goes wrong.
if [ -z ${CI_APP_NAME} ]; then
echo "Not in Ionic 3 skipping cordova hack";
else
echo "In Ionic 3...using cordova@8.1.2"
npm uninstall -g cordova
npm install -g cordova@8.1.2
fi
